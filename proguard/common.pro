-renamesourcefileattribute SourceFile
-keepattributes SourceFile
-keepattributes LineNumberTable
-keepattributes InnerClasses
-keepattributes EnclosingMethod
-keepattributes Exceptions
-keepattributes *Annotation*
-keepattributes Signature
-keepattributes Synthetic
-keepattributes RuntimeVisibleAnnotations
-keepattributes RuntimeInvisibleAnnotations
-keepattributes RuntimeVisibleParameterAnnotations
-keepattributes RuntimeInvisibleParameterAnnotations

-keep enum * {
    *;
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * extends com.jininsa.lib.chatkit.commons.ViewHolder { <init>(...); }

-keepnames class net.sourceforge.zbar.** { *; }
-keep class net.sourceforge.zbar.Image { *; }
-keep class kr.co.voiceware.license.VTLicenseConfig { *; }

-keep class ** { *** addNetworkInterceptor(...); }

-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-keep class okhttp3.Headers* { *; }

-keep class com.mobsandgeeks.saripaar.** { *; }

-keepnames class com.wang.avi.** { *; }

-keepclasseswithmembers class * { @retrofit2.* <methods>; }
-keepclasseswithmembers interface * { @retrofit2.* <methods>; }
# !!! 이 줄이 없으며 request에 빈 문자열만 들어간다.
-keepclasseswithmembers class * { @retrofit2.http.* <methods>; }

-keep class org.aspectj.runtime.reflect.** { *; }

-keep class com.annimon.stream.Optional { *; }

#-keep class org.bouncycastle.** { *; }
-keepnames class org.bouncycastle.** { *; }

#-keep class io.jsonwebtoken.** { *; }
-keepnames class io.jsonwebtoken.* { *; }
-keepnames interface io.jsonwebtoken.* { *; }

-keepnames class com.fasterxml.jackson.databind.** { *; }
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames interface com.fasterxml.jackson.** { *; }

-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}

-keep public class com.pierfrancescosoffritti.youtubeplayer.** {
   public *;
}

-keep class .R
-keep class **.R$* {
    <fields>;
}

-keepnames class com.pierfrancescosoffritti.youtubeplayer.*

-dontwarn org.bouncycastle.**
-dontwarn com.fasterxml.jackson.databind.*
-dontwarn com.fasterxml.jackson.databind.**
-dontwarn sun.misc.Unsafe
-dontwarn android.test.**
-dontwarn java.nio.**
-dontwarn java.lang.**
-dontwarn javax.annotation.**
-dontwarn com.squareup.okhttp.**
-dontwarn retrofit2.Platform$Java8
-dontwarn okio.**
-dontwarn android.content.ServiceConnection$$CC
-dontwarn com.annimon.stream.**
-dontwarn com.google.common.**
-dontwarn javax.lang.model.**
-dontwarn javax.xml.bind.DatatypeConverter
-dontwarn io.jsonwebtoken.impl.Base64Codec