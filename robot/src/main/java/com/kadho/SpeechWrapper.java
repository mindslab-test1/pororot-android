package com.kadho;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


/**
 * Created by durgesh on 16/04/18.
 */

public class SpeechWrapper {
  public class Client extends AsyncTask<byte[], Void, Void> {
    @Override
    protected Void doInBackground(byte[]... arg0) {
      Log.e(TAG, "sending " + arg0[0].length + " of accStates");
      final StringBuilder buffer;

      if (arg0[0].length == 3) {
        try {
          kidsenseSpeech.onStatus("Detected EOS");
          dataOutputStream.flush();
          try { //TODO: This is just a bandaid to the real issue, please fix when a better solution is found
            Thread.sleep(1000);
          }
          catch (InterruptedException e) {
            e.printStackTrace();
          }
          dataOutputStream.write(arg0[0]);
          dataOutputStream.flush();
          kidsenseSpeech.onStatus("Waiting for result");
          final BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
          buffer = new StringBuilder();
          buffer.append(input.readLine());

          if (buffer.toString().contains("result")) {
            Log.e(TAG, buffer.toString());
            Log.e("mindslab", buffer.toString());

            OpusRecorder.isInit = Boolean.TRUE;
            kidsenseSpeech.onKidsenseResult(buffer.toString());
            socket.close();
            dataOutputStream.close();
            mVoiceRecorder.stop();
          }
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
      else {
        try {
          dataOutputStream.write(arg0[0]);
        }
        catch (IOException e) {
          if (socket.isClosed()) {
            kidsenseServer = new KidsenseServer(kidsenseSpeech, kidsenseCredentials);
            encapsulator = kidsenseServer.initServer();
            socket = encapsulator.getSocket();
            dataOutputStream = encapsulator.getDataOutputStream();
            try {
              dataOutputStream.write(arg0[0]);
            }
            catch (IOException e1) {
              e1.printStackTrace();
            }
          }
          else {
            e.printStackTrace();
          }
        }
      }
      return null;
    }

    @Override
    protected void onPostExecute(Void result) {
      super.onPostExecute(result);
      try {
        dataOutputStream.flush();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
  public OpusRecorder mVoiceRecorder = null;
  public String TAG = "kidsense_asr-sdk";
  public Client clientTask = null;
  public KidsenseSpeech kidsenseSpeech = null;
  public KidsenseServer kidsenseServer = null;
  public DataOutputStream dataOutputStream = null;
  public Socket socket = null;
  public Encapsulator encapsulator = null;
  public Boolean isEosSent = Boolean.FALSE;
  public final OpusRecorder.Callback mVoiceCallback = new OpusRecorder.Callback() {

    @Override
    public void onVoiceStart() {}

    @Override
    public void onVoice(byte[] data, int size) {
      clientTask = new Client();
      clientTask.execute(data);
    }

    @Override
    public void onVoiceEnd() {
      if (!isEosSent) {
        String end = "EOS";
        byte[] b = end.getBytes();
        clientTask = new Client();
        clientTask.execute(b);
        isEosSent = true;
      }
      else {
        isEosSent = false;
      }
    }

    @Override
    public void onSilenceTimeout() {
      Log.e(TAG, "Silence timeout");
    }
  };
  public KidsenseCredentials kidsenseCredentials = null;

  public SpeechWrapper(KidsenseSpeech kidsenseSpeech, KidsenseServer kidsenseServer, KidsenseCredentials kidsenseCredentials) {
    this.kidsenseSpeech = kidsenseSpeech;
    this.kidsenseServer = kidsenseServer;
    this.kidsenseCredentials = kidsenseCredentials;
    kidsenseServer = new KidsenseServer(kidsenseSpeech, kidsenseCredentials);
    encapsulator = kidsenseServer.initServer();
    socket = encapsulator.getSocket();
    dataOutputStream = encapsulator.getDataOutputStream();
  }

  public void start() {
    if (mVoiceRecorder != null) {
      mVoiceRecorder.stop();
    }
    kidsenseSpeech.onStatus("START");
    mVoiceRecorder = OpusRecorder.getInstance(mVoiceCallback);
    mVoiceRecorder.start();
  }

  public void stop() {
    if (mVoiceRecorder != null) {
      mVoiceRecorder.stop();
    }
    kidsenseSpeech.onStatus("STOP");
  }
}