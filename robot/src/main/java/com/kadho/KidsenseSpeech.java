package com.kadho;

/**
 * Created by durgesh on 16/04/18.
 */

public interface KidsenseSpeech {
    public void onKidsenseResult(String result);
    public void onStatus(String status);
}
