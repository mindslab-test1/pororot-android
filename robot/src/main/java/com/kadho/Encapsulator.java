package com.kadho;

import java.io.DataOutputStream;
import java.net.Socket;

/**
 * Created by durgesh on 16/04/18.
 */

public class Encapsulator {
    public Socket socket;
    public DataOutputStream dataOutputStream;

    public void setSocket(Socket socket){
        this.socket = socket;
    }

    public void setDataOutputStream(DataOutputStream dataOutputStream){
        this.dataOutputStream = dataOutputStream;
    }

    public Socket getSocket(){
        return this.socket;
    }

    public DataOutputStream getDataOutputStream(){
        return this.dataOutputStream;
    }
}
