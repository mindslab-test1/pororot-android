package com.kadho;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Random;

/**
 * Created by durgesh on 16/04/18.
 */

public class KidsenseServer {
    public String host = "45.55.113.61";
    public int port = 6003;
    public Socket socket = null;
    public DataOutputStream dOut = null;
    public PrintWriter pw = null;
    public KidsenseCredentials kidsenseCredentials = null;
    public String TAG = "kidsense_asr-sdk";
    public OutputStream outputStream = null;
    public KidsenseSpeech kidsenseSpeech = null;

    public KidsenseServer(KidsenseSpeech kidsenseSpeech, KidsenseCredentials kidsenseCredentials){
        this.kidsenseCredentials = kidsenseCredentials;
        this.kidsenseSpeech = kidsenseSpeech;
    }

    public String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

    public Encapsulator initServer(){
        OpusRecorder.isInit = Boolean.TRUE;
        if (socket != null){
            try {
                Log.e(TAG,"Closing existing socket");
                socket.close();
                socket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (dOut != null){
            try {
                Log.e(TAG,"Closing accStates output stream");
                dOut.close();
                dOut = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (outputStream != null){
            try {
                Log.e(TAG,"Closing output stream");
                outputStream.close();
                outputStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        JSONObject auth = new JSONObject();
        try {
            auth.put("SESSID", getSaltString());
            auth.put("USER", kidsenseCredentials.USER);
            auth.put("LANG", kidsenseCredentials.LANG);
            auth.put("KEY", kidsenseCredentials.KEY);
            auth.put("GENDER", kidsenseCredentials.GENDER);
            auth.put("AGE", kidsenseCredentials.AGE);
            auth.put("VER", kidsenseCredentials.VER);
            auth.put("PATH", kidsenseCredentials.PATH);
            auth.put("COUNTRY",kidsenseCredentials.COUNTRY);
            if (kidsenseCredentials.HOST != null && kidsenseCredentials.PORT != 0){
                this.host = kidsenseCredentials.HOST;
                this.port = kidsenseCredentials.PORT;
            }
            socket = new Socket(host, port);
            socket.setKeepAlive(false);
            outputStream = socket.getOutputStream();
            dOut = new DataOutputStream(outputStream);
            pw = new PrintWriter(socket.getOutputStream(), true);
            pw.println(auth.toString());
            pw.flush();
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            final String response = in.readLine();
            kidsenseSpeech.onStatus(response);
            Log.e(TAG,response);
            Encapsulator encapsulator = new Encapsulator();
            encapsulator.setSocket(socket);
            encapsulator.setDataOutputStream(dOut);
            return encapsulator;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}