package com.kadho;

/**
 * Created by durgesh on 16/04/18.
 */

public class KidsenseCredentials {
    String USER,LANG,KEY,GENDER,COUNTRY,AGE,VER,PATH,HOST = null;
    int PORT = 0;
    public KidsenseCredentials(String USER, String LANG, String KEY, String GENDER, String COUNTRY, String AGE, String VER, String PATH){
        this.USER = USER;
        this.LANG = LANG;
        this.KEY = KEY;
        this.GENDER = GENDER;
        this.COUNTRY = COUNTRY;
        this.AGE = AGE;
        this.VER = VER;
        this.PATH = PATH;
    }

    public KidsenseCredentials(String USER, String LANG, String KEY, String GENDER, String COUNTRY, String AGE, String VER){
        this.USER = USER;
        this.LANG = LANG;
        this.KEY = KEY;
        this.GENDER = GENDER;
        this.COUNTRY = COUNTRY;
        this.AGE = AGE;
        this.VER = VER;
        this.PATH = "False";
    }

    public KidsenseCredentials(String USER, String LANG, String KEY, String GENDER, String COUNTRY, String AGE, String VER, String PATH, String HOST, int port){
        this.USER = USER;
        this.LANG = LANG;
        this.KEY = KEY;
        this.GENDER = GENDER;
        this.COUNTRY = COUNTRY;
        this.AGE = AGE;
        this.VER = VER;
        this.PATH = PATH;
        this.HOST = HOST;
        this.PORT = port;
    }
}