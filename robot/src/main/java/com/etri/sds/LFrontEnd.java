package com.etri.sds;

public class LFrontEnd
{
  private static LFrontEnd INSTANCE;

  private LFrontEnd()
  {
    System.loadLibrary("sds_laser");
  }

  static public LFrontEnd getInstance()
  {
    if (INSTANCE == null)
    {
      INSTANCE = new LFrontEnd();
    }

    return INSTANCE;
  }

  public native int createLFrontEnd();
  public native int setOptionLFrontEnd(String key, String val);
  public native int closeLFrontEnd();
  public native int stepFrameLFrontEnd(int size, short[] in, short[] out);
  public native int isEndPointDetected();
  public native int resetLFrontEnd();
}
