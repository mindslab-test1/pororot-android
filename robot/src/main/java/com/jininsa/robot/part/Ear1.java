package com.jininsa.robot.part;

import android.content.Context;
import android.media.AudioRecord;

import com.elvishew.xlog.XLog;
import com.jininsa.robot.connector.Stt;
import com.jininsa.robot.helper.Subscriptions;

import java.io.ByteArrayOutputStream;

import io.reactivex.schedulers.Schedulers;

class Ear1 extends Ear {

  private static final int HOW_MANY_SHORT_TO_READ = 9600; // 보통 1음절은 0.3초. 1초에 32000byte.
  private static Ear1 EAR;

  static Ear1 init(Context context) {
    if (EAR == null) {
      EAR = new Ear1(context);
    }
    return EAR;
  }

  //  final private Engine engine;
  private final Audio.Settings settings;
  private final Stt stt;
  private AudioRecord audioRecord;
  private boolean isWorking = false;
  //  private final LFrontEnd sentenceDetector;
  //  private WavWriter wavWriter;

  private Ear1(Context context) {
    super(context);
//    this.sentenceDetector = LFrontEnd.getInstance();
//    this.sentenceDetector.createLFrontEnd();
    this.settings = Audio.Settings.DEFAULT_IN;
    this.stt = Stt.create(
      new Stt.Credential("jininsa", "ko", "5552d4626fd8c265a63f3be459", "male", "KR", "adults", "v5"));
    do {
      this.audioRecord = new AudioRecord(Audio.Settings.AudioType.IN_MIC.type,
        this.settings.sampleRate.hz, this.settings.channelConfig.channel, this.settings.encodingFormat.format,
        HOW_MANY_SHORT_TO_READ * 2);
    } while (this.audioRecord.getState() != AudioRecord.STATE_INITIALIZED);

    Subscriptions.add(this.hashCode(), SUBSCRIPTION_COMMANDS,
      this.commands.observeOn(Schedulers.newThread()).subscribe(command -> {
        XLog.i(command);
        if (command == Command.START) {
          this.isWorking = true;
          this.hear();
        }

        if (command == Command.STOP) {
          this.isWorking = false;
          this.audioRecord.stop();
        }
      }));
    Nerve.events.accept(State.READY);
//    this.wavWriter = new WavWriter(this.settings.channelConfig.numChannels, this.settings.sampleRate.hz, this.settings.encodingFormat.bit);
  }

  @Override
  void start() {
    this.commands.accept(Command.START);
  }

  @Override
  void stop() {
    this.commands.accept(Command.STOP);
  }

  @Override
  void terminate() {
    this.commands.accept(Command.STOP);
  }

////  void start(boolean continuous) {
////    if (this.audioRecord == null) {
////
////    }
////
////    final AsyncTask<Void, Void, Void> worker = new HearingAsyncTask(this, continuous);
////    worker.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////  }
//
//  private void finish() {
//    if (this.audioRecord != null) {
//      this.audioRecord.stop();
//    }
//    this.isWorking = false;
////    EAR_STAT.accept(State.NOT_HEARING);
//  }

  private boolean isSounding(byte[] buffer, int size) {
    for (int i = 0; i < size - 1; i += 2) {
      // The buffer has LINEAR16 in little endian.
      int s = buffer[i + 1];
      if (s < 0) s *= -1;
      s <<= 8;
      s += Math.abs(buffer[i]);
      if (s > 1000) {
        return true;
      }
    }
    return false;
  }

  private void hear() {
    this.audioRecord.startRecording();
    while (this.isWorking) {
      short[] input = new short[HOW_MANY_SHORT_TO_READ];
      short[] previous = new short[HOW_MANY_SHORT_TO_READ];
      boolean isFirst = false;
      boolean isHearing = false;
      boolean isSentenceEnded = false;
      byte[] result;
      ByteArrayOutputStream baos = new ByteArrayOutputStream(32000 * 30); // 30초 기본 버퍼

//      this.sentenceDetector.resetLFrontEnd();

      while (!isSentenceEnded) {
        int offset;
        int size = this.audioRecord.read(input, 0, HOW_MANY_SHORT_TO_READ);
        byte[] temp = new byte[size * 2];
        for (int i = 0; i < size; i++) {
          temp[i * 2] = (byte) (input[i] & 0xFF);
          temp[i * 2 + 1] = (byte) ((input[i] >> 8) & 0xFF);
        }

        boolean isSounding = this.isSounding(temp, size);
        XLog.d("%d %s", size, isSounding);

//        if (isSounding) {
//          if ()
//        }

//        int detectedSize = this.sentenceDetector.stepFrameLFrontEnd(size, input, output); // 발화점을 찾고 나서
//        if (detectedSize > 0) {

//        if (this.isSounding(temp, size)) {
//          XLog.d("BOS");
//          if (isFirst) {
//            isFirst = false;
//            result = new byte[HOW_MANY_SHORT_TO_READ * 4];
//            for (int i = 0; i < HOW_MANY_SHORT_TO_READ; i++) {
//              result[i * 2] = (byte) (previous[i] & 0xFF);
//              result[i * 2 + 1] = (byte) ((previous[i] >> 8) & 0xFF);
//            }
//            offset = HOW_MANY_SHORT_TO_READ;
//          }
//          else {
//            result = new byte[HOW_MANY_SHORT_TO_READ * 2];
//            offset = 0;
//          }
//
//          for (int i = 0; i < HOW_MANY_SHORT_TO_READ; i++) {
//            result[(offset + i) * 2] = (byte) (input[i] & 0xFF);
//            result[(offset + i) * 2 + 1] = (byte) ((input[i] >> 8) & 0xFF);
//          }
//
//          baos.write(result, 0, result.length);
//        }
//        else {
//          isFirst = true;
//          System.arraycopy(input, 0, previous, 0, HOW_MANY_SHORT_TO_READ);
//        }
//
//        int r2 = this.sentenceDetector.isEndPointDetected();
//
//        if (r2 > 0) {
//          XLog.d("EOS: %d %d", detectedSize, baos.size());
//          isSentenceEnded = true;
//
//          try {
////          byte[] compressed = Compressor.compress(baos);
////          Nerve.UP.accept(Signal.HEARING.add(compressed));
////          this.wavWriter.write(new File(String.format("%s/%s.wav", Env.HEARING_PATH_STRING, Env.DATE_FORMAT.format(new Date()))), baos);
//            this.stt.send(baos.toByteArray());
//            baos.close();
//          }
//          catch (IOException e) {
//            e.printStackTrace();
//          }
//        }
//
//        if (!this.isWorking) {
//          isSentenceEnded = true;
//        }
      }
    }
  }
}
