package com.jininsa.robot.part;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.helper.Strings;
import com.jininsa.robot.helper.Subscriptions;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import io.reactivex.schedulers.Schedulers;

class Spp {


  enum LedColor {
    OFF(10), RED(11), BLUE(12), RED_AND_BLUE(13);
    final int value;

    LedColor(final int value) {this.value = value;}
  }

  static class State extends Nerve.Event {
    static final State READY = new State("READY");
    static final State FAILED = new State("FAILED");

    State(final String event) {
      super(event);
    }
  }

  static class Event extends Nerve.Event {
    static final Event FD_SET = new Event("FD_SET");
    static final Event FD_UNSET = new Event("FD_UNSET");
    static final Event LED_ON = new Event("LED_ON");
    static final Event LED_OFF = new Event("LED_OFF");
    static final Event LED_RESET = new Event("LED_RESET");
    static final Event SHAKING = new Event("LED_RESET");

    private Event(String name) { super(name); }
  }

  static final private String TAIL = "ABCD";
  static final private String HEAD_SET_FD_REQ = "DD10";
  static final private String HEAD_SET_FD_RES = "DD20";
  static final private String HEAD_UNSET_FD_REQ = "DD30";
  static final private String HEAD_UNSET_FD_RES = "DD40";
  static final private String HEAD_GET_FD_REQ = "DD50";
  static final private String HEAD_GET_FD_RES = "DD60";
  static final private String HEAD_TURN_OFF_LED_REQ = "DD70";
  static final private String HEAD_TURN_OFF_LED_RES = "DD80";
  static final private String HEAD_TURN_ON_LED_REQ = "DD90";
  static final private String HEAD_TURN_ON_LED_RES = "DDA0";
  static final private String HEAD_ACC_REQ = "DDB0";
  static final private String HEAD_ACC_RES = "DDC0";
  static final private String HEAD_RESET_LED_REQ = "DDD0";
  static final private String HEAD_RESET_LED_RES = "DDE0";
  static private final String RES_SUCCESSFUL = "10";
  static private final String REQ_START_ACC = String.format("%s11%s", HEAD_ACC_REQ, TAIL);

  static private final String RES_FAILED = "11";
  static private final String RES_FD_SET = "10";
  static private final String RES_FD_UNSET = "11";

  // @DebugLog
  static Spp init() {
    return new Spp();
  }

  static private final String SUBSCRIPTION_STARTER = "acotjEq6QwLzEN3L";
  static private final String SUBSCRIPTION_SENDER = "tdh2UK0ZgGKZWd8A";
  static private final String SUBSCRIPTION_RECEIVER = "9i4UNVY9H3X245Ea";
  static private final String SUBSCRIPTION_GATHERER = "DEVxCS5sdRE5NMf4";

  private final PublishRelay<Boolean> starter = PublishRelay.create();
  private final PublishRelay<String> gatherer = PublishRelay.create();
  private final PublishRelay<String> receiver = PublishRelay.create();
  private final PublishRelay<Boolean> sender = PublishRelay.create();
  private final Queue<String> sendQueue = new LinkedBlockingQueue<>();

  private boolean isReceiving = false;
  private boolean isGathering = false;

  private BluetoothSocket socket;
  private OutputStream outputStream;
  private InputStream inputStream;

  private Spp() {

    Subscriptions.add(this.hashCode(), Spp.SUBSCRIPTION_STARTER, this.starter.observeOn(Schedulers.newThread()).subscribe(this::receive));

    Subscriptions.add(this.hashCode(), Spp.SUBSCRIPTION_GATHERER, this.gatherer.observeOn(Schedulers.newThread()).subscribe(this::gather));

    Subscriptions.add(this.hashCode(), Spp.SUBSCRIPTION_SENDER, this.sender.observeOn(Schedulers.newThread()).subscribe(__ -> {
      String payload = this.sendQueue.poll();
      byte[] payloadBytes = Strings.hexToBytes(payload);
      this.send(payloadBytes);
    }));

    Subscriptions.add(this.hashCode(), Spp.SUBSCRIPTION_RECEIVER, this.receiver.subscribe(signal -> {
      String head = Strings.first(signal, 4);
      String body = signal.substring(4, signal.indexOf(TAIL));

      if (Spp.HEAD_ACC_RES.equals(head)) {
        this.interpretAcc(body);
      }
      else if (Spp.HEAD_SET_FD_RES.equals(head)) {
        if (Spp.RES_SUCCESSFUL.equals(body)) {
          Nerve.events.accept(Event.FD_SET);
        }
      }
      else if (Spp.HEAD_UNSET_FD_RES.equals(head)) {
        if (Spp.RES_SUCCESSFUL.equals(body)) {
          Nerve.events.accept(Event.FD_UNSET);
        }
      }
      else if (Spp.HEAD_TURN_ON_LED_RES.equals(head)) {
        if (Spp.RES_SUCCESSFUL.equals(body)) {
          Nerve.events.accept(Event.LED_ON);
        }
      }
      else if (Spp.HEAD_TURN_OFF_LED_RES.equals(head)) {
        if (Spp.RES_SUCCESSFUL.equals(body)) {
          Nerve.events.accept(Event.LED_OFF);
        }
      }
      else if (Spp.HEAD_RESET_LED_RES.equals(head)) {
        if (Spp.RES_SUCCESSFUL.equals(body)) {
          Nerve.events.accept(Event.LED_RESET);
        }
      }
    }));
  }

  private int[] baseAcc;

  private void interpretAcc(String payload) {
    int[] xyz = new int[3];

    for (int i = 1; i < 24; i += 2) {
      xyz[i / 8] += Character.getNumericValue(payload.charAt(i)) * Math.pow(10, 3 - ((i / 2) % 4));
    }

    if (this.baseAcc == null) {
      this.baseAcc = new int[] { xyz[0], xyz[1], xyz[2] };
    }
    else {
      int x = xyz[0] - this.baseAcc[0];
      if (x < 0) { x = -x; }
      if (x > 512) { x = 1024 - x; }
      int y = xyz[1] - this.baseAcc[1];
      if (y < 0) { y = -y; }
      if (y > 512) { y = 1024 - y; }
      int z = xyz[2] - this.baseAcc[2];
      if (z < 0) { z = -z; }
      if (z > 512) { z = 1024 - z; }

      XLog.v("ACC: (%d, %d, %d) (%d, %d, %d) -> (%d, %d, %d)", this.baseAcc[0], this.baseAcc[1], this.baseAcc[2], xyz[0], xyz[1], xyz[2], x, y, z);
      // !!! 기기마다 감도가 다르다...

      if (x + y + z > 512) {
        Nerve.events.accept(Event.SHAKING);
      }
    }
  }

  void start(BluetoothDevice device) {
    try {
      final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
      this.socket = device.createRfcommSocketToServiceRecord(uuid);
      this.socket.connect();
      this.outputStream = this.socket.getOutputStream();
      this.inputStream = this.socket.getInputStream();
      this.starter.accept(Boolean.TRUE);
      Nerve.events.accept(State.READY);
    }
    catch (IOException e) {
      XLog.e(e.getMessage(), e);
      Nerve.events.accept(State.FAILED);
    }
  }

  // @DebugLog
  void terminate() {
    Subscriptions.removeAllOf(this.hashCode());

    this.stopGathering();
    this.stopReceiving();

    try { if (this.socket != null) { this.socket.close(); } } catch (IOException e) { XLog.e(e); }
    try { if (this.outputStream != null) { this.outputStream.close(); } } catch (IOException e) { XLog.e(e); }
    try { if (this.inputStream != null) { this.inputStream.close(); } } catch (IOException e) { XLog.e(e); }
  }

  private void stopGathering() {
    this.isGathering = false;
  }

  private void gather(final String payload) {
    this.isGathering = true;
    while (this.isGathering) {
      Pauser.pauseAndRun(() -> this.send(payload), 200);
    }
  }

  private void stopReceiving() {
    this.isReceiving = false;
  }

  private void receive(boolean isReceiving) {
    this.isReceiving = isReceiving;
    byte[] buffer = new byte[64];
    while (this.socket.isConnected()) { // !!! 소켓 연결 도중에는 오류나도 계속 실행될 수 있어서
      try {
        while (this.isReceiving) {
          int count = this.inputStream.read(buffer);
          if (count > 0) {
            String raw = Strings.bytesToHex(Arrays.copyOf(buffer, count));
            XLog.v("[RECEIVE] %s", raw);
            this.receiver.accept(raw);
          }
        }
      }
      catch (IOException e) {
        XLog.e(e.getMessage(), e);
        Pauser.pause();
      }
    }
  }

  private void send(String payload) {
    XLog.v("[SEND] %s", payload);
    this.sendQueue.add(payload);
    this.sender.accept(Boolean.TRUE);
  }

  private void send(byte[] data) {
    try {
      this.outputStream.write(data);
    }
    catch (IOException e) {
      XLog.e(e.getMessage(), e);
    }
  }

  void startAcc() {
    if (!this.isGathering) {
      this.gatherer.accept(Spp.REQ_START_ACC);
    }
  }

  void stopAcc() {
    this.isGathering = false;
  }

  void turnOffLed() {
    this.send(String.format("%s%s%s", HEAD_TURN_OFF_LED_REQ, Env.DEFAULT_STRING, TAIL));
  }

  void resetLed() {
    this.send(String.format("%s%s%s", HEAD_RESET_LED_REQ, Env.DEFAULT_STRING, TAIL));
  }

  void turnOnLed(LedColor color, int period) {
    if (period < 0x0A) { period = 0x0A; }
    if (period > 0xFF) { period = 0xFF; }
    this.send(String.format("%s%s%s%s", HEAD_TURN_ON_LED_REQ, color.value, period, TAIL));
  }

  void retrieveFactoryDefaultState() {
    this.send(String.format("%s%s%s", HEAD_GET_FD_REQ, Env.DEFAULT_STRING, TAIL));
  }

  void setFactoryDefault() {
    this.send(String.format("%s%s%s", HEAD_SET_FD_REQ, Env.DEFAULT_STRING, TAIL));
  }

  void unsetFactoryDefault() {
    this.send(String.format("%s%s%s", HEAD_UNSET_FD_REQ, Env.DEFAULT_STRING, TAIL));
  }

}
