package com.jininsa.robot.part;

import android.content.Context;

import com.elvishew.xlog.XLog;
import com.jininsa.robot.helper.Subscriptions;
import com.kadho.KidsenseCredentials;
import com.kadho.KidsenseServer;
import com.kadho.KidsenseSpeech;
import com.kadho.SpeechWrapper;

import io.reactivex.schedulers.Schedulers;

class EarKdh extends Ear {
  private Engine engine;

  static synchronized EarKdh init(Context context) {
    return new EarKdh(context);
  }

  EarKdh(final Context context) {
    super(context);
    this.engine = new Engine();
    Subscriptions.add(this.hashCode(), SUBSCRIPTION_COMMANDS,
      this.commands.observeOn(Schedulers.newThread()).subscribe(command -> {
        if (command == Command.START) {
          this.engine.start();
        }

        if (command == Command.STOP) {
          this.engine.stop();
        }

        if (command == Command.TERMINATE) {
          this.engine.stop();
        }
      }));
    Nerve.events.accept(State.READY);
  }

  static class Engine implements KidsenseSpeech {
    private SpeechWrapper stt;
    private Engine() {
      KidsenseCredentials credentials = new KidsenseCredentials("jininsa","ko","5552d4626fd8c265a63f3be459","male","KR","adults","v5");
      KidsenseServer server = new KidsenseServer(this, credentials);
      this.stt = new SpeechWrapper(this, server, credentials);
    }

    @Override
    public void onKidsenseResult(final String result) {
      XLog.d(result);
    }

    @Override
    public void onStatus(final String status) {
      XLog.d(status);
    }

    private void start() {
      this.stt.start();
    }

    private void stop() {
      this.stt.stop();
    }
  }

  @Override
  void start() {
    this.commands.accept(Command.START);
  }

  @Override
  void stop() {
    this.commands.accept(Command.STOP);
  }

  @Override
  void terminate() {
    this.commands.accept(Command.STOP);
  }
}
