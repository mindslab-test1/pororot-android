package com.jininsa.robot.part;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.app.Const;
import com.jininsa.robot.connector.Adapter;
import com.jininsa.robot.connector.Scanner;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.helper.Subscriptions;
import com.jininsa.robot.model.robot.Robot;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

class Bluetooth {
  enum Command {
    SCAN,
    LINK_CONNECTED_DEVICE,
    LINK_PAIRED_DEVICE,
    PAIR_FOUND_DEVICE,
    TURN_ON_HEADSET,
    SUCCEED,
    NOTIFY_DISCONNECTED,
    FAIL_TO_LINK_PAIRED_DEVICE,
    FAIL_TO_LINK_CONNECTED_DEVICE,
    FAIL_TO_PAIR_FOUND_DEVICE,
    FAIL_TO_SCAN,
    FAIL_TO_CREATE_BOND,
    FAIL_TO_TURN_ON_HEADSET
  }

  static class State extends Nerve.Event {
    static final State SCANNING = new State("SCANNING");
    static final State PAIRING = new State("PAIRING");
    static final State LINKING = new State("LINKING");
    static final State TURNING_ON_HEADSET = new State("TURNING_ON_HEADSET");
    static final State TERMINATING = new State("TERMINATING");
    static final State READY = new State("READY");
    static final State FAILED = new State("FAILED");
    static final State DISCONNECTED = new State("DISCONNECTED");
    static final State FAILED_TO_TURN_ON_HEADSET = new State("FAILED_TO_TURN_ON_HEADSET");

    State(final String name) {
      super(name);
    }
  }

  private static final String SUBSCRIPTION_BLUETOOTH_INTERNAL_COMMANDS = "n0vCOGMSrhEMcBwi";
  private static final String SUBSCRIPTION_SCO_STATE = "OgSsuvcoLRA6LUh5";
  private static final String SUBSCRIPTION_SCAN_RESULT = "DnWs2LUJc9XggZfK";
  private static final String SUBSCRIPTION_ADAPTER_STATE = "SFcHCePKv8qz6GR9";

  static private String toDeviceBondStateString(int state) {
    switch (state) {
      case BluetoothDevice.BOND_NONE:
        return "BOND_NONE";
      case BluetoothDevice.BOND_BONDING:
        return "BOND_BONDING";
      case BluetoothDevice.BOND_BONDED:
        return "BOND_BONDED";
      default:
        return String.valueOf(state);
    }
  }

  static private String toHeadsetAudioStateString(final int state) {
    switch (state) {
      case BluetoothHeadset.STATE_AUDIO_CONNECTED:
        return "STATE_AUDIO_CONNECTED";
      case BluetoothHeadset.STATE_AUDIO_DISCONNECTED:
        return "STATE_AUDIO_DISCONNECTED";
      case BluetoothHeadset.STATE_AUDIO_CONNECTING:
        return "STATE_AUDIO_CONNECTING";
      default:
        return String.valueOf(state);
    }
  }

  static private String toProfileConnectionStateString(int state) {
    switch (state) {
      case BluetoothProfile.STATE_CONNECTED:
        return "STATE_CONNECTED";
      case BluetoothProfile.STATE_CONNECTING:
        return "STATE_CONNECTING";
      case BluetoothProfile.STATE_DISCONNECTED:
        return "STATE_DISCONNECTED";
      case BluetoothProfile.STATE_DISCONNECTING:
        return "STATE_DISCONNECTING";
      default:
        return String.valueOf(state);
    }
  }

  static private String toUnboundReasonString(int reason) {
    switch (reason) {
      case 1:
        return "UNBOND_REASON_AUTH_FAILED";
      case 2:
        return "UNBOND_REASON_AUTH_REJECTED";
      case 3:
        return "UNBOND_REASON_AUTH_CANCELED";
      case 4:
        return "UNBOND_REASON_REMOTE_DEVICE_DOWN";
      case 5:
        return "UNBOND_REASON_DISCOVERY_IN_PROGRESS";
      case 6:
        return "UNBOND_REASON_AUTH_TIMEOUT";
      case 7:
        return "UNBOND_REASON_REPEATED_ATTEMPTS";
      case 8:
        return "UNBOND_REASON_REMOTE_AUTH_CANCELED";
      case 9:
        return "UNBOND_REASON_REMOVED";
      default:
        return String.valueOf(reason);
    }
  }

  static Bluetooth init(Context context, Robot robot) {
    Bluetooth bluetooth = new Bluetooth(context.getApplicationContext(), robot);
    bluetooth.start();
    return bluetooth;
  }

  private final PublishRelay<Command> internalCommands = PublishRelay.create();
  private final Adapter adapter;
  private final Robot robot;
  private Scanner scanner;
  private State state;
  private Command command;
  private BluetoothDevice device;
  private BluetoothHeadset profile;
  private final BluetoothProfile.ServiceListener profileServiceListener = new BluetoothProfile.ServiceListener() {
    @Override
    public void onServiceConnected(final int type, final BluetoothProfile profile) {
      // Decompile 방지
      try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {
        request.close();
      }
      catch (IOException ignored) {}

//      Bluetooth.this.disconnectOtherHeadsets(); // !!! 연결된 다른 헤드셋이 있다면 끊는다.
      Bluetooth.this.profile = (BluetoothHeadset) profile;

      if (Bluetooth.this.command == Command.LINK_CONNECTED_DEVICE) {
        List<BluetoothDevice> devices = profile.getConnectedDevices();
        Stream.of(devices).filter((device) -> Bluetooth.this.robot.getMac().equalsIgnoreCase(device.getAddress())).findSingle().ifPresentOrElse(
          __ -> Bluetooth.this.internalCommands.accept(Command.TURN_ON_HEADSET),                  // !!! 발견한 로봇이 블루투스 연결되었다면 결합(LINK_BLUETOOTH) 성공
          () -> Bluetooth.this.internalCommands.accept(Command.FAIL_TO_LINK_CONNECTED_DEVICE));   // !!! 발견한 로봇이 블루투스 연결되지 않았다면 결합(LINK_BLUETOOTH) 실패
//        adapter.closeProfileProxy(profile);
      }
      else if (Bluetooth.this.command == Command.LINK_PAIRED_DEVICE) {
        try {
          @SuppressLint("PrivateApi")
          Method m = BluetoothHeadset.class.getDeclaredMethod("connect", BluetoothDevice.class);
          // !!! This API returns false in scenarios like the profile ready the device is already connected or Bluetooth is not turned ready.
          boolean successful = (boolean) m.invoke(profile, Bluetooth.this.device); // !!! connect 성공하면 BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED 이벤트 발생

//          if (!successful) {
//            Bluetooth.this.internalCommands.accept(Command.FAIL_TO_LINK_PAIRED_DEVICE); // !!! connect가 false를 반환한 경우
//          }
        }
        catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
          // todo 리플렉션 실패했을 때 수동으로 블루투스를 연결하는 화면을 띄운다.
          // todo 상태와 코맨드는?
          // todo FAIL + MANUAL_SETTING
        }
//        finally {
//          adapter.closeProfileProxy(profile);
//        }
      }
    }

    @Override
    public void onServiceDisconnected(final int profile) {
      XLog.w(profile);
    }
  };

  private Bluetooth(Context context, Robot robot) {

    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {
      request.close();
    }
    catch (IOException ignored) {}

    this.robot = robot;
    this.adapter = Adapter.get();
    this.scanner = new Scanner(context.getApplicationContext(), (device) -> this.robot.getMac().equalsIgnoreCase(device.getAddress()), Scanner.Option.SCAN_FIRST);
    this.scanner.register();

    this.addSubscription(Bluetooth.SUBSCRIPTION_SCO_STATE,
      Nerve.events.filter(event -> event instanceof Audio.Event).subscribe(event -> {
        XLog.i("%s %s", event, this.state);
        if (this.state == State.TURNING_ON_HEADSET) {
          if (event == Audio.Event.SCO_ON) {
            this.internalCommands.accept(Command.SUCCEED); // !!! 헤드셋 켜는 중이고 SCO가 켜졌다면 오디어 연결 성공
          }
          if (event == Audio.Event.SCO_OFF) {
            if (this.device != null && this.profile != null) {
              XLog.w("class=%s isBluetoothScoAvailableOffCall=%s isBluetoothScoOn=%s", device.getBluetoothClass().getDeviceClass(), Audio.isBluetoothScoAvailableOffCall(), Audio.isBluetoothScoOn());
            }
            this.internalCommands.accept(Command.FAIL_TO_TURN_ON_HEADSET); // !!! 헤드셋 켜는 중인데 SCO가 꺼졌다면 오디어 연결 실패
          }
        }
        // !!! 블루투스 상태에 따라 달리 처리해야 한다. 노래 듣기 위해 헤드셋 끊을 때도 발생한다.
//        else if (this.state == State.TERMINATING) {
//          if (event == Audio.Event.SCO_OFF) {
//            Subscriptions.removeAllOf(this.hashCode());
//            this.scanner.stop();
//            this.scanner.unregister();
//            this.scanner = null;
//            this.unbind();
//          }
//        }
      }));

    this.addSubscription(Bluetooth.SUBSCRIPTION_ADAPTER_STATE,
      this.adapter.event.filter(event -> event.device.getAddress().equalsIgnoreCase(this.robot.getMac())).subscribe(event -> {
        final String action = event.action;
        final Intent intent = event.intent;
        this.device = event.device;

        if (BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED.equals(action)) {
          int p = intent.getIntExtra(BluetoothHeadset.EXTRA_PREVIOUS_STATE, Env.DEFAULT_INT);
          int n = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, Env.DEFAULT_INT);
          XLog.i("[%s] %s-->%s %s(%s)", action, toHeadsetAudioStateString(p), toHeadsetAudioStateString(n), this.device.getAddress(), this.device.getName());
        }

        if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
          int n = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, Env.DEFAULT_INT);
          int p = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, Env.DEFAULT_INT);
          int reason = intent.getIntExtra("android.bluetooth.device.extra.REASON", Env.DEFAULT_INT);
          XLog.d("%s(%s): %s--[%s]-->%s (%s)", this.device.getAddress(), this.device.getName(), toDeviceBondStateString(p), action, toDeviceBondStateString(n), toUnboundReasonString(reason));
          if (n == BluetoothDevice.BOND_BONDED) {
            this.internalCommands.accept(Command.LINK_PAIRED_DEVICE); // !!! 페어링 성공하면 페어링된 기기와 연결 시작
          }
          // ToDo : Pairing 실패하는 경우​
          if (n == BluetoothDevice.BOND_NONE) {
            this.internalCommands.accept(Command.FAIL_TO_PAIR_FOUND_DEVICE); // !!! 페어링 끊어지면 실패
          }
        }
        // !!! ACTION_CONNECTION_STATE_CHANGED와 ACTION_AUDIO_STATE_CHANGED는 둘다 발생하거나 하나만 발생한다.
        else if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
          int p = intent.getIntExtra(BluetoothHeadset.EXTRA_PREVIOUS_STATE, Env.DEFAULT_INT);
          int n = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, Env.DEFAULT_INT);
          XLog.d("[%s] %s-->%s %s(%s)", action, toProfileConnectionStateString(p), toProfileConnectionStateString(n), this.device.getAddress(), this.device.getName());
          if (n == BluetoothHeadset.STATE_CONNECTED) {
            this.internalCommands.accept(Command.TURN_ON_HEADSET); // !!! 헤드셋과 연결되면 헤드셋 SCO 활성화
          }

          if (n == BluetoothHeadset.STATE_DISCONNECTED) {
            if (this.adapter.isEnabled()) {
              this.internalCommands.accept(Command.NOTIFY_DISCONNECTED); // !!! 로봇 전원이 꺼지거나 연결이 끊어지면
            }
          }
        }
      })
    );

    this.addSubscription(Bluetooth.SUBSCRIPTION_SCAN_RESULT,
      this.scanner.result.subscribe(result -> {
        if (result != Scanner.Result.FINISHED) {
          Scanner.Result.State state = result.state;
          this.device = result.device;
          XLog.d(result);
          if (state == Scanner.Result.State.FOUND_CONNECTED_DEVICE) {
            this.internalCommands.accept(Command.LINK_CONNECTED_DEVICE); // !!! 이미 연결된 로봇과 링크
          }
          if (state == Scanner.Result.State.FOUND_NEW_DEVICE) {
            Stream.of(this.adapter.getBondedDevices()).filter(bondedDevice -> this.device.getAddress().equalsIgnoreCase(bondedDevice.getAddress())).findFirst().ifPresentOrElse(
              __ -> this.internalCommands.accept(Command.LINK_PAIRED_DEVICE), // !!! 새로 발견한 로봇이고. 페어링 정보가 있으며 링크
              () -> this.internalCommands.accept(Command.PAIR_FOUND_DEVICE)); // !!! 새로 발견한 로봇이고. 페이링 정보가 없으면 페어
          }
        }
        else {
          this.scanner.unregister();
          this.scanner.stop();
          if (!this.scanner.isFound()) {
            this.internalCommands.accept(Command.FAIL_TO_SCAN); // !!! 스캐닝이 끝났고, 발견한 로봇이 없으면 실패
          }
        }
      }));

    this.addSubscription(Bluetooth.SUBSCRIPTION_BLUETOOTH_INTERNAL_COMMANDS,
      this.internalCommands.observeOn(Schedulers.newThread()).subscribe(command -> {
        this.command = command;
        if (command == Command.SCAN) {
          XLog.d("[%s] device=%s(%s)", command, this.device, this.device != null ? this.device.getName() : "null");
          this.scanner.start();
          this.changeState(State.SCANNING);
        }

        if (command == Command.PAIR_FOUND_DEVICE) {
          boolean successful = this.device.createBond();
          XLog.d("[%s] device=%s(%s) createBond=%s", command, this.device, this.device != null ? this.device.getName() : "null", successful);
          this.changeState(State.PAIRING);
        }

        if (command == Command.LINK_CONNECTED_DEVICE) {
          XLog.d("[%s] device=%s(%s) profileConnectionState=%s", command, this.device,
            this.device != null ? this.device.getName() : "null",
            Bluetooth.toProfileConnectionStateString(this.adapter.getHeadsetConnectionState()));
          this.adapter.getHeadsetProfileProxy(this.profileServiceListener);
          this.changeState(State.LINKING);
        }

        if (command == Command.LINK_PAIRED_DEVICE) {
          XLog.d("[%s] device=%s(%s) bondState=%s", command, this.device,
            this.device != null ? this.device.getName() : "null",
            this.device != null ? Bluetooth.toDeviceBondStateString(this.device.getBondState()) : "null");
          this.adapter.getHeadsetProfileProxy(this.profileServiceListener);
          this.changeState(State.LINKING);
        }

        if (command == Command.TURN_ON_HEADSET) {
          XLog.d("[%s] device=%s(%s) profile=%s scoOn=%s", command, this.device,
            this.device != null ? this.device.getName() : "null",
            this.profile,
            Audio.isBluetoothScoOn());
          Pauser.pause(800);
          this.changeState(State.TURNING_ON_HEADSET);
          Audio.turnOnHeadset();
        }

        if (command == Command.SUCCEED) {
          XLog.d("[%s] device=%s(%s) profile=%s scoOn=%s", command, this.device,
            this.device != null ? this.device.getName() : "null",
            this.profile,
            Audio.isBluetoothScoOn());
          this.changeState(State.READY);
        }

        if (command == Command.FAIL_TO_CREATE_BOND ||
          command == Command.FAIL_TO_LINK_CONNECTED_DEVICE ||
          command == Command.FAIL_TO_LINK_PAIRED_DEVICE ||
          command == Command.FAIL_TO_PAIR_FOUND_DEVICE ||
          command == Command.FAIL_TO_SCAN) {
          XLog.st(16).d("[%s] device=%s(%s) profile=%s scoOn=%s",
            command,
            this.device,
            this.device != null ? this.device.getName() : "null",
            this.profile,
            Audio.isBluetoothScoOn());
          Nerve.events.accept(State.FAILED);
        }

        if (command == Command.FAIL_TO_TURN_ON_HEADSET) {
          XLog.st(16).d("[%s] device=%s(%s) profile=%s scoOn=%s",
            command,
            this.device,
            this.device != null ? this.device.getName() : "null",
            this.profile,
            Audio.isBluetoothScoOn());
          Nerve.events.accept(State.FAILED_TO_TURN_ON_HEADSET);
        }

        // !!! 갑자기 연결이 끊어지는 경우
        // !!! Parts
        if (command == Command.NOTIFY_DISCONNECTED) {
          XLog.d("[%s] device=%s(%s) profile=%s ascoOn=%s", command,
            this.device,
            this.device != null ? this.device.getName() : "null",
            this.profile,
            Audio.isBluetoothScoOn());
//          this.disconnect();
//          this.unbind(); // !!! Command.NOTIFY_DISCONNECTED
          Nerve.events.accept(State.DISCONNECTED);
        }
      })
    );
  }

  private void changeState(State state) {
    XLog.d("%s->%s device=%s", this.state, state, this.device);
    if (this.state != state) {
      this.state = state;
      Nerve.events.accept(this.state);
    }
  }

  private void addSubscription(String name, Disposable subscription) {
    Subscriptions.add(this.hashCode(), name, subscription);
  }

  BluetoothDevice getDevice() {
    return this.device;
  }

  void start() {
    this.internalCommands.accept(Command.SCAN);
  }

  // @DebugLog
  void terminate() {
//    Audio.setBluetoothScoOn(false);
    Subscriptions.removeAllOf(this.hashCode());
    this.scanner.stop();
    this.scanner.unregister();
    if (!Const.get().isLite) {
      this.disconnect();
      this.adapter.closeProfileProxy(this.profile);
      this.unbind();
    }
  }

  void terminate2() {
    Subscriptions.removeAllOf(this.hashCode());
    this.scanner.stop();
    this.scanner.unregister();
    if (!Const.get().isLite) this.disconnect();
    this.adapter.closeProfileProxy(this.profile);
  }

//  // @DebugLog
//  private void stop() {
////    this.disconnect();
//    this.unbind();
//  }

  private void disconnect() {
//    this.adapter.closeProfileProxy(this.profile);
    this.disconnect(this.device);
  }

  private void disconnect(final BluetoothDevice device) {
    // Decompile 방지
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}

    if (this.profile != null && device != null) {
      try {
//        @SuppressLint("PrivateApi") Method m2 = BluetoothHeadset.class.getDeclaredMethod("setPriority", BluetoothDevice.class, int.class);
//        boolean result2 = (boolean) m2.invoke(this.profile, device, /* PRIORITY_ON */ 1000);
        @SuppressLint("PrivateApi") Method m1 = BluetoothHeadset.class.getDeclaredMethod("disconnect", BluetoothDevice.class);
        boolean result1 = (boolean) m1.invoke(this.profile, device);
        XLog.d("[disconnect] %s %s %s", this.profile, device, result1);
      }
      catch (Exception e) {
        XLog.e(e.getMessage(), e);
      }
    }
  }

  private void disconnectOtherHeadsets() {
    List<BluetoothDevice> devices = this.profile.getConnectedDevices();
    // !!! 이미 연결된 헤드셋이 있으면 강제로 연결을 끊는다.
    Stream.of(devices).filter((device) -> !Bluetooth.this.robot.getMac().equalsIgnoreCase(device.getAddress())).forEach(this::disconnect);
  }

  private void unbind() {
    this.unbind(this.device);
  }

  private void unbind(final BluetoothDevice device) {
    // Decompile 방지
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}

    if (device != null) {
      try {
        @SuppressLint("PrivateApi") Method m = BluetoothDevice.class.getDeclaredMethod("removeBond");
        boolean result = (boolean) m.invoke(device);
        XLog.d("[unbind] %s %s", device, result);
      }
      catch (Exception e) {
        XLog.e(e.getMessage(), e);
      }
    }
  }
}
