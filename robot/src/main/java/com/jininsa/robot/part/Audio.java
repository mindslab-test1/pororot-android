package com.jininsa.robot.part;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.elvishew.xlog.XLog;
import com.jininsa.robot.app.ManageableBroadcastReceiver;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.model.robot.Robot;

import java.io.IOException;
import java.io.OutputStreamWriter;

class Audio {
  static class Settings {
    enum ChannelConfig {
      IN_MONO(AudioFormat.CHANNEL_IN_MONO, (short) 1),
      IN_STEREO(AudioFormat.CHANNEL_IN_STEREO, (short) 2),
      OUT_MONO(AudioFormat.CHANNEL_OUT_MONO, (short) 1),
      OUT_STEREO(AudioFormat.CHANNEL_OUT_STEREO, (short) 2);
      final public int channel;
      final public short numChannels;

      ChannelConfig(int channel, short numChannels) {
        this.channel = channel;
        this.numChannels = numChannels;
      }
    }

    enum SampleRate {
      HZ_44100(44100),
      HZ_22050(22050),
      HZ_16000(16000),
      HZ_8000(8000);
      final public int hz;

      SampleRate(int hz) {
        this.hz = hz;
      }
    }

    enum EncodingFormat {
      ENCODING_PCM_8BIT(AudioFormat.ENCODING_PCM_8BIT, (short) 8),
      ENCODING_PCM_16BIT(AudioFormat.ENCODING_PCM_16BIT, (short) 16),
      ENCODING_ACC(MediaRecorder.AudioEncoder.AAC, (short) 16);
      final public int format;
      final public short bit;

      EncodingFormat(int format, short bit) {
        this.format = format;
        this.bit = bit;
      }
    }

    enum AudioType {
      IN_MIC(MediaRecorder.AudioSource.MIC),
      IN_VOICE_RECOGNITION(MediaRecorder.AudioSource.VOICE_RECOGNITION),
      IN_VOICE_COMMUNICATION(MediaRecorder.AudioSource.VOICE_COMMUNICATION),
      OUT_STREAM_SYSTEM(AudioManager.STREAM_SYSTEM),
      OUT_STREAM_VOICE_CALL(AudioManager.STREAM_VOICE_CALL),
      OUT_STREAM_MUSIC(AudioManager.STREAM_MUSIC);

      final public int type;

      AudioType(int type) {
        this.type = type;
      }
    }

    static class Builder {
      private SampleRate sampleRate;
      private ChannelConfig channelConfig;
      private EncodingFormat encodingFormat;

      Builder() {}

      Builder sampleRate(SampleRate sampleRate) {
        this.sampleRate = sampleRate;
        return this;
      }

      Builder channelConfig(ChannelConfig channelConfig) {
        this.channelConfig = channelConfig;
        return this;
      }

      Builder encodingFormat(EncodingFormat encodingFormat) {
        this.encodingFormat = encodingFormat;
        return this;
      }

      Settings build() {
        return new Settings(this.sampleRate, this.channelConfig, this.encodingFormat);
      }
    }

    // !!! 16000Hz 샘플링레이트, 16Bit 인코딩, 1 채널 - 32000byte/sec
    static final Settings DEFAULT_IN = new Settings(SampleRate.HZ_16000, ChannelConfig.IN_MONO, EncodingFormat.ENCODING_PCM_16BIT);
    static final Settings DEFAULT_OUT = new Settings(SampleRate.HZ_16000, ChannelConfig.OUT_MONO, EncodingFormat.ENCODING_PCM_16BIT);

    final SampleRate sampleRate;
    final ChannelConfig channelConfig;
    final EncodingFormat encodingFormat;

    private Settings(final SampleRate sampleRate, final ChannelConfig channelConfig, final EncodingFormat encodingFormat) {
      this.sampleRate = sampleRate;
      this.channelConfig = channelConfig;
      this.encodingFormat = encodingFormat;
    }
  }

  private static class AudioStateReceiver extends ManageableBroadcastReceiver {
    public AudioStateReceiver(final Context context) {
      super(context);
    }

    @Override
    protected IntentFilter intentFilter() {
      IntentFilter filter = new IntentFilter();
      filter.addAction(AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED);
      return filter;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
      // Decompile 방지
      try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}

      if (intent == null) { return; }
      final String action = intent.getAction();

      if (AudioManager.ACTION_SCO_AUDIO_STATE_UPDATED.equalsIgnoreCase(action)) {
        int p = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_PREVIOUS_STATE, Env.DEFAULT_INT);
        int n = intent.getIntExtra(AudioManager.EXTRA_SCO_AUDIO_STATE, Env.DEFAULT_INT);
        Robot robot = Robot.get();
        XLog.i("[%s] %s-->%s %s", action, Audio.toScoAudioStateString(p), Audio.toScoAudioStateString(n), robot);

        if (n != p) {
          if (n == AudioManager.SCO_AUDIO_STATE_DISCONNECTED) {
            // !!! 고치지 말것
            // !!! Audio.audioManager.setMicrophoneMute(true);
            // !!! Audio.setBluetoothScoOn(false);
            // !!! Audio.audioManager.setMode(AudioManager.MODE_NORMAL);
            Audio.audioManager.setMicrophoneMute(false);
            Audio.setBluetoothScoOn(false);
            Audio.audioManager.setMode(AudioManager.MODE_NORMAL);
            int maxVolume = Math.round(Audio.audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC) / 2.0F);
            int volume = Audio.audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            Audio.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, Math.max(maxVolume, volume), AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
//            Audio.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, Math.max(maxVolume, volume), AudioManager.FLAG_SHOW_UI);
            Nerve.events.accept(Event.SCO_OFF);
          }

          if (n == AudioManager.SCO_AUDIO_STATE_CONNECTED) {
            // !!! 고치지 말것
            // !!! Audio.setBluetoothScoOn(true);
            // !!! Audio.audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            Audio.audioManager.setMicrophoneMute(false);
            Audio.setBluetoothScoOn(true);
            Audio.audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            int maxVolume = Math.round(Audio.audioManager.getStreamMaxVolume(6) / 2.0F);
            int volume = Audio.audioManager.getStreamVolume(6);
            Audio.audioManager.setStreamVolume(6, Math.min(maxVolume, volume), AudioManager.FLAG_SHOW_UI);
            Audio.audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 1, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
            Nerve.events.accept(Event.SCO_ON);
          }
        }
      }
    }
  }

  static class Event extends Nerve.Event {
    static final Event SCO_ON = new Event("SCO_ON");
    static final Event SCO_OFF = new Event("SCO_OFF");

    private Event(final String name) {
      super(name);
    }
  }

  static AudioManager audioManager;
  private static Audio singleton;

  static private String toScoAudioStateString(int state) {
    switch (state) {
      case AudioManager.SCO_AUDIO_STATE_DISCONNECTED:
        return "SCO_AUDIO_STATE_DISCONNECTED";
      case AudioManager.SCO_AUDIO_STATE_CONNECTING:
        return "SCO_AUDIO_STATE_CONNECTING";
      case AudioManager.SCO_AUDIO_STATE_CONNECTED:
        return "SCO_AUDIO_STATE_CONNECTED";
      case AudioManager.SCO_AUDIO_STATE_ERROR:
        return "SCO_AUDIO_STATE_ERROR";
      default:
        return String.valueOf(state);
    }
  }

  static public Audio init(Context context) {
    if (Audio.singleton == null) {
      Audio.audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
      Audio.singleton = new Audio(context);
    }
    return Audio.singleton;
  }

  public static Audio get() {
    return Audio.singleton;
  }

  static boolean isBluetoothScoOn() {
    return Audio.audioManager.isBluetoothScoOn();
  }

  static boolean isBluetoothScoAvailableOffCall() {
    return Audio.audioManager.isBluetoothScoAvailableOffCall();
  }

  static boolean isMicrophoneMute() {
    return Audio.audioManager.isMicrophoneMute();
  }

  @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
  static boolean isVolumeFixed() {
    return Audio.audioManager.isVolumeFixed();
  }

  static boolean isMusicActive() {
    return Audio.audioManager.isMusicActive();
  }


  private final AudioStateReceiver receiver;

  private Audio(Context context) {
    this.receiver = new AudioStateReceiver(context.getApplicationContext());
    this.receiver.register();
  }

  static void setBluetoothScoOn(boolean on) {
    Audio.audioManager.setBluetoothScoOn(on);
  }

  // @DebugLog
  static void turnOffHeadset() {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}
    // !!! 고치지 말것
    // !!! Audio.audioManager.setMicrophoneMute(false);
    // !!! Audio.audioManager.stopBluetoothSco();

    Audio.audioManager.setMicrophoneMute(false);
    Audio.audioManager.stopBluetoothSco();
  }

  // @DebugLog
  static void turnOnHeadset() {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}
    // !!! startBluetoothSco를 하지 않으면 블루투스 마이크로 입력이 들어가지 않는다.
    // !!! MODE_IN_CALL : 블루투스 마이크로 입력이 들어간다. 볼륨 조절이 안된다.
    // !!! MODE_IN_COMMUNICATION : STREAM_MUSIC의 볼륨과 블루투스 마이크 입력세기가 비례한다. 볼륨이 0이면 핸드폰 마이크로 입력이 들어간다.

    // !!! 고치지 말것
    // !!! Audio.audioManager.setMicrophoneMute(false);
    // !!! Audio.audioManager.startBluetoothSco();

    Audio.audioManager.setMicrophoneMute(false);
    Audio.audioManager.startBluetoothSco();
  }

  static void setModeCommunication() {
    Audio.audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
  }

  static void setModeInCall() {
    Audio.audioManager.setMode(AudioManager.MODE_IN_CALL);
  }

  public void terminate() {
    this.receiver.unregister();
  }

  static void setModeNormal() {
    Audio.audioManager.setMode(AudioManager.MODE_NORMAL);
  }
}
