package com.jininsa.robot.part;

import android.content.Context;

import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.app.Const;

import java.lang.ref.WeakReference;

abstract class Ear {
  static final String SUBSCRIPTION_COMMANDS = "Z9GoE4Yyq4Fuosn6";

  static class Signal extends Nerve.Signal<String> {
    static final Signal SOUND = new Signal("SOUND");

    private Signal(String name) { super(name); }

    @Override
    Signal add(final String data) {
      return (Signal) super.add(data);
    }
  }

  enum Command {
    START, STOP, RESTART, TERMINATE
  }

  static class State extends Nerve.Event {
    static final State READY = new State("READY");
    static final State FAILED = new State("FAILED");

    State(final String event) {
      super(event);
    }
  }

  static Ear create(Context context) {
    String earType = Const.get().earType;
    Ear ear = null;
    switch (earType) {
      case "ASR":
        ear = EarAsr.init(context);
        break;
      case "KDH":
        ear = Ear1.init(context);
        break;
    }
    return ear;
  }

  final Audio audio;

  final PublishRelay<Command> commands = PublishRelay.create();

  final WeakReference<Context> contextWrapper;

  Ear(Context context) {
    this.contextWrapper = new WeakReference<>(context.getApplicationContext());
    this.audio = Audio.get();
  }

  abstract void start();

  abstract void stop();

  abstract void terminate();

}
