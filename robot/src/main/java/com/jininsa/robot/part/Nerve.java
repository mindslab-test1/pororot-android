package com.jininsa.robot.part;

import com.jakewharton.rxrelay2.PublishRelay;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

class Nerve {

  //  static final BehaviorRelay<Signal> signals = BehaviorRelay.create();
//  static final BehaviorRelay<State> event = BehaviorRelay.create();
  static final PublishRelay<Signal> signals = PublishRelay.create();
  static final PublishRelay<Event> events = PublishRelay.create();

  abstract static class Signal<T> {
    static final private int MAX_QUEUE_SIZE = 64;
    final String signal;
    private Queue<T> queue;

    Signal(String signal) {
      super();
      this.signal = signal;
      this.queue = new LinkedBlockingQueue<>(MAX_QUEUE_SIZE);
    }

    @Override
    public String toString() {
      String packageName = this.getClass().getPackage().getName();
      String className = this.getClass().getCanonicalName();
      return String.format("%s.%s/%s", className.replace(packageName + "." , ""), this.signal, this.queue);
    }

    T peek() {
      return this.queue.peek();
    }

    T poll() {
      return this.queue.poll();
    }

    Signal<T> add(T data) {
      if (this.queue.size() >= MAX_QUEUE_SIZE) { this.queue.poll(); }
      this.queue.add(data);
      return this;
    }

    void clear() {
      this.queue.clear();
    }

    void destroyQueue() {
      this.queue = null;
    }
  }

  abstract static class Event {
    final String event;
    Event(String event) {
      this.event = event;
    }

    @Override
    public String toString() {
      String packageName = this.getClass().getPackage().getName();
      String className = this.getClass().getCanonicalName();
      return String.format("%s.%s", className.replace(packageName + "." , ""), this.event);
    }
  }
}
