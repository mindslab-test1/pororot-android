package com.jininsa.robot.part;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;

import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.helper.Subscriptions;

import java.util.List;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;

class EarAsr extends Ear {
  enum RecognitionState {
    WAITING, RECOGNIZING, STOPPED
  }

  static final private class Engine implements RecognitionListener {

    private static String errorString(int code) {
      switch (code) {
        case /*1*/ SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
          return "ERROR_NETWORK_TIMEOUT";
        case /*2*/ SpeechRecognizer.ERROR_NETWORK:
          return "ERROR_NETWORK";
        case /*3*/ SpeechRecognizer.ERROR_AUDIO:
          return "ERROR_AUDIO";
        case /*4*/ SpeechRecognizer.ERROR_SERVER:
          return "ERROR_SERVER";
        case /*5*/ SpeechRecognizer.ERROR_CLIENT:
          return "ERROR_CLIENT";
        case /*6*/ SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
          return "ERROR_SPEECH_TIMEOUT";
        case /*7*/ SpeechRecognizer.ERROR_NO_MATCH:
          return "ERROR_NO_MATCH";
        case /*8*/ SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
          return "ERROR_RECOGNIZER_BUSY";
        case /*9*/ SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
          return "ERROR_INSUFFICIENT_PERMISSIONS";
      }

      return "ERROR_UNKNOWN";
    }

    private final EarAsr ear;

    private Engine(final EarAsr ear) {
      this.ear = ear;
    }

    @Override
    public void onReadyForSpeech(final Bundle params) {
      this.ear.recognitionState = RecognitionState.WAITING;
      this.ear.restarter.accept(Boolean.TRUE);
    }

    @Override
    public void onBeginningOfSpeech() {
      this.ear.recognitionState = RecognitionState.RECOGNIZING;
      this.ear.restarter.accept(Boolean.TRUE);
    }

    @Override
    public void onRmsChanged(final float rmsdB) {}

    @Override
    public void onBufferReceived(final byte[] buffer) {}

    @Override
    public void onEndOfSpeech() {}

    @Override
    // @DebugLog
    public void onError(final int error) {
      this.ear.interpret();
      XLog.e(errorString(error));
      if (error != SpeechRecognizer.ERROR_RECOGNIZER_BUSY) {
        if (error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT) {
          Pauser.pause(200);
        }
        this.ear.restartHarshly();
      }
    }

    @Override
    public void onResults(final Bundle results) {
      this.ear.heard = this.extractHeard(results);
      this.ear.interpret();
      this.ear.restartHarshly();
    }

    @Override
    public void onPartialResults(final Bundle partialResults) {
      this.ear.heard = this.extractHeard(partialResults);
    }

    @Override
    public void onEvent(final int eventType, final Bundle params) {}

    private String extractHeard(final Bundle results) {
      final List<String> words = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
      if (words != null && words.get(0).length() > 0) {
        return words.get(0);
      }
      else {
        return Env.DEFAULT_STRING;
      }
    }
  }

  static synchronized EarAsr init(Context context) {
    return new EarAsr(context);
  }

  private final PublishRelay<Boolean> restarter = PublishRelay.create();
  private final Intent recognizerIntent;
  private SpeechRecognizer recognizer;
  private String heard = Env.DEFAULT_STRING;
  private Engine engine;
  //  private RecognitionState previousRecognitionState = RecognitionState.WAITING;
  private RecognitionState recognitionState; // = RecognitionState.STOPPED;

  private EarAsr(Context context) {
    super(context);
    this.engine = new Engine(this);
    this.recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
      .putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
      .putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.KOREA.getLanguage())
//      .putExtra("android.speech.extra.DICTATION_MODE", true)
      .putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);

    Subscriptions.add(this.hashCode(), SUBSCRIPTION_COMMANDS,
      this.commands.observeOn(AndroidSchedulers.mainThread()).subscribe(command -> {
        if (command == Command.START) {
          this.startInternal();
        }

        if (command == Command.RESTART) {
          this.restartHarshly();
        }

        if (command == Command.STOP) {
          this.stopInternal();
        }

        if (command == Command.TERMINATE) {
          this.terminateInternal();
        }
      }));

    // !!! 일부 폰에서 화면이 꺼지면 음성인식 서비스도 중지도기 때문에 강제로 재시작함
    // !!! 음성인식 대기 상태에서 2초 동안 머문다면 음성인식 서비스 다시 시작
    // !!! 음성인식 상태에서 4초이상 음성인식 서비스 다시 시작
//    Subscriptions.add(this.hashCode(), SUBSCRIPTION_RESTARTER, this.restarter.observeOn(Schedulers.newThread()).subscribe(
//      __ -> {
//        if (this.recognitionState == RecognitionState.WAITING) {
//          Pauser.pause(2000);
//          if (this.recognitionState == RecognitionState.WAITING) {
//            this.interpret();
//            this.restart();
//          }
//        }
//        else if (this.recognitionState == RecognitionState.RECOGNIZING) {
//          Pauser.pause(4000);
//          if (this.recognitionState == RecognitionState.RECOGNIZING) {
//            this.interpret();
//            this.restart();
//          }
//        }
//      }));

    Nerve.events.accept(State.READY);
  }

  private void terminateInternal() {
    this.stopInternal();
    Subscriptions.removeAllOf(this.hashCode());
  }

  private void interpret() {
    if (!this.heard.equals(Env.DEFAULT_STRING)) {
      Nerve.signals.accept(Signal.SOUND.add(this.heard));
      this.heard = Env.DEFAULT_STRING;
    }
  }

  private void restart() {
    this.commands.accept(Command.RESTART);
  }

  @Override
  void start() {
    // !!! SpeechRecognizer#createSpeechRecognizer는 메인 스레드에서만 실행할 수 있기 때문에 직접 호출할 수 없다.
    this.commands.accept(Command.START);
  }

  @Override
  void stop() {
    this.commands.accept(Command.STOP);
  }

  // @DebugLog
  void terminate() {
    this.commands.accept(Command.TERMINATE);
  }

  // @DebugLog
  private void restartSoftly() {
    this.clear();
    this.start();
  }

  // @DebugLog
  private void restartHarshly() {
    this.clear();
    if (this.recognizer != null) {
      this.recognizer.setRecognitionListener(null);
    }
    this.engine = new Engine(this);
    this.start();
  }

//  // @DebugLog
//  private void restartHarshlyWithDelay() {
//    Pauser.pauseAndRun(this::clear);
//    if (this.recognizer != null) {
//      this.recognizer.setRecognitionListener(null);
//    }
//    this.engine = new Engine(this);
//    Pauser.pauseAndRun(this::start);
//  }

  private void startInternal() {
    try {
      Context context = this.contextWrapper.get();
      this.recognizer = SpeechRecognizer.createSpeechRecognizer(context);
      this.recognizer.setRecognitionListener(this.engine);
      this.recognizer.startListening(this.recognizerIntent);
    }
    catch (Exception e) {
      XLog.e(e.getMessage(), e);
    }
  }

  private void stopInternal() {
    this.clear();
    this.heard = Env.DEFAULT_STRING;
    this.recognitionState = RecognitionState.STOPPED;
  }

  private void clear() {
    XLog.d(this.recognizer);
    if (this.recognizer != null) {
      try {
        this.recognizer.setRecognitionListener(null);
        this.recognizer.destroy();
        this.recognizer = null;
      }
      catch (Exception e) {
        XLog.e(e.getMessage(), e);
      }
    }
  }
}
