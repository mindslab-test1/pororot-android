package com.jininsa.robot.part;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.AudioTrack;
import android.net.Uri;

import com.elvishew.xlog.XLog;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.app.Const;
import com.jininsa.robot.app.Key;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.connector.Tts;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.helper.Subscriptions;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

class Mouth {
  enum Doing {
    NONE, WAITING, SPEAKING, BEFORE_SINGING, PAUSE_SINGING, SINGING, AFTER_SINGING, READING, START_VIDEO, STOP_VIDEO;

    boolean canSound() {
      return this == WAITING || this == BEFORE_SINGING || this == READING || this == PAUSE_SINGING || this == STOP_VIDEO;
    }
  }

  enum Command {
    SOUND, SOUND_NEXT
  }

  static class Signal extends Nerve.Signal {
    static final Signal BEFORE_SINGING = new Signal("BEFORE_SINGING");
    static final Signal AFTER_SINGING = new Signal("AFTER_SINGING");
    static final Signal PAUSE_SINGING = new Signal("PAUSE_SINGING");
    static final Signal RESUME_SINGING = new Signal("RESUME_SINGING");
    static final Signal BEFORE_READING = new Signal("BEFORE_READING");
    static final Signal AFTER_READING = new Signal("AFTER_READING");
    static final Signal START_VIDEO = new Signal("STRAT_VIDEO");
    static final Signal STOP_VIDEO = new Signal("STOP_VIDEO");

    Signal(final String name) {
      super(name);
    }
  }

  static class State extends Nerve.Event {
    static final State READY = new State("READY");
    static final State FAILED = new State("FAILED");

    State(final String name) {
      super(name);
    }
  }

  static private class Item {
    enum Type {
      DIALOG, SONG, BOOK, VIDEO
    }

    final Type type;
    final String content;

    private Item(final Type type, final String content) {
      this.type = type;
      this.content = content;
    }

    @Override
    public String toString() {
      return String.format("%s(%s)", type, content);
    }
  }

  static private final int SHAKING_COUNT_THRESHOLD = 5;
  static private final String SUBSCRIPTION_SCO_CHANGE = "EhcG4X1hDdpeOsn1";
  static private final String SUBSCRIPTION_TTS_ENGINE_STATE = "acpyuBI3pZ4jPF0p";
  static private final String SUBSCRIPTION_INTERNAL_COMMANDS = "hnK8FWlGc98d5JSh";
  static private final String SUBSCRIPTION_ACC_EVENTS = "yo1Ls8UTE4GbJpXq";

  static Mouth init(final Context context) {
    return new Mouth(context.getApplicationContext());
  }

  private final Tts engine;
  private final PublishRelay<Command> commands = PublishRelay.create();
  private AssetManager assetManager;
  private Queue<Item> items;
  private String songToPlay;
  private String bookToRead;
  private Doing doing = Doing.NONE;
  private AudioTrack voicePlayer;
  private SimpleExoPlayer musicPlayer;
  private boolean onReading = false;
  private String bookTitle;
  private String songTitle;
  private String videoUrl;
  private int shakingCount;

  // @DebugLog
  private Mouth(final Context context) {
    this.engine = Tts.init(context.getApplicationContext());
    Subscriptions.add(this.hashCode(), Mouth.SUBSCRIPTION_TTS_ENGINE_STATE,
      this.engine.state.subscribe(ttsEvent -> {
        if (ttsEvent == Tts.State.READY) {
          this.assetManager = context.getAssets();
          this.items = new LinkedBlockingQueue<>();

          DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
          TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
          TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
          this.musicPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

          final Audio.Settings settings = Audio.Settings.DEFAULT_OUT;
          final int minBufferSize = AudioTrack.getMinBufferSize(settings.sampleRate.hz, settings.channelConfig.channel, settings.encodingFormat.format);
          this.voicePlayer = new AudioTrack(
            Audio.Settings.AudioType.OUT_STREAM_VOICE_CALL.type, // !!! "전화연결"로 음량 제어
            settings.sampleRate.hz,
            settings.channelConfig.channel,
            settings.encodingFormat.format,
            minBufferSize,
            AudioTrack.MODE_STREAM);

          Subscriptions.add(this.hashCode(), Mouth.SUBSCRIPTION_SCO_CHANGE,
            Nerve.events.filter(event -> event instanceof Audio.Event).observeOn(AndroidSchedulers.mainThread()).subscribe(event -> {
              if (event == Audio.Event.SCO_OFF && this.doing == Doing.BEFORE_SINGING) {
                this.play(context.getApplicationContext());
              }
              if (event == Audio.Event.SCO_ON && this.doing == Doing.PAUSE_SINGING) {
                this.changeState(Doing.WAITING);
              }
              if (event == Audio.Event.SCO_ON && this.doing == Doing.AFTER_SINGING) {
                this.changeState(Doing.WAITING);
                this.speak("노래 끝났다. 다른 거 하자.");
              }

              if (event == Audio.Event.SCO_OFF && this.doing == Doing.START_VIDEO) {
                this.watch(context.getApplicationContext());
              }
              if (event == Audio.Event.SCO_ON && this.doing == Doing.STOP_VIDEO) {
                this.changeState(Doing.WAITING);
//                this.speak("유튜브 다봤다. 다른 거 하자.");
              }
            }));

          Subscriptions.add(this.hashCode(), Mouth.SUBSCRIPTION_INTERNAL_COMMANDS,
            this.commands.observeOn(Schedulers.newThread()).subscribe(command -> {
              if (command == Command.SOUND) {
                if (this.doing.canSound()) {
                  this.sound();
                }
              }
              if (command == Command.SOUND_NEXT) {
                this.changeState(Doing.WAITING);
                Pauser.pauseAndRun(() -> this.commands.accept(Command.SOUND));
              }
            }));

          Subscriptions.add(this.hashCode(), Mouth.SUBSCRIPTION_ACC_EVENTS,
            Nerve.events.filter(event -> event == Spp.Event.SHAKING).subscribe(event -> {
                this.shakingCount++;
                XLog.d("[%s] %d %s", event, this.shakingCount, this.doing);
                boolean shutUpWithShaking = Env.getBoolean(Key.SETTINGS_STOP_BY_SHAKING, false);
                if (shutUpWithShaking && this.shakingCount >= Mouth.SHAKING_COUNT_THRESHOLD) {
                  this.shakingCount = 0;
                  if (this.doing == Doing.SPEAKING) {
                    this.stopSpeaking();
                  }

                  if (this.doing == Doing.SINGING) {
                    this.stopPlaying();
                  }
                }
              }
            ));

          this.changeState(Doing.WAITING);
          Nerve.events.accept(State.READY);
        }

        if (ttsEvent == Tts.State.FAILED) {
          this.changeState(Doing.NONE);
          Nerve.events.accept(State.FAILED);
        }
      }));
  }

  void start() {
    this.engine.start();
  }

  private void sound() {
    Item item = this.items.poll();
    if (item != null) {
      if (item.type == Item.Type.DIALOG) {
        if (this.onReading) {
          ContentManager.get().setNowReadingLine(item.content);
        }

        this.utter(item.content);
      }

      if (item.type == Item.Type.SONG) {
        this.prepareToPlay(item.content);
      }

      if (item.type == Item.Type.BOOK) {
        this.onReading = true;
        this.narrate();
      }

      if (item.type == Item.Type.VIDEO) {
        this.startVedio();
      }
    }
    else {
      if (this.onReading) {
        this.onReading = false;
        this.completeReading();
      }
    }
  }

  void speak(String speaking) {
    this.items.add(new Item(Item.Type.DIALOG, speaking));
    this.commands.accept(Command.SOUND);
  }

  private void utter(String speaking) {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}
    this.changeState(Doing.SPEAKING);
    byte[] pcmData = this.engine.speak(speaking);
    this.voicePlayer.play();
    this.voicePlayer.write(pcmData, 0, pcmData.length);
    Pauser.pause();
    this.commands.accept(Command.SOUND_NEXT);
  }

  // @DebugLog
  void sing(String uri, String title) {
    this.songToPlay = uri;
    this.songTitle = title;
    this.items.add(new Item(Item.Type.SONG, uri));
    this.commands.accept(Command.SOUND);
  }

  private void prepareToPlay(String key) {
    this.changeState(Doing.BEFORE_SINGING);
    Nerve.signals.accept(Signal.BEFORE_SINGING);
  }

  private void play(final Context context) {
    this.changeState(Doing.SINGING);
    Uri uri = Uri.parse(this.songToPlay);
    this.songToPlay = null;
    try {
      this.songToPlay = null;
      XLog.d(uri);

      DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
      DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context, Util.getUserAgent(context, "Pororot"), bandwidthMeter);
      MediaSource mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
      this.musicPlayer.setAudioAttributes(AudioAttributes.DEFAULT);
      this.musicPlayer.prepare(mediaSource);
      this.musicPlayer.setPlayWhenReady(true);
    }
    catch (Exception e) {
      XLog.e(e.getMessage(), e);
    }
  }

  void stopPlaying() {
    try {
      if (this.musicPlayer != null) {
        this.musicPlayer.stop();
        this.changeState(Doing.AFTER_SINGING);
        Nerve.signals.accept(Signal.AFTER_SINGING);
      }
    }
    catch (Exception e) {
      XLog.e(e);
    }
  }

  void pausePlaying() {
    if (this.musicPlayer != null) {
      this.musicPlayer.setPlayWhenReady(false);
      this.changeState(Doing.PAUSE_SINGING);
      Nerve.signals.accept(Signal.PAUSE_SINGING);
    }
  }

  void resumePlaying() {
    if (this.musicPlayer != null) {
      this.musicPlayer.setPlayWhenReady(true);
      this.changeState(Doing.SINGING);
      Nerve.signals.accept(Signal.RESUME_SINGING);
    }
  }

  SimpleExoPlayer getMusicPlayer() {
    return this.musicPlayer;
  }

  void read(String contents, String title) {
    this.bookTitle = title;
    this.bookToRead = contents;
    this.items.add(new Item(Item.Type.BOOK, contents));
    this.commands.accept(Command.SOUND);
  }

  private void narrate() {
    Nerve.signals.accept(Signal.BEFORE_READING);
    this.changeState(Doing.READING);

    String contents = this.bookToRead;
    this.bookToRead = null;
    String lines[] = contents.split("\\r?\\n");

    for (String line: lines) {
      if (!this.onReading) {
        break;
      }
      // todo 빈라인이면 잠깐 멈추기
      if (line.length() == 0) {
        Pauser.pause(200);
      }
      else {
        this.speak(line);
        Pauser.pause(line.length() / 32);
      }
    }
  }

  private void completeReading() {
    this.speak(String.format("%s 다 읽었다.", this.bookTitle));
    Nerve.signals.accept(Signal.AFTER_READING);
  }

  void stopSpeaking() {
    this.voicePlayer.stop();
    this.voicePlayer.flush();
    this.items.clear();
    this.onReading = false;
    this.speak("응. 조용히 할게.");
    Nerve.signals.accept(Signal.AFTER_READING);
  }

  void stopSpeakingWithoutSpeak() {
    this.voicePlayer.stop();
    this.voicePlayer.flush();
    this.items.clear();
    this.onReading = false;
    Nerve.signals.accept(Signal.AFTER_READING);
  }

  void playVideo(String videoUrl) {
    this.videoUrl = videoUrl;
    this.items.add(new Item(Item.Type.VIDEO, videoUrl));
    this.commands.accept(Command.SOUND);
  }

  void startVedio() {
    Nerve.signals.accept(Signal.START_VIDEO);
    this.changeState(Doing.START_VIDEO);
  }

  void watch(final Context context) {
//    Intent intent = new Intent(context, VideoPlayerActivity.class);
    Intent intent = new Intent(context, Const.get().videoPlayerContext);
    intent.putExtra("url", this.videoUrl);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    context.startActivity(intent);
  }

  void stopVideo() {
    this.changeState(Doing.STOP_VIDEO);
    Nerve.signals.accept(Signal.STOP_VIDEO);
  }

  // @DebugLog
  void terminate() {
    this.changeState(Doing.NONE);
    Subscriptions.removeAllOf(this.hashCode());
    if (this.items != null) {
      this.items.clear();
    }
    if (this.voicePlayer != null) {
      try { this.voicePlayer.stop(); } catch (Exception ignored) {}
      this.voicePlayer.flush();
    }
    if (this.musicPlayer != null) {
      try { this.musicPlayer.stop(); } catch (Exception ignored) {}
      this.musicPlayer.release();
    }
  }

  private void changeState(Doing doing) {
    if (this.doing != doing) {
      this.doing = doing;
    }
  }

  boolean isSinging() {
    return this.doing == Doing.SINGING || this.doing == Doing.BEFORE_SINGING;
  }

  boolean isReading() { return this.onReading; }
}
