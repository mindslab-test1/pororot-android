package com.jininsa.robot.part;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.jininsa.robot.app.Const;
import com.jininsa.robot.app.Key;
import com.jininsa.robot.connector.ChatBot;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.helper.Subscriptions;
import com.jininsa.robot.model.ChatMessage;
import com.jininsa.robot.model.Talker;
import com.jininsa.robot.model.contents.Book;
import com.jininsa.robot.model.contents.Song;
import com.jininsa.robot.model.contents.YouTube;
import com.jininsa.robot.model.party.Person;
import com.jininsa.robot.model.robot.Robot;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

// !!! Service는 원래 싱글톤
public class Brain extends Service {

  enum Part {
    MOUTH, EAR, SPP;

    static boolean isReady() {
      return !Stream.of(Part.values()).filter(value -> !value.ready).findFirst().isPresent();
    }

    static void clean() {
      Stream.of(Part.values()).forEach(value -> value.ready = false);
    }

    boolean ready = false;

    Part() {}
  }

  private static final List<String> STOP_COMMANDS = Arrays.asList("조용히해", "그만");
  private static final List<String> TIME_COMMANDS = Arrays.asList("TIME", "DATE", "DAYS");

  private static final String SUBSCRIPTION_CHATBOT = "AzlAhxMLIuW58Q9L";
  private static final String SUBSCRIPTION_STATE_OF_PARTS = "u2FKHH4WQSC6rKoI";
  private static final String SUBSCRIPTION_SIGNALS_FROM_EAR = "9KeXj7CL22RLXOy7";
  private static final String SUBSCRIPTION_SIGNALS_FROM_MOUTH = "OYtlH1c3E6DmbuLg";
  private static final String SUBSCRIPTION_PROBLEMS = "YSfU3hEJk0gCBN0U";

  private static final String KEY_RANDOM = "000";

  private static final String NOTIFICATION_CHANNEL_ID = "r7NT8hFzWxxh5kKK";
  private static final String NOTIFICATION_CHANNEL_NAME = "POROROT";

  public class Binder extends android.os.Binder {
    public Brain getConnector() {
      return Brain.this;
    }
  }

  private final Binder binder = new Binder();
  private Audio audio;
  private ChatBot chatBot;
  private Robot robot;
  private Bluetooth bluetooth;
  private Mouth mouth;
  private Ear ear;
  private Spp spp;

  private boolean singingBySpeak; // !!! 임시 나중에 다른 방식으로 수정하기
  private String lastHearing;

  // !!! 최초로 서비스 바인딩될 때 1회 실행
  // @DebugLog
  @Override
  public void onCreate() {
    super.onCreate();
    // Decompile 방지
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}

    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
      NotificationChannel notificationChannel = new NotificationChannel(Brain.NOTIFICATION_CHANNEL_ID, Brain.NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
      NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
      if (manager != null) {
        manager.createNotificationChannel(notificationChannel);
      }
    }

    // todo 앱마다 다르게
    Notification notification = new NotificationCompat.Builder(this.getApplicationContext(), Brain.NOTIFICATION_CHANNEL_ID)
//      .setSmallIcon(R.drawable.ic_stat_p)
      .setContentTitle("뽀로롯")
      .setContentText("노는 게 제일 좋아. 나랑 같이 놀자!")
//      .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
      .build();

    this.startForeground(this.hashCode(), notification);

//    this.chatBot = ChatBot.create(ChatBot.Type.MM);
    this.chatBot = ChatBot.create(ChatBot.Type.ML);
    this.chatBot.open();
    this.audio = Audio.init(this.getApplicationContext());
  }

  // @DebugLog
  @Override
  public void onDestroy() {
    this.removeAllSubscriptions();
    this.stopForeground(true);
    super.onDestroy();
  }

  // !!! 최초로 서비스 바인딩될 때 1회 실행
  @Override
  public IBinder onBind(Intent intent) {
    return this.binder;
  }

  void setUpSubscriptions() {
    XLog.st(8).d(this.robot);
    this.addSubscription(Brain.SUBSCRIPTION_CHATBOT,
      this.chatBot.answers.observeOn(Schedulers.newThread()).subscribe(this::processBeforeSpeaking));

    this.addSubscription(SUBSCRIPTION_PROBLEMS, Nerve.events.filter(event -> event == Bluetooth.State.DISCONNECTED || event == Bluetooth.State.FAILED || event == Spp.State.FAILED || event == Mouth.State.FAILED || event == Ear.State.FAILED || event == Bluetooth.State.FAILED_TO_TURN_ON_HEADSET)
      .subscribe(event -> {
        XLog.d("[%s] %s", event, this);
        // !!! 로봇 쪽 문제로 끊어진 경우
        if (event == Bluetooth.State.DISCONNECTED) {
//          if (this.mouth == null || (this.mouth != null && !this.mouth.isSinging())) {
          this.robot.changeState(Robot.State.OFF);
          this.deactivate();
//          }
        }
        if (event == Bluetooth.State.FAILED || event == Spp.State.FAILED || event == Mouth.State.FAILED || event == Ear.State.FAILED) {
          // todo 뭘 해야 할까?
          this.robot.changeState(Robot.State.FAILED);
          this.terminate();
        }
        if (event == Bluetooth.State.FAILED_TO_TURN_ON_HEADSET) {
          this.robot.changeState(Robot.State.FAILED);
          this.terminate2();
        }
      }));

    this.addSubscription(Brain.SUBSCRIPTION_STATE_OF_PARTS,
      Nerve.events.filter(event -> event instanceof Bluetooth.State || event instanceof Spp.State || event instanceof Ear.State || event instanceof Mouth.State)
        .observeOn(Schedulers.newThread()).subscribe(
        event -> {
          XLog.d("[%s] %s", event, this);

          if (event == Bluetooth.State.SCANNING) {
            this.robot.changeState(Robot.State.SCANNING);
          }
          if (event == Bluetooth.State.PAIRING) {
            this.robot.changeState(Robot.State.PAIRING);
          }
          if (event == Bluetooth.State.LINKING) {
            this.robot.changeState(Robot.State.LINKING);
          }
          if (event == Bluetooth.State.TURNING_ON_HEADSET) {
            this.robot.changeState(Robot.State.TURNING_ON_HEADSET);
          }
          if (event == Bluetooth.State.READY) {
            this.robot.changeState(Robot.State.LINKED);
            this.activateEar();
            this.activateMouth();
            if (Const.get().hasSpp) {
              this.activateSpp();
            } else {
              this.robot.changeState(Robot.State.SERIAL_PORT_READY);
              Part.SPP.ready = true;
              if (Part.isReady()) {
                this.robot.changeState(Robot.State.READY);
              }
            }

          }
          if (event == Spp.State.READY) {
            this.addSubscription("SPP",
              Nerve.events.filter(evt -> evt instanceof Spp.Event).map(evt -> (Spp.Event)evt).observeOn(Schedulers.newThread()).subscribe(
                evt -> {
                  if (evt == Spp.Event.LED_ON) {
                    this.robot.ledStates.accept(Robot.LedState.ON);
                  }
                  else if (evt == Spp.Event.LED_OFF) {
                    this.robot.ledStates.accept(Robot.LedState.OFF);
                  }
                  else if (evt == Spp.Event.LED_RESET) {
                    this.robot.ledStates.accept(Robot.LedState.RESET);
                  }
                  else if (evt == Spp.Event.FD_SET) {
                    this.robot.fdStates.accept(Boolean.TRUE);
                  }
                  else if (evt == Spp.Event.FD_UNSET) {
                    this.robot.fdStates.accept(Boolean.FALSE);
                  }
                  else if (evt == Spp.Event.SHAKING) {
                    this.robot.accStates.accept(Boolean.TRUE);
                  }
                }));
            this.robot.changeState(Robot.State.SERIAL_PORT_READY);
            Part.SPP.ready = true;
            if (Part.isReady()) {
              this.robot.changeState(Robot.State.READY);
            }
          }
          if (event == Ear.State.READY) {
            this.addSubscription(Brain.SUBSCRIPTION_SIGNALS_FROM_EAR,
              Nerve.signals.filter(signal -> signal instanceof Ear.Signal).map(signal -> (Ear.Signal) signal).observeOn(Schedulers.newThread()).subscribe(
                signal -> {
                  String hearing = signal.poll();
                  XLog.d("[HEAR] %s", hearing);
                  this.lastHearing = hearing;
                  if (hearing != null) {
                    boolean understood = hearing.length() > 0;
                    String chat = understood ? hearing : "!@#$%^&*";
                    ChatMessage msg = new ChatMessage(UUID.randomUUID().toString(), Talker.KID, chat);
                    this.robot.chats.accept(msg);
                    String condensed = chat.replaceAll("\\s+", "");
                    if (Brain.STOP_COMMANDS.contains(condensed)) {
                      this.processBeforeSpeaking("CALM");
                    }
                    else {
                      boolean beQuiet = Env.getBoolean(Key.SETTINGS_BE_QUIET, false);
                      boolean beQuietDuringChat = Env.getBoolean(Key.SETTINGS_BE_QUIET_IN_CHAT, false);
                      boolean isChatting = Env.getBoolean(Key.SETTINGS_IS_CHATTING, false);
                      boolean canITalk = !beQuiet && !(isChatting && beQuietDuringChat);
                      if (canITalk) {
                        this.chatBot.talk(chat);
                      }
                    }
                  }
                }));
            this.robot.changeState(Robot.State.EAR_READY);
            Part.EAR.ready = true;
            if (Part.isReady()) {
              this.robot.changeState(Robot.State.READY);
            }
          }

          if (event == Mouth.State.READY) {
            this.addSubscription(Brain.SUBSCRIPTION_SIGNALS_FROM_MOUTH,
              Nerve.signals.filter(signal -> signal instanceof Mouth.Signal).observeOn(Schedulers.newThread()).subscribe(
                signal -> {
                  XLog.d(signal);
                  if (signal == Mouth.Signal.BEFORE_SINGING || signal == Mouth.Signal.RESUME_SINGING) {
                    this.ear.stop();
                    boolean shutUpWithShaking = Env.getBoolean(Key.SETTINGS_STOP_BY_SHAKING, false);
                    if (shutUpWithShaking) {
                      this.startAcc();
                    }
                    Pauser.pauseAndRun(Audio::turnOffHeadset, 400);
                  }
                  if (signal == Mouth.Signal.AFTER_SINGING || signal == Mouth.Signal.PAUSE_SINGING) {
                    Audio.turnOnHeadset();
                    this.ear.start();
                    this.stopAcc();

                    if (this.singingBySpeak) {
                      Pauser.pauseAndRun(() -> this.singingBySpeak = false, 400);
                    }
                  }

                  if (signal == Mouth.Signal.BEFORE_READING) {
                    boolean shutUpWithShaking = Env.getBoolean(Key.SETTINGS_STOP_BY_SHAKING, false);
                    if (shutUpWithShaking) {
                      this.startAcc();
                    }
                  }
                  if (signal == Mouth.Signal.AFTER_READING) {
                    this.stopAcc();
                  }
                  if (signal == Mouth.Signal.START_VIDEO) {
                    this.ear.stop();
                    Pauser.pauseAndRun(Audio::turnOffHeadset, 400);
                  }
                  if (signal == Mouth.Signal.STOP_VIDEO) {
                    Audio.turnOnHeadset();
                    this.ear.start();
                    this.stopAcc();
                  }
                }));

            this.robot.changeState(Robot.State.MOUTH_READY);
            Part.MOUTH.ready = true;
            if (Part.isReady()) {
              this.robot.changeState(Robot.State.READY);
            }
          }
        }));
  }

  // @DebugLog
  public void start() {
    this.robot = Robot.get();
    XLog.w("[START] %s", this.robot);
    this.setUpSubscriptions();
    this.bluetooth = Bluetooth.init(this.getApplicationContext(), this.robot);
  }

  // @DebugLog
  public void terminate() {
    this.deactivateEar();
    this.deactivateMouth();
    this.deactivateSpp();
    this.deactivateBluetooth();
//    this.robot = null;
  }

  // @DebugLog
  public void terminate2() {
    this.deactivateEar();
    this.deactivateMouth();
    this.deactivateSpp();
    this.deactivateBluetooth2();
//    this.robot = null;
  }

  private void deactivate() {
    this.deactivateEar();
    this.deactivateMouth();
    this.deactivateSpp();
  }

  // @DebugLog
  private void deactivateEar() {
    this.removeSubscription(Brain.SUBSCRIPTION_SIGNALS_FROM_EAR);
    if (this.ear != null) {
      Part.EAR.ready = false;
      this.ear.terminate();
    }
  }

  // @DebugLog
  private void deactivateMouth() {
    this.removeSubscription(Brain.SUBSCRIPTION_SIGNALS_FROM_MOUTH);
    if (this.mouth != null) {
      Part.MOUTH.ready = false;
      this.mouth.terminate();
    }
  }

  // @DebugLog
  private void deactivateSpp() {
    if (this.spp != null) {
      Part.SPP.ready = false;
      this.spp.terminate();
    }
  }

  // @DebugLog
  private void deactivateBluetooth() {
    this.removeSubscription(Brain.SUBSCRIPTION_STATE_OF_PARTS);
    if (this.bluetooth != null) {
      this.bluetooth.terminate();
      this.bluetooth = null;
    }
  }

  // @DebugLog
  private void deactivateBluetooth2() {
    this.removeSubscription(Brain.SUBSCRIPTION_STATE_OF_PARTS);
    if (this.bluetooth != null) {
      this.bluetooth.terminate2();
      this.bluetooth = null;
    }
  }

  // @DebugLog
  private void activateEar() {
    this.ear = Ear.create(this.getApplicationContext());
    this.ear.start();
  }

  // @DebugLog
  private void activateMouth() {
    this.mouth = Mouth.init(this.getApplicationContext());
    this.mouth.start();
  }

  // @DebugLog
  private void activateSpp() {
    this.spp = Spp.init();
    this.spp.start(this.bluetooth.getDevice());
  }

  private void addSubscription(String name, Disposable subscription) {
    Subscriptions.add(this.hashCode(), name, subscription);
  }

  private void removeSubscription(String name) {
    Subscriptions.remove(this.hashCode(), name);
  }

  private void removeAllSubscriptions() {
    Subscriptions.removeAllOf(this.hashCode());
  }

  private void processBeforeSpeaking(final String speaking) {
    XLog.d("[SPEAK] %s", speaking);

    Matcher matcher = Env.COMMAND_PATTERN.matcher(speaking);
    if (matcher.matches()) {
      String command = matcher.group(1);
      String key = matcher.group(2);
      String title = matcher.group(3);

      if ("SONG".equals(command)) {
        this.speak(String.format("%s 들려 줄게.", title), Talker.POROROT);
        if (KEY_RANDOM.equals(key)) {
          key = this.makeRandomContentsName(32);
        }
        this.singingBySpeak = true;
        this.sing(key, title);
      }
      else if ("BOOK".equals(command)) {
        this.speak(String.format("%s 읽어 줄게.", title), Talker.POROROT);
        if (KEY_RANDOM.equals(key)) {
          key = this.makeRandomContentsName(100);
        }
        this.read(key, title);
      }
      else if ("YOUTUBE".equals(command)) {
        String query = this.lastHearing.replaceAll(" ", "").replace(title, "");
        XLog.d(query);

        if (query.isEmpty()) {
          this.speak("미안 뭐라고? 다시 한 번 말해줘.", Talker.POROROT);
          return;
        }

        this.speak(String.format("%s 보여 줄게.", query), Talker.POROROT);
        this.videoPlayByQuery(query);
      }
      else if (TIME_COMMANDS.contains(command)) {
        this.answerTime(command);
      }
    }
    else if ("CALM".equals(speaking)) {
      this.mouth.stopSpeaking();
    }
    else {
      this.speak(speaking, Talker.POROROT);
    }
  }

  private String makeRandomContentsName(int max) {
    Random r = new Random(System.currentTimeMillis());
    String s = String.valueOf(r.nextInt(max) + 1);
    return String.format("%1$3s", s).replace(' ', '0');
  }

  // todo 국제화
  private void answerTime(final String command) {
    Calendar now = Calendar.getInstance();
    String speaking = "";
    if ("TIME".equals(command)) {
      int hour = now.get(Calendar.HOUR);
      speaking = String.format(Locale.KOREAN, "지금은 %s %d시 %d분이야.",
        now.getDisplayName(Calendar.AM_PM, Calendar.LONG, Locale.KOREAN),
        hour == 0 ? 12 : hour,
        now.get(Calendar.MINUTE));
    }
    else if ("DATE".equals(command)) {
      speaking = String.format(Locale.KOREAN, "오늘은 %d년 %d월 %d일 %s이야.",
        now.get(Calendar.YEAR),
        now.get(Calendar.MONTH) + 1,
        now.get(Calendar.DATE),
        now.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.KOREAN));
    }
    else if ("DAYS".equals(command)) {
      speaking = String.format(Locale.KOREAN, "오늘은 %s이야.",
        now.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.KOREAN));
    }

    XLog.d(speaking);
    this.speak(speaking, Talker.POROROT);
  }

  // @DebugLog
  public void speak(String speaking, Talker talker) {
    // !!! todo 사용자 사진으로 바꿔야 한다.
    ChatMessage msg = new ChatMessage(UUID.randomUUID().toString(), talker, speaking);
    this.robot.chats.accept(msg);

    // !!! todo 어떻게 예외처리를 해야할까?
    if (this.mouth != null) {
      this.mouth.speak(speaking);
    }
  }

  public void turnOnRedLed(final int interval) {
    if (Const.get().hasSpp) this.spp.turnOnLed(Spp.LedColor.RED, interval);
  }

  public void turnOnBlueLed(final int interval) {
    if (Const.get().hasSpp) this.spp.turnOnLed(Spp.LedColor.BLUE, interval);
  }

  public void turnOnBothLed(final int interval) {
    if (Const.get().hasSpp) this.spp.turnOnLed(Spp.LedColor.RED_AND_BLUE, interval);
  }

  public void turnOffLed() {
    if (Const.get().hasSpp) this.spp.turnOffLed();
  }

  public void resetLed() {
    if (Const.get().hasSpp) this.spp.resetLed();
  }

  public void unsetFactoryDefault() {
    if (Const.get().hasSpp) this.spp.unsetFactoryDefault();
  }

  public void setFactoryDefault() {
    if (Const.get().hasSpp) this.spp.setFactoryDefault();
  }

  public void retrieveFactoryDefaultState() {
    if (Const.get().hasSpp) this.spp.retrieveFactoryDefaultState();
  }

  // @DebugLog
  public void startAcc() {
    if (Const.get().hasSpp) this.spp.startAcc();
  }

  // @DebugLog
  public void stopAcc() {
    if (Const.get().hasSpp) this.spp.stopAcc();
  }

  @Override
  public String toString() {
    return String.format("{Brain: {audio: %s, chatBot: %s, robot: %s, bluetooth: %s, mouth: %s, ear: %s, spp: %s}}",
      this.audio,
      this.chatBot,
      this.robot,
      this.bluetooth,
      this.mouth,
      this.ear,
      this.spp);
  }

  public void sing(String code, String title) {
    Song.findByCode(code)
      .subscribe(song -> {
        this.mouth.sing(song.getUrl(), title);
        ContentManager.get().setNowPlaySong(song);
      }, error -> {
        stopSinging();
        this.speak("무언가 문제가 생긴거같아", Talker.POROROT);
      });
  }

  public void stopSinging() {
    this.mouth.stopPlaying();
    ContentManager.get().setNowPlaySong(null);
  }

  public void pauseSinging() {
    this.mouth.pausePlaying();
  }

  public void resumeSinging() {
    this.mouth.resumePlaying();
  }

  public boolean isSinging() {
    return this.mouth.isSinging();
  }

  public SimpleExoPlayer getMusicPlayer() {
    return this.mouth.getMusicPlayer();
  }

  public boolean isSingingBySpeak() {
    return singingBySpeak;
  }

  public void read(String code, String title) {
    Book.findByCode(code)
      .subscribe(book -> {
        this.mouth.read(book.getContents(), title);
        ContentManager.get().setNowReadBook(book);
      }, error -> {
        this.speak("무언가 문제가 생긴거같아", Talker.POROROT);
      });
  }

  public void stopReading() {
    this.mouth.stopSpeakingWithoutSpeak();
    ContentManager.get().setNowReadBook(null);
  }

  public boolean isReading() {
    return this.mouth.isReading();
  }

  public void videoPlayByQuery(String query) {
    YouTube.search(query)
        .subscribe(youTubes -> {
          YouTube youTube = youTubes.get(0);
          this.mouth.playVideo(youTube.getId().getVideoId());
        }, error -> {
          this.speak("무언가 문제가 생긴거같아", Talker.POROROT);
        });
  }

  public void videoPlayById(String id) {
    this.mouth.playVideo(id);
  }

  public void videoStop() {
    this.mouth.stopVideo();
  }

  // todo Organ 제어
  public void changeFromRobotToSpeaker() {
    Audio.turnOffHeadset();
  }

  // todo Organ 제어
  public void changeFromSpeakerToRobot() {
    Audio.turnOnHeadset();
  }

  public void openEar() {
    if (this.ear != null) {
      this.ear.start();
    }
  }

  public void closeEar() {
    if (this.ear != null) {
      this.ear.stop();
    }
  }
}
