package com.jininsa.robot.connector;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Sets;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.app.ManageableBroadcastReceiver;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Scanner extends ManageableBroadcastReceiver {

  public enum Option {
    SCAN_ALL, SCAN_FIRST, SCAN_CONNECTED
  }

  public static class Result {
    public enum State {
      FOUND_CONNECTED_DEVICE,
      FOUND_NEW_DEVICE,
      FINISHED
    }

    public static final Result FINISHED = new Result(State.FINISHED);
    public final State state;
    public final int rssi;
    public final BluetoothDevice device;

    private Result(final State state) {
      this(state, Short.MAX_VALUE, null);
    }

    private Result(final State state, final int rssi, final BluetoothDevice device) {
      this.device = device;
      this.state = state;
      this.rssi = rssi;
    }

    @Override
    public String toString() {
      return String.format(
        "{Result: {state: %s, rssi: %s, device: %s}}",
        this.state,
        this.rssi,
        this.device);
    }
  }

  public final PublishRelay<Result> result = PublishRelay.create();

  private final Predicate<BluetoothDevice> filter;
  private final Set<BluetoothDevice> devices;
  private final Adapter adapter;
  private final Option option;
  private boolean isWorking;

  public Scanner(final Context context, final Predicate<BluetoothDevice> filter, Option option) {
    super(context);
    this.adapter = Adapter.get();
    this.filter = filter;
    this.option = option;
    this.devices = Sets.newHashSet();
  }

  public Set<BluetoothDevice> getFoundDevices() {
    return Collections.unmodifiableSet(this.devices);
  }

  public boolean isFound() {
    return !this.devices.isEmpty();
  }

  public boolean isWorking() {
    return this.isWorking;
  }

  // @DebugLog
  public void start() {
    this.isWorking = true;
    this.devices.clear();
    if (!this.adapter.isDiscovering()) {
      XLog.w("[start] %s", this);
      this.scanConnectedDevices();
    }
  }

  // @DebugLog
  private void scanConnectedDevices() {
    this.adapter.getHeadsetProfileProxy(new BluetoothProfile.ServiceListener() {
      @Override
      public void onServiceConnected(final int type, final BluetoothProfile profile) {
        List<BluetoothDevice> devices = profile.getConnectedDevices();
        Stream<BluetoothDevice> filteredDevices = Stream.of(devices).filter(Scanner.this.filter);

        if (Scanner.this.option == Option.SCAN_CONNECTED) {
          filteredDevices.findFirst().ifPresentOrElse(
            device -> {
              Scanner.this.publish(Result.State.FOUND_CONNECTED_DEVICE, Short.MIN_VALUE, device);
              Scanner.this.finish();
            },
            Scanner.this::finish);
        }

        if (Scanner.this.option == Option.SCAN_ALL) {
          filteredDevices.findFirst().ifPresent(device -> Scanner.this.publish(Result.State.FOUND_CONNECTED_DEVICE, Short.MIN_VALUE, device));
          // !!! Bluetooth#getBondedDevices()는 페어링했던 디바이스 기록을 반환한다. 따라서, 주변에 해당 디바이스가 없어도 목록에 포함된다.
          // !!! Bluetooth#getBondedDevices()이 반환하는 디바이스가 주변에 있다면 Bluetooth#startDescovery()나 Bluetooth#getHeadsetProfileProxy()에 걸리게 되어있다.
          // !!! 따라서, scanPairedDevices()가 아니라 discover()를 한다.
          Scanner.this.discover();
        }

        if (Scanner.this.option == Option.SCAN_FIRST) {
          filteredDevices.findFirst().ifPresentOrElse(
            device -> {
              Scanner.this.publish(Result.State.FOUND_CONNECTED_DEVICE, Short.MIN_VALUE, device);
              Scanner.this.finish();
            },
            Scanner.this::discover);
        }
      }

      @Override
      public void onServiceDisconnected(final int type) {
        XLog.w(Scanner.this);
      }
    });
  }

  // @DebugLog
  public void stop() {
    // !!! cancelDiscovery()는 현재 진행 중인 탐색(discovery)를 그냥 멈춘다. FOUND, FINISHED 이벤트가 발생하지 않는다.
    this.isWorking = false;
    this.adapter.cancelDiscovery();
  }

  // @DebugLog
  private void discover() {
    // !!! 현재 검색하지 않을 때(isDiscovering() == false 일 때)만 STARTED 이벤트가 발생한다.
    this.adapter.startDiscovery();
  }

  // @DebugLog
  private void publish(final Result.State state, final int rssi, final BluetoothDevice device) {
    XLog.d("[%s] %s + %s", state, this.devices, device);
    if (!this.devices.contains(device)) {
      this.devices.add(device);
      this.result.accept(new Result(state, rssi, device));
    }
  }

  // @DebugLog
  private void finish() {
    this.stop();
    this.result.accept(Result.FINISHED);
  }

  @Override
  protected IntentFilter intentFilter() {
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
    intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
    intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
    return intentFilter;
  }

  @Override
  public void onReceive(final Context context, final Intent intent) {
    final String action = intent.getAction();
    final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

    final int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);

    if (this.isWorking) {
      if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
        XLog.d("[%s] %s", action, this.toString());
      }

      if (BluetoothDevice.ACTION_FOUND.equals(action)) {
        boolean appliedResult = this.filter.test(device);
        if (appliedResult) {
          XLog.d("[%s] %s + %s(%ddB)", action, this.devices, device, rssi);
          this.publish(Result.State.FOUND_NEW_DEVICE, rssi, device);
          if (this.option == Option.SCAN_FIRST) {
            this.finish();
          }
        }
      }

      if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
        XLog.d("[%s] %s", action, this.toString());
        this.finish();
      }
    }
  }

  @SuppressLint("DefaultLocale")
  @Override
  public String toString() {
    return String.format(
      "{Scanner@%d: {devices: %s, option: %s, isWorking:%s, isDiscovering: %s}}",
      this.hashCode(),
      this.devices,
      this.option,
      this.isWorking,
      this.adapter.isDiscovering());
  }
}
