package com.jininsa.robot.connector;

import com.elvishew.xlog.XLog;
import com.koushikdutta.async.AsyncNetworkSocket;
import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.ByteBufferList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;


public class Stt {

  public static class Credential {
    private static String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private final String user;
    private final String lang;
    private final String key;
    private final String gender;
    private final String country;
    private final String age;
    private final String ver;
    private final String path;
    private final String salt;
    private final JSONObject auth;

    public Credential(final String user, final String lang, final String key, final String gender, final String country, final String age, final String ver) {

      this.user = user;
      this.lang = lang;
      this.key = key;
      this.gender = gender;
      this.country = country;
      this.age = age;
      this.ver = ver;
      this.path = "False";

      StringBuilder temp = new StringBuilder();
      Random rnd = new Random();
      while (temp.length() < 18) {
        int index = (int) (rnd.nextFloat() * SALTCHARS.length());
        temp.append(SALTCHARS.charAt(index));
      }
      this.salt = temp.toString();
      this.auth = new JSONObject();
      try {
        this.auth.put("SESSID", this.salt);
        this.auth.put("USER", this.user);
        this.auth.put("LANG", this.lang);
        this.auth.put("KEY", this.key);
        this.auth.put("GENDER", this.gender);
        this.auth.put("AGE", this.age);
        this.auth.put("VER", this.ver);
        this.auth.put("PATH", this.path);
        this.auth.put("COUNTRY", this.country);
      }
      catch (JSONException e) {
        XLog.e(e);
      }
    }
  }

  private static final String HOST = "45.55.113.61";
  private static final int PORT = 6003;

  private AsyncNetworkSocket socket;
  private final Credential credential;

//  private final Socket socket;
//  private final DataOutputStream dOut;
//  private final PrintWriter pw;

//  private final OutputStream outputStream ;
//  private final KidsenseSpeech kidsenseSpeech;

  private static Stt stt;

  public static Stt create(Credential credential) {
    if (stt == null) {
      stt = new Stt(credential);
    }
    return stt;
  }

  private Stt(Credential credential) {
    this.credential = credential;
    AsyncServer.getDefault().connectSocket(HOST, PORT, (e, socket) -> {
      if (e != null) {
        XLog.e(e);
        throw new IllegalStateException(e);
      }
      this.socket = (AsyncNetworkSocket)socket;

      this.send(this.credential.auth.toString().getBytes());

      this.socket.setDataCallback((emitter, bb) -> XLog.d(bb.readString()));
    });
  }

  public void send(byte[] voice) {
    this.socket.write(new ByteBufferList(voice));
  }

//    this.socket = new Socket(HOST, PORT);
//    this.socket.setKeepAlive(false);
//    outputStream = this.socket.getOutputStream();
//    dOut = new DataOutputStream(outputStream);
//    pw = new PrintWriter(socket.getOutputStream(), true);
//    pw.println(auth.toString());
//    pw.flush();
//    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//    final String response = in.readLine();
//    kidsenseSpeech.onStatus(response);
//    Log.e(TAG,response);
//    Encapsulator encapsulator = new Encapsulator();
//    encapsulator.setSocket(socket);
//    encapsulator.setDataOutputStream(dOut);

// public Encapsulator initServer() {
//    OpusRecorder.isInit = Boolean.TRUE;
//    if (socket != null){
//      try {
//        Log.e(TAG,"Closing existing socket");
//        socket.close();
//        socket = null;
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
//    if (dOut != null){
//      try {
//        Log.e(TAG,"Closing accStates output stream");
//        dOut.close();
//        dOut = null;
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
//    if (outputStream != null){
//      try {
//        Log.e(TAG,"Closing output stream");
//        outputStream.close();
//        outputStream = null;
//      } catch (IOException e) {
//        e.printStackTrace();
//      }
//    }
//    try {
//      socket = new Socket(HOST, PORT);
//      socket.setKeepAlive(false);
//      outputStream = socket.getOutputStream();
//      dOut = new DataOutputStream(outputStream);
//      pw = new PrintWriter(socket.getOutputStream(), true);
//      pw.println(auth.toString());
//      pw.flush();
//      BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//      final String response = in.readLine();
//      kidsenseSpeech.onStatus(response);
//      Log.e(TAG,response);
//      Encapsulator encapsulator = new Encapsulator();
//      encapsulator.setSocket(socket);
//      encapsulator.setDataOutputStream(dOut);
//      return encapsulator;
//    } catch (JSONException e) {
//      e.printStackTrace();
//    } catch (UnknownHostException e) {
//      e.printStackTrace();
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
//    return null;
//  }
}
