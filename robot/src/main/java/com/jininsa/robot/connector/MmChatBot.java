package com.jininsa.robot.connector;

import android.support.annotation.NonNull;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jininsa.robot.app.Const;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public class MmChatBot extends ChatBot {
  interface Service {
    @POST("v1/{chatBotId}")
    Call<Answer> talk(@Path("chatBotId") String chatBotId, @Body Question question);
  }

  static private class Answer {
    @SerializedName("context")
    @Expose
    private Context context;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("output")
    @Expose
    private Output output;
  }

  static private class Question {
    static final private Question singleton = new Question();

    static {
      Question.singleton.storyId = Const.get().chatBotId;
      Question.singleton.context = Context.singleton;
      Question.singleton.input = Input.singleton;
    }

    @SerializedName("story_id")
    @Expose
    private String storyId;
    @SerializedName("context")
    @Expose
    private Context context;
    @SerializedName("input")
    @Expose
    private Input input;
  }

  static private class Context {
    static final private Context singleton = new Context();

    static {
      Context.singleton.information = Information.singleton;
      Context.singleton.conversationId = UUID.randomUUID().toString();
      Context.singleton.visitCounter = 0;
      Context.singleton.random = false;
      Context.singleton.reprompt = false;
    }

    @SerializedName("conversation_id")
    @Expose
    private String conversationId;
    @SerializedName("information")
    @Expose
    private Information information;
    @SerializedName("visit_counter")
    @Expose
    private Integer visitCounter;
    @SerializedName("random")
    @Expose
    private Boolean random;
    @SerializedName("reprompt")
    @Expose
    private Boolean reprompt;
  }

  static private class Information {
    static final private Information singleton = new Information();

    static {
      Information.singleton.conversationStacks = new ArrayList<>();
      Information.singleton.conversationStacks.add(ConversationStack.singleton);
      Information.singleton.conversationCounter = 0;
      Information.singleton.userRequestCount = 0;
    }

    @SerializedName("conversation_stack")
    @Expose
    private List<ConversationStack> conversationStacks;
    @SerializedName("conversation_counter")
    @Expose
    private Integer conversationCounter;
    @SerializedName("user_request_counter")
    @Expose
    private Integer userRequestCount;
  }

  static private class ConversationStack {
    static final private ConversationStack singleton = new ConversationStack();

    static {
      ConversationStack.singleton.conversationNode = "root";
      ConversationStack.singleton.conversationNodeName = "ROOT";
    }

    @SerializedName("conversation_node")
    @Expose
    private String conversationNode;
    @SerializedName("conversation_node_name")
    @Expose
    private String conversationNodeName;
  }

  static private class Input {
    static private Input singleton = new Input();
    @SerializedName("text")
    @Expose
    private String text = "";
  }

  static private class Output {
    @SerializedName("text")
    @Expose
    private List<String> text = null;
    @SerializedName("visit_nodes_text")
    @Expose
    private List<String> visitNodesText = null;
    @SerializedName("visit_nodes")
    @Expose
    private List<String> visitNodes = null;
    @SerializedName("visit_nodes_name")
    @Expose
    private List<String> visitNodesName = null;
  }

  private static final MmChatBot singleton = new MmChatBot();

  static MmChatBot getInstance() {
    return singleton;
  }

  private final Service service;

  private MmChatBot() {
    this.service = chatBotServer.create(Service.class);
    //this.open();
  }

  public void open() {
    final Call<Answer> call = this.service.talk(Question.singleton.storyId, Question.singleton);
    call.enqueue(new Callback<Answer>() {
      @Override
      public void onResponse(@NonNull final Call<Answer> call, @NonNull final Response<Answer> response) {
        if (response.isSuccessful()) {
          Answer answer = response.body();
          if (answer != null) {
            Question.singleton.context = answer.context;
          }
        }
      }

      // todo ChatServer Down
      @Override
      public void onFailure(@NonNull final Call<Answer> call, @NonNull final Throwable t) {
        XLog.e(t.getMessage(), t);
      }
    });
  }

  public void talk(String text) {
    Question.singleton.input.text = text;
    final Call<Answer> call = this.service.talk(Question.singleton.storyId, Question.singleton);
    call.enqueue(new Callback<Answer>() {
      @Override
      public void onResponse(@NonNull final Call<Answer> call, @NonNull final Response<Answer> response) {
        if (response.isSuccessful()) {
          Answer result = response.body();

          if (result != null) {
            Question.singleton.context = result.context;
            List<String> texts = result.output.text;
            Stream.of(texts).forEach(MmChatBot.this.answers::accept);
          }
        }
      }

      // todo ChatServer Down
      @Override
      public void onFailure(@NonNull final Call<Answer> call, @NonNull final Throwable t) {
        XLog.e(t.getMessage(), t);
      }
    });
  }
}
