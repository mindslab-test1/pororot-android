package com.jininsa.robot.connector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Handler;
import android.os.HandlerThread;

import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.app.Const;
import com.jininsa.robot.app.Key;
import com.jininsa.robot.helper.Assets;
import com.jininsa.robot.helper.Env;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import kr.co.voiceware.comm.IVTDefine;
import kr.co.voiceware.comm.MessageTypes;
import kr.co.voiceware.common.VwException;
import kr.co.voiceware.voicetext.CVoiceText;

public class Tts {

  public enum State {READY, FAILED}

  static final private int SPEAKER_ID = 181;
//  static final private int SPEAKER_ID = -1;
  private static Tts singleton;

  public static Tts init(Context context) {
    if (Tts.singleton == null) {
      Tts.singleton = new Tts(context.getApplicationContext());
    }
    return Tts.singleton;
  }

  public final PublishRelay<State> state = PublishRelay.create();
  private final CVoiceText tts;
  private final PublishRelay<Boolean> starter = PublishRelay.create();
  private int tries = 0;

  @SuppressLint("CheckResult")
  private Tts(Context context) {
    Const c = Const.get();
    if (!c.ttsDbFile.exists()) {
      c.makeDirectories();
      AssetManager assetManager = context.getAssets();
      Assets.copy(assetManager, String.format("tts/%s", c.ttsDbFileName), c.ttsDbFile);
    }

    //noinspection ResultOfMethodCallIgnored
    this.starter.subscribe(__ -> this.startInternal());

    HandlerThread ht = new HandlerThread("ttsEngine");
    ht.start();
    final Handler ttsHandler = new Handler(ht.getLooper(),
      (msg) -> {
        switch (msg.what) {
          case MessageTypes.MESSAGE_LICENSE_DOWNLOAD_START:
            XLog.i("MESSAGE_LICENSE_DOWNLOAD_START");
            break;
          case MessageTypes.MESSAGE_LICENSE_DOWNLOAD_END:
            XLog.i("MESSAGE_LICENSE_DOWNLOAD_END");
            this.startInternal();
            break;
          case MessageTypes.MESSAGE_LICENSE_ERROR:
            XLog.w("MESSAGE_LICENSE_ERROR");
            break;
        }
        return true;
      });
    this.tts = new CVoiceText(context.getApplicationContext(), ttsHandler);
  }

  // !!! 비동기 호출
  public void start() {
    this.tries = 0;
    this.starter.accept(Boolean.TRUE);
  }

  private void startInternal() {
    // Decompile 방지
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) { request.close(); } catch (IOException ignored) {}

    Const c = Const.get();
    XLog.d("%s %s", c.ttsDbFileString, c.ttsKey);
    if (this.tries < 3) {
      this.tries++;
      boolean existLicense = this.tts.vtIsLicenseExist(c.ttsPathString);  // !!! 파일 존재만 확인하면 라이센스 확인이되지 않는다.
      if (!existLicense) {
        this.tts.vtLicenseDownload(c.ttsKey, c.ttsPathString);
      }
      else {
        this.loadTtsEngine();
      }
    }
    else {
      this.state.accept(State.FAILED);
    }
  }

  private void loadTtsEngine() {
    try {
      int result = this.tts.vtLOADTTSEXT(Const.get().ttsPathString, Tts.SPEAKER_ID);
      XLog.v("Load Tts Engine : result=%d", result);
      if (result != 0) {
        // !!! 라이센스 존재 여부는 확인되었지만 알 수 없는 이유로 라이브러리 로드에 실패하면
        //noinspection ResultOfMethodCallIgnored
        Const.get().ttsLicenseFile.delete();
        this.startInternal();
      }
      else {
        this.state.accept(State.READY);
      }
    }
    catch (VwException e) {
      XLog.e(e.getMessage(), e);
      this.state.accept(State.FAILED);
    }
  }

  public byte[] speak(String text) {
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      int flag = 0;

      int pitch = Env.getInt(Key.SETTINGS_PITCH, 0) + 100;
      while (true) {
        byte[] pcmBuffer = this.tts.vtTextToBuffer(Tts.SPEAKER_ID, IVTDefine.VT_BUFFER_API_FMT_S16PCM, text, flag, pitch, -1, -1, 200, -1);
        int result = Tts.this.tts.vtTextToBufferRTN(Tts.SPEAKER_ID);

        while (result == IVTDefine.VT_BUFFER_API_ERROR_THREAD_BUSY) {
          // flag == 2 면 현재 음성합성 쓰레드 강제 종료
          this.tts.vtTextToBuffer(Tts.SPEAKER_ID, IVTDefine.VT_BUFFER_API_FMT_S16PCM, text, 2, pitch, -1, -1, -1, -1);
          pcmBuffer = this.tts.vtTextToBuffer(Tts.SPEAKER_ID, IVTDefine.VT_BUFFER_API_FMT_S16PCM, text, flag, pitch, -1, -1, 200, -1);
          result = Tts.this.tts.vtTextToBufferRTN(Tts.SPEAKER_ID);
        }

        flag = 1;
        baos.write(pcmBuffer, 0, pcmBuffer.length);

        if (result == IVTDefine.VT_BUFFER_API_DONE && pcmBuffer.length != IVTDefine.BUFFER_SIZE_FOR_HZ) {
          break;
        }
      }

      return baos.toByteArray();
    }
    catch (VwException e) {
      XLog.e(e.getMessage(), e);
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return Env.EMPTY_BYTE_ARRAY;
  }
}
