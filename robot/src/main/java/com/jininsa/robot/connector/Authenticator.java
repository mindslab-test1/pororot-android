package com.jininsa.robot.connector;

import android.app.Activity;

import com.elvishew.xlog.XLog;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Subscriptions;
import com.jininsa.robot.model.party.Person;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

abstract public class Authenticator {

  public enum Provider {
    FACEBOOK("페이스북"), PHONE("전화번호"), KAKAO("카카오톡"), NAVER("네이버"), EMAIL("이메일");
    final public String desc;

    Provider(final String desc) {
      this.desc = desc;
    }
  }

  public enum Action {
    SIGN_IN("{t800-user-check}", "로그인"), SIGN_UP("{t800-user-plus}", "사용자등록");
    final public String desc;
    final public String icon;

    Action(final String icon, final String desc) {
      this.desc = desc;
      this.icon = icon;
    }
  }

  public interface AuthenticationService {
    @POST("auth/signin/{provider}")
    Observable<AuthToken> signInWithOAuthProvider(@Path("provider") String provider, @Body HashMap requestBody);

    @POST("auth/signin/firebase")
    Observable<AuthToken> signInWithPhone(@Body Map<String, String> request);

    @POST("auth/signup/firebase")
    Observable<AuthToken> signUpWithPhone(@Body Map<String, String> request);

    @POST("auth/signin")
    Observable<AuthToken> singInWithEmail(@Body Map<String, String> request);

    @POST("auth/signup")
    Observable<AuthToken> signUpWithEmail(@Body Map<String, String> request);

    @POST("auth/signout")
    Observable<Void> signOut(@Header("X-Auth-Token") String token);
  }

  public interface AuthChannel {
    Activity getActivity();
    void authenticate();
    void onAuthenticated();
    void onAuthenticationFailed(Throwable error);
  }

  static public class AuthToken {
    public static AuthToken create(final String token, final long expiredTime) {
      return new AuthToken(token, expiredTime);
    }
    @SerializedName("token")
    @Expose
    public final String token;
    @SerializedName("expiredTime")
    @Expose
    public final long expiredTime;

    // !!! For Retrofit
    private AuthToken() {
      this(Env.DEFAULT_STRING, Env.DEFAULT_LONG);
    }

    private AuthToken(String token, long expiredTime) {
      this.token = token;
      this.expiredTime = expiredTime;
    }

    /**
     * public Long getIdFromToken() {
     * try {
     * Jws<Claims> jws = Jwts.parser().setSigningKey("DB4AEF4719809709E560ED8DE2F9C77B886B963B28BA20E9A8A621BBD4ABA599".getBytes("UTF-8")).parseClaimsJws(this.token);
     * return Long.valueOf(jws.getBody().get("id").toString());
     * }
     * catch (UnsupportedEncodingException e) {
     * e.printStackTrace();
     * }
     * return Env.DEFAULT_ID;
     * }
     **/

    @Override
    public String toString() {
      return String.format(
        "{AuthToken: {token: %s, expiredTime: %s}}",
        this.token,
        this.expiredTime);
    }
  }

  static public class Email extends Authenticator {
    static private Email instance;

    private Email(AuthChannel channel) {
      super(channel);
    }

    @Override
    public void signUp(final Map<String, String> request) {
      this.addSubscription(SUBSCRIPTION_AUTHENTICATION, Authenticator.service.signUpWithEmail(request).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
        this::doAfterAuthenticationSucceeded, this::doAfterAuthenticationFailed));
    }

    @Override
    public void signIn(final Map<String, String> request) {
      this.addSubscription(SUBSCRIPTION_AUTHENTICATION, Authenticator.service.singInWithEmail(request).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
        this::doAfterAuthenticationSucceeded, this::doAfterAuthenticationFailed));
    }
  }

  static public class Firebase extends Authenticator {
    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authListener;

    private Firebase(AuthChannel channel) {
      super(channel);
      if (this.auth == null) {
        this.auth = FirebaseAuth.getInstance();
        this.authListener = result -> {
          FirebaseUser user = result.getCurrentUser();
          if (user != null) {
            XLog.d("[SUCCESSFUL] %s", user.getUid());
          }
          else {
            XLog.d("[FAILED]");
          }
        };
        this.auth.addAuthStateListener(this.authListener);
      }
    }

    private Observable<PhoneAuthCredential> verifyCode(String verificationId, String verificationCode) {
      return Observable.create(__ -> {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, verificationCode);
        __.onNext(credential);
        __.onComplete();
      });
    }

    @Override
    public void signUp(final Map<String, String> request) {
      final String verificationId = request.get("verificationId");
      final String verificationCode = request.get("verificationCode");
      request.remove("verificationId");
      request.remove("verificationCode");

      Observable<AuthToken> signUpWithPhone =
        this.verifyCode(verificationId, verificationCode)
          .flatMap(this::getUser)
          .flatMap(this::getUserIdToken)
          .flatMap(token -> {
            request.put("token", token); // !!! FirebaseToken
            return Authenticator.service.signUpWithPhone(request).subscribeOn(Schedulers.io());
          });

      this.addSubscription(SUBSCRIPTION_AUTHENTICATION, signUpWithPhone.observeOn(AndroidSchedulers.mainThread()).subscribe(
        this::doAfterAuthenticationSucceeded, this::doAfterAuthenticationFailed));
    }

    private Observable<FirebaseUser> getUser(PhoneAuthCredential credential) {
      Activity activity = Objects.requireNonNull(this.channel.get().getActivity());
      return Observable.create(__ -> this.auth.signInWithCredential(credential)
        .addOnCompleteListener(activity,
          task -> {
            if (task.isSuccessful()) {
              FirebaseUser user = task.getResult().getUser();
              __.onNext(user);
              __.onComplete();
            }
            else {
              __.onError(task.getException());
              __.onComplete();
            }
          }));
    }

    @Override
    public void signIn(final Map<String, String> request) {

      final String verificationId = request.get("verificationId");
      final String verificationCode = request.get("verificationCode");
      request.remove("verificationId");
      request.remove("verificationCode");

      Observable<AuthToken> signInWithPhone =
        this.verifyCode(verificationId, verificationCode)
          .flatMap(this::getUser)
          .flatMap(this::getUserIdToken)
          .flatMap(token -> {
            request.put("token", token); // !!! FirebaseToken
            return Authenticator.service.signInWithPhone(request).subscribeOn(Schedulers.io());
          });

      this.addSubscription(SUBSCRIPTION_AUTHENTICATION, signInWithPhone.observeOn(AndroidSchedulers.mainThread()).subscribe(
        this::doAfterAuthenticationSucceeded, this::doAfterAuthenticationFailed));
    }

    private Observable<String> getUserIdToken(FirebaseUser user) {
      return Observable.create(__ -> user.getIdToken(true)
        .addOnCompleteListener(task -> {
          if (task.isSuccessful()) {
            String idToken = task.getResult().getToken();
            __.onNext(idToken);
            __.onComplete();
          }
          else {
            __.onError(task.getException());
            __.onComplete();
          }
        }));
    }

    public Observable<String> requestVerificationCode(String phoneNumber) {
      Activity activity = Objects.requireNonNull(this.channel.get().getActivity());

      return Observable.create(__ -> PhoneAuthProvider.getInstance(this.auth).verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, activity,
        new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
          @Override
          public void onVerificationCompleted(PhoneAuthCredential credential) {
            XLog.d(credential);
            XLog.d(credential.getSmsCode());
          }

          @Override
          public void onVerificationFailed(FirebaseException e) {
            __.onError(e);
            __.onComplete();
          }

          @Override
          public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
            __.onNext(verificationId);
            __.onComplete();
          }
        }
      ));
    }
  }

  static final AuthenticationService service = Biz.connector().create(AuthenticationService.class);
  static final private String SUBSCRIPTION_AUTHENTICATION = "K7yYGYqpmIcVDvHG";

  public static Authenticator create(final Provider provider, AuthChannel channel) {
    Authenticator authenticator = null;
    if (provider == Provider.PHONE) {
      authenticator = new Firebase(channel);
    }
    else if (provider == Provider.EMAIL) {
      authenticator = new Email(channel);
    }
    return authenticator;
  }

  public static Observable<Void> signOut() {
    AuthToken authToken = Person.get().getAuthToken();
    return service.signOut(authToken.token).subscribeOn(Schedulers.io());
  }
  final WeakReference<AuthChannel> channel;

  Authenticator(AuthChannel channel) {
    this.channel = new WeakReference<>(channel);
  }

  public abstract void signUp(final Map<String, String> params);

  public abstract void signIn(final Map<String, String> params);

  void doAfterAuthenticationSucceeded(final AuthToken token) {
    final Person me = Person.get();
    me.setAuthToken(token);
    this.channel.get().onAuthenticated();
    this.removeSubscription(SUBSCRIPTION_AUTHENTICATION);
  }

  void doAfterAuthenticationFailed(Throwable error) {
    XLog.e(error.getMessage(), error);
    this.channel.get().onAuthenticationFailed(error);
    this.removeSubscription(SUBSCRIPTION_AUTHENTICATION);
  }

  void addSubscription(String name, Disposable subscription) {
    Subscriptions.add(this.hashCode(), name, subscription);
  }

  private void removeSubscription(String name) {
    Subscriptions.remove(this.hashCode(), name);
  }
}
