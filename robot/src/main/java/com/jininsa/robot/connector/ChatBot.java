package com.jininsa.robot.connector;

import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.BuildConfig;
import com.jininsa.robot.app.Const;

import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

abstract public class ChatBot {

  static final Retrofit chatBotServer;

  static {
    OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

    if (BuildConfig.DEBUG) {
      HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
      logger.setLevel(HttpLoggingInterceptor.Level.BODY);
      httpClientBuilder.addInterceptor(logger);
    }

    httpClientBuilder.addInterceptor(chain -> {
      final okhttp3.Request req = chain.request();
      final HttpUrl url = req.url();
      final List<String> segs = url.pathSegments();
      HttpUrl.Builder urlBuilder = url.newBuilder();
      for (int i = 0, n = segs.size(); i < n; i++) {
        if ("{chatBotId}".equals(segs.get(i))) {
          urlBuilder.setPathSegment(i, Const.get().chatBotId);
        }
      }
      okhttp3.Request newReq = req.newBuilder()
          .url(urlBuilder.build())
          .header("Content-Type", "application/json")
          .header("m2u-auth-internal", "m2u-auth-internal")
          .build();
      return chain.proceed(newReq);
    });

    XLog.d(Const.get().chatBotServerUrl);

    chatBotServer = new Retrofit.Builder() // header m2u 추가
      .baseUrl(Const.get().chatBotServerUrl)
      .addConverterFactory(GsonConverterFactory.create())
      .client(httpClientBuilder.build())
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .build();
  }

  public final PublishRelay<String> answers = PublishRelay.create();

  ChatBot() {}

  public abstract void open();
  public abstract void talk(String service);

  public enum Type {
    MM, ML;
  }

  static public ChatBot create(Type t) {
    switch (t) {
      case MM: return MmChatBot.getInstance();
      case ML: return MlChatBotNew.getInstance();
    }
    throw new IllegalArgumentException(t.name());
  }
}
