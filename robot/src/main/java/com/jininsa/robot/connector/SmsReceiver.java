package com.jininsa.robot.connector;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.app.ManageableBroadcastReceiver;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class SmsReceiver extends ManageableBroadcastReceiver {

  public static final PublishRelay<SmsMessage> received = PublishRelay.create();

  public SmsReceiver(final Context context) {
    super(context);
  }

  @Override
  protected IntentFilter intentFilter() {
    IntentFilter filter = new IntentFilter();
    filter.addAction("android.provider.Telephony.SMS_RECEIVED");
    return filter;
  }

  @Override
  // @DebugLog
  public void onReceive(final Context context, final Intent intent) {
    Bundle data = Objects.requireNonNull(intent.getExtras());
    XLog.d(data);
    List<Object> pdus = Arrays.asList((Object[])data.get("pdus"));
    Stream.of(pdus).forEach(pdu -> {
      SmsMessage sms = SmsMessage.createFromPdu((byte[])pdu);
      SmsReceiver.received.accept(sms);
    });
  }
}
