package com.jininsa.robot.connector;

import com.elvishew.xlog.XLog;
import com.jininsa.robot.helper.Subscriptions;
import com.jininsa.robot.model.contents.Book;
import com.jininsa.robot.model.contents.Song;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kciter on 2018. 1. 19..
 */

public class ContentManager {
  private static final String SUBSCRIPTION_RETRIEVE_SONG_LIST = "2etsTYqYhzTavEQ3";
  private static final String SUBSCRIPTION_RETRIEVE_BOOK_LIST = "DYQJ79XeQV3Zm8Ke";
  static private ContentManager contentManager = new ContentManager();
  static public ContentManager get() {
    return contentManager;
  }

  private List<Song> songs;
  private Song nowPlaySong;

  private List<Book> books;
  private Book nowReadBook;
  private String nowReadingLine;

  private ContentManager() {
    this.songs = new ArrayList<Song>();
    this.books = new ArrayList<Book>();
  }

  public void fetchContents() {
    Subscriptions.add(this.hashCode(), SUBSCRIPTION_RETRIEVE_SONG_LIST,  Song.getSongList(0, 100).retry(5).subscribe(
      songs ->  {
        this.songs = songs;
        Subscriptions.remove(this.hashCode(), SUBSCRIPTION_RETRIEVE_SONG_LIST);
      },
      error -> {
        XLog.d(error.getStackTrace());
        Subscriptions.remove(this.hashCode(), SUBSCRIPTION_RETRIEVE_SONG_LIST);
      }
    ));

    Subscriptions.add(this.hashCode(), SUBSCRIPTION_RETRIEVE_BOOK_LIST, Book.getBookList(0, 105).retry(5).subscribe(
      books ->  {
        this.books = books;
        Subscriptions.remove(this.hashCode(), SUBSCRIPTION_RETRIEVE_BOOK_LIST);
      },
      error -> {
        XLog.d(error.getStackTrace());
        Subscriptions.remove(this.hashCode(), SUBSCRIPTION_RETRIEVE_BOOK_LIST);
      }
    ));
  }

  public List<Song> getSongs() {
    return songs;
  }

  public Song getNowPlaySong() {
    return nowPlaySong;
  }

  public void setNowPlaySong(Song nowPlaySong) {
    this.nowPlaySong = nowPlaySong;
  }

  public Song getNextSongFromNow() {
    if (nowPlaySong == null) return null;

    Iterator<Song> iterator = songs.iterator();
    while (iterator.hasNext()) {
      Song song = iterator.next();
      if (song.getId().equals(nowPlaySong.getId()) && iterator.hasNext()) {
        return iterator.next();
      }
    }
    return null;
  }

  public Song getPrevSongFromNow() {
    if (nowPlaySong == null) return null;

    Iterator<Song> iterator = songs.iterator();
    Song song = iterator.next();
    while (iterator.hasNext()) {
      Song nextSong = iterator.next();
      if (nextSong.getId().equals(nowPlaySong.getId())) {
        return song;
      }
      song = nextSong;
    }
    return null;
  }

  public List<Book> getBooks() {
    return books;
  }

  public Book getNowReadBook() {
    return nowReadBook;
  }

  public void setNowReadBook(Book nowReadBook) {
    this.nowReadBook = nowReadBook;
  }

  public Book getNextBookFromNow() {
    if (nowReadBook == null) return null;

    Iterator<Book> iterator = books.iterator();
    while (iterator.hasNext()) {
      Book book = iterator.next();
      if (book.getId().equals(nowReadBook.getId()) && iterator.hasNext()) {
        return iterator.next();
      }
    }
    return null;
  }

  public Book getPrevBookFromNow() {
    if (nowReadBook == null) return null;

    Iterator<Book> iterator = books.iterator();
    Book book = iterator.next();
    while (iterator.hasNext()) {
      Book nextBook = iterator.next();
      if (nextBook.getId().equals(nowReadBook.getId())) {
        return book;
      }
      book = nextBook;
    }
    return null;
  }

  public String getNowReadingLine() {
    return nowReadingLine;
  }

  public void setNowReadingLine(String nowReadingLine) {
    this.nowReadingLine = nowReadingLine;
  }
}