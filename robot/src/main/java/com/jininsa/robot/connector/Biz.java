package com.jininsa.robot.connector;

import com.google.gson.GsonBuilder;
import com.jininsa.robot.BuildConfig;
import com.jininsa.robot.app.Const;

import java.io.IOException;
import java.io.OutputStreamWriter;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Biz {
  private Biz() {}

  private static Retrofit connector;
  public static Retrofit connector() {

    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}

    if (connector == null) {
      OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

      if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
        logger.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder.addInterceptor(logger);
      }

      connector = new Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(Const.get().bizServerUrl)
        .client(httpClientBuilder.build())
        .build();
    }

    return connector;
  }
}
