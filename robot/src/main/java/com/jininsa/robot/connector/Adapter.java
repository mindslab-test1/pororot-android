package com.jininsa.robot.connector;

import android.app.Activity;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.app.ManageableBroadcastReceiver;

import java.lang.ref.WeakReference;
import java.util.Set;

public class Adapter {

  static private String toAdapterStateString(final int state) {
    switch (state) {
      case BluetoothAdapter.STATE_ON:
        return "STATE_ON";
      case BluetoothAdapter.STATE_OFF:
        return "STATE_OFF";
      case BluetoothAdapter.STATE_TURNING_ON:
        return "STATE_TURNING_ON";
      case BluetoothAdapter.STATE_TURNING_OFF:
        return "STATE_TURNING_OFF";
      default:
        return String.valueOf(state);
    }
  }

  public enum State {
    ON, OFF
  }

  private final class StateManager extends ManageableBroadcastReceiver {
    StateManager() {
      super(Adapter.this.contextRef.get());
    }

    @Override
    protected IntentFilter intentFilter() {
      IntentFilter filter = new IntentFilter();
      filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
      return filter;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
      if (intent == null) { return; }
      final String action = intent.getAction();

      if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
        int n = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, Env.DEFAULT_INT);
        int p = intent.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_STATE, Env.DEFAULT_INT);
        XLog.v("[%s] %s-->%s", action, toAdapterStateString(p), toAdapterStateString(n));
        if (n != p) {
          if (n == BluetoothAdapter.STATE_OFF) {
            Adapter.this.state.accept(State.OFF);
          }

          if (n == BluetoothAdapter.STATE_ON) {
            Adapter.this.state.accept(State.ON);
          }
        }
      }
    }
  }

  static public class Event {
    public final String action;
    public final BluetoothDevice device;
    public final Intent intent;

    private Event(String action, BluetoothDevice device, Intent intent) {
      this.action = action;
      this.device = device;
      this.intent = intent;
    }

    @Override
    public String toString() {
      return String.format("[%s] %s(%s)", action, device, device != null ? device.getName() : "null");
    }
  }

  private final class EventManager extends ManageableBroadcastReceiver {
    public EventManager() {
      super(Adapter.this.contextRef.get());
    }

    @Override
    protected IntentFilter intentFilter() {
      IntentFilter filter = new IntentFilter();
      filter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
      filter.addAction(BluetoothDevice.ACTION_PAIRING_REQUEST);
      filter.addAction(BluetoothHeadset.ACTION_AUDIO_STATE_CHANGED);
      filter.addAction(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED);
      filter.addAction(BluetoothAdapter.ACTION_CONNECTION_STATE_CHANGED);
      filter.addAction(BluetoothA2dp.ACTION_PLAYING_STATE_CHANGED);
      return filter;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
      if (intent == null) { return; }
      final String action = intent.getAction();
      final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//      XLog.d("[%s] %s", action, device);
      if (device != null) {
        Adapter.this.event.accept(new Event(action, device, intent));
      }
    }
  }

  public final PublishRelay<State> state = PublishRelay.create();
  public final PublishRelay<Event> event = PublishRelay.create();
  private final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();

  static private Adapter singleton;
  public static Adapter init(Context context) {
    if (Adapter.singleton == null) {
      Adapter.singleton = new Adapter(context.getApplicationContext());
    }
    return Adapter.singleton;
  }

//  @Contract(pure = true)
  public static Adapter get() {
    return Adapter.singleton;
  }

  public boolean isAvailable() {
    return this.adapter != null;
  }

  public boolean isOn() {
    return this.adapter.isEnabled();
  }

  public static void turnOnAdapter(final Activity activity) {
    Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
    activity.startActivityForResult(intent, Env.REQ_CODE_ASK_TO_TURN_ON_BLUETOOTH_ADAPTER);
  }

  private final StateManager stateManager;
  private final EventManager eventManager;
  private final WeakReference<Context> contextRef;

  private Adapter(Context context) {
    this.contextRef = new WeakReference<>(context.getApplicationContext());
    this.stateManager = new StateManager();
    this.eventManager = new EventManager();
    this.stateManager.register();
    this.eventManager.register();
  }

  public void stop() {
    this.contextRef.clear();
    this.stateManager.unregister();
    this.eventManager.unregister();
  }

  public boolean isEnabled() {
    return this.adapter.isEnabled();
  }

  public Set<BluetoothDevice> getBondedDevices() {
    return this.adapter.getBondedDevices();
  }

  public int getHeadsetConnectionState() {
    return this.adapter.getProfileConnectionState(BluetoothProfile.HEADSET);
  }

  public void getHeadsetProfileProxy(final BluetoothProfile.ServiceListener profileServiceListener) {
    this.adapter.getProfileProxy(this.contextRef.get(), profileServiceListener, BluetoothProfile.HEADSET);
  }

  public void getA2dpProfileProxy(final BluetoothProfile.ServiceListener profileServiceListener) {
    this.adapter.getProfileProxy(this.contextRef.get(), profileServiceListener, BluetoothProfile.A2DP);
  }

  public void closeProfileProxy(final BluetoothProfile profile) {
    this.adapter.closeProfileProxy(BluetoothHeadset.HEADSET, profile);
  }

  void cancelDiscovery() {
    this.adapter.cancelDiscovery();
  }

  void startDiscovery() {
    this.adapter.startDiscovery();
  }

  boolean isDiscovering() {
    return this.adapter.isDiscovering();
  }
}
