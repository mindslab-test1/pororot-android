package com.jininsa.robot.connector;

import com.elvishew.xlog.XLog;
import com.google.android.gms.common.util.Strings;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public class MlChatBot extends ChatBot {
  interface Service {
    @POST("v3/ConsoleUsers/login?include=user")
    Call<Auth> login(@Body Login login);
    @POST("v3/Grpc/openDialogService")
    Call<Session> open(@Header("Authorization") String token, @Body Open open);
    @POST("v3/Grpc/sendTalkAnalyze")
    Call<Answer> talk(@Header("Authorization") String token, @Body Question question);
  }

  static private class Login {
    private static final Login singleton = new Login("admin", "1234");

    @SerializedName("username")
    @Expose
    final private String username;
    @SerializedName("password")
    @Expose
    final private String password;

    private Login(final String username, final String password) {
      this.username = username;
      this.password = password;
    }
  }

  static private class Auth {
    private static class User {
      @SerializedName("activated")
      @Expose
      private Boolean activated;
      @SerializedName("username")
      @Expose
      private String username;
      @SerializedName("email")
      @Expose
      private String email;
      @SerializedName("id")
      @Expose
      private Integer id;
    }
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ttl")
    @Expose
    private Integer ttl;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("user")
    @Expose
    private User user;
  }

  static private class Open {
    private static final Open singleton = new Open("pororot", "admin", "MOBILE_ANDROID");

    @SerializedName("chatbot")
    @Expose
    final private String chatbot;
    @SerializedName("name")
    @Expose
    final private String name;
    @SerializedName("accessFrom")
    @Expose
    final private String accessFrom;

    private Open(final String chatbot, final String name, final String accessFrom) {
      this.chatbot = chatbot;
      this.name = name;
      this.accessFrom = accessFrom;
    }
  }

  static private class Session {
    static private class Contexts {
      static private class Fields {}
      @SerializedName("fields")
      @Expose
      public Fields fields;
    }

    static private class Duration {
      @SerializedName("seconds")
      @Expose
      public String seconds;
      @SerializedName("nanos")
      @Expose
      public Integer nanos;
    }
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("chatbot")
    @Expose
    public String chatbot;
    @SerializedName("user_key")
    @Expose
    public String userKey;
    @SerializedName("accessFrom")
    @Expose
    public String accessFrom;
    @SerializedName("contexts")
    @Expose
    public Contexts contexts;
    @SerializedName("start_time")
    @Expose
    public Object startTime;
    @SerializedName("duration")
    @Expose
    public Duration duration;
    @SerializedName("human_assisted")
    @Expose
    public Boolean humanAssisted;
    @SerializedName("intent_finder")
    @Expose
    public String intentFinder;
    @SerializedName("$metadata")
    @Expose
    public List<Object> $metadata = null;
  }

  static private class Question {
    private static final Question singleton = new Question();
    static {
      Question.singleton.talkQuery = new TalkQuery();
      Question.singleton.talkQuery.chatbot = "pororot";
//      Question.singleton.talkQuery.meta = new TalkQuery.Meta();
//      Question.singleton.talkQuery.meta.fields = new TalkQuery.Meta.Fields();
//      Question.singleton.talkQuery.meta.fields.inDevicetoken = new TalkQuery.Meta.Fields.InDevicetoken();
//      Question.singleton.talkQuery.meta.fields.inDevicetoken.kind ="string_value";
//      Question.singleton.talkQuery.meta.fields.inLang = new TalkQuery.Meta.Fields.InLang();
//      Question.singleton.talkQuery.meta.fields.inLang.kind = "string_value";
//      Question.singleton.talkQuery.meta.fields.inLang.stringValue = "kor";
//      Question.singleton.talkQuery.meta.fields.inSessionid = new TalkQuery.Meta.Fields.InSessionid();
//      Question.singleton.talkQuery.meta.fields.inSessionid.kind = "string_value";
      Question.singleton.talkQuery.query = new TalkQuery.Query();
      Question.singleton.talkQuery.query.lang = "ko_KR";
//      Question.Metadatum d1 = new Metadatum();
//      d1.attr = "in.sessionid";
//      Question.Metadatum d2 = new Metadatum();
//      d2.attr = "in.lang";
//      d2.value = "kor";
      Question.Metadatum d3 = new Metadatum();
      d3.attr = "in.devicetoken";
      Question.singleton.metadata = new ArrayList<>(3);
//      Question.singleton.metadata.add(d1);
//      Question.singleton.metadata.add(d2);
      Question.singleton.metadata.add(d3);
    }

    static private class TalkQuery {
//      static private class Meta {
//        static private class Fields {
//          static private class InDevicetoken {
//            @SerializedName("kind")
//            @Expose
//            private String kind;
//            @SerializedName("string_value")
//            @Expose
//            private String stringValue;
//          }
//
//          static private class InLang {
//            @SerializedName("kind")
//            @Expose
//            private String kind;
//            @SerializedName("string_value")
//            @Expose
//            private String stringValue;
//          }
//
//          static private class InSessionid {
//            @SerializedName("kind")
//            @Expose
//            private String kind;
//            @SerializedName("string_value")
//            @Expose
//            private String stringValue;
//          }
//
//          @SerializedName("in.devicetoken")
//          @Expose
//          private InDevicetoken inDevicetoken;
//          @SerializedName("in.lang")
//          @Expose
//          private InLang inLang;
//          @SerializedName("in.sessionid")
//          @Expose
//          private InSessionid inSessionid;
//        }
//        @SerializedName("fields")
//        @Expose
//        private Fields fields;
//      }

      static private class Query {
        @SerializedName("utter")
        @Expose
        private String utter;
        @SerializedName("lang")
        @Expose
        private String lang;
      }

      @SerializedName("chatbot")
      @Expose
      private String chatbot;
//      @SerializedName("meta")
//      @Expose
//      private Meta meta;
      @SerializedName("query")
      @Expose
      private Query query;
    }

    static private class Metadatum {
      @SerializedName("attr")
      @Expose
      private String attr;
      @SerializedName("value")
      @Expose
      private String value;
    }
    @SerializedName("TalkQuery")
    @Expose
    private TalkQuery talkQuery;
    @SerializedName("Metadata")
    @Expose
    private List<Metadatum> metadata = null;
  }

  static private class Answer {

    static private class Answer_ {
      static private class Meta {
        static private class Fields {
          static private class DaEnginePath {
            @SerializedName("kind")
            @Expose
            public String kind;
            @SerializedName("null_value")
            @Expose
            public String nullValue;
            @SerializedName("number_value")
            @Expose
            public Integer numberValue;
            @SerializedName("string_value")
            @Expose
            public String stringValue;
            @SerializedName("bool_value")
            @Expose
            public Boolean boolValue;
            @SerializedName("struct_value")
            @Expose
            public Object structValue;
            @SerializedName("list_value")
            @Expose
            public Object listValue;
          }

          static private class DaEngineName {
            @SerializedName("kind")
            @Expose
            public String kind;
            @SerializedName("null_value")
            @Expose
            public String nullValue;
            @SerializedName("number_value")
            @Expose
            public Integer numberValue;
            @SerializedName("string_value")
            @Expose
            public String stringValue;
            @SerializedName("bool_value")
            @Expose
            public Boolean boolValue;
            @SerializedName("struct_value")
            @Expose
            public Object structValue;
            @SerializedName("list_value")
            @Expose
            public Object listValue;
          }

          static private class SessionId {
            @SerializedName("kind")
            @Expose
            public String kind;
            @SerializedName("null_value")
            @Expose
            public String nullValue;
            @SerializedName("number_value")
            @Expose
            public Integer numberValue;
            @SerializedName("string_value")
            @Expose
            public String stringValue;
            @SerializedName("bool_value")
            @Expose
            public Boolean boolValue;
            @SerializedName("struct_value")
            @Expose
            public Object structValue;
            @SerializedName("list_value")
            @Expose
            public Object listValue;
          }

          static private class StatusCode {
            @SerializedName("kind")
            @Expose
            public String kind;
            @SerializedName("null_value")
            @Expose
            public String nullValue;
            @SerializedName("number_value")
            @Expose
            public Integer numberValue;
            @SerializedName("string_value")
            @Expose
            public String stringValue;
            @SerializedName("bool_value")
            @Expose
            public Boolean boolValue;
            @SerializedName("struct_value")
            @Expose
            public Object structValue;
            @SerializedName("list_value")
            @Expose
            public Object listValue;
          }

          static private class StatusMessage {
            @SerializedName("kind")
            @Expose
            public String kind;
            @SerializedName("null_value")
            @Expose
            public String nullValue;
            @SerializedName("number_value")
            @Expose
            public Integer numberValue;
            @SerializedName("string_value")
            @Expose
            public String stringValue;
            @SerializedName("bool_value")
            @Expose
            public Boolean boolValue;
            @SerializedName("struct_value")
            @Expose
            public Object structValue;
            @SerializedName("list_value")
            @Expose
            public Object listValue;
          }

          static private class SelvasTts {
            @SerializedName("kind")
            @Expose
            public String kind;
            @SerializedName("null_value")
            @Expose
            public String nullValue;
            @SerializedName("number_value")
            @Expose
            public Integer numberValue;
            @SerializedName("string_value")
            @Expose
            public String stringValue;
            @SerializedName("bool_value")
            @Expose
            public Boolean boolValue;
            @SerializedName("struct_value")
            @Expose
            public Object structValue;
            @SerializedName("list_value")
            @Expose
            public Object listValue;
          }
          @SerializedName("da.engine.name")
          @Expose
          public DaEngineName daEngineName;
          @SerializedName("da.engine.path")
          @Expose
          public DaEnginePath daEnginePath;
          @SerializedName("status.code")
          @Expose
          public StatusCode statusCode;
          @SerializedName("sessionId")
          @Expose
          public SessionId sessionId;
          @SerializedName("status.message")
          @Expose
          public StatusMessage statusMessage;
          @SerializedName("selvas.tts")
          @Expose
          public SelvasTts selvasTts;
        }
        @SerializedName("fields")
        @Expose
        public Fields fields;
      }
      @SerializedName("utter")
      @Expose
      public String utter;
      @SerializedName("lang")
      @Expose
      public String lang;
      @SerializedName("meta")
      @Expose
      public Meta meta;
    }

    static private class FilledSlots {
      static private class Fields {}
      @SerializedName("fields")
      @Expose
      public Fields fields;
    }
    @SerializedName("res_code")
    @Expose
    public String resCode;
    @SerializedName("result_type")
    @Expose
    public String resultType;
    @SerializedName("error_message")
    @Expose
    public String errorMessage;
    @SerializedName("skill")
    @Expose
    public String skill;
    @SerializedName("intent")
    @Expose
    public String intent;
    @SerializedName("context_flag")
    @Expose
    public String contextFlag;
    @SerializedName("is_answer")
    @Expose
    public Boolean isAnswer;
    @SerializedName("needs_listening")
    @Expose
    public Boolean needsListening;
    @SerializedName("has_more_answer")
    @Expose
    public Boolean hasMoreAnswer;
    @SerializedName("has_slots")
    @Expose
    public Boolean hasSlots;
    @SerializedName("serviceResultcode")
    @Expose
    public String serviceResultcode;
    @SerializedName("answer")
    @Expose
    public Answer_ answer;
    @SerializedName("filled_slots")
    @Expose
    public FilledSlots filledSlots;
    @SerializedName("empty_slots")
    @Expose
    public List<Object> emptySlots = null;
    @SerializedName("$metadata")
    @Expose
    public List<Object> $metadata = null;
  }

  private static final MlChatBot singleton = new MlChatBot();

  private String token;

  static ChatBot getInstance() {
    return singleton;
  }

  private final MlChatBot.Service service;

  private MlChatBot() {
    this.service = chatBotServer.create(MlChatBot.Service.class);
    // this.open();
  }

  @Override
  public void open() {
    final Call<Auth> loginCall = this.service.login(Login.singleton);
    loginCall.enqueue(new Callback<Auth>() {
      @Override
      public void onResponse(final Call<Auth> call, final Response<Auth> response) {
        if (response.isSuccessful()) {
          Auth auth = response.body();
          if (auth != null) {
            MlChatBot.this.token = auth.id;
            final Call<Session> sessionCall = MlChatBot.this.service.open(MlChatBot.this.token, Open.singleton);
            sessionCall.enqueue(new Callback<Session>() {
              @Override
              public void onResponse(final Call<Session> call, final Response<Session> response) {
                if (response.isSuccessful()) {
                  Session session = response.body();
                  if (session != null) {
                    String sessionId = session.id;
                    Question.Metadatum d1 = Question.singleton.metadata.get(0);
                    d1.value = sessionId;
                  }
                }
              }

              @Override
              public void onFailure(final Call<Session> call, final Throwable t) { XLog.e(t.getMessage(), t); }
            });
          }
        }
      }

      @Override
      public void onFailure(final Call<Auth> call, final Throwable t) { XLog.e(t.getMessage(), t); }
    });
  }

  @Override
  public void talk(final String text) {
    Question.singleton.talkQuery.query.utter = text;
    final Call<Answer> answerCall = this.service.talk(this.token, Question.singleton);
    answerCall.enqueue(new Callback<Answer>() {
      @Override
      public void onResponse(final Call<Answer> call, final Response<Answer> response) {
        if (response.isSuccessful()) {
          Answer answer = response.body();
          if (answer != null) {
            String utter = answer.answer.utter;
            if (Strings.isEmptyOrWhitespace(utter)) {
              utter = "NO ANSWER";
            }
            MlChatBot.this.answers.accept(utter);
          }
        }
      }

      @Override
      public void onFailure(final Call<Answer> call, final Throwable t) { XLog.e(t.getMessage(), t); }
    });
  }
}
