package com.jininsa.robot.app;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.StackingBehavior;
import com.annimon.stream.IntPair;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Sets;
import com.jininsa.robot.connector.Adapter;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Connectivity;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.helper.Subscriptions;
import com.jininsa.robot.model.party.Person;
import com.jininsa.robot.model.robot.Robot;
import com.pisome.iconify.IconifyLayoutInflaterFactory;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public abstract class RobotActivity extends AppCompatActivity implements Checker.CheckRequired {
  static private final String SUBSCRIPTION_ADAPTER_ON_OFF_CHANGE = "uiU7ifszg0JrZRgx";
  static private final String SUBSCRIPTION_NETWORK_ON_OFF_CHANGE = "OmPY7dOvOE70kCmX";
  static private final String SUBSCRIPTION_ROBOT_STATE = "YWz3lcDadmUUHlsy";

  private MaterialDialog warnRobotOffDialog;

  abstract protected @LayoutRes int getLayoutResId();

  protected Person me;
  protected Robot robot;

  private MaterialDialog askToTurnOnBluetoothAdapterDialog;
  private MaterialDialog askToConnectNetworkDialog;

  private Checker checker;

  @Override
  protected void onCreate(@Nullable final Bundle savedInstanceState) {
    this.me = Person.get();
    this.robot = Robot.get();
    XLog.i("[onCreate] %s %s %s", this.getClass().getSimpleName(), this.me, this.robot);
    LayoutInflaterCompat.setFactory2(this.getLayoutInflater(), new IconifyLayoutInflaterFactory(this.getDelegate()));
    super.onCreate(savedInstanceState);

    if (this.getLayoutResId() != Env.DEFAULT_INT) {
      this.setContentView(this.getLayoutResId());
      ButterKnife.bind(this);
    }

    this.askToTurnOnBluetoothAdapterDialog = new MaterialDialog.Builder(this)
      .cancelable(false)
      .content("뽀로롯이 동작하려면 블루투스가 꼭 필요합니다")
      .positiveText("블루투스 켜기")
      .onPositive((dialog, which) -> Adapter.turnOnAdapter(this))
      .build();

    this.askToConnectNetworkDialog = new MaterialDialog.Builder(this)
      .cancelable(false)
      .content("뽀로롯이 동작하려면 인터넷 연결이 꼭 필요합니다.")
      .positiveText("와이파이 연결")
      .negativeText("모바일데이터 연결")
      .stackingBehavior(StackingBehavior.ALWAYS)
      .onPositive((dialog, which) -> ActivityCompat.startActivityForResult(this, new Intent(Settings.ACTION_WIFI_SETTINGS), Env.REQ_CODE_ASK_TO_CONNECT_NETWORK, null))
      .onNegative((dialog, which) -> {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
        ActivityCompat.startActivityForResult(this, intent, Env.REQ_CODE_ASK_TO_CONNECT_NETWORK, null);
      })
      .build();

    this.warnRobotOffDialog = new MaterialDialog.Builder(this)
      .cancelable(false)
      .content("뽀로롯과 연결이 끊어졌습니다. 뽀로롯을 켜면 자동으로 연결됩니다.")
      .positiveText("확인")
      .build();

    if (this.robot != null) {
      this.addSubscription(SUBSCRIPTION_ROBOT_STATE,
        this.robot.states.observeOn(AndroidSchedulers.mainThread()).subscribe(state -> {
          if (state == Robot.State.OFF) {
            if (!this.isFinishing()) {
              this.warnRobotOffDialog.show();
            }
          }
          if (state == Robot.State.READY) {
            this.warnRobotOffDialog.dismiss();
          }
        }));
    }

    this.checker = new Checker(this.getCheckSubjects(), this);

    this.addSubscription(SUBSCRIPTION_ADAPTER_ON_OFF_CHANGE,
      Adapter.get().state.subscribe(state -> {
        // !!! "블루투스 꺼짐" 이벤트 발생하면 "블루투스 켜기 다이얼로그 보이기" 이벤트 발생
        if (state == Adapter.State.OFF) {
          if (!this.isFinishing()) {
            this.askToTurnOnBluetoothAdapterDialog.show();
          }
        }
        // !!! "블루투스 켜짐" 이벤트 발생하면 "블루투스 켜기 다이얼로그 감추기" 이벤트 발생
        if (state == Adapter.State.ON) {
          this.checker.actions.accept(Checker.Finish.BLUETOOTH_ADAPTER);
          this.askToTurnOnBluetoothAdapterDialog.dismiss();
        }
      }));

    this.addSubscription(SUBSCRIPTION_NETWORK_ON_OFF_CHANGE,
      Connectivity.stateRelay.subscribe(state -> {
        // !!! "네트워크 끊김" 이벤트 발생하면 "네트워크 뎐결 다이얼로그 보이기" 이벤트 발생
        if (state == Connectivity.State.DISCONNECTED) {
          this.askToConnectNetworkDialog.show();
        }
        // !!! "네트워크 연결" 이벤트 발생하면 "네트워크 뎐결 다이얼로그 감추기" 이벤트 발생
        if (state == Connectivity.State.CONNECTED) {
          this.checker.actions.accept(Checker.Finish.NETWORK);
          this.askToConnectNetworkDialog.dismiss();
        }
      }));

    this.doBeforeChecking();
    this.checker.begin();
  }

  protected void addSubscription(final String name, final Disposable subscription) {
    Subscriptions.add(this.hashCode(), name, subscription);
  }

  protected void removeSubscription(final String name) {
    Subscriptions.remove(this.hashCode(), name);
  }

  protected void removeAllSubscription() {
    Subscriptions.removeAllOf(this.hashCode());
  }

  @Override
  public Set<Checker.Subject> getCheckSubjects() {
    return Sets.newHashSet(Checker.Subject.values());
  }

  @Override
  public void doBeforeChecking() {}

  @Override
  public void doAfterChecking() {}

  protected boolean isChecked() {
    return this.checker.isCheckCompleted();
  }

  @Override
  protected void onDestroy() {
    XLog.v("[onDestroy] %s", this.getClass().getSimpleName());
    this.removeAllSubscription();
    this.warnRobotOffDialog.dismiss();
    this.askToTurnOnBluetoothAdapterDialog.dismiss();
    this.askToConnectNetworkDialog.dismiss();
    super.onDestroy();
  }

  @Override
  @SuppressLint("ObsoleteSdkInt")
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
      if (hasFocus) {
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
      }
    }
  }

  @Override
  protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == Env.REQ_CODE_ASK_TO_TURN_ON_BLUETOOTH_ADAPTER) {
      if (resultCode == RESULT_OK) {
        this.checker.actions.accept(Checker.Finish.BLUETOOTH_ADAPTER);
      }
      if (resultCode == RESULT_CANCELED) {
        this.checker.actions.accept(Checker.Begin.BLUETOOTH_ADAPTER);
      }
    }
    else if (requestCode == Env.REQ_CODE_ASK_TO_CONNECT_NETWORK) {
      boolean isConnected = Connectivity.isConnected();
      if (isConnected) {
        this.checker.actions.accept(Checker.Finish.NETWORK);
      }
      else {
        this.checker.actions.accept(Checker.Begin.NETWORK);
      }
    }
    else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (Permission.isRequestCode(requestCode)) {
        Permission permission = Permission.from(requestCode);
        permission.requestOrGo(this, () -> this.checker.actions.accept(Checker.Begin.PERMISSION));
      }
    }
  }

  @Override
  public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (Permission.isRequestCode(requestCode)) {
        Optional<String> userDeniedPermission = Stream.of(permissions).indexed()
          .filter(__ -> grantResults[__.getFirst()] == PackageManager.PERMISSION_DENIED).map(IntPair::getSecond).findSingle();
        if (userDeniedPermission.isPresent()) {
          Permission permission = Permission.from(requestCode);
          this.askToGrantPermission(permission);
        }
        else {
          // !!! 남은 권한 처리를 위해
          if (!this.checker.isCheckCompleted()) {
            this.checker.actions.accept(Checker.Begin.PERMISSION);
          }
        }
      }
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    return Collections.emptyList();
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public void explainPermissionRationale(final Permission permission) {
    new MaterialDialog.Builder(this)
      .title("권한 요청")
      .content(Const.get().permissionRationales.get(permission))
      .cancelable(false)
      .positiveText("허용")
      .onPositive((dialog, which) -> permission.request(this))
      .show();
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public void askToGrantPermission(Permission permission) {
    boolean doNotShowAgain = !this.shouldShowRequestPermissionRationale(permission.what);
    if (doNotShowAgain) {
      new MaterialDialog.Builder(this)
        .cancelable(false)
        .title("권한 요청")
        .content(String.format("%s\n%s", Const.get().permissionRationales.get(permission), "'다시 묻지 않음'을 선택했기 때문에 앱설정에서 직접 권한을 허용하셔야 합니다."))
        .positiveText("설정")
        .onPositive((dialog, which) -> {
          Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + this.getPackageName()));
          ActivityCompat.startActivityForResult(this, intent, permission.requestCode, null);
        })
        .show();
    }
    else {
      permission.request(this);
    }
  }

  @Override
  public void askToTurnOnBluetoothAdapter() {
    this.askToTurnOnBluetoothAdapterDialog.show();
  }

  @Override
  public void askToConnectNetwork() {
    this.askToConnectNetworkDialog.show();
  }

  @Override
  public void warnNoBluetoothAdapter() {
    new MaterialDialog.Builder(this)
      .content("이 기기는 블루투스 기능이 없습니다. 뽀로롯은 블루투스가 있어야 동작합니다.")
      .onPositive((dialog, which) -> this.finish())
      .show();
  }
}
