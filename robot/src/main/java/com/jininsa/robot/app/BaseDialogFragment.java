package com.jininsa.robot.app;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.jininsa.robot.helper.Subscriptions;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.Disposable;

abstract public class BaseDialogFragment extends DialogFragment {
  private Unbinder unbinder;

  public BaseDialogFragment() {}

  @Override
  public void onCreate(@Nullable final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setRetainInstance(true);
  }

  @NonNull
  @Override
  public Dialog onCreateDialog(final Bundle savedInstanceState) {
    Dialog dialog = super.onCreateDialog(savedInstanceState);
    Window window = dialog.getWindow();
    if (window != null) {
      window.requestFeature(Window.FEATURE_NO_TITLE);
    }

    return dialog;
  }

  @Override
  public void onStart() {
    super.onStart();
    Dialog dialog = this.getDialog();
    if (dialog != null) {
      Window window = dialog.getWindow();
      if (window != null) {
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
      }
    }
  }

  @Override
  public void onDestroyView() {
    this.unbinder.unbind();
    this.removeAllSubscribers();
    super.onDestroyView();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(this.getLayoutResId(), container, false);
    this.unbinder = ButterKnife.bind(this, view);
    this.setCancelable(false);
    return view;
  }

//  @Override
//  public void onSaveInstanceState(final Bundle outState) {
//    outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
//    super.onSaveInstanceState(outState);
//  }

  protected void addSubscriber(final String name, final Disposable subscriber) {
    Subscriptions.add(this.hashCode(), name, subscriber);
  }

  protected void removeAllSubscribers() {
    Subscriptions.removeAllOf(this.hashCode());
  }

  abstract protected int getLayoutResId();

}
