package com.jininsa.robot.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

public class MaskActivity extends AppCompatActivity
{
  static final private class DozeDetector extends BroadcastReceiver
  {
    static final private IntentFilter INTENT_FILTER = new IntentFilter(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);

    private WeakReference<MaskActivity> reference;

    private DozeDetector(MaskActivity activity)
    {
      this.reference = new WeakReference<MaskActivity>(activity);
    }

    @Override
    public void onReceive(final Context context, final Intent intent)
    {
      this.reference.get().finish();
    }
  }

  private DozeDetector dozeDetector;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
    {
      this.dozeDetector = new DozeDetector(this);
      this.registerReceiver(this.dozeDetector, DozeDetector.INTENT_FILTER);
    }
  }

  @Override
  protected void onDestroy()
  {
    super.onDestroy();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
    {
      this.unregisterReceiver(this.dozeDetector);
    }
  }
}
