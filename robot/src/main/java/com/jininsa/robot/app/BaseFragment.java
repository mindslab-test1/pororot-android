package com.jininsa.robot.app;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jininsa.robot.helper.Subscriptions;
import com.jininsa.robot.model.party.Person;
import com.jininsa.robot.model.robot.Robot;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.Disposable;

abstract public class BaseFragment extends Fragment {
  private Unbinder unbinder;
  protected Robot robot;
  protected Person me;

  public BaseFragment() {}

  @Override
  public void onCreate(@Nullable final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.setRetainInstance(true);
    this.robot = Robot.get();
    this.me = Person.get();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(this.getLayoutResId(), container, false);
    this.unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override
  public void onDestroyView() {
    if (this.unbinder != null) {
      this.unbinder.unbind();
    }
    this.removeAllSubscriptions();
    super.onDestroyView();
  }

  protected void addSubscription(final String name, final Disposable subscriber) {
    Subscriptions.add(this.hashCode(), name, subscriber);
  }

  protected void removeSubscription(final String name) {
    Subscriptions.remove(this.hashCode(), name);
  }

  protected void removeAllSubscriptions() {
    Subscriptions.removeAllOf(this.hashCode());
  }

  abstract protected int getLayoutResId();
}
