package com.jininsa.robot.app;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.multidex.MultiDex;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jakewharton.rxrelay2.Relay;
import com.jininsa.robot.connector.Adapter;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.helper.Connectivity;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.model.party.Person;
import com.jininsa.robot.model.robot.Robot;
import com.jininsa.robot.part.Brain;

abstract public class App extends Application {
  static class DozeBlocker extends ManageableBroadcastReceiver {
    public DozeBlocker(final Context context) {
      super(context);
    }

    @Override
    protected IntentFilter intentFilter() {
      return new IntentFilter(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
      XLog.d(intent.getAction());
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (pm != null && pm.isDeviceIdleMode()) {
          this.launchMaskActivity(context);
        }
      }
    }

    void launchMaskActivity(Context context) {
      Intent startLauncher = new Intent(Intent.ACTION_MAIN);
      startLauncher.addCategory(Intent.CATEGORY_HOME);
      startLauncher.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      context.startActivity(startLauncher);
    }
  }

  public static final Relay<Boolean> brainActivator = PublishRelay.create();
  private static boolean isBrainActivated = false;

  private DozeBlocker dozeBlocker;
  private Connectivity.ChangeReceiver connectivityChangeReceiver;
  @SuppressWarnings("FieldCanBeLocal") private PhoneStateListener phoneStateListener; // !!! 지역변수로 정의하면 동작하지 않는다.
  private Brain brain;
  private ServiceConnection serviceConnection;
  private Adapter adapter;
  abstract protected Const.Settings getSettings();

  public static boolean isBrainActivated() {
    return isBrainActivated;
  }

  @Override
  public void onCreate() {
    XLog.w("====START====");
    super.onCreate();

    Const.init(this.getSettings());
    Env.init(this.getApplicationContext());

    this.adapter = Adapter.init(this.getApplicationContext());

    Connectivity.init(this.getApplicationContext());
    this.connectivityChangeReceiver = new Connectivity.ChangeReceiver(this.getApplicationContext());
    this.connectivityChangeReceiver.register();

    this.serviceConnection = new ServiceConnection() {
      @Override
      public void onServiceConnected(final ComponentName name, final IBinder service) {
        XLog.w("====CONNECTED====");
        App.this.brain = ((Brain.Binder) service).getConnector();
        App.isBrainActivated = true;
        App.brainActivator.accept(App.isBrainActivated);

        if (Const.get().shouldPhoneControl) {
          App.this.registerPhoneStateListener();
        }
      }

      @Override
      public void onServiceDisconnected(final ComponentName name) {
        XLog.w("====DISCONNECTED====");
        App.this.brain = null;
        App.isBrainActivated = false;
        App.brainActivator.accept(App.isBrainActivated);
      }
    };
    Context context = this.getApplicationContext();
    context.bindService(new Intent(context, Brain.class), this.serviceConnection, BIND_AUTO_CREATE);
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      this.dozeBlocker = new DozeBlocker(this.getApplicationContext());
      this.dozeBlocker.register();
    }

    Person.init();
    ContentManager.get().fetchContents();
  }

  @Override
  public void onTerminate() {
    this.adapter.stop();
    this.connectivityChangeReceiver.unregister();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      this.dozeBlocker.unregister();
    }
    if (this.brain != null) {
      this.brain.unbindService(this.serviceConnection);
    }
    XLog.w("====TERMINATED====");
    super.onTerminate();
  }

  @Override
  protected void attachBaseContext(final Context base) {
    super.attachBaseContext(base);
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
      MultiDex.install(this);
    }
  }

  private void registerPhoneStateListener() {
    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
    if (telephonyManager != null) {
      this.phoneStateListener = new PhoneStateListener() {
        private int state = Env.DEFAULT_INT;

        private String stateToString(int state) {
          if (state == TelephonyManager.CALL_STATE_RINGING) { return "CALL_STATE_RINGING"; }
          if (state == TelephonyManager.CALL_STATE_OFFHOOK) { return "CALL_STATE_OFFHOOK"; }
          if (state == TelephonyManager.CALL_STATE_IDLE) { return "CALL_STATE_IDLE"; }
          return "NONE";
        }

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
          XLog.v("PhoneState : %s -> %s", this.stateToString(this.state), this.stateToString(state));

          if ((this.state == TelephonyManager.CALL_STATE_RINGING || this.state == TelephonyManager.CALL_STATE_OFFHOOK) && state == TelephonyManager.CALL_STATE_IDLE) {
            // todo: 전화를 끊는 경우
            // todo: 다시 시작
            XLog.w("전화 끊었다.");
            if (Robot.get() != null && App.this.brain != null) {
              App.this.brain.start();
            }
          }

          if (this.state == TelephonyManager.CALL_STATE_IDLE && (state == TelephonyManager.CALL_STATE_RINGING || state == TelephonyManager.CALL_STATE_OFFHOOK)) {
            // todo: 전화가 오거나 전화를 거는 경우
            // todo: 동작 그만
            XLog.w("전화 왔다.");
            if (Robot.get() != null && App.this.brain != null) {
              App.this.brain.terminate();
            }
          }

          this.state = state;
        }
      };

      telephonyManager.listen(this.phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }
  }
}
