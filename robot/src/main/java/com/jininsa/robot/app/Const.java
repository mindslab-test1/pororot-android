package com.jininsa.robot.app;

import android.os.Environment;

import com.jininsa.robot.helper.Permission;

import java.io.File;
import java.util.Map;

public class Const {

  public interface Settings {
    String robotName();
    String chatBotId();
    String ttsDatabaseName();
    String earType();
    String bizServerUrl();
    String chatBotServerUrl();
    Map<Permission, String> permissionRationales();
    String ttsKey();
    Class videoPlayerContext();
    Boolean hasSpp();
    Boolean isLite();
    Boolean shouldPhoneControl();
  }

  static Const C;

  static void init(final Settings settings) {
    if (C == null) {
      C = new Const(settings);
    }
  }

  static public Const get() {
    return C;
  }

  public final String rootPathString;
  public final File rootPath;
  public final String ttsPathString;
  public final File ttsPath;
  public final String ttsDbFileName;
  public final String ttsDbFileString;
  public final File ttsDbFile;
  public final String ttsLicenseFileString;
  public final File ttsLicenseFile;
  public final String chatBotId;
  public final String earType;
  public final String bizServerUrl;
  public final String chatBotServerUrl;
  public final Map<Permission, String> permissionRationales;
  public final String ttsKey;
  public final Class videoPlayerContext;
  public final Boolean hasSpp;
  public final Boolean isLite;
  public final Boolean shouldPhoneControl;

  protected Const(Settings settings) {
    this.rootPathString = String.format("%s/%s", Environment.getExternalStorageDirectory().getAbsolutePath(), settings.robotName());
    this.rootPath = new File(this.rootPathString);

    this.ttsPathString = String.format("%s/%s/", this.rootPathString, "tts");
    this.ttsPath = new File(this.ttsPathString);
    this.ttsKey = settings.ttsKey();

    this.ttsDbFileName = settings.ttsDatabaseName();
    this.ttsDbFileString = String.format("%s%s", this.ttsPathString, ttsDbFileName);
    this.ttsDbFile = new File(this.ttsDbFileString);

    this.ttsLicenseFileString = String.format("%s%s", this.ttsPathString, "verification.txt");
    this.ttsLicenseFile = new File(this.ttsLicenseFileString);

    this.chatBotId = settings.chatBotId();
    this.earType = settings.earType();
    this.bizServerUrl = settings.bizServerUrl();
    this.chatBotServerUrl = settings.chatBotServerUrl();

    this.videoPlayerContext = settings.videoPlayerContext();

    this.permissionRationales = settings.permissionRationales();

    this.hasSpp = settings.hasSpp();
    this.isLite = settings.isLite();
    this.shouldPhoneControl = settings.shouldPhoneControl();
  }


  public void makeDirectories() {
    if (!this.rootPath.exists()) { this.rootPath.mkdir(); }
    if (!this.ttsPath.exists()) { this.ttsPath.mkdir(); }
  }
}
