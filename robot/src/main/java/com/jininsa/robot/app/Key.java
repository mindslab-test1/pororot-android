package com.jininsa.robot.app;

public class Key {
  static public final String AUTH_ACTION = "xA0xaWtuWl8Gp10F";
  static public final String SELECTED_ROBOT_UUID = "yWb49DThr3q4Ys5A";
  static public final String LAST_MUSIC_VOLUME = "Te2utl5YgRc4HSoT";
  static public final String LAST_ROBOT_UUID = "z5AeASg0Ytf55MHa";
  static public final String LAST_AUTH_TOKEN = "HDpA97lBbe1JX78w";
  static public final String LAST_AUTH_TOKEN_EXPIRED_TIME = "o7fk4RE2k4aCLKD5";
  static public final String SETTINGS_PITCH = "AP2RgNP2ZPLaCoYN";
  static public final String SETTINGS_BE_QUIET_IN_CHAT = "xORFoH156rALTGD9";
  static public final String SETTINGS_BE_QUIET = "ZyutIYIF7NeqB4LW";
  static public final String SETTINGS_STOP_BY_SHAKING = "SFGt6XdNBEMdIJD0";
  static public final String SETTINGS_IS_CHATTING = "yUadiPxTouD7cuQj";
  static public final String SETTINGS_SHOW_TALKS = "uNE57igzJR9vDE3v";

  private Key() {}
}
