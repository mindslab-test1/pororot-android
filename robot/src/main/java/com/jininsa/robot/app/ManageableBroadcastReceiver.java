package com.jininsa.robot.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;

import java.util.concurrent.atomic.AtomicBoolean;

abstract public class ManageableBroadcastReceiver extends BroadcastReceiver {
  private AtomicBoolean isRegistered = new AtomicBoolean(false);

  public void register() {
    if (!this.isRegistered.get()) {
      this.isRegistered.set(true);
//      LocalBroadcastManager.getInstance(this.context).registerReceiver(this, this.intentFilter());
      this.context.registerReceiver(this, this.intentFilter());
    }
  }

  public void unregister() {
    if (this.isRegistered.get()) {
      this.isRegistered.set(false);
//      LocalBroadcastManager.getInstance(this.context).unregisterReceiver(this);
      this.context.unregisterReceiver(this);
    }
  }

  private final Context context;

  public ManageableBroadcastReceiver(Context context) {
    this.context = context.getApplicationContext();
  }

  protected abstract IntentFilter intentFilter();
}
