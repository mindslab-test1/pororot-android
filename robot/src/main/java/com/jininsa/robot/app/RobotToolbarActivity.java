package com.jininsa.robot.app;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.jininsa.robot.R;

abstract public class RobotToolbarActivity extends RobotActivity {

  protected TextView titleTextView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Toolbar toolbar = this.findViewById(R.id.toolbar);
    this.titleTextView = this.findViewById(R.id.titleTextView);
    this.titleTextView.setText(this.title());
    this.setSupportActionBar(toolbar);
    ActionBar actionBar = this.getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayShowTitleEnabled(false);
      if (NavUtils.getParentActivityName(this) != null) {
        actionBar.setDisplayHomeAsUpEnabled(true);
      }
    }
  }

  abstract protected String title();

  protected void setTitle(String title) {
    this.titleTextView.setText(title);
  }

}
