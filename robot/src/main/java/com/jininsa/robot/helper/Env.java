package com.jininsa.robot.helper;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

public class Env {
  static final public int REQ_CODE_ASK_TO_TURN_ON_BLUETOOTH_ADAPTER = 0xFE01;
  static final public int REQ_CODE_ASK_TO_CONNECT_NETWORK = 0xFE02;
             
  static final public SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US);
  static final public String LINE_SEPARATOR = System.getProperty("line.separator");
  static final public Pattern COMMAND_PATTERN = Pattern.compile("\\[(.+)/(\\d+)/(.+)\\]");

  static final public boolean DEFAULT_BOOLEAN = Boolean.FALSE;
  static final public float DEFAULT_FLOAT = Float.NaN;
  static final public int DEFAULT_INT = Integer.MIN_VALUE;
  static final public long DEFAULT_LONG = Long.MIN_VALUE;
  static final public Long DEFAULT_ID = Long.MIN_VALUE;
  static final public Set<String> DEFAULT_SET = Collections.emptySet();
  static final public byte[] EMPTY_BYTE_ARRAY = new byte[0];
  static final public String DEFAULT_STRING = "";
  static final public String EMPTY_STRING = "";

  static private SharedPreferences S;

//  static private void apply(SharedPreferences.Editor editor) {
//    S.edit().apply();
//  }

  static public void setFloat(String key, float value) {
    SharedPreferences.Editor e = S.edit();
    e.putFloat(key, value).apply();
  }

  static public void setString(String key, String value) {
    SharedPreferences.Editor e = S.edit();
    e.putString(key, value).apply();
  }

  static public void setInt(String key, int value) {
    SharedPreferences.Editor e = S.edit();
    e.putInt(key, value).apply();
  }

  static public void setBoolean(String key, boolean value) {
    SharedPreferences.Editor e = S.edit();
    e.putBoolean(key, value).apply();
  }

  static public void setLong(String key, long value) {
    SharedPreferences.Editor e = S.edit();
    e.putLong(key, value).apply();
  }

  static public void setStrings(String key, Set<String> s) {
    SharedPreferences.Editor e = S.edit();
    e.putStringSet(key, s).apply();
  }

  static public String getString(String key) {
    return S.getString(key, DEFAULT_STRING);
  }

  static public String getString(String key, String value) {
    return S.getString(key, value);
  }

  static public boolean getBoolean(String key) {
    return S.getBoolean(key, DEFAULT_BOOLEAN);
  }

  static public boolean getBoolean(String key, boolean value) {
    return S.getBoolean(key, value);
  }

  static public int getInt(String key) {
    return S.getInt(key, DEFAULT_INT);
  }

  static public int getInt(String key, int value) {
    return S.getInt(key, value);
  }

  static public long getLong(String key) {
    return S.getLong(key, DEFAULT_LONG);
  }

  static public long getLong(String key, long value) {
    return S.getLong(key, value);
  }

  static public float getFloat(String key) {
    return S.getFloat(key, DEFAULT_FLOAT);
  }

  static public float getFlaot(String key, float value) {
    return S.getFloat(key, value);
  }

  static public Set<String> getStrings(String key) {
    return S.getStringSet(key, DEFAULT_SET);
  }

  static public Set<String> getStrings(String key, Set<String> values) {
    return S.getStringSet(key, values);
  }

  static public void init(Context ctx) {
    if (S == null) {
      S = ctx.getSharedPreferences(ctx.getPackageName(), Context.MODE_PRIVATE);
    }
  }
}
