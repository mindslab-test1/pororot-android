package com.jininsa.robot.helper;

import android.media.AudioRecord;

import com.elvishew.xlog.XLog;
import com.jakewharton.rxrelay2.PublishRelay;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import io.reactivex.schedulers.Schedulers;

public class WavWriter {
  public interface Recordable {
    int readAudio(byte[] buffer);

    int getRecordingBlockSize();
  }

  private static class Command {
    static final Command WRITE = new Command();
    File file;
    byte[] pcm;
  }

  private final static String CHUNK_ID = "RIFF";
  private final static String FORMAT = "WAVE";
  private final static String SUBCHUNK1_ID = "fmt ";
  private final static int SUBCHUNK1_SIZE = Integer.reverseBytes(16);
  private final static short AUDIO_FORMAT = Short.reverseBytes((short) 1);
  private final static String SUBCHUNK2_ID = "accStates";
  private final static int CHUNK_SIZE_POS = 4;
  private final static int CHUNK_SIZE_TOTAL = 36;
  private final static int SUBCHUNK2_SIZE_POS = 40;
  private final short numChannels;
  private final int sampleRate;
  private final int byteRate;
  private final short blockAlign;
  private final short bitPerSample;
  private int size;
  private RandomAccessFile fileWriter;
  private boolean isRecording = false;
  private PublishRelay<Command> writeCommandRunner = PublishRelay.create();

  public WavWriter(short numChannels, int sampleRate, short bit) {
    this.numChannels = numChannels;
    this.sampleRate = sampleRate;
    this.bitPerSample = bit;
    this.byteRate = this.sampleRate * this.numChannels * this.bitPerSample / 8;
    this.blockAlign = (short) (this.numChannels * this.bitPerSample / 8);

    this.writeCommandRunner.observeOn(Schedulers.newThread()).subscribe((command) ->
    {
      try {
        this.prepare(command.file);
        this.size = command.pcm.length;
        this.fileWriter.write(command.pcm);
        this.complete();
      }
      finally {
        try {
          this.fileWriter.close();
        }
        catch (IOException e) {
          XLog.e(e.getMessage(), e);
        }
      }
    });
  }

  public void write(File file, ByteArrayOutputStream baos) throws IOException {
    this.write(file, baos.toByteArray());
  }

  public void write(File file, byte[] pcm) throws IOException {
    Command.WRITE.file = file;
    Command.WRITE.pcm = pcm;
    this.writeCommandRunner.accept(Command.WRITE);
  }


  private void prepare(final File file) throws IOException {
    this.fileWriter = new RandomAccessFile(file, "rw");
    this.fileWriter.setLength(0); // Set file length to 0, to prevent unexpected behavior in case the file already existed
    this.fileWriter.writeBytes(CHUNK_ID);
    this.fileWriter.writeInt(0); // Final file size not known yet, write 0
    this.fileWriter.writeBytes(FORMAT);
    this.fileWriter.writeBytes(SUBCHUNK1_ID);
    this.fileWriter.writeInt(SUBCHUNK1_SIZE); // Sub-chunk size, 16 for PCM
    this.fileWriter.writeShort(AUDIO_FORMAT); // AudioFormat, 1 for PCM
    this.fileWriter.writeShort(Short.reverseBytes(this.numChannels));// Number of channels, 1 for mono, 2 for stereo
    this.fileWriter.writeInt(Integer.reverseBytes(this.sampleRate)); // Sample rate
    this.fileWriter.writeInt(Integer.reverseBytes(this.byteRate)); // Byte rate, SampleRate*NumberOfChannels*BitsPerSample/8
    this.fileWriter.writeShort(Short.reverseBytes(this.blockAlign)); // Block align, NumberOfChannels*BitsPerSample/8
    this.fileWriter.writeShort(Short.reverseBytes(this.bitPerSample)); // Bits per sample
    this.fileWriter.writeBytes(SUBCHUNK2_ID);
    this.fileWriter.writeInt(0); // Data chunk size not known yet, write 0
  }

  private void complete() throws IOException {
    this.fileWriter.seek(CHUNK_SIZE_POS);
    this.fileWriter.writeInt(Integer.reverseBytes(CHUNK_SIZE_TOTAL + this.size));
    this.fileWriter.seek(SUBCHUNK2_SIZE_POS);
    this.fileWriter.writeInt(Integer.reverseBytes(this.size));
  }

  public void finish() {
    this.isRecording = false;
  }

  public void record(File file, Recordable recorder) {
    try {
      this.prepare(file);
      this.isRecording = true;
      byte[] buffer = new byte[recorder.getRecordingBlockSize()];

      while (this.isRecording) {
        int n = recorder.readAudio(buffer);
        if (n != AudioRecord.ERROR_INVALID_OPERATION && n != AudioRecord.ERROR_BAD_VALUE && n != AudioRecord.ERROR_DEAD_OBJECT) {
          this.fileWriter.write(buffer);
          this.size += n;
        }
        else {
          this.isRecording = false;
        }
      }
      this.complete();
    }
    catch (IOException e) {
      XLog.e(e.getMessage(), e);
    }
    finally {
      if (this.fileWriter != null) {
        try {
          this.fileWriter.close();
        }
        catch (IOException e) {
          XLog.e(e.getMessage(), e);
        }
      }
    }
  }
}
