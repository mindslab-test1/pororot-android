package com.jininsa.robot.helper;

import android.content.res.AssetManager;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Assets
{
  public static void copy(AssetManager manager, String assetName, File dest)
  {
    try (InputStream is = manager.open(assetName); FileOutputStream fos = new FileOutputStream(dest))
    {
      byte[] buffer = new byte[1024];
      int read = 0;
      while ((read = is.read(buffer)) != -1)
      {
        fos.write(buffer, 0, read);
      }
    }
    catch (IOException e)
    {
      XLog.e(e.getMessage(), e);
    }
  }

  public static void copyAll(AssetManager manager, String assetFolderName, File destPath)
  {
    try
    {
      String[] assets = manager.list(assetFolderName);

      Stream.of(assets).forEach((asset) ->
      {
        String assetName = String.format("%s/%s", assetFolderName, asset);
        File dest = new File(destPath, asset);
        if (!dest.exists())
        {
          copy(manager, assetName, dest);
        }
      });
    }
    catch (IOException e)
    {
      XLog.e(e.getMessage(), e);
    }
  }

  private Assets() {}
}
