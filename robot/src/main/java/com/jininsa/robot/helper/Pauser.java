package com.jininsa.robot.helper;

import com.elvishew.xlog.XLog;

public final class Pauser {

  private static final long DEFAULT_PAUSE_TIME_MILIS = 400L;
  private static final Runnable NOOP = () -> {};

  static public void pause() {
    Pauser.pauseAndRun(Pauser.NOOP, Pauser.DEFAULT_PAUSE_TIME_MILIS);
  }

  static public void pause(long msec) {
    Pauser.pauseAndRun(Pauser.NOOP, msec);
  }

  static public void pauseAndRun(Runnable action) {
    Pauser.pauseAndRun(action, Pauser.DEFAULT_PAUSE_TIME_MILIS);
  }

  static public void pauseAndRun(Runnable action, long msec) {
    try {
      Thread.sleep(msec);
    }
    catch (InterruptedException e) {
      XLog.e(e);
    }
    action.run();
  }

  private Pauser() {}
}
