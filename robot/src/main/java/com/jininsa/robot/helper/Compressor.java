package com.jininsa.robot.helper;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DeflaterOutputStream;

/**
 * Spring bean
 */
public class Compressor {
  public static byte[] compress(ByteArrayOutputStream baos) {
    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
         DeflaterOutputStream dfos = new DeflaterOutputStream(out)) {
      baos.writeTo(dfos);
      dfos.flush();
      dfos.finish();
      return out.toByteArray();
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return Env.EMPTY_BYTE_ARRAY;
  }

  private Compressor() {}
}
