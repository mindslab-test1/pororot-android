package com.jininsa.robot.helper;

import android.util.ArrayMap;
import android.util.SparseArray;

import com.annimon.stream.Stream;

import java.util.Map;

import io.reactivex.disposables.Disposable;

public final class Subscriptions {
  private static final SparseArray<Map<String, Disposable>> SUBS = new SparseArray<>();

  public static void add(int key, String name, Disposable subscription) {
    Subscriptions.remove(key, name);
    Map<String, Disposable> map = SUBS.get(key);
    if (map == null) {
      map = new ArrayMap<>();
      SUBS.put(key, map);
    }
    map.put(name, subscription);
  }

  public static void removeAllOf(int key) {
    Map<String, Disposable> map = SUBS.get(key);
    if (map != null) {
      Stream.of(map.values()).forEach(Disposable::dispose);
      map.clear();
      SUBS.delete(key);
    }
  }

  public static void remove(int key, String name) {
    Map<String, Disposable> map = SUBS.get(key);
    if (map != null) {
      Disposable subscription = map.get(name);
      if (subscription != null) {
        subscription.dispose();
      }
      map.remove(name);
    }
  }

  private Subscriptions() {}
}
