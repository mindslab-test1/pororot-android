package com.jininsa.robot.helper;

import com.google.gson.Gson;

public class Parser {
  private Parser() {}
  private static final Gson gson = new Gson();
//  private static final Map<String, String> map = new HashMap<>(0);

  static public <T> T parse(String jsonString, Class<T> clazz) {
    return gson.fromJson(jsonString, clazz);
  }
}
