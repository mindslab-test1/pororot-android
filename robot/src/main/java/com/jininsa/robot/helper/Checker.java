package com.jininsa.robot.helper;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.annimon.stream.Stream;
import com.google.common.collect.Lists;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.connector.Adapter;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Checker {
  public enum Subject {
    PERMISSION, BLUETOOTH_ADAPTER, NETWORK
  }

  public interface CheckRequired extends Permission.PermissionRequired {
    @RequiresApi(api = Build.VERSION_CODES.M)
    List<PermissionCheck.PermissionCheckRequest> getRequiredPermissions();
    @RequiresApi(api = Build.VERSION_CODES.M)
    void explainPermissionRationale(Permission permission);
    @RequiresApi(api = Build.VERSION_CODES.M)
    void askToGrantPermission(Permission permission);
    void askToTurnOnBluetoothAdapter();
    void warnNoBluetoothAdapter();
    void askToConnectNetwork();
    void doBeforeChecking();
    void doAfterChecking();
    Set<Subject> getCheckSubjects();
  }

  static abstract class Check {
    final CheckRequired client;
    private final Checker checker;
    private final Subject subject;

    private Check(final Checker checker, final Subject subject) {
      this.checker = checker;
      this.client = this.checker.client;
      this.subject = subject;
    }

    abstract void begin();

    void finish() {
      // !!!: 액티비티를 시작할 때 검사하는 경우라면 다음 검사로 넘어간다. 이벤트가 발생해서 검사하는 경우는 다음 검사로 넘어가지 않고 그냥 끝낸다
      if (!this.checker.isCheckCompleted) {
        int step = Stream.of(this.checker.checks).findIndexed((index, check) -> this.subject == check.subject).get().getFirst();
        this.checker.finish(step);
      }
      else {
        this.checker.complete();
      }
    }

    @Override
    public String toString() {
      return this.getClass().getSimpleName();
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  public static class PermissionCheck extends Check {
    public static class PermissionCheckRequest {
      public final Permission permission;
      private boolean isFirstRequest = true;

      public PermissionCheckRequest(final Permission p) {
        this.permission = p;
      }
    }

    private Iterator<PermissionCheckRequest> iterator;

    private PermissionCheck(final Checker checker) {
      super(checker, Subject.PERMISSION);
      this.iterator = this.client.getRequiredPermissions().iterator();
    }

    @Override
    void begin() {
      if (this.iterator.hasNext()) {
        PermissionCheckRequest request = this.iterator.next();
        Permission permission = request.permission;
        boolean isGranted = permission.isGranted(this.client);
        if (isGranted) {
          this.begin();
        }
        else {
          if (request.isFirstRequest) {
            request.isFirstRequest = false;
            this.client.explainPermissionRationale(permission);
          }
          else {
            permission.request(this.client);
          }
        }
      }
      else {
        this.finish();
      }
    }
  }

  public static class BluetoothAdapterCheck extends Check {
    private BluetoothAdapterCheck(final Checker checker) {
      super(checker, Subject.BLUETOOTH_ADAPTER);
    }

    @Override
    void begin() {
      Adapter adapter = Adapter.get();
      if (adapter.isAvailable()) {
        if (adapter.isOn()) {
          this.finish();
        }
        else {
          this.client.askToTurnOnBluetoothAdapter();
        }
      }
      else {
        this.client.warnNoBluetoothAdapter();
      }
    }
  }

  public static class NetworkCheck extends Check {
    private NetworkCheck(final Checker checker) {
      super(checker, Subject.NETWORK);
    }

    @Override
    void begin() {
      if (Connectivity.isConnected()) {
        this.finish();
      }
      else {
        this.client.askToConnectNetwork();
      }
    }
  }

  static class Action {
    final Subject subject;

    private Action(final Subject subject) {
      this.subject = subject;
    }

    @Override
    public String toString() {
      return String.format(
        "{Action: {subject: %s}}",
        this.subject);
    }
  }

  public static class Begin extends Action {
    public static final Begin PERMISSION = new Begin(Subject.PERMISSION);
    public static final Begin BLUETOOTH_ADAPTER = new Begin(Subject.BLUETOOTH_ADAPTER);
    public static final Begin NETWORK = new Begin(Subject.NETWORK);

    private Begin(final Subject subject) {
      super(subject);
    }
  }

  public static class Finish extends Action {
    public static final Finish PERMISSION = new Finish(Subject.PERMISSION);
    public static final Finish BLUETOOTH_ADAPTER = new Finish(Subject.BLUETOOTH_ADAPTER);
    public static final Finish NETWORK = new Finish(Subject.NETWORK);

    private Finish(final Subject subject) {
      super(subject);
    }
  }

  static private final String SUBSCRIPTION_CHECKER_ACTIONS = "92OonFY3wJPRb8mB";
  public final PublishRelay<Action> actions = PublishRelay.create();
  private final List<Check> checks = Lists.newArrayList();
  private CheckRequired client;
  private boolean isCheckCompleted = false;

  public Checker(Set<Subject> subjects, CheckRequired client) {
    this.client = client;
    Stream.of(subjects).forEach(
      subject -> {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          if (subject == Subject.PERMISSION) {
            this.checks.add(new PermissionCheck(this));
          }
        }

        if (subject == Subject.BLUETOOTH_ADAPTER) {
          this.checks.add(new BluetoothAdapterCheck(this));
        }

        if (subject == Subject.NETWORK) {
          this.checks.add(new NetworkCheck(this));
        }
      });

    Subscriptions.add(this.hashCode(), Checker.SUBSCRIPTION_CHECKER_ACTIONS,
      this.actions.subscribe(action -> {
        int step = Stream.of(this.checks).findIndexed((index, check) -> action.subject == check.subject).get().getFirst();
        if (action instanceof Begin) {
          this.begin(step);
        }
        if (action instanceof Finish) {
          this.finish(step);
        }
      }));
  }

  public void begin() {
    this.begin(0);
  }

  private void begin(int step) {
    this.checks.get(step).begin();
  }

  private void finish(int step) {
    step++;
    if (step == this.checks.size()) {
      this.complete();
    }
    else {
      this.begin(step);
    }
  }

  // !!! 상태: isChecking == true, index == checks.size()
  // @DebugLog
  private void complete() {
    this.isCheckCompleted = true;
    Subscriptions.remove(this.hashCode(), Checker.SUBSCRIPTION_CHECKER_ACTIONS);
    this.client.doAfterChecking();
  }

  public boolean isCheckCompleted() {
    return this.isCheckCompleted;
  }
}
