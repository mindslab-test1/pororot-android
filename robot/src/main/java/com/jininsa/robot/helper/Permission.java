/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.jininsa.robot.helper;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

/**
 * Utility class for permissions.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public enum Permission {
  READ_CALENDAR(0xFF00, Manifest.permission.READ_CALENDAR, "일정 읽기"),
  WRITE_CALENDAR(0xFF01, Manifest.permission.WRITE_CALENDAR, "일정 쓰기"),
  CAMERA(0xFF10, Manifest.permission.CAMERA, "카메라"),
  READ_CONTACTS(0xFF20, Manifest.permission.READ_CONTACTS, "연락처 읽기"),
  WRITE_CONTACTS(0xFF21, Manifest.permission.WRITE_CONTACTS, "연락처 쓰기"),
  GET_ACCOUNTS(0xFF30, Manifest.permission.GET_ACCOUNTS, "계정 가져오기"),
//  ACCESS_FINE_LOCATION(0xFF41, Manifest.permission.ACCESS_FINE_LOCATION, "상세 위치 접금"),
//  ACCESS_COARSE_LOCATION(0xFF42, Manifest.permission.ACCESS_COARSE_LOCATION, "대략 위치 접근"),
  RECORD_AUDIO(0xFF50, Manifest.permission.RECORD_AUDIO, "녹음"),
  READ_PHONE_STATE(0xFF60, Manifest.permission.READ_PHONE_STATE, "전화 상태 읽기"),
  @RequiresApi(api = Build.VERSION_CODES.O) READ_PHONE_NUMBERS(0xFF61, Manifest.permission.READ_PHONE_NUMBERS, "전화 상태 읽기"),
  CALL_PHONE(0xFF62, Manifest.permission.CALL_PHONE, "전화 걸기"),
  READ_CALL_LOG(0xFF63, Manifest.permission.READ_CALL_LOG, "통화 기록 읽기"),
  WRITE_CALL_LOG(0xFF64, Manifest.permission.WRITE_CALL_LOG, "통화 기록 쓰기"),
  ADD_VOICE_MAIL(0xFF65, Manifest.permission.ADD_VOICEMAIL, "음성메세지 보내기"),
  USE_SIP(0xFF66, Manifest.permission.USE_SIP, "SIP 이용하기"),
  PROCESS_OUTGOING_CALLS(0xFF67, Manifest.permission.PROCESS_OUTGOING_CALLS, "착신 번호 접근"),
  SEND_SMS(0xFF70, Manifest.permission.SEND_SMS, "문자 보내기"),
  RECEIVE_SMS(0xFF71, Manifest.permission.RECEIVE_SMS, "문자 받기"),
  READ_SMS(0xFF72, Manifest.permission.READ_SMS, "문자 읽기"),
  RECEIVE_WAP_PUSH(0xFF73, Manifest.permission.RECEIVE_WAP_PUSH, "WAP 푸시 받기"),
  RECEIVE_MMS(0xFF74, Manifest.permission.RECEIVE_MMS, "MMS 받기"),
  BODY_SENSORS(0xF80, Manifest.permission.BODY_SENSORS, "생체 센서 접근"),
  READ_EXTERNAL_STORAGE(0xFF90, Manifest.permission.READ_EXTERNAL_STORAGE, "파일 읽기"),
  WRITE_EXTERNAL_STORAGE(0xFF91, Manifest.permission.WRITE_EXTERNAL_STORAGE, "파일 쓰기"),
  INVALID(0xFFFF, "INVALID", "INVALID");

  public interface PermissionRequired {
    int checkSelfPermission(String what);
    void requestPermissions(String[] whats, int requestCode);
  }

  public static boolean isRequestCode(int reqCode) {
    Permission[] permissions = Permission.values();
    return Stream.of(permissions).filter(permission -> permission.requestCode == reqCode).findFirst().isPresent();
  }

  public static Permission from(int reqCode) {
    Permission[] permissions = Permission.values();
    Optional<Permission> result = Stream.of(permissions).filter(permission -> permission.requestCode == reqCode).findFirst();
    return result.isPresent() ? result.get() : Permission.INVALID;
  }

  final public int requestCode;
  final public String what;
  final public String desc;

  Permission(int requestCode, String what, String desc) {
    this.requestCode = requestCode;
    this.what = what;
    this.desc = desc;
  }

  @Override
  public String toString() {
    return String.format(
      "{Permission: {requestCode: %s, what: %s}}",
      this.requestCode,
      this.what);
  }

  public boolean isGranted(final PermissionRequired permissionRequired) {
    return permissionRequired.checkSelfPermission(this.what) == PackageManager.PERMISSION_GRANTED;
  }

  public void requestOrGo(final PermissionRequired permissionRequired, final Runnable actionForGrantedPermission) {
    if (!this.isGranted(permissionRequired)) {
      this.request(permissionRequired);
    }
    else {
      actionForGrantedPermission.run();
    }
  }

  public void request(final PermissionRequired permissionRequired) {
    permissionRequired.requestPermissions(new String[]{this.what}, this.requestCode);
  }
}
