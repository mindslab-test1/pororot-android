package com.jininsa.robot.helper;

public class Koreans {
  private static final int CHARACTER_COUNT_PER_SYLLABLE = 3;
  private static final int KOR_CHARACTER_BASE = 0xAC00;
  private static final int INDEX_F0 = 0; // 초성
  private static final int INDEX_M0 = 1; // 중성1
  private static final int INDEX_L0 = 2; // 종성1

  private static final String YGA = "이가";
  private static final String GA = "가";
  private static final String Y = "이";

  private static final String YNN = "이는";
  private static final String NN = "는";
  private static final String N = "은";

  private static final String YRL = "이를";
  private static final String RL = "를";
  private static final String L = "을";

  private static final String YNE = "이네";
  private static final String NE = "네";

  /**
   * codePoint에 해당하는 문자가 종성으로 끝나면 true.
   * '가' => false, '강' => true
   *
   * @param codePoint
   * @return
   */
  public static boolean hasLastConsonant(final int codePoint) {
    int[] split = splitSyllable(codePoint);
    return split[INDEX_L0] != 0;
  }

  public static boolean hasLastConsonant(final CharSequence word) {
    int codePoint = Character.codePointAt(word, word.length() - 1);
    return hasLastConsonant(codePoint);
  }

  /**
   * 이가/가
   * 길동 => 길동이가, 영희 => 영희가
   *
   * @param word
   * @return
   */
  public static String attachYgaOrGa(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? YGA : GA);
  }

  /**
   * 이/가
   * 사람 => 사람이, 내 => 내가
   *
   * @param word
   * @return
   */
  public static String attachYOrGa(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? Y : GA);
  }

  /**
   * 이는/는
   * 길동 => 길동이는, 영희 => 영희는
   *
   * @param word
   * @return
   */
  public static String attachYnnOrNn(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? YNN : NN);
  }

  /**
   * 은/는
   * 사람 => 사람은, 나 => 나는
   *
   * @param word
   * @return
   */
  public static String attachNOrNn(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? N : NN);
  }

  /**
   * 이를/를
   * 길동 => 길동이를, 영희 => 영희를
   *
   * @param word
   * @return
   */
  public static String attachYrlOrRl(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? YRL : RL);
  }

  /**
   * 을/를
   * 사람 => 사람을, 나 => 나를
   *
   * @param word
   * @return
   */
  public static String attachLOrRl(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? L : RL);
  }

  public static String attachY(final CharSequence word){
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? Y : "");
  }

  /**
   * 이네/네
   * 길동 => 길동이네, 영희 => 영희네
   *
   * @param word
   * @return
   */
  public static String attachYneOrNe(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return word + (hasLastConsonant(word) ? YNE : NE);
  }

  public static String yneOrNe(final CharSequence word) {
    if (word.length() <= 0) {
      return word.toString();
    }
    return (hasLastConsonant(word) ? YNE : NE); }

  public static int[] splitSyllable(final int codePoint) {
    int[] indexes = new int[CHARACTER_COUNT_PER_SYLLABLE];
    int zeroBasedLastTextCodePoint = codePoint - KOR_CHARACTER_BASE;
    indexes[INDEX_F0] = zeroBasedLastTextCodePoint / 588;
    indexes[INDEX_M0] = (zeroBasedLastTextCodePoint - 588 * indexes[INDEX_F0]) / 28;
    indexes[INDEX_L0] = (zeroBasedLastTextCodePoint - 588 * indexes[INDEX_F0] - 28 * indexes[INDEX_M0]);
    return indexes;
  }
}
