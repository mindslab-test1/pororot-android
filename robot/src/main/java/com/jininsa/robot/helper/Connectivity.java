package com.jininsa.robot.helper;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jakewharton.rxrelay2.BehaviorRelay;
import com.jininsa.robot.app.ManageableBroadcastReceiver;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

public class Connectivity {

  public enum State {
    CONNECTED, DISCONNECTED
  }

  public static final BehaviorRelay<State> stateRelay = BehaviorRelay.create();

  public static class ChangeReceiver extends ManageableBroadcastReceiver {

    private State previousState = Connectivity.getState();

    public ChangeReceiver(final Context context) {
      super(context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      if (ConnectivityManager.CONNECTIVITY_ACTION.equals(action)) {
        boolean isConnected = !intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
        State state = isConnected ? State.CONNECTED : State.DISCONNECTED;
        XLog.d("[%s] %s-->%s", action, this.previousState, state);
        if (state != this.previousState) {
          this.previousState = state;
          boolean same = state == Connectivity.getState();
          if (same) {
            Connectivity.stateRelay.accept(state);
          }
        }
      }
    }

    @Override
    protected IntentFilter intentFilter() {
      IntentFilter filter = new IntentFilter();
      filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
      return filter;
    }
  }

  private static final Integer[] ALL = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};

  public enum Type {
    NONE(-1), WIFI(ConnectivityManager.TYPE_WIFI), MOBILE(ConnectivityManager.TYPE_MOBILE);
    final Integer connectivityType;
    Type(final Integer connectivityType) {
      this.connectivityType = connectivityType;
    }

    static Type from(final Integer connectivityType) {
      switch (connectivityType) {
        case ConnectivityManager.TYPE_WIFI : return WIFI;
        case ConnectivityManager.TYPE_MOBILE : return MOBILE;
        default : return NONE;
      }
    }
  }

  private static WeakReference<Context> contextWrapper;

  public static void init(Context context) {
    contextWrapper = new WeakReference<>(context.getApplicationContext());
  }

  public static State getState() {
    return isConnected() ? State.CONNECTED : State.DISCONNECTED;
  }

  public static boolean isConnected() {
    return isConnected(Type.values());
  }

  public static boolean isConnected(Type... types) {
    ConnectivityManager cm = Connectivity.getConnectivityManager();
    if (cm != null) {
      NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
      return activeNetworkInfo != null
        && activeNetworkInfo.isConnectedOrConnecting()
        && Stream.of(types).filter(type -> type.connectivityType == activeNetworkInfo.getType()).findFirst().isPresent();
    }
    return false;
  }

  public static Type getType() {
    if (isMobileConnected()) {
      return Type.MOBILE;
    }
    else if (isWifiConnected()) {
      return Type.WIFI;
    }

    return Type.NONE;
  }

  public static List<Type> getAllConnectedNetwork() {
    List<Type> types = Lists.newArrayList();
    if (isMobileConnected()) {
      types.add(Type.MOBILE);
    }
    if (isWifiConnected()) {
      types.add(Type.WIFI);
    }
    return types.isEmpty() ? Collections.emptyList() : types;
  }

  private static ConnectivityManager getConnectivityManager() {
    Context ctx = contextWrapper.get();
    return (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
  }

  public static boolean isMobileConnected() {
    return isConnected(Type.MOBILE);
  }

  public static boolean isWifiConnected() {
    return isConnected(Type.WIFI);
  }

  public static void turnOn() {
    Context ctx = contextWrapper.get();
    ComponentName receiver = new ComponentName(ctx, ChangeReceiver.class);
    PackageManager pm = ctx.getPackageManager();
    pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
  }

  public static void turnOff() {
    Context ctx = contextWrapper.get();
    ComponentName receiver = new ComponentName(ctx, ChangeReceiver.class);
    PackageManager pm = ctx.getPackageManager();
    pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
  }

  private Connectivity() {}
}