package com.jininsa.robot.model;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.Date;

/**
 * Created by architect on 2017. 5. 8..
 */

public class ChatMessage implements IMessage,
  MessageContentType.Image, /*this is for default image messages implementation*/
  MessageContentType /*and this one is for custom content type (in this case - sound set)*/
{

  public static class Image
  {
    private String url;
    public Image(String url)
    {
      this.url = url;
    }
  }

  public static class Sound
  {
    final private String url;
    final private int duration;

    public Sound(String url, int duration)
    {
      this.url = url;
      this.duration = duration;
    }

    public String getUrl()
    {
      return url;
    }

    public int getDuration()
    {
      return duration;
    }
  }

  final private String id;
  final private String text;
  final private Date createdAt;
  final private Talker talker;
  final private Image image;
  final private Sound sound;

  public ChatMessage(String id, Talker talker, String text)
  {
    this(id, talker, text, new Date(), null, null);
  }

  public ChatMessage(String id, Talker talker, String text, Date createdAt, Image image, Sound sound)
  {
    this.id = id;
    this.text = text;
    this.talker = talker;
    this.createdAt = createdAt;
    this.image = image;
    this.sound = sound;
  }

  @Override
  public String getId()
  {
    return id;
  }

  @Override
  public String getText()
  {
    return text;
  }

  public Talker getTalker() {
    return this.talker;
  }

//  public void setText(String text)
//  {
//    this.text = text;
//  }

  @Override
  public Date getCreatedAt()
  {
    return createdAt;
  }

//  public void setCreatedAt(Date createdAt)
//  {
//    this.createdAt = createdAt;
//  }

  @Override
  public Talker getUser()
  {
    return this.talker;
  }

  @Override
  public String getImageUrl()
  {
    return image == null ? null : image.url;
  }

  public Sound getSound()
  {
    return sound;
  }

//  public void setVoice(Sound sound)
//  {
//    this.sound = sound;
//  }

  public String getStatus()
  {
    return "Sent";
  }

//  public void setImage(Image image)
//  {
//    this.image = image;
//  }
}
