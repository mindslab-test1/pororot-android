package com.jininsa.robot.model.robot;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.elvishew.xlog.XLog;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.common.collect.Maps;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jakewharton.rxrelay2.PublishRelay;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Strings;
import com.jininsa.robot.model.ChatMessage;
import com.jininsa.robot.model.Talker;
import com.jininsa.robot.model.party.Family;
import com.jininsa.robot.part.Brain;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class Robot {

  public enum LifeCycle {
    NONE,     // 기계 없음
    UNBORN,   // 미등록
    BORN,     // 하드웨어 테스트 마친 날 상태
    SELLING,  // 식별자 부여되어 판매 가능한 상태
    RETURNED, // 반품
    OPEN,     // 개통
    ADOPTED,   // 가족에 속함
    CLOSED,   // 중지
    TRANSFERRING,   // 양도중
    DISPOSED  // 폐기
  }

  public enum State {
    NONE,
    FOUND,
    STARTING,
    SCANNING,
    PAIRING,
    LINKING,
    LINKED,
    MOUTH_READY,
    EAR_READY,
    SERIAL_PORT_READY,
    READY,
    FAILED,
    TURNING_ON_HEADSET,
    OFF
  }

  public enum LedState {
    ON,
    OFF,
    RESET
  }

//  public static class RawAccData {
//    public final int px;
//    public final int cx;
//    public final int dx;
//    public final int py;
//    public final int cy;
//    public final int dy;
//    public final int pz;
//    public final int cz;
//    public final int dz;
//    public final int ps;
//    public final int cs;
//    public final int ds;
//
//    public RawAccData(String payload) {
//      String[] v = payload.split(":");
//      this.px = Integer.valueOf(v[0]);
//      this.py = Integer.valueOf(v[1]);
//      this.pz = Integer.valueOf(v[2]);
//      this.ps = Integer.valueOf(v[3]);
//      this.cx = Integer.valueOf(v[4]);
//      this.cy = Integer.valueOf(v[5]);
//      this.cz = Integer.valueOf(v[6]);
//      this.cs = Integer.valueOf(v[7]);
//      this.dx = this.cx - this.px;
//      this.dy = this.cy - this.py;
//      this.dz = this.cz - this.pz;
//      this.ds = this.cs - this.ps;
//    }
//  }

  private static Robot activeRobot;

  public static void setSelectedRobot(final Robot selectedRobot) {
    Robot.selectedRobot = selectedRobot;
  }

  public static Robot getSelectedRobot() {
    return Robot.selectedRobot;
  }

  public static Robot get() { return Robot.activeRobot; }

  public final PublishRelay<ChatMessage> chats = PublishRelay.create();
  public final PublishRelay<State> states = PublishRelay.create();
  public final PublishRelay<LedState> ledStates = PublishRelay.create();
  public final PublishRelay<Boolean> fdStates = PublishRelay.create();
  public final PublishRelay<Boolean> accStates = PublishRelay.create();

  private ServiceConnection serviceConnection;

  @Expose
  @SerializedName("id")
  private Long id;
  @Expose
  @SerializedName("mac")
  private String mac;
  @Expose
  @SerializedName("uuid")
  private String uuid;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("lifeCycle")
  private LifeCycle lifeCycle;
  @Expose
  @SerializedName("familyId")
  private Long familyId;
  @Expose
  @SerializedName("robotTypeId")
  private Long robotTypeId = 1L;
  @Expose
  @SerializedName("family")
  private Family family;
  @Expose
  @SerializedName("robotType")
  private RobotType robotType;
  @Expose
  @SerializedName("deviceName")
  private String deviceName;

  private int proximity;
  private State state;
  private Brain brain;
  private static Robot selectedRobot;

  // !!! Retrofit이 이용하는 생성자
  private Robot() {
    this.lifeCycle = LifeCycle.NONE;
    this.state = State.NONE;

    this.serviceConnection = new ServiceConnection() {
      @Override
      public void onServiceConnected(final ComponentName name, final IBinder service) {
        XLog.i("===CONNECTED===");
        Robot.this.brain = ((Brain.Binder) service).getConnector();
        Robot.this.brain.start();
      }
      // !!! https://developer.android.com/reference/android/content/ServiceConnection.html#onServiceDisconnected(android.content.ComponentName)
      // !!! 프로세스가 크래쉬될 때 호출된다.
      @Override
      public void onServiceDisconnected(final ComponentName name) {
        XLog.i("===DISCONNECTED===");
        Robot.this.brain = null;
      }
    };
  }

  public Robot(final String mac, final String deviceName, final int proximity) {
    this();
    this.mac = mac;
    this.deviceName = deviceName;
    this.proximity = proximity;
  }

  public Robot(final String mac, final int proximity) {
    this(mac, "POROROT(000001)", proximity);
  }

  public void refreshWith(final Robot newRobot) {
    this.id = newRobot.id;
    this.name = newRobot.name;
    this.uuid = newRobot.uuid;
    this.lifeCycle = newRobot.lifeCycle;
    this.familyId = newRobot.familyId;
    this.family = newRobot.family;
    this.robotTypeId = newRobot.robotTypeId;
    this.robotType = newRobot.robotType;
  }

  public Long getId() { return this.id; }

  public String getMac() { return this.mac; }

  public String getName() {
    return (this.name == null) ? Env.DEFAULT_STRING : this.name;
  }

  public Long getFamilyId() { return this.familyId; }

  public Family getFamily() { return this.family; }

  public Long getRobotTypeId() { return this.robotTypeId; }

  public RobotType getRobotType() { return robotType; }

  public LifeCycle getLifeCycle() { return this.lifeCycle; }

  public void setLifeCycle(final LifeCycle lifeCycle) {
    this.lifeCycle = lifeCycle;
  }

  public State getState() { return this.state; }

  public String getSeq() {
    if (Strings.isValidUuidV4(this.uuid)) {
      return this.uuid.substring(0, 8);
    }
    return Env.EMPTY_STRING;
  }

  @Override
  public String toString() {
    return String.format(
      "{Robot: {id: %s, mac: %s, uuid: %s, state: %s, name: %s, lifeCycle: %s, brain: %s, proximity: %s, familyId: %s, robotTypeId: %s, family: %s, robotType: %s}}",
      this.id,
      this.mac,
      this.uuid,
      this.state,
      this.name,
      this.lifeCycle,
      this.brain == null ? "unbound" : "bound",
      this.proximity,
      this.familyId,
      this.robotTypeId,
      this.family,
      this.robotType);
  }

  private void startBrain(final Context context) {
    // !!! 어떤 Context로 binding하느냐에 따라 Service 생명주기가 달라진다.
    // !!! ApplicationContext로 binding하면 앱이 종료될 때까지 서비스는 살아 있다.
    // !!! 유일한 binding context가 Activity라면 해당 Activity가 종료되면 서비스도 종료된다.
    Robot.activeRobot = this;
    if (this.brain == null) {
      context.getApplicationContext().bindService(new Intent(context.getApplicationContext(), Brain.class),
        this.serviceConnection, android.app.Service.BIND_AUTO_CREATE | android.app.Service.BIND_IMPORTANT);
    }
    else {
      this.brain.start();
    }
  }

  private void stopBrain() {
    this.brain.getApplicationContext().unbindService(this.serviceConnection);
    this.brain = null;
  }

  public void stop() {
    this.terminate();
    this.stopBrain();
  }

  public void start(final Context context) {
    XLog.w("%s selected=%s active=%s", this == Robot.activeRobot, this, Robot.activeRobot);
    // !!! 현재로봇과 새로봇이 같으면(같은 로봇을 선택했거나 현재로봇이 없었던 경우)

    if (Robot.activeRobot == this) {
      // !!! 현재로봇 상태가 준비완료면
      if (this.state == State.READY) {
        XLog.w("준비완료 상태: 활성로봇(%s/%s) 재사용", this.getUuid(), this.getState());
        // !!! 모든 파트 준비 완료 알림
        this.changeState(State.LINKED);
        this.changeState(State.EAR_READY);
        this.changeState(State.MOUTH_READY);
        this.changeState(State.SERIAL_PORT_READY);
        this.changeState(State.READY);
      }
      else if (this.state == State.LINKED) {
        this.changeState(State.LINKED);
      }
      // !!! 현재로봇 상태가 준비안됨이면
      else {
        XLog.w("준비안됨 상태: 현재로봇(%s/%s) 재시작", this.getMac(), this.getState());
        // !!! 준비안됨 상태 로봇은 문제가 발생한 시점에 자원들을 모두 정리해 놓아야 한다.
        this.startBrain(context.getApplicationContext());
      }
    }
    else {
      if (Robot.activeRobot != null) {
        XLog.w("[Robot.start] 현재로봇(%s/%s) 멈춤", Robot.activeRobot.getMac(), Robot.activeRobot.getState());
        Robot.activeRobot.stop();
      }
      XLog.w("[Robot.start] 새로봇(%s/%s) 시작", this.getMac(), this.getState());
      this.startBrain(context.getApplicationContext());
    }
  }

  public void terminate() {
    Robot.setActiveRobot(null);
    Robot.setSelectedRobot(null);
    if (this.brain != null) {
      this.brain.terminate();
    }
    this.changeState(State.FOUND);
  }

  private static void setActiveRobot(final Robot robot) {
    Robot.activeRobot = robot;
  }

  public void changeState(final State state) {
    XLog.v("[%s]-->[%s] %s(%s,%s)", this.state.name(), state.name(), this.uuid, this.mac, this.lifeCycle);
    if (this.state != state) {
      this.state = state;
      this.states.accept(this.state);
    }
  }

  public void setState(final State state) {
    this.state = state;
  }

  public Observable<Robot> retrieve() {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    return Service.api.getRobot(this.mac).subscribeOn(Schedulers.io());
  }

  public Observable<Robot> adoptToNewFamily(String token) {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    return this.adoptToNewFamily(token, this.getFamilyName(), this.getName());
  }

  public Observable<Robot> adoptToNewFamily(String token, String familyName, String robotName) {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    Map<String, String> body = Maps.newHashMap();
    body.put("id", String.valueOf(this.id));
    body.put("uuid", this.uuid);
    body.put("mac", this.mac);
    body.put("name", robotName);
    body.put("familyName", familyName);
    return Service.api.adoptToNewFamily(token, body).subscribeOn(Schedulers.io());
  }

  public Observable<Robot> adoptToOldFamily(String token) {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    return this.adoptToOldFamily(token, this.getName(), this.getFamily());
  }

  public Observable<Robot> adoptToOldFamily(String token, String robotName, Family family) {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    Map<String, String> body = Maps.newHashMap();
    body.put("id", String.valueOf(this.id));
    body.put("uuid", this.uuid);
    body.put("mac", this.mac);
    body.put("familyId", String.valueOf(family.getId()));
    body.put("name", robotName);
    body.put("familyName", family.getName());

    return Service.api.adoptToOldFamily(token, body).subscribeOn(Schedulers.io());
  }

  public Observable<Robot> changeNames(String token, String familyName, String robotName) {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    Map<String, String> body = Maps.newHashMap();
    body.put("id", String.valueOf(this.id));
    body.put("uuid", this.uuid);
    body.put("mac", this.mac);
    body.put("familyId", String.valueOf(this.familyId));
    body.put("name", robotName);
    body.put("familyName", familyName);

    return Service.api.changeNames(token, body).subscribeOn(Schedulers.io());
  }

  public Observable<Robot> assignUuid() {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    return Service.api.assignUuid(this).subscribeOn(Schedulers.io());
  }

  public Observable<Robot> register() {
    try (OutputStreamWriter request = new OutputStreamWriter(System.out)) {} catch (IOException ignored) {}
    return Service.api.register(this).subscribeOn(Schedulers.io());
  }

  public String getUuid() {
    return this.uuid;
  }

  public void setUuid(final String uudi) {
    this.uuid = uudi;
  }

  public void delegate(CharSequence text) {
    if (this.state == State.READY) {
      this.brain.speak(text.toString(), Talker.MOM);
    }
  }

  // @DebugLog
  public void say(final String words) {
    if (this.state == State.READY) {
      this.brain.speak(words, Talker.POROROT);
    }
  }

  public void startAcc() {
    this.brain.startAcc();
  }

  public void stopAcc() {
    this.brain.stopAcc();
  }

  public void turnOnRedLed(final int interval) {
    this.brain.turnOnRedLed(interval);
  }

  public void turnOnBlueLed(final int interval) {
    this.brain.turnOnBlueLed(interval);
  }

  public void turnOnBothLed(final int interval) {
    this.brain.turnOnBothLed(interval);
  }

  public void turnOffLed() {
    this.brain.turnOffLed();
  }

  public void resetLed() {
    this.brain.resetLed();
  }

  public void unsetFactoryDefault() {
    this.brain.unsetFactoryDefault();
  }

  public void setFactoryDefault() {
    this.brain.setFactoryDefault();
  }

  public void retrieveFactoryDefaultState() {
    this.brain.retrieveFactoryDefaultState();
  }

  public int getProximity() {
    return this.proximity;
  }

  public void setProximity(final int proximity) {
    this.proximity = proximity;
  }

  public void startHearing() {
    this.brain.openEar();
  }

  public void stopHearing() {
    this.brain.closeEar();
  }

  public void videoPlayByQuery(String query) {
    this.brain.videoPlayByQuery(query);
  }

  public void videoPlayById(String id) {
    this.brain.videoPlayById(id);
  }

  public void videoStop() {
    this.brain.videoStop();
  }

  public void startSinging(String code, String title) {
    this.brain.sing(code, title);
  }

  public void stopSinging() {
    this.brain.stopSinging();
  }

  public void pauseSinging() {
    this.brain.pauseSinging();
  }

  public void resumeSinging() {
    this.brain.resumeSinging();
  }

  public boolean isSinging() {
    return this.brain.isSinging();
  }

  public boolean isSingingBySpeak() {
    return this.brain != null && this.brain.isSingingBySpeak();
  }

  public SimpleExoPlayer getMusicPlayer() {
    return this.brain == null ? null : this.brain.getMusicPlayer();
  }

  public void startReading(String code, String title) {
    this.brain.read(code, title);
  }

  public void stopReading() {
    this.brain.stopReading();
  }

  public boolean isReading() {
    return this.brain.isReading();
  }

  public String getFamilyName() {
    if (this.family == null) {
      return "";
    }
    else {
      return this.family.getName();
    }
  }

  public boolean hasFamily() {
    return this.family != null;
  }

}
