package com.jininsa.robot.model.party;

import com.annimon.stream.Stream;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.app.Key;
import com.jininsa.robot.connector.Authenticator;
import com.jininsa.robot.model.robot.Robot;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class Person {

  private static Person me;

  public static Person init() {
    if (Person.me == null) {
      Person.me = new Person();
      final String lastAuthToken = Env.getString(Key.LAST_AUTH_TOKEN);
      if (lastAuthToken.equals(Env.DEFAULT_STRING)) {
        me.setAuthToken(null);
      }
      else {
        long lastSignedInExpiredTime = Env.getLong(Key.LAST_AUTH_TOKEN_EXPIRED_TIME);
        me.setAuthToken(Authenticator.AuthToken.create(lastAuthToken, lastSignedInExpiredTime));
      }
    }
    return Person.me;
  }

  public static Person get() {
    return me;
  }

  @Expose
  @SerializedName("id")
  private Long id;
  @Expose
  @SerializedName("email")
  private String email;
  @Expose
  @SerializedName("phone")
  private String phone;
  @Expose
  @SerializedName("name")
  private String name;
  @Expose
  @SerializedName("nickname")
  private String nickname;

  private boolean isAuthenticated;
  private Authenticator.AuthToken authToken;
  @Expose
  @SerializedName("families")
  private List<Family> families = Collections.emptyList();
  @Expose
  @SerializedName("robots")
  private List<Robot> robots = Collections.emptyList();

  private Person() {}

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNickname() {
    return this.nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getPhone() {
    return this.phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Authenticator.AuthToken getAuthToken() {
    return authToken;
  }

  public void setAuthToken(Authenticator.AuthToken authToken) {
    if (authToken != null) {
      Env.setString(Key.LAST_AUTH_TOKEN, authToken.token);
      Env.setLong(Key.LAST_AUTH_TOKEN_EXPIRED_TIME, authToken.expiredTime);
    }
    else {
      Env.setString(Key.LAST_AUTH_TOKEN, Env.DEFAULT_STRING);
      Env.setLong(Key.LAST_AUTH_TOKEN_EXPIRED_TIME, Env.DEFAULT_LONG);
    }
    this.authToken = authToken;
  }

  public Observable<Person> retrieve() {
    return Service.api.retrieve(this.authToken.token).subscribeOn(Schedulers.io());
  }

//  public Observable<Boolean> containFamilyCheck(long familyId) {
//    return Service.api.containFamilyCheck(this.authToken.token, familyId).subscribeOn(Schedulers.io());
//  }

  @Override
  public String toString() {
    return String.format(
      "{Person: {id: %s, email: %s, phone: %s, name: %s, nickname: %s, isAuthenticated: %s, families: %s, robots: %s, authToken: %s}}",
      this.id,
      this.email,
      this.phone,
      this.name,
      this.nickname,
      this.isAuthenticated,
      this.families,
      this.robots,
      this.authToken);
  }

  public boolean isGhost() {
    return this.authToken == null;
  }

  public boolean isAuthenticated() {
    return this.isAuthenticated;
  }

  public void setAuthenticated(final Person person) {
    this.id = person.id;
    this.nickname = person.nickname;
    this.phone = person.phone;
    this.email = person.email;
    this.families = person.families;
    this.robots = person.robots;
    this.isAuthenticated = true;
  }

  public List<Robot> getRobots() {
    return Collections.unmodifiableList(this.robots);
  }

  public List<Family> getFamilies() { return Collections.unmodifiableList(this.families); }

  public List<String> getFamilyNames() { return Collections.unmodifiableList(Stream.of(this.getFamilies()).map(Family::getName).toList()); }

  public void addFamily(final Family family) {
    this.families.add(family);
  }

  public void addRobot(final Robot robot) {
    this.robots.add(robot);
  }
  public void removeRobot(final Robot robot) {
    this.robots.remove(robot);
  }
}
