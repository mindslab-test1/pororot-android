package com.jininsa.robot.model.contents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by kciter on 2018. 2. 7..
 */

public class YouTube {
  interface YouTubeService {
    @GET("videos/search/youtube")
    Observable<List<YouTube>> search(@Query("query") String query);
  }

  public class Id {
    @Expose @SerializedName("kind")
    private String kind;

    @Expose @SerializedName("videoId")
    private String videoId;

    public String getKind() {
      return kind;
    }

    public String getVideoId() {
      return videoId;
    }
  }

  public class Snippet {
    public class Thumbnails {
      class Info {
        @Expose @SerializedName("url")
        private String url;

        @Expose @SerializedName("width")
        private String width;

        @Expose @SerializedName("height")
        private String height;

        public String getUrl() {
          return url;
        }

        public String getWidth() {
          return width;
        }

        public String getHeight() {
          return height;
        }
      }

      @Expose @SerializedName("default")
      private Info defaultInfo;

      @Expose @SerializedName("medium")
      private Info medium;

      @Expose @SerializedName("high")
      private Info high;

      public Info getDefaultInfo() {
        return defaultInfo;
      }

      public Info getMedium() {
        return medium;
      }

      public Info getHigh() {
        return high;
      }
    }

    @Expose @SerializedName("publishedAt")
    private String publishedAt;

    @Expose @SerializedName("channelId")
    private String channelId;

    @Expose @SerializedName("title")
    private String title;

    @Expose @SerializedName("description")
    private String description;

    @Expose @SerializedName("channelTitle")
    private String channelTitle;

    @Expose @SerializedName("liveBroadcastContent")
    private String liveBroadcastContent;

    @Expose @SerializedName("thumbnails")
    private Thumbnails thumbnails;

    public String getPublishedAt() {
      return publishedAt;
    }

    public String getChannelId() {
      return channelId;
    }

    public String getTitle() {
      return title;
    }

    public String getDescription() {
      return description;
    }

    public String getChannelTitle() {
      return channelTitle;
    }

    public String getLiveBroadcastContent() {
      return liveBroadcastContent;
    }

    public Thumbnails getThumbnails() {
      return thumbnails;
    }
  }

  @Expose @SerializedName("kind")
  private String kind;

  @Expose @SerializedName("etag")
  private String etag;

  @Expose @SerializedName("id")
  private Id id;

  @Expose @SerializedName("snippet")
  private Snippet snippet;

  static public Observable<List<YouTube>> search(String query) {
    return Service.api.searchYouTube(query)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public String getKind() {
    return kind;
  }

  public String getEtag() {
    return etag;
  }

  public Id getId() {
    return id;
  }

  public Snippet getSnippet() {
    return snippet;
  }
}
