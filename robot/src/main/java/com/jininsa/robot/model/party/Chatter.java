package com.jininsa.robot.model.party;

import android.support.annotation.DrawableRes;

import com.jininsa.robot.R;

/**
 * Created by architect on 2017. 2. 20..
 */

// tofix 개별 앱으로 옮겨야 함
public class Chatter {
  final public static Chatter POROROT = new Chatter("001", "PororoT", R.drawable.pororot);
  final public static Chatter KID = new Chatter("002", "공주님", R.drawable.kid);
  final public static Chatter MOM = new Chatter("003", "엄마", R.drawable.mom);

  final private String id;
  final private String name;
  final private @DrawableRes
  int avatarRes;

  public Chatter(final String id, final String name, @DrawableRes final int avatarRes) {
    this.id = id;
    this.name = name;
    this.avatarRes = avatarRes;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }
}
