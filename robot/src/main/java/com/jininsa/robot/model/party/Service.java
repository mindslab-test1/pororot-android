package com.jininsa.robot.model.party;

import com.jininsa.robot.connector.Biz;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;

class Service {
  interface Api {
    @GET("person")
    Observable<Person> retrieve(@Header("X-Auth-Token") String token);
//    @GET("family/{familyId}/check")
//    Observable<Boolean> containFamilyCheck(@Header("X-Auth-Token") String token, @Path("familyId") long familyId);
  }
  static final Service.Api api = Biz.connector().create(Service.Api.class);
}

