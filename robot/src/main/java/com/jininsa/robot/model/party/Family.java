package com.jininsa.robot.model.party;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jininsa.robot.helper.Env;

public class Family {
  @Expose @SerializedName("id")
  private Long id = Env.DEFAULT_ID;
  @Expose @SerializedName("name")
  private String name;
  @Expose @SerializedName("state")
  private String state = "ACTIVE";

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  private Family() {}

  public Family(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return String.format(
      "{Family: {id: %s, name: %s, state: %s}}",
      this.id,
      this.name,
      this.state);
  }
}
