package com.jininsa.robot.model.contents;

import com.jininsa.robot.connector.Biz;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

class Service {
  interface Api {
    @GET("books")
    Observable<List<Book>> getBooks(@Query("start") int start, @Query("length") int length);
    @GET("book/findByCode")
    Observable<Book> getBook(@Query("code") String code);
    @GET("songs")
    Observable<List<Song>> getSongs(@Query("start") int start, @Query("length") int length);
    @GET("song/findByCode")
    Observable<Song> getSong(@Query("code") String code);
    @GET("videos")
    Observable<List<Video>> getVideos(@Query("start") int start, @Query("length") int length);
    @GET("videos/search/youtube")
    Observable<List<YouTube>> searchYouTube(@Query("query") String query);
  }

  static final Service.Api api = Biz.connector().create(Service.Api.class);
}
