package com.jininsa.robot.model.contents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by kciter on 2018. 1. 15..
 */

public class Song {

  @Expose @SerializedName("id")
  private Long id;

  @Expose @SerializedName("code")
  private String code;

  @Expose @SerializedName("title")
  private String title;

  @Expose @SerializedName("mood")
  private String mood;

  @Expose @SerializedName("genre")
  private String genre;

  @Expose @SerializedName("artist")
  private String artist;

  @Expose @SerializedName("canuse")
  private boolean canuse;

  @Expose @SerializedName("imageUrl")
  private String imageUrl;

  @Expose @SerializedName("url")
  private String url;

  static public Observable<List<Song>> getSongList(int start, int length) {
    return Service.api.getSongs(start, length)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  static public Observable<Song> findByCode(String code) {
    return Service.api.getSong(code)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public Long getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getMood() {
    return mood;
  }

  public String getGenre() {
    return genre;
  }

  public String getArtist() {
    return artist;
  }

  public boolean isCanuse() {
    return canuse;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public String getUrl() {
    return url;
  }
}
