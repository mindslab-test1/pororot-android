package com.jininsa.robot.model.robot;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RobotType {
  @SerializedName("id")
  @Expose
  private Long id;
  @SerializedName("name")
  @Expose
  private String name;

  public Long getId() { return this.id; }

  public String getName() { return this.name; }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("RobotType{");
    sb.append("id=").append(id);
    sb.append(", name='").append(name).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
