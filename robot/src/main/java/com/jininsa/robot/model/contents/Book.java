package com.jininsa.robot.model.contents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Book {
    @Expose
    @SerializedName("id")
    private Long id;

    @Expose @SerializedName("code")
    private String code;

    @Expose @SerializedName("title")
    private String title;

    @Expose @SerializedName("contents")
    private String contents;

    @Expose @SerializedName("author")
    private String author;

    @Expose @SerializedName("canuse")
    private boolean canuse;

    static public Observable<List<Book>> getBookList(int start, int length) {
        return Service.api.getBooks(start, length)
          .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    static public Observable<Book> findByCode(String code) {
        return Service.api.getBook(code)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
    }

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public String getContents() {
        return contents;
    }

    public String getAuthor() {
        return author;
    }

    public boolean isCanuse() {
        return canuse;
    }

    // 임의로 아이템을 추가해보기위해 함수 추가
    public void setTitle(String title) {
        this.title = title;
    }
}