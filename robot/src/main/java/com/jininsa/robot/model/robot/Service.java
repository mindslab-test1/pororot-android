package com.jininsa.robot.model.robot;

import com.jininsa.robot.connector.Biz;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

class Service {
  interface Api {
    @GET("robot/{mac}")
    Observable<Robot> getRobot(@Path("mac") String mac);
    @POST("robot/findByMac")
    Observable<Robot> findByMac(@Body Robot robot);
    @POST("robot/register")
    Observable<Robot> register(@Body Robot robot);
    @PATCH("robot/assignUuid")
    Observable<Robot> assignUuid(@Body Robot robot);
    @POST("robot/adoptToNewFamily")
    Observable<Robot> adoptToNewFamily(@Header("X-Auth-Token") String token, @Body Map<String, String> body);
    @POST("robot/adoptToOldFamily")
    Observable<Robot> adoptToOldFamily(@Header("X-Auth-Token") String token, @Body Map<String, String> body);
    @POST("robot/changeNames")
    Observable<Robot> changeNames(@Header("X-Auth-Token") String token, @Body Map<String, String> body);
  }

  static final Service.Api api = Biz.connector().create(Service.Api.class);
}
