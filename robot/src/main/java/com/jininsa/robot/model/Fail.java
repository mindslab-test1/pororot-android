package com.jininsa.robot.model;

import android.support.v4.util.Pair;

import com.elvishew.xlog.XLog;
import com.google.common.collect.BiMap;
import com.google.common.collect.EnumHashBiMap;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.jininsa.robot.helper.Parser;

import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class Fail {

  public enum Type {
    SYSTEM(0),
    REQUEST(100),
    AUTH(400),
    BIZ(500);

    final public int code;

    Type(final int code) {
      this.code = code;
    }
  }

  public enum Problem {
    UNKNOWN("E000", Type.SYSTEM), UNEXPECTED("E001", Type.SYSTEM),
    BAD_REQUEST("E100", Type.REQUEST),
    USER_NOT_FOUND("E400", Type.AUTH),
    AUTH_DENIED("E401", Type.AUTH),
    AUTH_TOKEN_GENERATION_FAILED("E402", Type.AUTH),
    AUTH_TOKEN_INVALID("E403", Type.AUTH),
    ENTITY_NOT_FOUND("E500", Type.BIZ),
    ENTITY_NOT_FOUND_TEMP("E200", Type.BIZ);

    private static final BiMap<Problem, String> problemCodeMap = EnumHashBiMap.create(Problem.class);

    static {
      for (Problem p: Problem.values()) {
        problemCodeMap.put(p, p.code);
      }
    }

    static Problem find(String code) {
      if (problemCodeMap.inverse().containsKey(code)) return problemCodeMap.inverse().get(code);
      return Problem.UNKNOWN;
    }

    final public String code;
    final public Type type;

    Problem(final String code, final Type type) {
      this.code = code;
      this.type = type;
    }

    @Override
    public String toString() {
      return String.format("%s(%s, %s)", this.name(), this.code, this.type);
    }
  }

  public static Pair<Problem, String> parse(Throwable e) {
    if (e instanceof HttpException) {
      HttpException httpException = (HttpException) e;
      ResponseBody body = httpException.response().errorBody();
      if (body != null) {
        try {
          Fail fail = Parser.parse(body.string(), Fail.class);
          XLog.e(fail);
          return new Pair<>(Problem.find(fail.code), fail.message);
        }
        catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    }

    return new Pair<>(Problem.UNKNOWN, e.getMessage());
  }

  @Expose
  @SerializedName("code")
  private String code;

  @SerializedName("message")
  @Expose
  private String message;

  @Override
  public String toString() {
    return String.format(
      "{Fail: {code: %s, message: %s}}",
      this.code,
      this.message);
  }
}
