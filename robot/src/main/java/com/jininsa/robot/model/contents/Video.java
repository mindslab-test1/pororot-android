package com.jininsa.robot.model.contents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class Video {
  interface VideoService {
    @GET("videos")
    Observable<List<Video>> videoList(@Query("start") int start, @Query("length") int length);
  }

  @Expose @SerializedName("id")
  private int id;

  @Expose @SerializedName("title")
  private String title;

  @Expose @SerializedName("description")
  private String description;

  @Expose @SerializedName("author")
  private String author;

  @Expose @SerializedName("url")
  private String url;

  @Expose @SerializedName("imageUrl")
  private String imageUrl;

  @Expose @SerializedName("videoType")
  private String videoType;

  @Expose @SerializedName("playTime")
  private String playTime;

  @Expose @SerializedName("canuse")
  private boolean canuse;

  static public Observable<List<Video>> getVideoList(int start, int length) {
    return Service.api.getVideos(start, length)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread());
  }

  @Override
  public String toString() {
    return String.format(
      "{Video: {id: %s, title: %s, description: %s, author: %s, url: %s, imageUrl: %s, videoType: %s, playTime: %s, canuse: %s}}",
      this.id,
      this.title,
      this.description,
      this.author,
      this.url,
      this.imageUrl,
      this.videoType,
      this.playTime,
      this.canuse);
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getVideoType() {
    return videoType;
  }

  public void setVideoType(String videoType) {
    this.videoType = videoType;
  }

  public String getPlayTime() {
    return playTime;
  }

  public void setPlayTime(String playTime) {
    this.playTime = playTime;
  }

  public boolean isCanuse() {
    return canuse;
  }

  public void setCanuse(boolean canuse) {
    this.canuse = canuse;
  }
}