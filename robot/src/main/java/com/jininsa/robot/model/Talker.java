package com.jininsa.robot.model;

import com.stfalcon.chatkit.commons.models.IUser;

/**
 * Created by architect on 2017. 5. 8..
 */

public class Talker implements IUser
{
  final private String id;
  final private String name;
  final private String avatar;

  public Talker(String id, String name, String avatar)
  {
    this.id = id;
    this.name = name;
    this.avatar = avatar;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getAvatar() {
    return avatar;
  }

  public static final Talker POROROT = new Talker("2", "뽀로롯", "http://i.imgur.com/HLJv1CI.png");
  public static final Talker KID = new Talker("0", "공주님", "http://i.imgur.com/7T6Rm2I.png");
  public static final Talker MOM = new Talker("2", "엄마​", "http://i.imgur.com/mgRWRUY.png");
}
