package com.jininsa.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;

import com.jininsa.robot.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

public class FileTextView extends android.support.v7.widget.AppCompatTextView
{
  private static final String TAG = FileTextView.class.getSimpleName();

  public FileTextView(Context context)
  {
    super(context);
    init(null, 0);
  }

  public FileTextView(Context context, AttributeSet attrs)
  {
    super(context, attrs);
    init(attrs, 0);
  }

  public FileTextView(Context context, AttributeSet attrs, int defStyle)
  {
    super(context, attrs, defStyle);
    init(attrs, defStyle);
  }

  private void init(AttributeSet attrs, int defStyle)
  {
    // Load attributes
    final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FileTextView, defStyle, 0);
    final String fileName = a.getString(R.styleable.FileTextView_file_name);
    a.recycle();
    StringBuilder sb = new StringBuilder();
    try (InputStream is = this.getContext().getAssets().open(fileName); BufferedReader br = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")))) {
      String line;
      String lf = System.getProperty("line.separator");
      while ((line = br.readLine()) != null) {
        sb.append(line).append(lf);
      }
    }
    catch (IOException e) {
      Log.e(TAG, "[init] : e" + e.getMessage(), e);
    }
    this.setText(sb);
  }
}