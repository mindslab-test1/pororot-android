package kr.co.voiceware.comm;

public class MessageTypes {
	public static final int MESSAGE_LICENSE_ERROR = 101;
	public static final int MESSAGE_LICENSE_DOWNLOAD_START = 102;
	public static final int MESSAGE_LICENSE_DOWNLOAD_END = 103;
}
