package kr.co.voiceware.comm;

public interface IVTDefine {

  int SAMPLE_RATE_16000 = 16000;
  int SAMPLE_RATE_44000 = 44000;
  /**
   * Sample Rate
   */
  int SAMPLE_RATE_IN_HZ = SAMPLE_RATE_16000;


  int BUFFER_SIZE_16K = 60000;
  int BUFFER_SIZE_44K = 165374;
  /**
   * Buffer Size
   */
  int BUFFER_SIZE_FOR_HZ = BUFFER_SIZE_16K;

//  /**
//   * Junwoo (Korean, Male)
//   */
//  int SPEAKER_ID_JUNWOO = 3;
//
//  /**
//   * Sujin (Korean, Female)
//   */
//  int SPEAKER_ID_SUJIN = 8;
//
//  /**
//   * Yumi (Korean, Female)
//   */
//  int SPEAKER_ID_YUMI = 10;
//
//  /**
//   * Gyuri (Korean, Female)
//   */
//  int SPEAKER_ID_GYURI = 11;
//
//  /**
//   * Dayoung (Korean, Female)
//   */
//  int SPEAKER_ID_DAYOUNG = 12;
//
//  /**
//   * Chorong (Korean, Female)
//   */
//  int SPEAKER_ID_CHORONG = 13;
//
//  /**
//   * Hyeryun (Korean, Female)
//   */
//  int SPEAKER_ID_HYERYUN = 14;
//
//  /**
//   * Hyuna (Korean, Female)
//   */
//  int SPEAKER_ID_HYUNA = 15;
//  /**
//   * Jimin (Korean, Female)
//   */
//  int SPEAKER_ID_JIMIN = 17;
//  /**
//   * Jihun (Korean, Female)
//   */
//  int SPEAKER_ID_JIHUN = 18;
//  /**
//   * Sena (Korean, Female)
//   */
//  int SPEAKER_ID_SENA = 19;
//  /**
//   * Yura (Korean, Female)
//   */
//  int SPEAKER_ID_YURA = 20;
//  /**
//   * Maru (Korean, Female)
//   */
//  int SPEAKER_ID_MARU = 21;
//
//
//  /**
//   * Kate (English, Female)
//   */
//  int SPEAKER_ID_KATE = 100;
//
//  /**
//   * Paul (English, Male)
//   */
//  int SPEAKER_ID_PAUL = 101;
//
//  /**
//   * Julie (English, Female)
//   */
//  int SPEAKER_ID_JULIE = 103;
//
//  /**
//   * James (English, Male)
//   */
//  int SPEAKER_ID_JAMES = 104;
//
//  /**
//   * Ashley (English, Female)
//   */
//  int SPEAKER_ID_ASHLEY = 105;
//
//  /**
//   * Beth (English, Female)
//   */
//  int SPEAKER_ID_BETH = 106;
//
//  /**
//   * Lily (Chinese, Female)
//   */
//  int SPEAKER_ID_LILY = 200;
//
//  /**
//   * Wang (Chinese, Male)
//   */
//  int SPEAKER_ID_WANG = 201;
//
//  /**
//   * Hui (Chinese, Female)
//   */
//  int SPEAKER_ID_HUI = 202;
//
//  /**
//   * Liang (Chinese, Male)
//   */
//  int SPEAKER_ID_LIANG = 203;
//
//  /**
//   * HONG (Chinese, Male)
//   */
//  int SPEAKER_ID_HONG = 204;
//
//  /**
//   * Qiang (Chinese, Male)
//   */
//  int SPEAKER_ID_QIANG = 205;
//
//
//  /**
//   * Miyu (Japanese, Female)
//   */
//  int SPEAKER_ID_MIYU = 300;
//
//  /**
//   * Show (Japanese, Male)
//   */
//  int SPEAKER_ID_SHOW = 301;
//
//  /**
//   * Misaki (Japanese, Female)
//   */
//  int SPEAKER_ID_MISAKI = 302;
//
//  /**
//   * Haruka (Japanese, Female)
//   */
//  int SPEAKER_ID_HARUKA = 303;
//
//  /**
//   * Sayaka (Japanese, Female)
//   */
//  int SPEAKER_ID_SAYAKA = 304;
//
//  /**
//   * Ryo (Japanese, Male)
//   */
//  int SPEAKER_ID_RYO = 305;
//
//  /**
//   * Hikari (Japanese, Female)
//   */
//  int SPEAKER_ID_HIKARI = 306;
//
//  /**
//   * Takeru (Japanese, Male)
//   */
//  int SPEAKER_ID_TAKERU = 307;
//
//  /**
//   * Risa (Japanese, Female)
//   */
//  int SPEAKER_ID_RISA = 308;
//
//  /**
//   * Akira (Japanese, Male)
//   */
//  int SPEAKER_ID_AKIRA = 309;
//
//  /**
//   * Violeta (Spanish, Female)
//   */
//  int SPEAKER_ID_VIOLETA = 400;
//
//  /**
//   * Francisco (Spanish, Male)
//   */
//  int SPEAKER_ID_FRANCISCO = 401;
//
//  /**
//   * GLORIA (Spanish, Female)
//   */
//  int SPEAKER_ID_GLORIA = 402;
//  /**
//   * Bridget (British, female)
//   */
//  int SPEAKER_ID_BRIDGET = 500;
//  /**
//   * Hugh (British, Male)
//   */
//  int SPEAKER_ID_HUGH = 501;
//
//  /**
//   * Chloe (French, female)
//   */
//  int SPEAKER_ID_CHLOE = 600;
//
//  /**
//   * Leo (French, Male)
//   */
//  int SPEAKER_ID_LEO = 601;
//
//  /**
//   * Yafang (TWN, Female)
//   */
//  int SPEAKER_ID_YAFANG = 700;
//
//  /**
//   * Junwoo (Korean, Male)
//   */
//  String SPEAKER_NAME_JUNWOO = "JUNWOO";
//
//  /**
//   * Sujin (Korean, Female)
//   */
//  String SPEAKER_NAME_SUJIN = "SUJIN";
//
//  /**
//   * Yumi (Korean, Female)
//   */
//  String SPEAKER_NAME_YUMI = "YUMI";
//
//  /**
//   * Gyuri (Korean, Female)
//   */
//  String SPEAKER_NAME_GYURI = "GYURI";
//
//  /**
//   * Dayoung (Korean, Female)
//   */
//  String SPEAKER_NAME_DAYOUNG = "DAYOUNG";
//
//  /**
//   * Chorong (Korean, Female)
//   */
//  String SPEAKER_NAME_CHORONG = "CHORONG";
//
//  /**
//   * Hyeryun (Korean, Female)
//   */
//  String SPEAKER_NAME_HYERYUN = "HYERYUN";
//
//  /**
//   * Hyeryun (Korean, Female)
//   */
//  String SPEAKER_NAME_HYUNA = "HYUNA";
//
//  /**
//   * Hyeryun (Korean, Female)
//   */
//  String SPEAKER_NAME_JIMIN = "JIMIN";
//  /**
//   * Jihun (Korean, Female)
//   */
//  String SPEAKER_NAME_JIHUN = "JIHUN";
//  /**
//   * Sena (Korean, Female)
//   */
//  String SPEAKER_NAME_SENA = "SENA";
//  /**
//   * Yura (Korean, Female)
//   */
//  String SPEAKER_NAME_YURA = "YURA";
//  /**
//   * Maru (Korean, Female)
//   */
//  String SPEAKER_NAME_MARU = "MARU";
//
//  /**
//   * Kate (English, Female)
//   */
//  String SPEAKER_NAME_KATE = "KATE";
//
//  /**
//   * Paul (English, Male)
//   */
//  String SPEAKER_NAME_PAUL = "PAUL";
//
//  /**
//   * Julie (English, Female)
//   */
//  String SPEAKER_NAME_JULIE = "JULIE";
//
//  /**
//   * James (English, Male)
//   */
//  String SPEAKER_NAME_JAMES = "JAMES";
//
//  /**
//   * Ashley (English, Female)
//   */
//  String SPEAKER_NAME_ASHLEY = "ASHLEY";
//
//  /**
//   * Beth (English, Female)
//   */
//  String SPEAKER_NAME_BETH = "BETH";
//
//  /**
//   * Lily (Chinese, Female)
//   */
//  String SPEAKER_NAME_LILY = "LILY";
//
//  /**
//   * Wang (Chinese, Male)
//   */
//  String SPEAKER_NAME_WANG = "WANG";
//
//  /**
//   * Hui (Chinese, Female)
//   */
//  String SPEAKER_NAME_HUI = "HUI";
//
//  /**
//   * Liang (Chinese, Male)
//   */
//  String SPEAKER_NAME_LIANG = "LIANG";
//
//  /**
//   * HONG (Chinese, Male)
//   */
//  String SPEAKER_NAME_HONG = "HONG";
//
//  /**
//   * Qiang (Chinese, Male)
//   */
//  String SPEAKER_NAME_QIANG = "QIANG";
//
//  /**
//   * Miyu (Japanese, Female)
//   */
//  String SPEAKER_NAME_MIYU = "MIYU";
//
//  /**
//   * Show (Japanese, Male)
//   */
//  String SPEAKER_NAME_SHOW = "PROCESSING";
//
//  /**
//   * Misaki (Japanese, Female)
//   */
//  String SPEAKER_NAME_MISAKI = "MISAKI";
//
//  /**
//   * Haruka (Japanese, Female)
//   */
//  String SPEAKER_NAME_HARUKA = "HARUKA";
//
//  /**
//   * Sayaka (Japanese, Female)
//   */
//  String SPEAKER_NAME_SAYAKA = "SAYAKA";
//
//  /**
//   * Ryo (Japanese, Male)
//   */
//  String SPEAKER_NAME_RYO = "RYO";
//
//  /**
//   * Hikari (Japanese, Female)
//   */
//  String SPEAKER_NAME_HIKARI = "HIKARI";
//
//  /**
//   * Takeru (Japanese, Male)
//   */
//  String SPEAKER_NAME_TAKERU = "TAKERU";
//
//  /**
//   * Risa (Japanese, Female)
//   */
//  String SPEAKER_NAME_RISA = "RISA";
//
//  /**
//   * Akira (Japanese, Male)
//   */
//  String SPEAKER_NAME_AKIRA = "AKIRA";
//
//  /**
//   * Violeta (Spanish, Female)
//   */
//  String SPEAKER_NAME_VIOLETA = "VIOLETA";
//
//  /**
//   * Francisco (Spanish, Male)
//   */
//  String SPEAKER_NAME_FRANCISCO = "FRANCISCO";
//
//  /**
//   * GLORIA (Spanish, Female)
//   */
//  String SPEAKER_NAME_GLORIA = "GLORIA";
//
//  /**
//   * Bridget (British, female)
//   */
//  String SPEAKER_NAME_BRIDGET = "BRIDGET";
//
//  /**
//   * Hugh (British, Male)
//   */
//  String SPEAKER_NAME_HUGH = "HUGH";
//
//  /**
//   * Chloe (French, female)
//   */
//  String SPEAKER_NAME_CHLOE = "CHLOE";
//
//  /**
//   * Leo (French, Male)
//   */
//  String SPEAKER_NAME_LEO = "LEO";
//
//  /**
//   * Yafang (TWN, Female)
//   */
//  String SPEAKER_NAME_YAFANG = "YAFANG";

  /**
   * Text format (used in texttype)
   */
  int VT_TEXT_FMT_PLAIN_TEXT = 0;

  /**
   * Text format (used in texttype)
   */
  int VT_TEXT_FMT_JEITA = 4;

	
	
	
/* LOADTTS() & LOADTTSEXT() */
  /**
   * Return value of LOADTTS() API
   */
  int VT_LOADTTS_SUCCESS = 0;

  /**
   * Return value of LOADTTS() API
   * Tried to load the synthesizer database with different values of db_path in case of using multiple synthesizer databases
   */
  int VT_LOADTTS_ERROR_CONFLICT_DBPATH = 1;

  /**
   * Return value of LOADTTS() API
   * Failed to secure channel memory
   */
  int VT_LOADTTS_ERROR_TTS_STRUCTURE = 2;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for the Morpheme Analysis
   */
  int VT_LOADTTS_ERROR_TAGGER = 3;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for the Break Index
   */
  int VT_LOADTTS_ERROR_BREAK_INDEX = 4;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for the Text Pre-Processing
   */
  int VT_LOADTTS_ERROR_TPP_DICT = 5;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for the Acoustic Model
   */
  int VT_LOADTTS_ERROR_TABLE = 6;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for Unit Selection
   */
  int VT_LOADTTS_ERROR_UNIT_INDEX = 7;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for Prosody Model
   */
  int VT_LOADTTS_ERROR_PROSODY_DB = 8;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for Speech Database
   */
  int VT_LOADTTS_ERROR_PCM_DB = 9;

  /**
   * Return value of LOADTTS() API
   * Failed to load Storage for Pitch Location Information
   */
  int VT_LOADTTS_ERROR_PM_DB = 10;

  /**
   * Return value of LOADTTS() API
   * Other errors
   */
  int VT_LOADTTS_ERROR_UNKNOWN = 11;

  int VT_LOADTTS_CHECKLICENSE_SUCCESS = 0;
  int VT_LOADTTS_CHECKLICENSE_ERROR_NOTEXIST = -1;
  int VT_LOADTTS_CHECKLICENSE_ERROR_INVALIDFORMAT = -2;
  int VT_LOADTTS_CHECKLICENSE_ERROR_EXPIREDATE = -3;
  int VT_LOADTTS_CHECKLICENSE_ERROR_HOSTID = -4;
  int VT_LOADTTS_CHECKLICENSE_ERROR_OS = -5;
  int VT_LOADTTS_CHECKLICENSE_ERROR_LANG = -6;
  int VT_LOADTTS_CHECKLICENSE_ERROR_SPEAKER = -7;
  int VT_LOADTTS_CHECKLICENSE_ERROR_VERSION = -8;
  int VT_LOADTTS_CHECKLICENSE_ERROR_DBACCESS = -9;
  int VT_LOADTTS_CHECKLICENSE_ERROR_SAMPLING = -10;
  int VT_LOADTTS_CHECKLICENSE_ERROR_DBSIZE = -11;


  /**
   * Return value of LOADUserDict() API
   * user dictionary is successfully loaded.
   */
  int VT_LOAD_USERDICT_SUCCESS = 1;

  /**
   * Return value of LOADUserDict() API
   * dictidx value is not within the valid range
   */
  int VT_LOAD_USERDICT_ERROR_INVALID_INDEX = -1;

  /**
   * Return value of LOADUserDict() API
   * Talker dictionary file corresponding to dictidx is already loaded.
   */
  int VT_LOAD_USERDICT_ERROR_INDEX_BUSY = -2;

  /**
   * Return value of LOADUserDict() API
   * Loading failed because there was no user dictionary files or valid entry
   */
  int VT_LOAD_USERDICT_ERROR_LOAD_FAIL = -3;

  /**
   * Return value of LOADUserDict() API
   * Other errors
   */
  int VT_LOAD_USERDICT_ERROR_UNKNOWN = -4;

  /**
   * Return value of UNLOADUserDict() API
   * user dictionary is successfully unloaded.
   */
  int VT_UNLOAD_USERDICT_SUCCESS = 1;

  /**
   * Return value of UNLOADUserDict() API
   * Talker dictionary file corresponding to dictidx is already unloaded.
   */
  int VT_UNLOAD_USERDICT_ERROR_NULL_INDEX = -1;

  /**
   * Return value of UNLOADUserDict() API
   * dictidx value is not within the valid range
   */
  int VT_UNLOAD_USERDICT_ERROR_INVALID_INDEX = -2;

  /**
   * Return value of UNLOADUserDict() API
   * Other errors
   */
  int VT_UNLOAD_USERDICT_ERROR_UNKNOWN = -3;

  /**
   * Return value of TextToFile() API
   * successfully synthesized
   */
  int VT_FILE_API_SUCCESS = 1;

  /**
   * Return value of TextToFile() API
   * Used format that is not supported.
   */
  int VT_FILE_API_ERROR_INVALID_FORMAT = -1;

  /**
   * Return value of TextToFile() API
   * Failed to secure channel memory
   */
  int VT_FILE_API_ERROR_CREATE_THREAD = -2;

  /**
   * Return value of TextToFile() API
   * Text string is a NULL pointer
   */
  int VT_FILE_API_ERROR_NULL_TEXT = -3;

  /**
   * Return value of TextToFile() API
   * The length of text string is 0.
   */
  int VT_FILE_API_ERROR_EMPTY_TEXT = -4;

  /**
   * Return value of TextToFile() API
   * The Tts Storage of the voice requested is not loaded
   */
  int VT_FILE_API_ERROR_DB_NOT_LOADED = -5;

  /**
   * Return value of TextToFile() API
   * Failed to generate the synthesized voice file
   */
  int VT_FILE_API_ERROR_OUT_FILE_OPEN = -6;

  /**
   * Return value of TextToFile() API
   * Other errors
   */
  int VT_FILE_API_ERROR_UNKNOWN = -7;

  /**
   * Audio format of TextToFile() API
   * 16bits Linear PCM
   */
  int VT_FILE_API_FMT_S16PCM = 0;

  /**
   * Audio format of TextToFile() API
   * 8bits A-law PCM
   */
  int VT_FILE_API_FMT_ALAW = 1;

  /**
   * Audio format of TextToFile() API
   * 8bits Mu-law PCM
   */
  int VT_FILE_API_FMT_MULAW = 2;

  /**
   * Audio format of TextToFile() API
   * 4bits Dialogic ADPCM
   */
  int VT_FILE_API_FMT_DADPCM = 3;

  /**
   * Audio format of TextToFile() API
   * 16bits Linear PCM WAVE
   */
  int VT_FILE_API_FMT_S16PCM_WAVE = 4;

  /**
   * Audio format of TextToFile() API
   * 8bits Unsigned Linear PCM WAVE
   */
  int VT_FILE_API_FMT_U08PCM_WAVE = 5;

  /**
   * Audio format of TextToFile() API
   * 8bits A-law PCM WAVE
   */
  int VT_FILE_API_FMT_ALAW_WAVE = 7;

  /**
   * Audio format of TextToFile() API
   * 8bits Mu-law PCM WAVE
   */
  int VT_FILE_API_FMT_MULAW_WAVE = 8;

  /**
   * Audio format of TextToFile() API
   * 8bits Mu-law PCM SUN AU
   */
  int VT_FILE_API_FMT_MULAW_AU = 9;

  /**
   * Audio format of TextToBuffer() API
   * 16bits Linear PCM
   */
  int VT_BUFFER_API_FMT_S16PCM = VT_FILE_API_FMT_S16PCM;

  /**
   * Audio format of TextToBuffer() API
   * 8bits A-law PCM
   */
  int VT_BUFFER_API_FMT_ALAW = VT_FILE_API_FMT_ALAW;

  /**
   * Audio format of TextToBuffer() API
   * 8bits Mu-law PCM
   */
  int VT_BUFFER_API_FMT_MULAW = VT_FILE_API_FMT_MULAW;

  /**
   * Audio format of TextToBuffer() API
   * 4bits Dialogic ADPCM
   */
  int VT_BUFFER_API_FMT_DADPCM = VT_FILE_API_FMT_DADPCM;


  /* BUFFER ERROR */
  int VT_BUFFER_API_PROCESSING = 0;
  int VT_BUFFER_API_DONE = 1;
  int VT_BUFFER_API_ERROR_INVALID_FORMAT = -1;
  int VT_BUFFER_API_ERROR_CREATE_THREAD = -2;
  int VT_BUFFER_API_ERROR_NULL_TEXT = -3;
  int VT_BUFFER_API_ERROR_EMPTY_TEXT = -4;
  int VT_BUFFER_API_ERROR_NULL_BUFFER = -5;
  int VT_BUFFER_API_ERROR_DB_NOT_LOADED = -6;
  int VT_BUFFER_API_ERROR_THREAD_BUSY = -7;
  int VT_BUFFER_API_ERROR_ABNORMAL_CONDITION = -8;
  int VT_BUFFER_API_ERROR_UNKNOWN = -9;


  /**
   * Return value of GetTTSInfo() API
   * the information is successfully drawn
   */
  int VT_INFO_SUCCESS = 0;

  /**
   * Return value of GetTTSInfo() API
   * Unqualified request that cannot be used to getString any information
   */
  int VT_INFO_ERROR_NOT_SUPPORTED_REQUEST = 1;

  /**
   * Return value of GetTTSInfo() API
   * Undefined value of request is used
   */
  int VT_INFO_ERROR_INVALID_REQUEST = 2;

  /**
   * Return value of GetTTSInfo() API
   * value is NULL pointer
   */
  int VT_INFO_ERROR_NULL_VALUE = 3;

  /**
   * Return value of GetTTSInfo() API
   * valuesize is too small to be characterize the corresponding attribute/information
   */
  int VT_INFO_ERROR_SHORT_LENGTH_VALUE = 4;

  /**
   * Return value of GetTTSInfo() API
   * Other errors
   */
  int VT_INFO_ERROR_UNKNOWN = 5;

  /**
   * Request of GetTTSInfo() API
   */
  int VT_BUILD_DATE = 0;
  int VT_VERIFY_CODE = 1;
  int VT_MAX_CHANNEL = 2;
  int VT_DB_DIRECTORY = 3;
  int VT_LOAD_SUCCESS_CODE = 4;
  int VT_MAX_SPEAKER = 5;
  int VT_DEF_SPEAKER = 6;
  int VT_CODEPAGE = 7;
  int VT_DB_ACCESS_MODE = 8;
  int VT_FIXED_POINT_SUPPORT = 9;
  int VT_SAMPLING_FREQUENCY = 10;
  int VT_MAX_PITCH_RATE = 11;
  int VT_DEF_PITCH_RATE = 12;
  int VT_MIN_PITCH_RATE = 13;
  int VT_MAX_SPEED_RATE = 14;
  int VT_DEF_SPEED_RATE = 15;
  int VT_MIN_SPEED_RATE = 16;
  int VT_MAX_VOLUME = 17;
  int VT_DEF_VOLUME = 18;
  int VT_MIN_VOLUME = 19;
  int VT_MAX_SENT_PAUSE = 20;
  int VT_DEF_SENT_PAUSE = 21;
  int VT_MIN_SENT_PAUSE = 22;
  int VT_DB_BUILD_DATE = 23;
  int VT_MAX_COMMA_PAUSE = 24;
  int VT_DEF_COMMA_PAUSE = 25;
  int VT_MIN_COMMA_PAUSE = 26;


  int VT_CHECKLICENSE_SUCCESS = 0;
  int VT_CHECKLICENSE_ERROR_NOTEXIST = -1;
  int VT_CHECKLICENSE_ERROR_INVALIDFORMAT = -2;
  int VT_CHECKLICENSE_ERROR_EXPIREDATE = -3;
  int VT_CHECKLICENSE_ERROR_HOSTID = -4;
  int VT_CHECKLICENSE_ERROR_OS = -5;
  int VT_CHECKLICENSE_ERROR_LANG = -6;
  int VT_CHECKLICENSE_ERROR_SPEAKER = -7;
  int VT_CHECKLICENSE_ERROR_VERSION = -8;
  int VT_CHECKLICENSE_ERROR_DBACCESS = -9;
  int VT_CHECKLICENSE_ERROR_SAMPLING = -10;
  int VT_CHECKLICENSE_ERROR_DBSIZE = -11;
  int VT_CHECKLICENSE_ERROR_UNKNOWN = -12;
}
