package com.jininsa.t800.tester.controller;

import android.content.Intent;

import com.jininsa.t800.tester.R;

import butterknife.OnClick;

public class AccStandingTestActivity extends AccTestActivity {

  @Override
  public void doAfterChecking() {
//    this.shakingTextView.setText(String.valueOf(this.shakingCount));
//    this.addSubscription("rawAccData", this.robot.accStates.observeOn(AndroidSchedulers.mainThread()).subscribe(data -> {
//      this.pxTextView.setText(String.valueOf(data.px));
//      this.pyTextView.setText(String.valueOf(data.py));
//      this.pzTextView.setText(String.valueOf(data.pz));
//      this.psTextView.setText(String.valueOf(data.ps));
//      this.cxTextView.setText(String.valueOf(data.cx));
//      this.cyTextView.setText(String.valueOf(data.cy));
//      this.czTextView.setText(String.valueOf(data.cz));
//      this.csTextView.setText(String.valueOf(data.cs));
//      this.dxTextView.setText(String.valueOf(data.dx));
//      this.dyTextView.setText(String.valueOf(data.dy));
//      this.dzTextView.setText(String.valueOf(data.dz));
//      this.dsTextView.setText(String.valueOf(data.ds));
//
//      if (data.cs < 16) {
//        this.shakingCount--;
//      }
//      else {
//        this.shakingCount++;
//      }
//
//      this.shakingTextView.setText(String.valueOf(this.shakingCount));
//      if (this.shakingCount <= -8) {
//        this.goodButton.setEnabled(true);
//        this.OnClickStopAccButton();
//      }
//      if (this.shakingCount >= 8) {
//        this.shakingTextView.setTextColor(0xffffcc00);
//        this.goodButton.setEnabled(false);
//        this.OnClickStopAccButton();
//      }
//    }));

//    this.addSubscription("shaking", this.robot.shaking.observeOn(AndroidSchedulers.mainThread()).subscribe(shaking -> {
//        this.shakingCount++;
//        this.shakingTextView.setText(String.valueOf(this.shakingCount));
//        if (this.shakingCount >= 8) {
//          this.shakingTextView.setTextColor(0xffffcc00);
//          this.goodButton.setEnabled(false);
//          this.goodButton.setBackgroundColor(Color.GRAY);
//          this.OnClickStopAccButton();
//        }
//    }));
  }

  private int shakingCount = 0;
  @Override
  protected int getLayoutResId() {
    return R.layout.acc_standing_test_activity;
  }

  @OnClick(R.id.startAccButton)
  void OnClickStartAccButton() {
    this.robot.startAcc();
    this.startAccButton.setEnabled(false);
    this.stopAccButton.setEnabled(true);
    this.shakingCount = 0;
    this.shakingTextView.setText(String.valueOf(this.shakingCount));
  }

  @OnClick(R.id.stopAccButton)
  void OnClickStopAccButton() {
    this.robot.stopAcc();
    this.startAccButton.setEnabled(true);
    this.stopAccButton.setEnabled(false);
  }

  @OnClick(R.id.goodButton)
  void onClickGood() {
    Intent intent = new Intent(this, AccShakingTestActivity.class);
    this.startActivity(intent);
    this.removeAllSubscription();
    this.robot.stopAcc();
  }

  @OnClick(R.id.badButton)
  void onClickBad() {
    Intent intent = new Intent(this, ScanRobotActivity.class);
    this.startActivity(intent);
    this.removeAllSubscription();
    this.robot.stopAcc();
  }

  @Override
  protected void onDestroy() {
    this.robot.stopAcc();
    super.onDestroy();
  }
}
