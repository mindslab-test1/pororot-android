package com.jininsa.t800.tester.controller;

import android.content.Intent;

import com.jininsa.t800.tester.R;

import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class AccShakingTestActivity extends AccTestActivity {

  @Override
  public void doAfterChecking() {
//    this.addSubscription("rawAccData", this.robot.accStates.observeOn(AndroidSchedulers.mainThread()).subscribe(data -> {
//      this.pxTextView.setText(String.valueOf(data.px));
//      this.pyTextView.setText(String.valueOf(data.py));
//      this.pzTextView.setText(String.valueOf(data.pz));
//      this.psTextView.setText(String.valueOf(data.ps));
//      this.cxTextView.setText(String.valueOf(data.cx));
//      this.cyTextView.setText(String.valueOf(data.cy));
//      this.czTextView.setText(String.valueOf(data.cz));
//      this.csTextView.setText(String.valueOf(data.cs));
//      this.dxTextView.setText(String.valueOf(data.dx));
//      this.dyTextView.setText(String.valueOf(data.dy));
//      this.dzTextView.setText(String.valueOf(data.dz));
//      this.dsTextView.setText(String.valueOf(data.ds));
//
//      if (data.cs >= 1024) {
//        this.shakingCount++;
//      }

//      this.shakingTextView.setText(String.valueOf(this.shakingCount));
//
//      if (this.shakingCount >= 5) {
//        this.shakingTextView.setTextColor(0xff669900);
//        this.goodButton.setEnabled(true);
//      }
//    }));

    this.addSubscription("shaking", this.robot.accStates.observeOn(AndroidSchedulers.mainThread()).subscribe(shaking -> {
      if (shaking) {
        this.shakingCount++;
        this.shakingTextView.setText(String.valueOf(this.shakingCount));
        if (this.shakingCount >= 5) {
          this.shakingTextView.setTextColor(0xff669900);
          this.goodButton.setEnabled(true);
          this.goodButton.setBackgroundColor(0xff669900);
        }
      }
    }));
  }

  private int shakingCount;
  @Override
  protected int getLayoutResId() {
    return R.layout.acc_shaking_test_activity;
  }

  @OnClick(R.id.startAccButton)
  void OnClickStartAccButton() {
    this.robot.startAcc();
    this.startAccButton.setEnabled(false);
    this.stopAccButton.setEnabled(true);
    this.shakingCount = 0;
    this.shakingTextView.setText(String.valueOf(this.shakingCount));
  }

  @OnClick(R.id.stopAccButton)
  void OnClickStopAccButton() {
    this.robot.stopAcc();
    this.startAccButton.setEnabled(true);
    this.stopAccButton.setEnabled(false);
  }

  @OnClick(R.id.goodButton)
  void onClickGood() {
    this.robot.stopAcc();
    this.removeAllSubscription();
    Intent intent = new Intent(this, FdmTestActivity.class);
    this.startActivity(intent);
    this.finish();
  }

  @OnClick(R.id.badButton)
  void onClickBad() {
    this.robot.stopAcc();
    this.removeAllSubscription();
    Intent intent = new Intent(this, ScanRobotActivity.class);
    this.startActivity(intent);
    this.finish();
  }

  @Override
  protected void onDestroy() {
    this.robot.stopAcc();
    super.onDestroy();
  }
}
