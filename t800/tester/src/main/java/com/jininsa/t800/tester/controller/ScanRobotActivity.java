package com.jininsa.t800.tester.controller;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.connector.Scanner;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.model.Fail;
import com.jininsa.robot.model.robot.Robot;
import com.jininsa.t800.tester.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ScanRobotActivity extends RobotActivity {

  // !!! Butterknife 때문에 package-private으로 선언한다
  static class RobotListAdapter extends RecyclerView.Adapter<RobotListAdapter.ViewHolder> {
    public interface OnItemClickListener {
      void OnItemClick(Robot robot);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
      final View containerView;
      @BindView(R.id.stateTextView)
      TextView stateTextView;
      @BindView(R.id.macTextView)
      TextView macTextView;
      @BindView(R.id.proximityTextView)
      TextView proximityTextView;
      @BindView(R.id.okTextView)
      TextView okTextView;

      ViewHolder(View view) {
        super(view);
        this.containerView = view;
        ButterKnife.bind(this, view);
      }
    }

    private final List<Robot> robots;
    private final OnItemClickListener listener;

    RobotListAdapter(OnItemClickListener listener) {
      this.robots = new ArrayList<>();
      this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.scan_robot_list_item_view, parent, false);
      return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
      final Robot robot = this.robots.get(position);

      holder.macTextView.setText(robot.getMac());
      holder.stateTextView.setText(robot.getLifeCycle().name());
      holder.proximityTextView.setText(String.format("%sdB", robot.getProximity()));
//      if (robot.getLifeCycle() != Robot.LifeCycle.BORN) {
//        holder.okTextView.setText("Abnormal");
//        holder.okTextView.setTextColor(0xFFFF0000);
//      }
      holder.containerView.setOnClickListener(__ -> this.listener.OnItemClick(robot));
    }

    @Override
    public int getItemCount() {
      return this.robots.size();
    }

    void clear() {
      this.robots.clear();
      this.notifyDataSetChanged();
    }

    void addItem(final Robot robot) {
      this.robots.add(robot);
      this.notifyItemInserted(this.getItemCount());
    }

    void updateItem(final Robot robot) {
      Optional<Robot> old = Stream.of(this.robots).filter(r -> r.getMac().equals(robot.getMac())).findFirst();
      if (old.isPresent()) {
        int position = this.robots.indexOf(old.get());
        this.robots.remove(position);
        this.robots.add(position, robot);
        this.notifyItemChanged(position);
      }
    }
  }

  private static final String SUBSCRIPTION_SCANNER_RESULT = "4Er6SQNWznvdz87o";

  @BindView(R.id.scanRobotStateTextView)
  TextView scanRobotStateTextView;
  @BindView(R.id.robotListRecyclerView)
  RecyclerView robotListRecyclerView;
  @BindView(R.id.indicatorView)
  AVLoadingIndicatorView indicatorView;
  @BindView(R.id.startScanningButton)
  Button startScanningButton;
  @BindView(R.id.stopScanningButton)
  Button stopScanningButton;

  private RobotListAdapter robotListAdapter;
  private Scanner scanner;

  private void startScanning() {
    this.scanner.register();
    this.startScanningButton.setEnabled(false);
    this.stopScanningButton.setEnabled(true);
    this.indicatorView.setVisibility(View.VISIBLE);
    this.scanRobotStateTextView.setText("Scanning");
    this.robotListAdapter.clear();
    this.robotListRecyclerView.setAdapter(this.robotListAdapter);
    this.scanner.start();
  }

  private void stopScanning() {
    this.scanner.unregister();
    this.startScanningButton.setEnabled(true);
    this.stopScanningButton.setEnabled(false);
    this.indicatorView.setVisibility(View.GONE);
    this.scanRobotStateTextView.setText("");
    if (this.scanner.isWorking()) {
      this.scanner.stop();
    }
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.scan_robot_activity;
  }

  @Override
  public void doAfterChecking() {
    this.scanner = new Scanner(this, device -> {
      if (device == null) { return false; }
      String name = device.getName();
      return (name != null) && name.startsWith("POROROT");
    }, Scanner.Option.SCAN_ALL);

    this.robotListAdapter = new RobotListAdapter(robot -> {
      this.stopScanning();
      Robot.setSelectedRobot(robot);
      Intent intent = new Intent(this, LinkTestActivity.class);
      this.startActivity(intent);
      this.finish();
    });

    this.addSubscription(SUBSCRIPTION_SCANNER_RESULT,
      this.scanner.result.subscribe(result -> {
        XLog.v(result);
        if (result.state == Scanner.Result.State.FOUND_NEW_DEVICE || result.state == Scanner.Result.State.FOUND_CONNECTED_DEVICE) {
          final Robot foundRobot = new Robot(result.device.getAddress(), result.device.getName(), result.rssi);
          this.robotListAdapter.addItem(foundRobot);
          final String mac = foundRobot.getMac();
          this.addSubscription(mac, foundRobot.retrieve().observeOn(AndroidSchedulers.mainThread()).subscribe(
            robotFromServer -> {
              // !!! 발견로봇을 최신 정보로 없데이트 (robotFromServer에는 proximity가 없기 때문)
              foundRobot.refreshWith(robotFromServer);
              this.robotListAdapter.updateItem(foundRobot);
              this.removeSubscription(mac);
            },
            error -> {
              Pair<Fail.Problem, String> fail = Fail.parse(error);
              XLog.d("[%s] %s", fail, foundRobot);
              Fail.Problem problem = fail.first;
              // !!! 등록되지 않는 뽀로롯인 경우
              if (problem == Fail.Problem.ENTITY_NOT_FOUND || problem == Fail.Problem.ENTITY_NOT_FOUND_TEMP) {
                foundRobot.setLifeCycle(Robot.LifeCycle.UNBORN);
                this.robotListAdapter.updateItem(foundRobot);
              }
              // !!! 서버 장애나 네트워크 오류가 발생한 경우
              else {
                new MaterialDialog.Builder(this)
                  .cancelable(false)
                  .content("서버와 연결이 원활하지 않습니다. 잠시 후 다시 검색하세요.")
                  .positiveText("확인")
                  .show();
              }
              this.removeSubscription(mac);
            }));
        }

        if (result.state == Scanner.Result.State.FINISHED) {
          this.stopScanning();
          if (!this.scanner.isFound()) {
            this.scanRobotStateTextView.setText("Not Found");
          }
          else {
            this.scanRobotStateTextView.setText("Found");
          }
        }
      }));

    this.startScanningButton.setEnabled(true);
}

  @Override
  protected void onDestroy() {
    if (this.scanner != null) {
      this.scanner.unregister();
    }
    super.onDestroy();
  }

/*  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    List<Checker.PermissionCheck.PermissionCheckRequest> permissions = Lists.newArrayList();
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.ACCESS_COARSE_LOCATION));
    return permissions;
  }*/

//  @Override
//  protected void onStart() {
//    super.onStart();
//    if (this.isChecked() && !this.scanner.isWorking()) {
//      this.startScanning();
//    }
//  }
//
//  @Override
//  protected void onStop() {
//    if (this.scanner.isWorking()) {
//      this.scanner.stop();
//      this.startScanningButton.setEnabled(true);
//    }
//    super.onStop();
//  }

  @OnClick(R.id.startScanningButton)
  public void onClickStartScanning() {
    this.startScanning();
  }

  @OnClick(R.id.stopScanningButton)
  public void onClickStopScanning() {
    this.stopScanning();
  }
}
