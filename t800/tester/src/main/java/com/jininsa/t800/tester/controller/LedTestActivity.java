package com.jininsa.t800.tester.controller;

import android.content.Intent;
import android.widget.Button;

import com.jininsa.robot.app.RobotActivity;
import com.jininsa.t800.tester.R;

import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class LedTestActivity extends RobotActivity
{
  enum Step {
    RED_ON(R.id.redOnButton),
    RED_BLINK(R.id.redBlinkButton),
    BLUE_ON(R.id.blueOnButton),
    BLUE_BLINK(R.id.blueBlinkButton),
    BOTH_ON(R.id.redBlueOnButton),
    BOTH_BLINK(R.id.redBlueBlinkButton),
    OFF(R.id.offButton),
    RESET(R.id.resetButton),
    GOOD(R.id.goodButton);

    final int action;
    Step(final int action) {this.action = action;}

    static Step next(int action) {
      switch (action) {
        case R.id.redOnButton:
          return RED_BLINK;
        case R.id.redBlinkButton:
          return BLUE_ON;
        case R.id.blueOnButton:
          return BLUE_BLINK;
        case R.id.blueBlinkButton:
          return BOTH_ON;
        case R.id.redBlueOnButton:
          return BOTH_BLINK;
        case R.id.redBlueBlinkButton:
          return OFF;
        case R.id.offButton:
          return RESET;
        case R.id.resetButton:
          return GOOD;
        default:
          return RED_ON;
      }
    }
  }

  @Override
  public void doAfterChecking() {
    this.addSubscription("response", this.robot.ledStates.observeOn(AndroidSchedulers.mainThread()).subscribe(ledState -> {
      Button button = this.findViewById(this.action);
      button.setText(String.format("%s - OK", button.getText()));
      this.action = Step.next(this.action).action;
      button = this.findViewById(this.action);
      button.setEnabled(true);
    }));
  }

  private int action ;

  @Override
  protected int getLayoutResId()
  {
    return R.layout.led_test_activity;
  }

  @OnClick(R.id.redOnButton)
  void onClickRedOn(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.turnOnRedLed(0x0A);
  }

  @OnClick(R.id.redBlinkButton)
  void onClickRedBlink(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.turnOnRedLed(0x20);
  }

  @OnClick(R.id.blueOnButton)
  void onClickBlueOn(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.turnOnBlueLed(0x0A);
  }

  @OnClick(R.id.blueBlinkButton)
  void onClickBlueBlink(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.turnOnBlueLed(0x20);
  }

  @OnClick(R.id.redBlueOnButton)
  void onClickBothOn(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.turnOnBothLed(0x0A);
  }

  @OnClick(R.id.redBlueBlinkButton)
  void onClickBothBlink(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.turnOnBothLed(0x20);
  }

  @OnClick(R.id.offButton)
  void onClickOff(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.turnOffLed();
  }

  @OnClick(R.id.resetButton)
  void onClickReset(Button button) {
    this.action = button.getId();
    button.setEnabled(false);
    this.robot.resetLed();
  }

  @OnClick(R.id.goodButton)
  void onClickGood() {
    Intent intent = new Intent(this, AccShakingTestActivity.class);
    this.startActivity(intent);
    this.finish();
  }

  @OnClick(R.id.badButton)
  void onClickBad() {
    Intent intent = new Intent(this, ScanRobotActivity.class);
    this.startActivity(intent);
    this.finish();
  }
}
