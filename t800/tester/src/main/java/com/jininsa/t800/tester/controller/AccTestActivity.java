package com.jininsa.t800.tester.controller;

import android.widget.Button;
import android.widget.TextView;

import com.jininsa.robot.app.RobotActivity;
import com.jininsa.t800.tester.R;

import butterknife.BindView;

public abstract class AccTestActivity extends RobotActivity {
  @BindView(R.id.startAccButton)
  Button startAccButton;

  @BindView(R.id.stopAccButton)
  Button stopAccButton;

//  @BindView(R.id.pxTextView)
//  TextView pxTextView;
//  @BindView(R.id.pyTextView)
//  TextView pyTextView;
//  @BindView(R.id.pzTextView)
//  TextView pzTextView;
//  @BindView(R.id.cxTextView)
//  TextView cxTextView;
//  @BindView(R.id.cyTextView)
//  TextView cyTextView;
//  @BindView(R.id.czTextView)
//  TextView czTextView;
//  @BindView(R.id.dxTextView)
//  TextView dxTextView;
//  @BindView(R.id.dyTextView)
//  TextView dyTextView;
//  @BindView(R.id.dzTextView)
//  TextView dzTextView;
//  @BindView(R.id.psTextView)
//  TextView psTextView;
//  @BindView(R.id.csTextView)
//  TextView csTextView;
//  @BindView(R.id.dsTextView)
//  TextView dsTextView;
  @BindView(R.id.shakingTextView)
  TextView shakingTextView;
  @BindView(R.id.goodButton)
  Button goodButton;
}
