package com.jininsa.t800.tester.controller;

import android.app.AlertDialog;
import android.content.Intent;
import android.widget.Button;

import com.elvishew.xlog.XLog;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.model.robot.Robot;
import com.jininsa.t800.tester.R;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class FdmTestActivity extends RobotActivity {

  @BindView(R.id.setFdButton)
  Button setFdButton;

  @BindView(R.id.unsetFdButton)
  Button unsetFdButton;

  private int step;

  @Override
  public void doAfterChecking() {
    this.addSubscription("fdstate", this.robot.fdStates.observeOn(AndroidSchedulers.mainThread()).subscribe(factoryDefault -> {
      if (!factoryDefault && (step == R.id.unsetFdButton)) {
        this.setFdButton.setEnabled(true);
        this.unsetFdButton.setText(String.format("%s - OK", this.unsetFdButton.getText()));
        this.unsetFdButton.setEnabled(false);
      }
    }));
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.fdm_test_activity;
  }

  @OnClick(R.id.unsetFdButton)
  void onClickUnsetFd(Button button) {
    this.step = button.getId();
    button.setEnabled(false);
    this.robot.unsetFactoryDefault();
    Pauser.pause(1000);
    this.robot.retrieveFactoryDefaultState();
  }

  @OnClick(R.id.setFdButton)
  void onClickSetFd(Button button) {
    this.step = button.getId();
    button.setEnabled(false);
    this.robot.setFactoryDefault();
  }

  @OnClick(R.id.goodButton)
  void onClickGood() {
    // todo 서버에 등록
    this.robot.setLifeCycle(Robot.LifeCycle.BORN);
    XLog.d(this.robot);
    this.addSubscription("registerRobot", this.robot.register().observeOn(AndroidSchedulers.mainThread()).subscribe(
      robotFromServer -> {
        this.robot.refreshWith(robotFromServer);
        new AlertDialog.Builder(this, R.style.T800DialogTheme)
          .setTitle("COMPLETE")
          .setMessage(String.format("%s is %s", this.robot.getMac(), this.robot.getLifeCycle()))
          .setCancelable(false)
          .setPositiveButton("OK", (dialog, which) -> {
            this.robot.stop();
            dialog.dismiss();
            Intent intent = new Intent(this, ScanRobotActivity.class);
            this.startActivity(intent);
            this.finish();
          }).create().show();
      },
      error -> {
        this.robot.setLifeCycle(Robot.LifeCycle.NONE);
        new AlertDialog.Builder(this, R.style.T800DialogTheme)
          .setTitle("FAIL")
          .setMessage(String.format("%s is %s", this.robot.getMac(), this.robot.getLifeCycle()))
          .setCancelable(false)
          .setPositiveButton("OK", (dialog, which) -> {
            this.robot.stop();
            dialog.dismiss();
            Intent intent = new Intent(this, ScanRobotActivity.class);
            this.startActivity(intent);
            this.finish();
          }).create().show();
      }));
  }

  @OnClick(R.id.badButton)
  void onClickBad() {
    this.removeAllSubscription();
    this.robot.stop();
    Intent intent = new Intent(this, ScanRobotActivity.class);
    this.startActivity(intent);
    this.finish();
  }
}
