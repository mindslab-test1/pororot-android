package com.jininsa.t800.tester;

import android.os.Build;

import com.elvishew.xlog.LogConfiguration;
import com.elvishew.xlog.LogLevel;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Maps;
import com.jininsa.robot.app.App;
import com.jininsa.robot.app.Const;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.helper.XLogBorderFormatter;

import java.util.Collections;
import java.util.Map;

public class Tester extends App {

  @Override
  protected Const.Settings getSettings() {
    return new Const.Settings() {
      @Override
      public String robotName() {
        return "tester";
      }

      @Override
      public String chatBotId() {
        return "5977face65d4406c084583a6";
      }

      @Override
      public String ttsDatabaseName() {
        return "tts_single_db_kor_custom13.vtdb";
      }

      @Override
      public String earType() {
        return "ASR";
      }

      @Override
      public String bizServerUrl() { return BuildConfig.BIZ_SERVER_URL; }

      @Override
      public String chatBotServerUrl() { return BuildConfig.CHATBOT_URL; }

      @Override
      public String ttsKey() {
        return BuildConfig.TTS_KEY;
      }

      @Override
      public Map<Permission, String> permissionRationales() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          Map<Permission, String> permissionRationales = Maps.newHashMap();
          //permissionRationales.put(Permission.ACCESS_COARSE_LOCATION, String.format("주변 로봇을 찾기 위해 '%s' 권한을 요청합니다.", Permission.ACCESS_COARSE_LOCATION.desc));
          permissionRationales.put(Permission.CAMERA, String.format("로봇 식별 코드를 인식하기 위해 '%s' 권한을 요청합니다.", Permission.CAMERA.desc));
          permissionRationales.put(Permission.RECORD_AUDIO, String.format("음성을 인식하기 위해 '%s' 권한을 요청합니다.", Permission.RECORD_AUDIO.desc));
          permissionRationales.put(Permission.WRITE_EXTERNAL_STORAGE, String.format("노래와 책을 채생하기 위해 '%s' 권한을 요청합니다.", Permission.WRITE_EXTERNAL_STORAGE.desc));
          permissionRationales.put(Permission.READ_PHONE_STATE, String.format("전화번호로 가입하기 위해 '%s' 권한을 요청합니다.", Permission.READ_PHONE_STATE.desc));
          return permissionRationales;
        }
        else {
          return Collections.emptyMap();
        }
      }

      @Override
      public Class videoPlayerContext() {
        return null;
      }

      @Override
      public Boolean hasSpp() {
        return true;
      }

      @Override
      public Boolean isLite() { return false; }

      @Override
      public Boolean shouldPhoneControl() { return false; }
    };
  }

  @Override
  public void onCreate() {
    XLog.init(new LogConfiguration.Builder()
      .logLevel(BuildConfig.DEBUG ? LogLevel.ALL : LogLevel.NONE)
      .tag("T800T").t().b().st(1)
      .borderFormatter(XLogBorderFormatter.INSTANCE).build());
    super.onCreate();
  }
}
