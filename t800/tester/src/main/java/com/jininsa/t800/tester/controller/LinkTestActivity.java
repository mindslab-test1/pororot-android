package com.jininsa.t800.tester.controller;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.Button;
import android.widget.TextView;

import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jininsa.robot.app.RobotToolbarActivity;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.model.robot.Robot;
import com.jininsa.t800.tester.R;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class LinkTestActivity extends RobotToolbarActivity {
  @BindView(R.id.robotInfoTextView)
  TextView robotInfoTextView;

  @BindView(R.id.robotStateTextView)
  TextView robotStateTextView;

  @BindView(R.id.nextActionButton)
  Button nextActionButton;

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    List<Checker.PermissionCheck.PermissionCheckRequest> permissions = Lists.newArrayList();
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.WRITE_EXTERNAL_STORAGE));
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.RECORD_AUDIO));
    return permissions;
  }

  @Override
  public void doAfterChecking() {

    this.robot = Robot.getSelectedRobot();

    XLog.d("%s", this.robot);

    this.robotInfoTextView.setText(String.format("Connecting: %s", this.robot.getMac()));
    this.addSubscription("activatingStateRelay",
      this.robot.states.observeOn(AndroidSchedulers.mainThread()).subscribe((state) -> {
        String text = this.robotStateTextView.getText() + "\n" + state;
        this.robotStateTextView.setText(text);
        if (state == Robot.State.READY) {
          this.nextActionButton.setEnabled(true);
          Pauser.pauseAndRun(() -> this.robot.say("으라차차!"), 2000);
        }
      }));
    this.robot.start(this.getApplicationContext());
  }

  @OnClick(R.id.nextActionButton)
  void onClick() {
    this.robot.stopHearing();
    Intent intent = new Intent(this, LedTestActivity.class);
    this.startActivity(intent);
    this.finish();
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.link_robot_activity;
  }

  @Override
  protected String title() {
    return null;
  }
}
