package com.jininsa.t800.registrar.controller;

import android.content.Intent;
import android.graphics.Color;
import android.widget.Button;
import android.widget.TextView;

import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.model.robot.Robot;
import com.jininsa.t800.registrar.R;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class AssignUuidActivity extends RobotActivity {

  @BindView(R.id.macTextView)
  TextView macTextView;
  @BindView(R.id.uuidTextView)
  TextView uuidTextView;
  @BindView(R.id.indicatorView)
  AVLoadingIndicatorView indicatorView;
  @BindView(R.id.resultTextView)
  TextView resultTextView;
  @BindView(R.id.retryButton)
  Button retryButton;
  @BindView(R.id.nextButton)
  Button nextButton;

  @Override
  protected int getLayoutResId() {
    return R.layout.assign_uuid_activity;
  }

  @Override
  public void doAfterChecking() {
    this.robot = Robot.getSelectedRobot();
    this.macTextView.setText(this.robot.getMac());
    this.uuidTextView.setText(this.robot.getUuid());
    this.indicatorView.smoothToShow();
    this.addSubscription("registerRobotUuid", this.robot.assignUuid().observeOn(AndroidSchedulers.mainThread()).subscribe(
      uuid -> {
        this.indicatorView.smoothToHide();
        this.resultTextView.setTextColor(Color.GREEN);
        this.resultTextView.setText("SUCCESSFUL");
        this.nextButton.setEnabled(true);
      },
      error -> {
        this.resultTextView.setTextColor(Color.RED);
        this.resultTextView.setText("FAILED");
        this.nextButton.setEnabled(true);
        this.retryButton.setEnabled(true);
      }
    ));
  }

  @OnClick(R.id.nextButton)
  void onClickNext() {
    Intent intent = new Intent(this, ScanRobotActivity.class);
    this.startActivity(intent);
    this.finish();
  }

  @OnClick(R.id.retryButton)
  void onClickRetry() {
    this.retryButton.setEnabled(false);
    this.doAfterChecking();
  }
}

