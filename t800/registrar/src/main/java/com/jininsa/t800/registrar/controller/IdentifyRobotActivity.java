package com.jininsa.t800.registrar.controller;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.helper.Strings;
import com.jininsa.robot.model.robot.Robot;
import com.jininsa.t800.registrar.R;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import me.philio.pinentry.PinEntryView;

public class IdentifyRobotActivity extends RobotActivity {
  private static final String SUBSCRIPTION_CAMERA_PAUSER = "irTGHFswJsYsZS0C";

  @BindView(R.id.guideTextView)
  TextView bodyTextView;
  @BindView(R.id.qrCodeScannerView)
  ZBarScannerView scannerView;
  private final ZBarScannerView.ResultHandler scanResultHandler = (result) -> {
    this.completeRecognizingUuid(result.getContents());
    this.addSubscription(SUBSCRIPTION_CAMERA_PAUSER, Observable.just(Boolean.TRUE).delay(800, TimeUnit.MILLISECONDS)
      .observeOn(AndroidSchedulers.mainThread()).subscribe(__ -> this.scannerView.resumeCameraPreview(this.scanResultHandler)));
  };
  @BindView(R.id.serialNumberEditText)
  PinEntryView serialNumberEditTextView;

  private void completeRecognizingUuid(String scannedText) {
    XLog.d(this.robot);

    if (!Strings.isValidUuidV4(scannedText)) { // || !scannedText.equalsIgnoreCase(selectedRobot.getUuid())) {
      new MaterialDialog.Builder(this)
        .content(String.format("'%s'은 잘못된 식별코드입니다.", scannedText))
        .positiveText("확인")
        .show();
    }
    else {
      this.robot.setUuid(scannedText);
      this.route();
    }
  }

  private void route() {
    Intent intent = new Intent(this, AssignUuidActivity.class);
    ActivityCompat.startActivity(this, intent, null);
    this.finish();
  }

  @Override
  protected int getLayoutResId() { return R.layout.identify_robot_activity; }

  @Override
  public void doAfterChecking() {
    this.scannerView.setSquareViewFinder(true);
    this.scannerView.setAspectTolerance(0.85f);
    this.scannerView.setResultHandler(this.scanResultHandler);

    this.robot = Robot.getSelectedRobot();

    this.serialNumberEditTextView.setText(this.robot.getSeq());
    this.serialNumberEditTextView.setEnabled(false);
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    List<Checker.PermissionCheck.PermissionCheckRequest> permissions = Lists.newArrayList();
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.CAMERA));
    return permissions;
  }

  @Override
  protected void onPause() {
    this.scannerView.stopCamera();
    super.onPause();
  }

  @Override
  protected void onResume() {
    super.onResume();
    this.scannerView.startCamera();
  }
}
