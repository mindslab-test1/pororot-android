package com.jininsa.pororot.controller;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.Pair;
import android.support.v4.view.ViewPager;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jininsa.pororot.R;
import com.jininsa.pororot.view.AuthenticationFragment;
import com.jininsa.robot.connector.Authenticator;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.model.Fail;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;

abstract public class AuthenticateActivity extends PororotToolbarActivity implements AuthenticationFragment.Host {

  static final private String SUBSCRIPTION_RETRIEVE_PERSON = "pr8ahF55oRYKfZIg";

  static private class AuthenticationViewPagerAdapter extends FragmentPagerAdapter {
    final Authenticator.Action action;

    AuthenticationViewPagerAdapter(final FragmentManager fm, Authenticator.Action action) {
      super(fm);
      this.action = action;
    }

    @Override
    public Fragment getItem(final int position) {
      if (position == 0) {
        return AuthenticationFragment.create(Authenticator.Provider.PHONE, this.action);
      }
      else if (position == 1) {
        return AuthenticationFragment.create(Authenticator.Provider.EMAIL, this.action);
      }
      return null;
    }

    @Override
    public int getCount() {
      return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(final int position) {
      if (position == 0) {
        return String.format("{t800-phone}  전화번호로 %s", this.action.desc);
      }
      else if (position == 1) {
        return String.format("{t800-email}  이메일로 %s", this.action.desc);
      }
      return super.getPageTitle(position);
    }
  }

  @BindView(R.id.tab)
  TabLayout tab;
  @BindView(R.id.pager)
  ViewPager pager;

  @Override
  public void doAfterChecking() {
    this.pager.setAdapter(new AuthenticationViewPagerAdapter(this.getSupportFragmentManager(), this.getAction()));
    this.tab.setupWithViewPager(this.pager);
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    List<Checker.PermissionCheck.PermissionCheckRequest> permissions = Lists.newArrayList();
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.READ_PHONE_NUMBERS));
    }
    else {
      permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.READ_PHONE_STATE));
    }
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.READ_SMS));
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.RECEIVE_SMS));
    return permissions;
  }

  // @DebugLog
  @Override
  public void onAuthenticationSucceeded() {
    this.addSubscription(SUBSCRIPTION_RETRIEVE_PERSON, me.retrieve().observeOn(AndroidSchedulers.mainThread()).subscribe(
      person -> {
        this.me.setAuthenticated(person);
        XLog.d(this.me);
        Intent intent = new Intent(this, this.getDestination());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);
        this.finish();
        this.removeSubscription(SUBSCRIPTION_RETRIEVE_PERSON);
      },
      error -> {
        this.onAuthenticationFailed(error);
        this.removeSubscription(SUBSCRIPTION_RETRIEVE_PERSON);
      }));
  }

  // !!! todo 오류 메시지 처리 모듈 필요
  @Override
  public void onAuthenticationFailed(final Throwable error) {
    XLog.e(error);

    Pair<Fail.Problem, String> fail = Fail.parse(error);
    String message = Objects.requireNonNull(fail.second);

    new MaterialDialog.Builder(this)
      .cancelable(false)
      .title(String.format("로그인 하지 못했습니다.", this.getAction().desc))
      .content(message)
      .positiveText("확인")
      .show();
  }

  abstract public Authenticator.Action getAction();
  abstract public Class getDestination();
}
