package com.jininsa.pororot.controller;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jininsa.pororot.R;
import com.jininsa.robot.connector.Scanner;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Koreans;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.helper.Strings;
import com.jininsa.robot.model.Fail;
import com.jininsa.robot.model.party.Person;
import com.jininsa.robot.model.robot.Robot;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

// todo: 사용자가 바로 로그인으로 이동할 수 있는 수단 제공
public class ScanRobotsActivity extends PororotToolbarActivity {
  static class RobotListAdapter extends RecyclerView.Adapter<RobotListAdapter.Page> {
    static class Item {
      Robot robot;
      Class next;

      Item(final Robot robot) {
        this.robot = robot;
      }

      @Override
      public String toString() {
        return String.format(
          "{Item: {robot: %s, next: %s}}",
          this.robot,
          this.next);
      }
    }

    static class Page extends RecyclerView.ViewHolder {
      @BindView(R.id.serialTextView)
      TextView serialTextView;
      @BindView(R.id.familyAndNameTextView)
      TextView familyAndNameTextView;
      @BindView(R.id.actionButton)
      Button actionButton;

      Page(View view) {
        super(view);
        ButterKnife.bind(this, view);
      }
    }

    private final List<Item> items;
    private final Consumer<Item> action;

    RobotListAdapter(Consumer<Item> action) {
      this.items = Lists.newArrayList();
      this.action = action;
    }

    @Override
    public Page onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.scan_robots_item_view, parent, false);
      return new Page(view);
    }

    @Override
    public void onBindViewHolder(final Page page, int position) {
      final Item item = this.items.get(position);
      XLog.d("item=%s", item);
//      page.serialTextView.setText(String.format("뽀로롯[%s]", item.robot.getSeq()));

      // !!! 처음 시작할 때
      if (Robot.LifeCycle.NONE == item.robot.getLifeCycle()) {
        page.serialTextView.setText("정품 뽀로롯인지 확인합니다.");
      }


      // Edu AI_사용자가 연결된 상태(ADOPTED)
      else if (Robot.LifeCycle.SELLING == item.robot.getLifeCycle() || Robot.LifeCycle.OPEN == item.robot.getLifeCycle() || Robot.LifeCycle.ADOPTED == item.robot.getLifeCycle()) {
        page.actionButton.setText(Strings.fromHtml("<u>계속하기</u>"));
        item.next = LinkRobotActivity.class;
      }


      /*
      // !!! 처음 연결 상태(SELLING)
      else if (Robot.LifeCycle.SELLING == item.robot.getLifeCycle()) {
        page.serialTextView.setText(String.format("뽀로롯[%s]", item.robot.getSeq()));
        page.familyAndNameTextView.setText("가족이 필요한 뽀로롯입니다.");
        page.actionButton.setText(Strings.fromHtml("<u>가족 되기</u>"));
        item.next = IdentifyRobotActivity.class;
      }
      // !!! 사용자를 등록하지 않은 상태(OPEN)
      else if (Robot.LifeCycle.OPEN == item.robot.getLifeCycle()) {
        page.serialTextView.setText(String.format("%s %s", Koreans.attachYneOrNe(item.robot.getFamilyName()), item.robot.getName()));
        page.familyAndNameTextView.setText("인증이 필요한 뽀로롯입니다.");
        page.actionButton.setText(Strings.fromHtml("<u>인증 하기</u>"));
        item.next = IdentifyRobotActivity.class;
      }
      // !!! 사용자가 연결된 상태(ADOPTED)
      else if (Robot.LifeCycle.ADOPTED == item.robot.getLifeCycle()) {
        final Person me = Person.get();
        final List<String> uuids = Stream.of(me.getRobots()).map(Robot::getUuid).toList();
        boolean isMyRobot = uuids.contains(item.robot.getUuid());
        if (isMyRobot) {
          page.serialTextView.setText(String.format("뽀로롯[%s]", item.robot.getSeq()));
          page.familyAndNameTextView.setText(String.format("%s %s 찾았습니다.", Koreans.attachYneOrNe(item.robot.getFamilyName()), Koreans.attachLOrRl(item.robot.getName())));
          page.actionButton.setText(Strings.fromHtml("<u>연결하기</u>"));
          item.next = LinkRobotActivity.class;
        }
        else {
          page.serialTextView.setText(String.format("%s %s", Koreans.attachYneOrNe(item.robot.getFamilyName()), item.robot.getName()));
          page.familyAndNameTextView.setText("우리집 뽀로롯이 아닙니다.");
        }
      }
      */
      else {
        page.serialTextView.setText("정품 뽀로롯이 아닙니다.");
        page.actionButton.setText(Strings.fromHtml("<u>고객선터로 문의하세요.</u>"));
      }

      if (item.next != null) {
        page.actionButton.setOnClickListener(__ -> {
          try {
            this.action.accept(item);
          }
          catch (Exception e) {
            XLog.e(e.getMessage(), e);
          }
        });
      }
      else {
        page.actionButton.setOnClickListener(null);
      }
    }

    @Override
    public int getItemCount() {
      return this.items.size();
    }

    void addItem(final RobotListAdapter.Item item) {
      this.items.add(item);
      this.notifyItemInserted(this.getItemCount());
    }

    void clear() {
      this.items.clear();
      this.notifyDataSetChanged();
    }

    void updateItem(final RobotListAdapter.Item item) {
      Optional<Item> oldItemOpt = Stream.of(this.items).filter(old -> old.robot.getMac().equals(item.robot.getMac())).findFirst();
      if (oldItemOpt.isPresent()) {
        RobotListAdapter.Item oldItem = oldItemOpt.get();
        oldItem.robot = item.robot;
        int position = this.items.indexOf(oldItem);
        this.notifyItemChanged(position);
      }
      else {
        this.items.add(item);
        this.notifyItemInserted(this.getItemCount());
      }
    }
  }

  private static final String SUBSCRIPTION_SCANNER_RESULT = "4Er6SQNWznvdz87o";

  @BindView(R.id.dashBoard)
  View dashBoard;
  @BindView(R.id.robotListRecyclerView)
  RecyclerView robotListRecyclerView;
  @BindView(R.id.indicatorView)
  AVLoadingIndicatorView indicatorView;
  @BindView(R.id.stateTextView)
  TextView stateTextView;
  @BindView(R.id.rescanTextView)
  TextView rescanTextView;

  private RobotListAdapter robotListAdapter;
  private Scanner scanner;

  @Override
  protected int getLayoutResId() {
    return R.layout.scan_robots_activity;
  }

  @Override
  // @DebugLog
  public void doAfterChecking() {
    final Consumer<RobotListAdapter.Item> action = item -> {
      XLog.d(item);
      this.scanner.unregister();
      if (this.scanner.isWorking()) {
        this.scanner.stop();
      }
//      this.stopScanning();

      if (item.next != null) {
        Robot.setSelectedRobot(item.robot);
        Intent intent = new Intent(this, item.next);
        this.startActivity(intent);
        this.finish();
      }
    };

    this.robotListAdapter = new RobotListAdapter(action);

    this.robotListRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
    SnapHelper snapHelper = new PagerSnapHelper();
    this.robotListRecyclerView.setOnFlingListener(null);
    snapHelper.attachToRecyclerView(this.robotListRecyclerView);

    final Predicate<BluetoothDevice> filter =
      device -> {
        String name = device.getName();
        return (name != null) && (name.startsWith("POROROT"));
//        return (name != null) && (name.startsWith("EXO"));
      };

    this.scanner = new Scanner(this.getApplicationContext(), filter, Scanner.Option.SCAN_ALL);

    this.addSubscription(ScanRobotsActivity.SUBSCRIPTION_SCANNER_RESULT, this.scanner.result.subscribe(
      result -> {
        // !!! 로봇을 찾았다.
        if (result.state == Scanner.Result.State.FOUND_NEW_DEVICE || result.state == Scanner.Result.State.FOUND_CONNECTED_DEVICE) {
          final List<Robot> myRobots = this.me.getRobots();
          final Optional<Robot> foundRobotOpt = Stream.of(myRobots).filter(myRobot -> myRobot.getMac().equalsIgnoreCase(result.device.getAddress())).findSingle();
          final boolean isMyRobot = foundRobotOpt.isPresent();

          // !!! 발견한 로봇이 내 로봇이면 기존 로봇 객체 이용
          // !!! 내 로봇이 아니면 새로운 로봇 객체 생성
          final Robot foundRobot = isMyRobot ? foundRobotOpt.get() : new Robot(result.device.getAddress(), result.rssi);

          if (result.state == Scanner.Result.State.FOUND_NEW_DEVICE) {
            foundRobot.setState(Robot.State.FOUND);
          }
          if (result.state == Scanner.Result.State.FOUND_CONNECTED_DEVICE) {
            if (foundRobot.getState() != Robot.State.READY) {
              foundRobot.setState(Robot.State.LINKED);
            }
          }

          if (!isMyRobot) {
            final String mac = foundRobot.getMac();
            this.addSubscription(mac, foundRobot.retrieve().observeOn(AndroidSchedulers.mainThread()).subscribe(
              robotFromServer -> {
                // !!! 서버에서 받아온 정보로 발견한 로봇 정보 갱신
                foundRobot.refreshWith(robotFromServer);
                this.robotListAdapter.updateItem(new RobotListAdapter.Item(foundRobot));
                this.stateTextView.setText("뽀로롯을 찾았습니다.");
                this.removeSubscription(mac);
              },
              error -> {
                Pair<Fail.Problem, String> fail = Fail.parse(error);
                XLog.d("[%s] %s", fail, foundRobot);
                Fail.Problem problem = fail.first;
                this.rescanTextView.setVisibility(View.VISIBLE);
                this.dashBoard.setClickable(true);
                // !!! 등록되지 않는 뽀로롯인 경우
                if (problem == Fail.Problem.ENTITY_NOT_FOUND || problem == Fail.Problem.ENTITY_NOT_FOUND_TEMP) {
                  foundRobot.setLifeCycle(Robot.LifeCycle.UNBORN);
                  this.robotListAdapter.updateItem(new RobotListAdapter.Item(foundRobot));
                }
                // !!! 서버 장애나 네트워크 오류가 발생한 경우
                else {
                  new MaterialDialog.Builder(this)
                    .cancelable(false)
                    .content("서버와 연결이 원활하지 않습니다. 잠시 후 다시 검색하세요.")
                    .positiveText("확인")
                    .show();
                }
                this.removeSubscription(mac);
              }));
          }
          else {
            this.stateTextView.setText(String.format("뽀로롯[%s]을 찾았습니다.", foundRobot.getSeq()));
            this.robotListAdapter.updateItem(new RobotListAdapter.Item(foundRobot));
          }
        }

        // !!! 검색이 끝난 경우
        if (result.state == Scanner.Result.State.FINISHED) {
          this.indicatorView.setVisibility(View.INVISIBLE);
          this.rescanTextView.setVisibility(View.VISIBLE);
          this.dashBoard.setClickable(true);
          // !!! 뽀로롯을 찾지 못한 경우
          if (!this.scanner.isFound()) {
            this.stateTextView.setText("뽀로롯을 찾지 못했습니다.");
          }
          else {
            this.stateTextView.setText(String.format("뽀로롯을 %s대 찾았습니다.", this.robotListAdapter.getItemCount()));
          }
        }
      }));
    this.scanner.register();
    this.scan();
  }

  @Override
  protected void onDestroy() {
    if (this.scanner != null) {
      this.scanner.unregister();
    }
    super.onDestroy();
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    List<Checker.PermissionCheck.PermissionCheckRequest> permissions = Lists.newArrayList();
//    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.ACCESS_COARSE_LOCATION));
    return permissions;
  }

  @Override
  // @DebugLog
  protected void onStart() {
    super.onStart();
    if (this.isChecked() && !this.scanner.isWorking()) {
      this.scan();
    }
  }

  @Override
  // @DebugLog
  protected void onStop() {
    if (this.scanner.isWorking()) {
      this.scanner.stop();
    }
    super.onStop();
  }

//  private void stopScanning() {
//    if (this.scanner != null) {
//      this.scanner.stop();
//    }
//    this.rescanTextView.setVisibility(View.VISIBLE);
//  }

  private void scan() {
    this.robotListAdapter.clear();
    this.indicatorView.setVisibility(View.VISIBLE);
    this.rescanTextView.setVisibility(View.INVISIBLE);
    this.dashBoard.setClickable(false);
    this.robotListRecyclerView.setAdapter(this.robotListAdapter);
    this.stateTextView.setText("뽀로롯을 찾고 있습니다.");
    this.scanner.start();
  }

  @OnClick(R.id.dashBoard)
  void onClickRescanButton() {
    this.scan();
  }

  @Override
  protected String title() {
    return "뽀로롯 검색";
  }
}
