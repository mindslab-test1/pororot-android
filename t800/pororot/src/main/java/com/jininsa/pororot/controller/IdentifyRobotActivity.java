package com.jininsa.pororot.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.annimon.stream.Stream;
import com.google.common.collect.Lists;
import com.jininsa.pororot.R;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Koreans;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.helper.Strings;
import com.jininsa.robot.model.robot.Robot;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import me.dm7.barcodescanner.zbar.ZBarScannerView;
import me.philio.pinentry.PinEntryView;

public class IdentifyRobotActivity extends PororotToolbarActivity {
  private static final String SUBSCRIPTION_CAMERA_PAUSER = "irTGHFswJsYsZS0C";
  private static final String SUBSCRIPTION_ADOPT_ROBOT = "hGw6YdgxCdYSoAEQ";

  @BindView(R.id.guideTextView)
  TextView bodyTextView;
  @BindView(R.id.qrCodeScannerView)
  ZBarScannerView scannerView;
  private final ZBarScannerView.ResultHandler scanResultHandler = (result) -> {
    this.completeRecognizingUuid(result.getContents());
    this.addSubscription(SUBSCRIPTION_CAMERA_PAUSER, Observable.just(Boolean.TRUE).delay(800, TimeUnit.MILLISECONDS)
      .observeOn(AndroidSchedulers.mainThread()).subscribe(__ -> this.scannerView.resumeCameraPreview(this.scanResultHandler)));
  };
  @BindView(R.id.serialNumberEditText)
  PinEntryView serialNumberEditTextView;

  private void completeRecognizingUuid(String scannedText) {
    Robot selectedRobot = Robot.getSelectedRobot();

    if (!Strings.isValidUuidV4(scannedText) || !scannedText.equalsIgnoreCase(selectedRobot.getUuid())) {
      new MaterialDialog.Builder(this)
        .content(String.format("'%s'은 잘못된 식별코드입니다.", scannedText))
        .positiveText("확인")
        .show();
    }
    else {
      this.route();
    }
  }

  private void route() {
    Intent intent = new Intent(this, LinkRobotActivity.class);
    ActivityCompat.startActivity(this, intent, null);
    this.finish();
  }

  @Override
  protected String title() {
    return "뽀로롯 확인";
  }

  @Override
  protected int getLayoutResId() { return R.layout.identify_robot_activity; }

  @Override
  public void doAfterChecking() {
    this.scannerView.setSquareViewFinder(true);
    this.scannerView.setAspectTolerance(0.85f);
    this.scannerView.setResultHandler(this.scanResultHandler);

    Robot selectedRobot = Robot.getSelectedRobot();

    if (selectedRobot.getLifeCycle() == Robot.LifeCycle.SELLING) {
      this.bodyTextView.setText("다음 일련번호를 가진 뽀로롯의\nQR 코드를 찍어주세요.");
      this.serialNumberEditTextView.setText(selectedRobot.getSeq());
      this.serialNumberEditTextView.setEnabled(false);
    }
    else if (selectedRobot.getLifeCycle() == Robot.LifeCycle.OPEN) {
      this.bodyTextView.setText(String.format("뽀로롯 일련번호를 입력하거나 QR 코드를 찎어주세요.\n이 작업은 한 번만 하면 됩니다.", Koreans.attachYneOrNe(selectedRobot.getFamilyName())));
      this.serialNumberEditTextView.setFocusable(true);
      if (this.serialNumberEditTextView.requestFocus()) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
          imm.showSoftInput(this.serialNumberEditTextView, InputMethodManager.SHOW_IMPLICIT);
        }
      }
      this.serialNumberEditTextView.setOnPinEnteredListener(text -> {
        String seq = selectedRobot.getSeq();
        if (!seq.equalsIgnoreCase(text)) {
          new MaterialDialog.Builder(this)
            .content("잘못된 일련번호입니다.")
            .positiveText("확인")
            .onPositive((dialog, which) -> this.serialNumberEditTextView.setText(""))
            .show();
        }
        // !!! OPEN 상태 뽀로롯은 전부 ADOPTED로 바꾼다.
        else {
          boolean isOldFamily = Stream.of(this.me.getFamilies()).filter(family -> family.getId().equals(selectedRobot.getFamilyId())).findFirst().isPresent();
          if (isOldFamily) {
            this.addSubscription(SUBSCRIPTION_ADOPT_ROBOT, selectedRobot.adoptToOldFamily(this.me.getAuthToken().token).subscribe(
              robotFromServer -> {
                selectedRobot.refreshWith(robotFromServer);
                this.route();
                this.removeSubscription(SUBSCRIPTION_ADOPT_ROBOT);
              }));
          }
          else {
            this.addSubscription(SUBSCRIPTION_ADOPT_ROBOT, selectedRobot.adoptToNewFamily(this.me.getAuthToken().token).subscribe(
              robotFromServer -> {
                selectedRobot.refreshWith(robotFromServer);
                this.me.addFamily(selectedRobot.getFamily());
                this.me.addRobot(selectedRobot);
                this.route();
                this.removeSubscription(SUBSCRIPTION_ADOPT_ROBOT);
              }));
          }
        }
      });
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    List<Checker.PermissionCheck.PermissionCheckRequest> permissions = Lists.newArrayList();
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.CAMERA));
    return permissions;
  }

  @Override
  protected void onPause() {
    this.scannerView.stopCamera();
    super.onPause();
  }

  @Override
  protected void onResume() {
    super.onResume();
    this.scannerView.startCamera();
  }
}
