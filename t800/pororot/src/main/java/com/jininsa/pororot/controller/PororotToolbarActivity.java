package com.jininsa.pororot.controller;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jininsa.pororot.Icons;
import com.jininsa.robot.app.Key;
import com.jininsa.pororot.R;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.app.ExitActivity;
import com.jininsa.robot.app.RobotToolbarActivity;
import com.jininsa.robot.connector.Authenticator;
import com.pisome.iconify.IconDrawable;

public abstract class PororotToolbarActivity extends RobotToolbarActivity {

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    this.getMenuInflater().inflate(R.menu.toolbar_menu, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(final Menu menu) {
    MenuItem shutdownMenuItem = menu.findItem(R.id.shutDownMenuItem);
    IconDrawable icon = new IconDrawable(this.getApplicationContext(), Icons.Type.t800_off).colorRes(R.color.white).sizeDp(20);
    shutdownMenuItem.setIcon(icon);
    MenuItem logoutMenuItem = menu.findItem(R.id.logoutMenuItem);
    if (this.me.isAuthenticated()) {
      logoutMenuItem.setVisible(true);
      IconDrawable icon2 = new IconDrawable(this.getApplicationContext(), Icons.Type.t800_logout).colorRes(R.color.white).sizeDp(20);
      logoutMenuItem.setIcon(icon2);
    }
    else {
      logoutMenuItem.setVisible(false);
    }
    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(final MenuItem item) {
    if (item.getItemId() == R.id.shutDownMenuItem) {
      new MaterialDialog.Builder(this)
        .cancelable(false)
        .content("뽀로롯을 종료합니다.")
        .positiveText("확인")
        .negativeText("취소")
        .onPositive((dialog, which) -> {
          if (this.robot != null) {
            this.robot.stop();
          }
          ExitActivity.exit(this.getApplicationContext());
        })
        .show();
    }
    else if (item.getItemId() == R.id.logoutMenuItem) {
      new MaterialDialog.Builder(this)
        .cancelable(false)
        .content(String.format("%s님, 로그아웃합니다.", this.me.getNickname()))
        .positiveText("확인")
        .negativeText("취소")
        .onPositive(
          (dialog, which) -> Authenticator.signOut().subscribe(
            __ -> {
              Env.setString(Key.LAST_ROBOT_UUID, Env.EMPTY_STRING);
              if (this.robot != null) {
                this.robot.terminate();
              }
              this.me.setAuthToken(null);
              Intent intent = new Intent(this, StartActivity.class);
              ActivityCompat.startActivity(this, intent, null);
              this.finish();
            }))
        .show();

    }
    return super.onOptionsItemSelected(item);
  }
}
