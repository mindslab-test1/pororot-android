package com.jininsa.pororot.controller;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.elvishew.xlog.XLog;
import com.jininsa.robot.helper.Env;
import com.jininsa.pororot.R;
import com.jininsa.robot.helper.Koreans;
import com.jininsa.robot.model.party.Family;
import com.jininsa.robot.model.robot.Robot;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

public class WelcomeActivity extends PororotToolbarActivity implements TextView.OnEditorActionListener {
  static final private String SUBSCRIPTION_ADOPT = "qLAg7VYcYrTM1PCW";
  @BindView(R.id.oldFamilySpinner)
  MaterialBetterSpinner oldFamilySpinner;
  @BindView(R.id.familyNameEditText)
  MaterialEditText familyNameEditText;
  @BindView(R.id.robotNameEditText)
  MaterialEditText robotNameEditText;
  @BindView(R.id.nextButton)
  Button nextButton;

  private Family family;
  private String robotName = Env.EMPTY_STRING;

  @Override
  protected int getLayoutResId() {
    return R.layout.welcome_activity;
  }

  @Override
  public void doAfterChecking() {
    this.robot.stopHearing();

    this.robot.say("반가워요! 뽀로롯은 가족과 이름이 필요해요.");

    this.familyNameEditText.setText(this.robot.getFamilyName());

    this.familyNameEditText.setOnFocusChangeListener(
      (view, hasFocus) -> {
        if (!hasFocus) {
          final String newFamilyName = this.familyNameEditText.getText().toString();
          if (!newFamilyName.isEmpty()) {
            this.oldFamilySpinner.setText(Env.EMPTY_STRING);
            this.family = new Family(newFamilyName);
          }
          else {
            this.family = null;
          }
        }
      });

    if (this.me.getFamilies().isEmpty()) {
      this.oldFamilySpinner.setVisibility(View.GONE);
    }
    else {
      final ArrayAdapter<String> familyListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, this.me.getFamilyNames());
      this.oldFamilySpinner.setAdapter(familyListAdapter);
      this.oldFamilySpinner.setOnItemClickListener((parent, view, position, id) -> {
        this.familyNameEditText.setText(Env.EMPTY_STRING);
        this.family = this.me.getFamilies().get(position);
        XLog.d(this.family);
      });

      // !!! 필요없음.
//      if (this.robot.hasFamily()) {
//        Family robotFamily = this.robot.getFamily();
//        Optional<IntPair<Family>> intFamilyPairOpt = Stream.of(this.me.getFamilies()).indexed().filter(pair -> pair.getSecond().getId().equals(robotFamily.getId()))
//          .findSingle().executeIfPresent(pair -> this.oldFamilySpinner.setListSelection(pair.getFirst()));
//      }
    }

    this.robotNameEditText.setText(this.robot.getName());
    this.robotNameEditText.setOnEditorActionListener(this);
    this.robotNameEditText.setOnFocusChangeListener(
      (view, hasFocus) -> {
        if (!hasFocus) {
          this.robotName = this.robotNameEditText.getText().toString();
        }
      });
  }

  @Override
  protected String title() {
    return "뽀로롯 환영하기";
  }

  @OnClick(R.id.nextButton)
  void onClickNext() {
    if (this.family == null) {
      this.robot.say("우리 가족은 누구일까?");
      this.familyNameEditText.requestFocus();
      return;
    }

    if (this.robotName.isEmpty()) {
      this.robot.say("내 이름은 뭘까?");
      this.robotNameEditText.requestFocus();
      return;
    }

    this.robot.say(String.format("%s %s 되어 볼까요.", Koreans.attachYneOrNe(this.family.getName()), Koreans.attachYOrGa(this.robotName)));

    Consumer<Robot> onNext = robotFromServer -> {
      XLog.d(robotFromServer);
      this.robot.refreshWith(robotFromServer);
      // !!! 새로운 가족이면
      // !!! 가족 추가, 로봇 추가 해야 한다.
      if (this.family.getId().equals(Env.DEFAULT_ID)) {
        this.me.addFamily(this.robot.getFamily());
        this.me.addRobot(this.robot);
      }

      this.robot.unsetFactoryDefault();
      this.robot.say(String.format("%s %s집 가족이 되었어. 신난다, 신나!", Koreans.attachYOrGa(this.robot.getName()), Koreans.attachYneOrNe(this.robot.getFamilyName())));

      Intent intent = new Intent(this, HomeActivity.class);
      ActivityCompat.startActivity(this, intent, null);
      this.finish();
      this.removeSubscription(SUBSCRIPTION_ADOPT);
    };

    Consumer<Throwable> onError = error -> {
      XLog.e(error);
      this.robot.say("뭔가 문제가 생겼나봐...");
      this.removeSubscription(SUBSCRIPTION_ADOPT);
    };

    XLog.d("%s %s", this.family, this.robotName);

    // !!! 새로운 가족
    if (this.family.getId().equals(Env.DEFAULT_ID)) {
      this.addSubscription(SUBSCRIPTION_ADOPT, this.robot.adoptToNewFamily(this.me.getAuthToken().token, this.family.getName(), this.robotName).observeOn(AndroidSchedulers.mainThread()).subscribe(onNext, onError));
    }
    // !!! 기존 가족
    else {
      this.addSubscription(SUBSCRIPTION_ADOPT, this.robot.adoptToOldFamily(this.me.getAuthToken().token, this.robotName, this.family).observeOn(AndroidSchedulers.mainThread()).subscribe(onNext, onError));
    }
  }

  @Override
  // @DebugLog
  public boolean onEditorAction(final TextView view, final int actionId, final KeyEvent event) {
    if (actionId == EditorInfo.IME_ACTION_NEXT) {
      view.clearFocus();
      InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
      if (imm != null) {
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
      }
      return true;
    }
    return false;
  }
}