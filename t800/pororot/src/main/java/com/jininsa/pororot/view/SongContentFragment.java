package com.jininsa.pororot.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jininsa.pororot.R;
import com.jininsa.pororot.T800;
import com.jininsa.pororot.controller.SongPlayerActivity;
import com.jininsa.robot.app.BaseFragment;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.model.contents.Song;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

public class SongContentFragment extends BaseFragment {
  static class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
      final View containerView;
      @BindView(R.id.titleTextView)
      TextView contentTitleTextView;
      @BindView(R.id.descTextView)
      TextView contentArtistAndPlayTimeTextView;
      @BindView(R.id.thumbnailImageView)
      ImageView albumImageView;

      ViewHolder(View view) {
        super(view);
        this.containerView = view;
        ButterKnife.bind(this, view);
      }
    }

    private final List<Song> items;
    private final Consumer<Song> action;
    private final Context context;

    SongListAdapter(Consumer<Song> action, Context context) {
      this.items = Lists.newArrayList();
      this.action = action;
      this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contents_item, parent, false);
      return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      holder.contentTitleTextView.setText(items.get(position).getTitle());
      holder.contentArtistAndPlayTimeTextView.setText(items.get(position).getArtist());
      if (!items.get(position).getImageUrl().isEmpty()) {
        T800.imageLoader.loadImage(holder.albumImageView, items.get(position).getImageUrl());
      }
      holder.containerView.setOnClickListener(__ -> {
        try {
          this.action.accept(this.items.get(position));
          Bundle bundle = new Bundle();
          bundle.putString("title", this.items.get(position).getTitle());
          FirebaseAnalytics.getInstance(context).logEvent("Song", bundle);
        }
        catch (Exception e) {
          XLog.e(e.getMessage(), e);
        }
      });
    }

    @Override
    public int getItemCount() {
      return items.size();
    }

    void reset() {
      int size = this.getItemCount();
      this.items.clear();
      this.notifyItemRangeRemoved(0, size);
    }

    void addItem(final Song item) {
      this.items.add(item);
      this.notifyItemInserted(this.getItemCount());
    }

    void setItems(final List<Song> items) {
      this.items.clear();
      this.items.addAll(items);
      this.notifyItemInserted(this.getItemCount());
    }
  }

  @BindView(R.id.songListRecyclerView)
  RecyclerView songListRecyclerView;
  private SongListAdapter songListAdapter;

  @Override
  protected int getLayoutResId() {
    return R.layout.song_fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    this.songListAdapter = new SongListAdapter(song -> {
      Intent intent = new Intent(this.getActivity(), SongPlayerActivity.class);

      intent.putExtra("title", song.getTitle());
      intent.putExtra("artist", song.getArtist());
      intent.putExtra("code", song.getCode());
      intent.putExtra("imageUrl", song.getImageUrl());

      this.startActivity(intent);
    }, getContext());
    this.songListRecyclerView.setAdapter(this.songListAdapter);
    this.songListAdapter.setItems(ContentManager.get().getSongs());
    return view;
  }
}
