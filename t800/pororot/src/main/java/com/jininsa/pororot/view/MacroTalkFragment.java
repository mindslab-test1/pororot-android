package com.jininsa.pororot.view;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jininsa.pororot.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

public class MacroTalkFragment extends HomeFragment {
  static class MacroTalkListAdapter extends RecyclerView.Adapter<MacroTalkFragment.MacroTalkListAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
      final View containerView;
      @BindView(R.id.talkTextView)
      TextView talkTextView;

      ViewHolder(View view) {
        super(view);
        this.containerView = view;
        ButterKnife.bind(this, view);
      }
    }

    private final List<String> items;
    private final Consumer<String> action;

    MacroTalkListAdapter(Consumer<String> action) {
      this.items = Lists.newArrayList();
      this.action = action;
    }

    @Override
    public MacroTalkFragment.MacroTalkListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.talk_macro_item, parent, false);
      return new MacroTalkFragment.MacroTalkListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MacroTalkFragment.MacroTalkListAdapter.ViewHolder holder, int position) {
      holder.talkTextView.setText(items.get(position));
      holder.containerView.setOnClickListener(__ -> {
        try {
          this.action.accept(this.items.get(position));
        }
        catch (Exception e) {
          XLog.e(e.getMessage(), e);
        }
      });
    }

    @Override
    public int getItemCount() {
      return items.size();
    }

    void reset() {
      int size = this.getItemCount();
      this.items.clear();
      this.notifyItemRangeRemoved(0, size);
    }

    void addItem(final String item) {
      this.items.add(item);
      this.notifyItemInserted(this.getItemCount());
    }

    void setItems(final List<String> items) {
      this.items.clear();
      this.items.addAll(items);
      this.notifyItemInserted(this.getItemCount());
    }
  }

  @BindView(R.id.macroTalkListRecyclerView)
  RecyclerView macroTalkListRecyclerView;
  MacroTalkListAdapter macroTalkListAdapter;
  List<String> talkList = new ArrayList<>();

  @Override
  protected int getLayoutResId() {
    return R.layout.talk_macro_fragment;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    macroTalkListAdapter = new MacroTalkListAdapter(macro -> {
      ((TalkFragment) getParentFragment()).messageInputView.getInputEditText().setText(macro);
      ((TalkFragment) getParentFragment()).closeMacro();
    });
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    return view;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    macroTalkListRecyclerView.setAdapter(macroTalkListAdapter);
    macroTalkListAdapter.setItems(talkList);
  }

  public void onDestroyView() {
    super.onDestroyView();
  }
}
