package com.jininsa.pororot.controller;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Sets;
import com.jininsa.robot.app.App;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.model.robot.Robot;

import java.util.List;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class StartActivity extends RobotActivity {
  static final private String SUBSCRIPTION_BRAIN_ACTIVATOR = "O9xxT36MFnH2W8K8";
  static final private String SUBSCRIPTION_RETRIEVE_PERSON = "pr8ahF55oRYKfZIg";

//  @Override
//  protected void onCreate(@Nullable final Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    FirebaseApp.initializeApp(this.getApplicationContext());
//  }

  @Override
  protected int getLayoutResId() {
    return Env.DEFAULT_INT;
  }

  @Override
  public Set<Checker.Subject> getCheckSubjects() {
    return Sets.newHashSet(Checker.Subject.NETWORK);
  }

  @Override
  public void doAfterChecking() {
    if (App.isBrainActivated()) {
      this.route();
    }
    else {
      this.addSubscription(SUBSCRIPTION_BRAIN_ACTIVATOR, App.brainActivator.filter(on -> on).subscribe(__ -> this.route()));
    }
  }

  private void route() {

    // Edu AI_사용자 로그인을 없애기 위한 코드
      Intent intent = new Intent(this, ScanRobotsActivity.class);
      ActivityCompat.startActivity(this, intent, null);
      this.finish();
    //


/*
    // !!! 앱을 새로 설치해서 사용자 정보가 남아 있지 않은 경우 로봇 검색으로
    // !!! 사용자 정보가 있는 경우 사인인으로
    if (!this.me.isGhost()) {
      this.addSubscription(SUBSCRIPTION_RETRIEVE_PERSON, this.me.retrieve().observeOn(AndroidSchedulers.mainThread()).subscribe(
        person -> {
          // !!! 토큰이 유효하면 고객 정보를 가져오고 마지막 연결 뽀로롯을 자동으로 연결한다.
          this.me.setAuthenticated(person);
          XLog.d(this.me);
          Class next;
          List<Robot> myRobots = this.me.getRobots();
          if (this.robot != null) {
            Stream.of(myRobots).filter(robot -> robot.getUuid().equalsIgnoreCase(this.robot.getUuid())).findSingle().ifPresent(
              robot -> {
                this.robot.refreshWith(robot);
                this.me.removeRobot(robot);
                this.me.addRobot(this.robot);
              });

            next = (this.robot.getState() == Robot.State.READY && this.robot.getLifeCycle() == Robot.LifeCycle.ADOPTED) ? HomeActivity.class : ScanMyRobotActivity.class;
          }
          else {
            next = ScanMyRobotActivity.class;
          }

          Intent intent = new Intent(this, next);
          ActivityCompat.startActivity(this, intent, null);
          this.finish();
        },
        error -> {
          // !!! 토큰이 비정상이면 사인인으로
          XLog.d(error);
          this.me.setAuthToken(null);
          Intent intent = new Intent(this, SignInActivity.class);
          ActivityCompat.startActivity(this, intent, null);
          this.finish();
        }));
    }
    else {
      // !!! 토큰이 없으면 사인인으로
      Intent intent = new Intent(this, SignInActivity.class);
      ActivityCompat.startActivity(this, intent, null);
      this.finish();
    }
*/
  }
}
