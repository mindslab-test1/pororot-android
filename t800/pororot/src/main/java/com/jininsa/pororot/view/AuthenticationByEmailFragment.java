package com.jininsa.pororot.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.jininsa.robot.helper.Env;
import com.jininsa.pororot.R;
import com.jininsa.robot.connector.Authenticator;
import com.jininsa.robot.helper.Strings;

import java.util.Objects;

import butterknife.BindView;

public class AuthenticationByEmailFragment extends AuthenticationFragment {

  @BindView(R.id.emailEditText)
  EditText emailEditText;
  @BindView(R.id.passwordEditText)
  EditText passwordEditText;
  @BindView(R.id.passwordConfirmEditText)
  EditText passwordConfirmEditText;

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    this.authenticator = Authenticator.create(Authenticator.Provider.EMAIL, this);
    if (this.action == Authenticator.Action.SIGN_IN) {
      this.passwordConfirmEditText.setVisibility(View.GONE);
    }
    return view;
  }

  @Override
  public void authenticate() {
    Activity activity = Objects.requireNonNull(this.getActivity());
    String email = this.emailEditText.getText().toString();
    if (email.isEmpty()) {
      new MaterialDialog.Builder(activity)
        .cancelable(false)
        .content(String.format("이메일을 입력하세요.", email))
        .positiveText("확인")
        .onPositive(
          (dialog, which) -> {
            this.emailEditText.setText(Env.EMPTY_STRING);
            this.emailEditText.requestFocus();
          })
        .show();
      return;
    }

    if (!Strings.isValidEmail(email)) {
      new MaterialDialog.Builder(activity)
        .cancelable(false)
        .content(String.format("%s는 잘못된 이메일입니다.", email))
        .positiveText("확인")
        .onPositive(
          (dialog, which) -> {
            this.emailEditText.setText(Env.EMPTY_STRING);
            this.emailEditText.requestFocus();
          })
        .show();
      return;
    }

    String password = this.passwordEditText.getText().toString();

    if (this.action == Authenticator.Action.SIGN_UP) {
      if (password.length() < 6) {
        new MaterialDialog.Builder(activity)
          .cancelable(false)
          .content("비밀번호는 6글자 이상이어야 합니다.")
          .positiveText("확인")
          .onPositive(
            (dialog, which) -> {
              this.passwordEditText.setText(Env.EMPTY_STRING);
              this.passwordConfirmEditText.setText(Env.EMPTY_STRING);
              this.passwordEditText.requestFocus();
            })
          .show();
        return;
      }
      String passwordConfirm = this.passwordConfirmEditText.getText().toString();
      if (!password.equals(passwordConfirm)) {
        new MaterialDialog.Builder(activity)
          .cancelable(false)
          .content("비밀번호를 확인하세요.")
          .positiveText("확인")
          .onPositive(
            (dialog, which) -> {
              this.passwordEditText.setText(Env.EMPTY_STRING);
              this.passwordConfirmEditText.setText(Env.EMPTY_STRING);
              this.passwordConfirmEditText.requestFocus();
            })
          .show();
        return;
      }
    }

    this.params.put("email", email);
    this.params.put("password", password);

    this.loadingProgress.show();

    if (this.action == Authenticator.Action.SIGN_IN) {
      this.authenticator.signIn(this.params);
    }
    else if (this.action == Authenticator.Action.SIGN_UP) {
      this.authenticator.signUp(this.params);
    }
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.authentication_by_email_fragment;
  }
}
