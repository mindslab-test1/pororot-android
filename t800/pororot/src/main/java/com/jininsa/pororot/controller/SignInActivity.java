package com.jininsa.pororot.controller;

import android.content.Intent;

import com.jininsa.pororot.R;
import com.jininsa.robot.connector.Authenticator;

import java.util.Collections;
import java.util.Map;

import butterknife.OnClick;

public class SignInActivity extends AuthenticateActivity {

  @OnClick(R.id.moveToSignUpButton)
  void onClickMoveToSignUpButton() {
    Intent intent = new Intent(this, SignUpActivity.class);
    this.startActivity(intent);
  }

  @Override
  protected String title() {
    return "로그인";
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.sign_in_activity;
  }

  @Override
  public void onAuthenticationNotReady() {}

  @Override
  public boolean isReady() {
    return true;
  }

  @Override
  public Map<String, String> getAuthenticationParameters() {
    return Collections.emptyMap();
  }

  @Override
  public Authenticator.Action getAction() {
    return Authenticator.Action.SIGN_IN;
  }

  @Override
  public Class getDestination() {
    return ScanMyRobotActivity.class;
  }
}
