package com.jininsa.pororot.controller;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.widget.TextView;

import com.annimon.stream.Optional;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.elvishew.xlog.XLog;
import com.jininsa.robot.app.Key;
import com.jininsa.pororot.R;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.connector.Scanner;
import com.jininsa.robot.model.robot.Robot;

import butterknife.BindView;

public class ScanMyRobotActivity extends PororotToolbarActivity {
  private static final String SUBSCRIPTION_SCANNER_RESULT = "4Er6SQNWznvdz87o";
  @BindView(R.id.msg1TextView)
  TextView msg1TextView;
  @BindView(R.id.msg2TextView)
  TextView msg2TextView;
  private Scanner scanner;

  @Override
  protected String title() {
    return "내 뽀로롯 찾기";
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.scan_my_robot_activity;
  }

  @Override
  public void doAfterChecking() {
    // !!! 인증된 경우에만 동작한다.
    final String lastUuid = Env.getString(Key.LAST_ROBOT_UUID, Env.EMPTY_STRING);
    final Optional<Robot> lastRobotOpt = Stream.of(this.me.getRobots()).filter(robot -> lastUuid.equalsIgnoreCase(robot.getUuid())).findFirst();
    XLog.d("%s %s", lastUuid, lastRobotOpt);
    // !!! 마지막으로 선택된 로봇이 내 로봇이면
    if (lastRobotOpt.isPresent()) {
      final Robot lastRobot = lastRobotOpt.get();
      XLog.d(lastRobot);
      this.msg1TextView.setText(String.format("%s님", this.me.getNickname()));
      this.msg2TextView.setText(String.format("마지막으로 연결한 %s[%s]을 찾습니다.", lastRobot.getName(), lastRobot.getSeq()));
      final Predicate<BluetoothDevice> filter = device -> lastRobot.getMac().equalsIgnoreCase(device.getAddress());
      this.scanner = new Scanner(this.getApplicationContext(), filter, Scanner.Option.SCAN_FIRST);
      this.addSubscription(SUBSCRIPTION_SCANNER_RESULT, this.scanner.result.subscribe(
        result -> {
          XLog.v(result);
          // !!! 마지막으로 선택된 로봇을 찾았으면
          // !!! 연결로 이동한다.
          if (result.state == Scanner.Result.State.FOUND_NEW_DEVICE) {
            lastRobot.setState(Robot.State.FOUND);
          }

          if (result.state == Scanner.Result.State.FOUND_CONNECTED_DEVICE) {
            if (lastRobot.getState() != Robot.State.READY) {
              lastRobot.setState(Robot.State.LINKED);
            }
          }

          if (result.state == Scanner.Result.State.FINISHED) {
            this.scanner.unregister();
            if (this.scanner.isFound()) {
              Env.setString(Key.SELECTED_ROBOT_UUID, lastRobot.getUuid());
              Robot.setSelectedRobot(lastRobot);
              Intent intent = new Intent(this, LinkRobotActivity.class);
              this.startActivity(intent);
              this.finish();
            }
            else {
              this.msg2TextView.setText(String.format("%s[%s]을 찾지 못했습니다.", lastRobot.getName(), lastRobot.getSeq()));
              Intent intent = new Intent(this, ScanRobotsActivity.class);
              this.startActivity(intent);
              this.finish();
            }
            this.removeSubscription(SUBSCRIPTION_SCANNER_RESULT);
          }
        }));
      this.scanner.register();
      this.scanner.start();
    }
    // !!! 마지막으로 선택된 로봇이 내 로봇이 아니면
    else {
      Intent intent = new Intent(this, ScanRobotsActivity.class);
      this.startActivity(intent);
      this.finish();
    }
  }
}
