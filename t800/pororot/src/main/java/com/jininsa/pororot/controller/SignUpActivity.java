package com.jininsa.pororot.controller;

import android.text.Spanned;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Maps;
import com.jininsa.pororot.R;
import com.jininsa.robot.connector.Authenticator;
import com.jininsa.robot.helper.Strings;

import net.colindodd.toggleimagebutton.ToggleImageButton;

import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class SignUpActivity extends AuthenticateActivity {

  @BindView(R.id.nickNameEditText)
  EditText nickNameEditText;
  @BindView(R.id.agreeWithContractTextView)
  TextView agreeWithContractTextView;
  @BindView(R.id.agreeWithPrivacyTextView)
  TextView agreeWithPrivacyTextView;
  @BindView(R.id.agreeWithContractButton)
  ToggleImageButton agreeWithContractButton;
  @BindView(R.id.agreeWithPrivacyButton)
  ToggleImageButton agreeWithPrivacyButton;

  @OnClick(R.id.agreeWithContractTextView)
  void onClickAgreeWithContractTextView() {
    new MaterialDialog.Builder(this)
      .title("이용약관")
      .customView(R.layout.agree_contract_view, true)
      .positiveText("확인")
      .show();
  }

  @OnClick(R.id.agreeWithPrivacyTextView)
  void onClickAgreeWithPolicyTextView() {
    new MaterialDialog.Builder(this)
      .title("이용약관")
      .customView(R.layout.agree_privacy_view, true)
      .positiveText("확인")
      .show();
  }

  @Override
  public void doAfterChecking() {
    super.doAfterChecking();
    Spanned contract = Strings.fromHtml("뽀로롯 서비스 <font color='#2775ce'>이용약관</font>에 동의합니다.");
    this.agreeWithContractTextView.setText(contract);
    Spanned privacy = Strings.fromHtml("뽀로롯 서비스 <font color='#2775ce'>개인정보 취급방침</font>에 동의합니다.");
    this.agreeWithPrivacyTextView.setText(privacy);
  }

  @Override
  public Authenticator.Action getAction() {
    return Authenticator.Action.SIGN_UP;
  }

  @Override
  public Class getDestination() {
    return ScanRobotsActivity.class;
  }

  @Override
  public boolean isReady() {
    return  this.isNicknameFilled() && this.isAgreed();
  }

  private boolean isNicknameFilled() {
    return !this.nickNameEditText.getText().toString().isEmpty();
  }

  private boolean isAgreed() {
    return  this.agreeWithContractButton.isChecked() && this.agreeWithPrivacyButton.isChecked();
  }

  @Override
  public Map<String, String> getAuthenticationParameters() {
    Map<String, String> params = Maps.newHashMap();
    params.put("nickname", this.nickNameEditText.getText().toString());
    return params;
  }

  @Override
  protected String title() {
    return "사용자 등록";
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.sign_up_activity;
  }

  @Override
  public void onAuthenticationNotReady() {
    if (!this.isNicknameFilled()) {
      new MaterialDialog.Builder(this)
        .cancelable(false)
        .content("별칭을 입력해야 합니다.")
        .positiveText("확인")
        .show();
    }
    else if (!this.isAgreed()) {
      new MaterialDialog.Builder(this)
        .cancelable(false)
        .content("약관과 개인정보 취급방침에 동의해야 합니다.")
        .positiveText("확인")
        .show();
    }
  }
}
