package com.jininsa.pororot.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elvishew.xlog.XLog;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hbb20.CountryCodePicker;
import com.jininsa.robot.helper.Env;
import com.jininsa.pororot.R;
import com.jininsa.robot.connector.Authenticator;
import com.jininsa.robot.connector.SmsReceiver;

import java.util.Locale;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;

public class AuthenticationByPhoneFragment extends AuthenticationFragment {
  static final private String SUBSCRIPTION_SEND_SMS = "1Gbzovd5ensCZyCJ";
  static final private String SUBSCRIPTION_RECEIVE_SMS = "dxRZCC756nTHlIiE";
  static final private Pattern VERIFICATION_CODE_PATTERN = Pattern.compile("\\d{6}");

  @BindView(R.id.phoneNumberEditText)
  EditText phoneNumberEditText;
  @BindView(R.id.requestVerificationNumberButton)
  Button requestVerificationNumberButton;
  @BindView(R.id.verificationCodeEditText)
  EditText verificationCodeEditText;
  @BindView(R.id.countryCodePicker)
  CountryCodePicker ccp;
  private String verificationId = Env.EMPTY_STRING;
  private String myPhoneNumber = Env.EMPTY_STRING;
  private boolean hasPhoneNumner;


  @Override
  protected int getLayoutResId() {
    return R.layout.authentication_by_phone_fragment;
  }

  @Override
  @SuppressLint({"MissingPermission", "HardwareIds"})
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    this.authenticator = Authenticator.create(Authenticator.Provider.PHONE, this);
    // todo i18n
    this.ccp.changeDefaultLanguage(CountryCodePicker.Language.KOREAN);
    Context context = Objects.requireNonNull(this.getContext());
    TelephonyManager mgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    if (mgr != null) {
      try {
        String phoneNumberStr = mgr.getLine1Number();
        this.hasPhoneNumner = !(phoneNumberStr == null || phoneNumberStr.isEmpty());
        if (this.hasPhoneNumner) {
          PhoneNumberUtil util = PhoneNumberUtil.getInstance();
          Phonenumber.PhoneNumber phoneNumber = util.parse(phoneNumberStr, Locale.getDefault().getCountry());
          XLog.d("%s %s", phoneNumberStr, phoneNumber);
          this.ccp.setCountryForPhoneCode(phoneNumber.getCountryCode());
          this.phoneNumberEditText.setText(String.valueOf(phoneNumber.getNationalNumber()));
        }
      }
      catch (Exception e) {
        XLog.e(e);
      }
    }
    return view;
  }

  @Override
  public void onAuthenticationFailed(final Throwable error) {
    this.verificationId = Env.EMPTY_STRING;
    this.verificationCodeEditText.setText(Env.EMPTY_STRING);
    super.onAuthenticationFailed(error);
  }

  @OnClick(R.id.requestVerificationNumberButton)
  void onClickRequestVerificationNumberButton() {
    this.verificationId = Env.EMPTY_STRING;
    final Activity activity = Objects.requireNonNull(this.getActivity());
    String nationalNumber = this.phoneNumberEditText.getText().toString();
    if (nationalNumber.isEmpty()) {
      new MaterialDialog.Builder(activity)
        .cancelable(false)
        .content("전화번호를 입력하세요.")
        .positiveText("확인")
        .onPositive((dialog, which) -> this.phoneNumberEditText.requestFocus())
        .show();
      return;
    }

    String regionalCode = this.ccp.getSelectedCountryCodeWithPlus();
    String countryCode = this.ccp.getDefaultCountryNameCode();

    final String phoneNumberStr = String.format("%s%s", regionalCode, nationalNumber);

    PhoneNumberUtil util = PhoneNumberUtil.getInstance();
    Phonenumber.PhoneNumber phoneNumber;
    try {
      phoneNumber = util.parse(phoneNumberStr, countryCode);
    }
    catch (NumberParseException e) {
      new MaterialDialog.Builder(activity)
        .cancelable(false)
        .content(e.getLocalizedMessage())
        .positiveText("확인")
        .onPositive((dialog, which) -> this.phoneNumberEditText.requestFocus())
        .show();
      return;
    }

    this.myPhoneNumber = String.format("+%s%s", String.valueOf(phoneNumber.getCountryCode()), String.valueOf(phoneNumber.getNationalNumber()));

    if (!util.isValidNumber(phoneNumber)) {
      new MaterialDialog.Builder(activity)
        .cancelable(false)
        .content(String.format("%s는 잘못된 전화번호입니다.", this.myPhoneNumber))
        .positiveText("확인")
        .onPositive((dialog, which) -> this.phoneNumberEditText.requestFocus())
        .show();
      this.myPhoneNumber = Env.EMPTY_STRING;
      return;
    }

    this.loadingProgress.show();
    this.requestVerificationNumberButton.setEnabled(false);
    if (this.hasPhoneNumner) {
      final SmsReceiver smsReceiver = new SmsReceiver(activity);
      smsReceiver.register();
      this.addSubscription(SUBSCRIPTION_RECEIVE_SMS, SmsReceiver.received.subscribe(sms -> {
        String msg = sms.getMessageBody();
        Matcher codeMatcher = VERIFICATION_CODE_PATTERN.matcher(msg);
        if (codeMatcher.find()) {
          String code = codeMatcher.group(0);
          XLog.d("%s %s", msg, code);
          this.enableSendVerificationButton();
          this.verificationCodeEditText.setText(code);
          smsReceiver.unregister();
          this.removeSubscription(SUBSCRIPTION_RECEIVE_SMS);
        }
      }));
    }

    this.addSubscription(SUBSCRIPTION_SEND_SMS, ((Authenticator.Firebase) authenticator).requestVerificationCode(this.myPhoneNumber).subscribe(
      verificationId -> {
        this.loadingProgress.dismiss();
        this.verificationId = verificationId;
        if (!this.hasPhoneNumner) {
          this.enableSendVerificationButton();
        }
        this.removeSubscription(SUBSCRIPTION_SEND_SMS);
      },
      error -> {
        XLog.st(8).e(error);
        this.loadingProgress.dismiss();
        new MaterialDialog.Builder(activity)
          .cancelable(false)
          .content("인증번호를 보내지 못했습니다. 다시 시도해 주세요.")
          .positiveText("확인")
          .show();
        this.enableSendVerificationButton();
        this.removeSubscription(SUBSCRIPTION_SEND_SMS);
      }));
  }

  private void enableSendVerificationButton() {
    this.requestVerificationNumberButton.setEnabled(true);
    this.requestVerificationNumberButton.setText("{t800-verification}  인증번호 재요청");
  }

  @Override
  public void authenticate() {
    if (this.verificationId.isEmpty()) {
      new MaterialDialog.Builder(Objects.requireNonNull(this.getActivity()))
        .cancelable(false)
        .content("전화번호를 먼저 인증해야 합니다.")
        .positiveText("확인")
        .show();
      return;
    }

    String verificationCode = this.verificationCodeEditText.getText().toString();
    if (verificationCode.isEmpty()) {
      new MaterialDialog.Builder(Objects.requireNonNull(this.getActivity()))
        .cancelable(false)
        .content("인증번호를 입력해야 합니다.")
        .positiveText("확인")
        .show();
      return;
    }

    this.params.put("phone", this.myPhoneNumber);
    this.params.put("verificationCode", verificationCode);
    this.params.put("verificationId", this.verificationId);

    this.loadingProgress.show();

    if (this.action == Authenticator.Action.SIGN_IN) {
      this.authenticator.signIn(this.params);
    }
    else if (this.action == Authenticator.Action.SIGN_UP) {
      this.authenticator.signUp(this.params);
    }
  }
}
