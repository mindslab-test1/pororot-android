package com.jininsa.pororot.controller;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.widget.ImageView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.jininsa.robot.app.Key;
import com.jininsa.pororot.R;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.model.robot.Robot;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class LinkRobotActivity extends PororotToolbarActivity {
  static private final String SUBSCRIPTION_ROBOT_STATES = "73WTuDfM8JtbD4VL";
  static private final String SUBSCRIPTION_MOVER = "SiheEbBXTpKX5PDI";

  @BindView(R.id.bluetooth)
  ImageView bluetoothImageView;
  @BindView(R.id.linkEar)
  ImageView linkEarImageView;
  @BindView(R.id.linkMouth)
  ImageView linkMouseImageView;
  @BindView(R.id.linkSerialPort)
  ImageView linkSerialPortImageView;
  private MaterialDialog warnRobotFailedDialog;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.warnRobotFailedDialog = new MaterialDialog.Builder(this)
      .content("뽀로롯에 연결하지 못했습니다")
      .positiveText("다른 뽀로롯을 연결합니다")
      .negativeText("다시 연결합니다")
      .cancelable(false)
      .onPositive(
        (dialog, which) -> {
          Intent intent = new Intent(this, ScanRobotsActivity.class);
          this.startActivity(intent);
          this.finish();
        })
      .onNegative((dialog, which) -> this.doAfterChecking())
      .build();
  }

  @Override
  protected String title() {
    return "뽀로롯 연결";
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.link_robot_activity;
  }

  @Override
  public void doAfterChecking() {
    //    final String selectedUuid = Env.getString(Key.SELECTED_ROBOT_UUID);
//    this.robot = Robot.determineRobotToStart(selectedUuid);
    final Robot selectedRobot = Robot.getSelectedRobot();
    XLog.d("%s", selectedRobot);
    this.addSubscription(SUBSCRIPTION_ROBOT_STATES, selectedRobot.states.observeOn(AndroidSchedulers.mainThread()).subscribe(
      state -> {
        XLog.d("[%s]", state);
        if (state == Robot.State.FAILED) {
          if (!this.isFinishing()) {
            this.warnRobotFailedDialog.show();
          }
        }
        if (state == Robot.State.LINKED) {
          this.bluetoothImageView.setImageResource(R.drawable.linkrobot_bluetooth_pressed);
        }
        else if (state == Robot.State.EAR_READY) {
          this.linkEarImageView.setImageResource(R.drawable.linkrobot_img001_pressed);
        }
        else if (state == Robot.State.MOUTH_READY) {
          this.linkMouseImageView.setImageResource(R.drawable.linkrobot_img002_pressed);
        }
        else if (state == Robot.State.SERIAL_PORT_READY) {
          this.linkSerialPortImageView.setImageResource(R.drawable.linkrobot_img003_pressed);
        }
        else if (state == Robot.State.READY) {
          this.warnRobotFailedDialog.dismiss();
          this.route();
        }
      }));

    selectedRobot.start(this.getApplicationContext());
  }

  @Override
  protected void onDestroy() {
    if (this.warnRobotFailedDialog != null) {
      this.warnRobotFailedDialog.dismiss();
    }
    super.onDestroy();
  }

  @RequiresApi(api = Build.VERSION_CODES.M)
  @Override
  public List<Checker.PermissionCheck.PermissionCheckRequest> getRequiredPermissions() {
    List<Checker.PermissionCheck.PermissionCheckRequest> permissions = Lists.newArrayList();
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.WRITE_EXTERNAL_STORAGE));
    permissions.add(new Checker.PermissionCheck.PermissionCheckRequest(Permission.RECORD_AUDIO));
    return permissions;
  }

  void route() {
    this.robot = Robot.get();
    Env.setString(Key.SELECTED_ROBOT_UUID, Env.DEFAULT_STRING);
    Env.setString(Key.LAST_ROBOT_UUID, this.robot.getUuid());
    this.addSubscription(LinkRobotActivity.SUBSCRIPTION_MOVER, Observable.just(Boolean.TRUE).delay(800, TimeUnit.MILLISECONDS, Schedulers.newThread()).subscribe(
      __ -> {
        this.robot.say("으라차차!");
        Pauser.pauseAndRun(() -> {
          Class next = HomeActivity.class; //Edu AI_원래는 null로 설정

/*
          Class next = null;
          // !!! 사용자 등록하지 않은 상태
          if (this.robot.getLifeCycle() == Robot.LifeCycle.SELLING || this.robot.getLifeCycle() == Robot.LifeCycle.OPEN) {
            next = WelcomeActivity.class;
          }
          // !!! 사용자까지 등록한 상태
          else if (this.robot.getLifeCycle() == Robot.LifeCycle.ADOPTED) {
            next = HomeActivity.class;
          }
*/
          if (next != null) {
            Intent intent = new Intent(this, next);
            this.startActivity(intent);
            this.finish();
          }
        });
        this.removeSubscription(SUBSCRIPTION_MOVER);
      }));
  }
}
