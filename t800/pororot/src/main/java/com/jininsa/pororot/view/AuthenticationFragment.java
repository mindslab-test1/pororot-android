package com.jininsa.pororot.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.common.collect.Maps;
import com.jininsa.robot.app.Key;
import com.jininsa.pororot.R;
import com.jininsa.robot.app.BaseFragment;
import com.jininsa.robot.connector.Authenticator;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

abstract public class AuthenticationFragment extends BaseFragment implements Authenticator.AuthChannel {

  public interface Host {
    void onAuthenticationNotReady();
    void onAuthenticationSucceeded();
    void onAuthenticationFailed(Throwable error);
    boolean isReady();
    Map<String, String> getAuthenticationParameters();
  }

  public static AuthenticationFragment create(Authenticator.Provider provider, Authenticator.Action action) {
    final Bundle args = new Bundle();
    args.putSerializable(Key.AUTH_ACTION, action);
    AuthenticationFragment fragment = null;
    if (provider == Authenticator.Provider.EMAIL) {
      fragment = new AuthenticationByEmailFragment();
      fragment.setArguments(args);
    }
    else if (provider == Authenticator.Provider.PHONE) {
      fragment = new AuthenticationByPhoneFragment();
    }

    if (fragment != null) {
      fragment.setArguments(args);
      return fragment;
    }

    throw new IllegalArgumentException();
  }

  KProgressHUD loadingProgress;
  final Map<String, String> params = Maps.newHashMap();
  Host host;
  Authenticator authenticator;
  Authenticator.Action action;

  @BindView(R.id.authenticateButton)
  Button authenticateButton;

  @Override
  public void onCreate(@Nullable final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    final Bundle args = Objects.requireNonNull(this.getArguments());
    this.action = (Authenticator.Action)args.get(Key.AUTH_ACTION);
  }

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    String authenticationButtonText = String.format("%s  %s", this.action.icon, this.action.desc);
    this.authenticateButton.setText(authenticationButtonText);
    this.loadingProgress = KProgressHUD.create(Objects.requireNonNull(this.getActivity()))
      .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
      .setCancellable(true)
      .setAnimationSpeed(2)
      .setDimAmount(0.5f);
    return view;
  }

  @OnClick(R.id.authenticateButton)
  void onClickAuthenticateButton() {
    if (!this.host.isReady()) {
      this.host.onAuthenticationNotReady();
      return;
    }
    this.params.putAll(this.host.getAuthenticationParameters());
    this.authenticate();
  }

  @Override
  public void onAttach(final Context context) {
    super.onAttach(context);
    this.host = (Host) context;
  }

  @Override
  public void onDetach() {
    this.host = null;
    super.onDetach();
  }

  @Override
  public void onAuthenticated() {
    this.loadingProgress.dismiss();
    this.host.onAuthenticationSucceeded();
  }

  // @DebugLog
  @Override
  public void onAuthenticationFailed(Throwable error) {
    this.loadingProgress.dismiss();
    this.host.onAuthenticationFailed(error);
  }
}
