package com.jininsa.pororot.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.jininsa.pororot.R;
import com.jininsa.pororot.T800;
import com.jininsa.robot.app.Key;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.model.ChatMessage;
import com.jininsa.robot.model.Talker;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class TalkFragment extends HomeFragment {
  public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> fragments = new ArrayList<>();
    private final List<String> fragmentTitles = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
      super(manager);
    }

    @Override
    public Fragment getItem(int position) {
      return fragments.get(position);
    }

    @Override
    public int getCount() {
      return fragments.size();
    }

    public void addFragment(Fragment fragment, String title) {
      fragments.add(fragment);
      fragmentTitles.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return fragmentTitles.get(position);
    }
  }

  static final private String SUBSCRIPTION_CHAT = "";

  @BindView(R.id.macroViewPager)
  ViewPager macroViewPager;

  @BindView(R.id.tabLayout)
  TabLayout tabLayout;

  @BindView(R.id.message_input_view)
  MessageInput messageInputView;

  @BindView(R.id.message_list_view)
  MessagesList messageListView;

  private MessagesListAdapter<ChatMessage> messageListAdapter;
  private boolean isMacroOpen = false;

  @Override
  protected int getLayoutResId() {
    return R.layout.talk_fragment;
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.messageListAdapter = new MessagesListAdapter<>("0", T800.imageLoader);
  }

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    this.messageListView.setAdapter(this.messageListAdapter);
    this.messageInputView.setInputListener(input -> {
      this.robot.delegate(input);
      Bundle bundle = new Bundle();
      bundle.putString("input", input.toString());
      FirebaseAnalytics.getInstance(getContext()).logEvent("AvatarTalk", bundle);
      return true;
    });

    this.messageInputView.setAttachmentsListener(() -> {
      if (this.isMacroOpen == false) {
        macroViewPager.setVisibility(View.VISIBLE);
//        messageInputView.getAttachmentButton().setImageResource(R.drawable.ic_talk_add_closed);
      } else {
        macroViewPager.setVisibility(View.GONE);
//        messageInputView.getAttachmentButton().setImageResource(R.drawable.ic_talk_add);
      }
      isMacroOpen = !isMacroOpen;
    });

    Env.setBoolean(Key.SETTINGS_IS_CHATTING, true);
    this.addSubscription(SUBSCRIPTION_CHAT, robot.chats.observeOn(AndroidSchedulers.mainThread()).subscribe(msg -> {
      Boolean isShowingDailog = Env.getBoolean(Key.SETTINGS_SHOW_TALKS);
      if (isShowingDailog) {
        this.messageListAdapter.addToStart(msg, true);
      }
      else {
        if (msg.getTalker() == Talker.MOM) {
          this.messageListAdapter.addToStart(msg, true);
        }
      }
    }));

    setupViewPager(macroViewPager);

    tabLayout.setupWithViewPager(macroViewPager);
    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        macroViewPager.setCurrentItem(tab.getPosition());
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {

      }

      @Override
      public void onTabReselected(TabLayout.Tab tab) {

      }
    });

    return view;
  }

  @Override
  public void onDestroyView() {
    Env.setBoolean(Key.SETTINGS_IS_CHATTING, false);
    super.onDestroyView();
  }

  public void closeMacro() {
    this.macroViewPager.setVisibility(View.GONE);
//    messageInputView.getAttachmentButton().setImageResource(R.drawable.ic_talk_add);
    this.isMacroOpen = false;
  }

  private void setupViewPager(ViewPager viewPager) {
    TalkFragment.ViewPagerAdapter adapter = new TalkFragment.ViewPagerAdapter(getChildFragmentManager());

    MacroTalkFragment sleepTalkFragment = new MacroTalkFragment();
    sleepTalkFragment.talkList.add("이제 잠 잘 시간이야.");
    sleepTalkFragment.talkList.add("엄마 말 들어야지, 이제 텔레비전은 그만 보고 자러가자.");
    sleepTalkFragment.talkList.add("잘 자. 좋은 꿈 꿔.");
    sleepTalkFragment.talkList.add("우리 꿈에서 만나자. 사랑해.");
    sleepTalkFragment.talkList.add("어서 자고 내일 일어나서 놀자.");
    sleepTalkFragment.talkList.add("나는 너무 졸려. 빨리 자고 싶다.");
    sleepTalkFragment.talkList.add("이제 졸리지 않아? 나는 졸린데.");
    adapter.addFragment(sleepTalkFragment, "잠자기");

    MacroTalkFragment mealTalkFragment = new MacroTalkFragment();
    mealTalkFragment.talkList.add("이제 밥 먹을 시간이야.");
    mealTalkFragment.talkList.add("밥을 먹을 때는 나처럼 가만히 앉아있어야 해.");
    mealTalkFragment.talkList.add("오른쪽 왼쪽 꼭꼭 씹어서 먹어야 해.");
    mealTalkFragment.talkList.add("나는 야채 잘 먹는 친구가 좋더라.");
    mealTalkFragment.talkList.add("야채를 꼭꼭 씹어 먹으면 키가 무럭무럭 자라나.");
    mealTalkFragment.talkList.add("남기지 않고 먹었네! 역시 내 친구야.");
    mealTalkFragment.talkList.add("내가 먹은 그릇을 정리해보자!");
    adapter.addFragment(mealTalkFragment, "식사하기");

    MacroTalkFragment brushingTeethTalkFragment = new MacroTalkFragment();
    brushingTeethTalkFragment.talkList.add("자기 전에는 언제나 이를 닦아야 한다구.");
    brushingTeethTalkFragment.talkList.add("밥을 먹었으니 이제 이를 닦아볼까!");
    brushingTeethTalkFragment.talkList.add("밖에 나갔다 왔으니 우리 이를 닦아보자!");
    brushingTeethTalkFragment.talkList.add("이를 닦지 않으면 세균맨이 괴롭힐거야.");
    brushingTeethTalkFragment.talkList.add("치카치카하고 오면 놀아주지.");
    brushingTeethTalkFragment.talkList.add("엄마가 말하기 전에 이 닦고 칭찬받자!");
    brushingTeethTalkFragment.talkList.add("치카치카를 잘해서 너무 좋아.");
    adapter.addFragment(brushingTeethTalkFragment, "양치하기");

    MacroTalkFragment washTalkFragment = new MacroTalkFragment();
    washTalkFragment.talkList.add("나는 깨끗한 손으로 만져 주는 게 좋아.");
    washTalkFragment.talkList.add("밖에서 재밌게 놀고 왔구나! 어서 씻고오자!");
    washTalkFragment.talkList.add("깨끗하게 씻고 오면 재밌는 얘기 들려줄게.");
    washTalkFragment.talkList.add("나는 같이 목욕할 수 없어.");
    washTalkFragment.talkList.add("나는 목욕을 좋아하는 친구가 좋아.");
    washTalkFragment.talkList.add("쓱싹쓱싹 혼자서도 머리를 감아보자.");
    washTalkFragment.talkList.add("밖에서 돌아왔으면 손발부터 씻어야해.");
    adapter.addFragment(washTalkFragment, "목욕하기");

    MacroTalkFragment daycareCenterTalkFragment = new MacroTalkFragment();
    daycareCenterTalkFragment.talkList.add("어린이집 갈 시간이야. 이제 일어나자.");
    daycareCenterTalkFragment.talkList.add("어린이집 잘 다녀오면 놀아줄게.");
    daycareCenterTalkFragment.talkList.add("오늘도 선생님 말 잘 듣고 올거지?");
    daycareCenterTalkFragment.talkList.add("점심시간에 밥먹을 때는 꼭꼭 씹어먹자.");
    daycareCenterTalkFragment.talkList.add("오늘은 어린이집에서 무슨 재밌는 일이 생길까?");
    daycareCenterTalkFragment.talkList.add("내일 준비물은 다 챙겼어?");
    daycareCenterTalkFragment.talkList.add("어린이집 가기 싫다고 울면 나도 속상해.");
    adapter.addFragment(daycareCenterTalkFragment, "어린이집가기");

    viewPager.setAdapter(adapter);
  }
}
