package com.jininsa.pororot.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jininsa.pororot.R;
import java.util.ArrayList;
import java.util.List;

public class ContentsFragment extends HomeFragment {
  ViewPager contentViewPager;
  TabLayout tabLayout;

  @Override
  protected int getLayoutResId() {
        return R.layout.contents_fragment;
    }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = super.onCreateView(inflater, container, savedInstanceState);
    tabLayout = (TabLayout) v.findViewById(R.id.contentTabs);

    contentViewPager = (ViewPager) v.findViewById(R.id.contentViewPager);
    setupViewPager(contentViewPager);

    tabLayout.setupWithViewPager(contentViewPager);
    tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
      @Override
      public void onTabSelected(TabLayout.Tab tab) {
        contentViewPager.setCurrentItem(tab.getPosition());
      }

      @Override
      public void onTabUnselected(TabLayout.Tab tab) {}

      @Override
      public void onTabReselected(TabLayout.Tab tab) {}
    });

    return v;
  }

  private void setupViewPager(ViewPager viewPager) {
    ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
    adapter.addFragment(new SongContentFragment(), "노래");
    adapter.addFragment(new BookContentFragment(), "동화책");
    adapter.addFragment(new VideoContentFragment(), "동영상");
    viewPager.setAdapter(adapter);
  }

  public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

    @Override
    public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

    @Override
    public int getCount() {
            return mFragmentList.size();
        }

    public void addFragment(Fragment fragment, String title) {
      mFragmentList.add(fragment);
      mFragmentTitleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
  }
}