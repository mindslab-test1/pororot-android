package com.jininsa.pororot.view;

import android.app.Activity;
import android.bluetooth.BluetoothHeadset;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.elvishew.xlog.XLog;
import com.jininsa.robot.app.Key;
import com.jininsa.pororot.R;
import com.jininsa.pororot.controller.ScanRobotsActivity;
import com.jininsa.pororot.controller.StartActivity;
import com.jininsa.robot.connector.Adapter;
import com.jininsa.robot.connector.Authenticator;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Koreans;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.schedulers.Schedulers;

public class SettingFragment extends HomeFragment {
  static final private String SUBSCRIPTION_ROBOT_TERMINATED = "6nqHxsuitffkDFOe";
  static final private String SUBSCRIPTION_CHANGE_NAMES = "u5CYQCPwCXajNAR9";

  /*Edu AI
  @BindView(R.id.familyNameEditText)
  EditText familyNameEditText;
  @BindView(R.id.yneTextView)
  TextView yneTextView;
  @BindView(R.id.robotNameEditText)
  EditText robotNameEditText;
  @BindView(R.id.signOutLabel)
  TextView signOutLabel;
  */
  @BindView(R.id.changeNamesButton)
  Button changeNamesButton;
  @BindView(R.id.uuidTextView)
  TextView uuidTextView;
  @BindView(R.id.pitchTextView)
  TextView pitchTextView;
  @BindView(R.id.pitchSeekBar)
  SeekBar pitchSeekBar;
  @BindView(R.id.showTalksSwitch)
  Switch showTalksSwitch;
  @BindView(R.id.beQuietSwitch)
  Switch beQuietSwitch;
  @BindView(R.id.beQuietInChatSwitch)
  Switch beQuietInChatSwitch;
  @BindView(R.id.stopByShakingSwitch)
  Switch stopByShakingSwitch;

  private KProgressHUD loadingProgress;

  @OnClick(R.id.changeNamesButton)
  void onClickChangeNamesButton() {
    //String familyName = this.familyNameEditText.getText().toString();
    //String robotName = this.robotNameEditText.getText().toString();

    /*Edu AI
    if (familyName.isEmpty() || robotName.isEmpty()) {
      new MaterialDialog.Builder(Objects.requireNonNull(this.getActivity()))
        .content("가족 이름과 로봇 이름을 모두 입력하세요.")
        .positiveText("확인")
        .show();
    }

    else {
      this.loadingProgress.show();
      this.addSubscription(SUBSCRIPTION_CHANGE_NAMES, this.robot.changeNames(this.me.getAuthToken().token, familyName, robotName).subscribe(
        robotFromServer -> {
          this.robot.refreshWith(robotFromServer);
          this.loadingProgress.dismiss();
          this.removeSubscription(SUBSCRIPTION_CHANGE_NAMES);
        },
        error -> {
          new MaterialDialog.Builder(Objects.requireNonNull(this.getActivity()))
            .content("이름을 변경하지 못했습니다. 고객센터에 문의해 주세요.")
            .positiveText("확인")
            .show();
          this.loadingProgress.dismiss();
          this.removeSubscription(SUBSCRIPTION_CHANGE_NAMES);
        }));
    }*/
  }

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    View v = super.onCreateView(inflater, container, savedInstanceState);
    this.loadingProgress = KProgressHUD.create(Objects.requireNonNull(this.getActivity()))
      .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
      .setCancellable(true)
      .setAnimationSpeed(2)
      .setDimAmount(0.5f);

    /*Edu AI
    this.familyNameEditText.setText(this.robot.getFamilyName());
    this.yneTextView.setText(Koreans.yneOrNe(this.robot.getFamilyName()));
    this.robotNameEditText.setText(this.robot.getName());
    this.signOutLabel.setText(String.format("%s님으로 로그인", this.me.getNickname()));
    */
    this.uuidTextView.setText(this.robot.getUuid());



    boolean beQuiet = Env.getBoolean(Key.SETTINGS_BE_QUIET, false);
    boolean beQuietInChat = Env.getBoolean(Key.SETTINGS_BE_QUIET_IN_CHAT, false);
    boolean stopByShaking = Env.getBoolean(Key.SETTINGS_STOP_BY_SHAKING, false);
    boolean showTalks = Env.getBoolean(Key.SETTINGS_SHOW_TALKS, false);

    this.beQuietInChatSwitch.setChecked(beQuietInChat);
    this.beQuietSwitch.setChecked(beQuiet);
    this.stopByShakingSwitch.setChecked(stopByShaking);
    this.showTalksSwitch.setChecked(showTalks);

    int pitch = Env.getInt(Key.SETTINGS_PITCH, 0);
    this.pitchSeekBar.setMax(100);
    this.pitchSeekBar.setProgress(pitch);
    this.pitchTextView.setText(String.valueOf(pitch));
    this.pitchSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Env.setInt(Key.SETTINGS_PITCH, progress);
        SettingFragment.this.pitchTextView.setText(String.valueOf(progress));
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {}

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {}
    });
    return v;
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.setting_fragment;
  }

  /* Edu AI
  @OnClick(R.id.disconnectButton)
  void onClickDisconnect() {
    final Activity activity = Objects.requireNonNull(this.getActivity());
    new MaterialDialog.Builder(activity)
      .content("현재 뽀로롯과 연결을 끊습니다.")
      .positiveText("네")
      .onPositive((dialog, which) -> {
        Env.setString(Key.LAST_ROBOT_UUID, Env.EMPTY_STRING);
        this.robot.terminate();
        this.addSubscription(SUBSCRIPTION_ROBOT_TERMINATED, Adapter.get().event.observeOn(Schedulers.newThread()).subscribe(event -> {
          String action = event.action;
          if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
            int p = event.intent.getIntExtra(BluetoothHeadset.EXTRA_PREVIOUS_STATE, Env.DEFAULT_INT);
            int n = event.intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, Env.DEFAULT_INT);
            XLog.d("[%s] %d-->%d", action, p, n);
            if (n == BluetoothHeadset.STATE_DISCONNECTED) {
              if (event.device != null && this.robot.getMac().equals(event.device.getAddress())) {
                final Intent intent = new Intent(activity, ScanRobotsActivity.class);
                ActivityCompat.startActivity(activity, intent, null);
                this.getActivity().finish();
              }
              this.removeSubscription(SUBSCRIPTION_ROBOT_TERMINATED);
            }
          }
        }));
      })
      .negativeText("아니오")
      .show();
  }

  @OnClick(R.id.signOutButton)
  void onClickSignOut() {
    final Activity activity = Objects.requireNonNull(this.getActivity());
    new MaterialDialog.Builder(activity)
      .content("로그아웃 합니다.")
      .positiveText("네")
      .onPositive(
        (dialog, which) -> {
          this.loadingProgress.show();
          Authenticator.signOut().subscribe(nothing -> {
              Env.setString(Key.LAST_ROBOT_UUID, Env.EMPTY_STRING);
              this.loadingProgress.dismiss();
              this.robot.terminate();
              this.me.setAuthToken(null);
              Intent intent = new Intent(activity, StartActivity.class);
              ActivityCompat.startActivity(activity, intent, null);
              getActivity().finish();
            },
            error -> {
              XLog.e(error);
              this.loadingProgress.dismiss();
            });
        })
    .negativeText("아니오")
    .show();
  }
  */

  @OnClick(R.id.aboutButton)
  void onClickAbout() {
    new LibsBuilder()
      //provide a style (optional) (LIGHT, DARK, LIGHT_DARK_TOOLBAR)
      .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
      //start the activity
      .start(Objects.requireNonNull(this.getActivity()));
  }

  @OnClick(R.id.showTalksSwitch)
  void onClickShowDialogCheckBox() {
    Env.setBoolean(Key.SETTINGS_SHOW_TALKS, this.showTalksSwitch.isChecked());
  }

  @OnClick(R.id.beQuietInChatSwitch)
  void onClickBeQuietDuringChatCheckBox() {
    Env.setBoolean(Key.SETTINGS_BE_QUIET_IN_CHAT, this.beQuietInChatSwitch.isChecked());
  }

  @OnClick(R.id.beQuietSwitch)
  void onClickBeQuietCheckBox() {
    Env.setBoolean(Key.SETTINGS_BE_QUIET, this.beQuietSwitch.isChecked());
  }

  @OnClick(R.id.stopByShakingSwitch)
  void onShutUpWithShakingCheckBox() {
    Env.setBoolean(Key.SETTINGS_STOP_BY_SHAKING, this.stopByShakingSwitch.isChecked());
  }
}
