package com.jininsa.pororotlite.controller;

import android.content.Intent;
import android.support.v4.app.ActivityCompat;

import com.google.common.collect.Sets;
import com.jininsa.robot.app.App;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.helper.Checker;
import com.jininsa.robot.helper.Env;

import java.util.Set;

public class StartActivity extends RobotActivity {
  static final private String SUBSCRIPTION_BRAIN_ACTIVATOR = "O9xxT36MFnH2W8K8";
  static final private String SUBSCRIPTION_RETRIEVE_PERSON = "pr8ahF55oRYKfZIg";

//  @Override
//  protected void onCreate(@Nullable final Bundle savedInstanceState) {
//    super.onCreate(savedInstanceState);
//    FirebaseApp.initializeApp(this.getApplicationContext());
//  }

  @Override
  protected int getLayoutResId() {
    return Env.DEFAULT_INT;
  }

  @Override
  public Set<Checker.Subject> getCheckSubjects() {
    return Sets.newHashSet(Checker.Subject.NETWORK);
  }

  @Override
  public void doAfterChecking() {
    if (App.isBrainActivated()) {
      this.route();
    }
    else {
      this.addSubscription(SUBSCRIPTION_BRAIN_ACTIVATOR, App.brainActivator.filter(on -> on).subscribe(__ -> this.route()));
    }
  }

  private void route() {
//    if (this.robot == null) {
      Intent intent = new Intent(this, ScanRobotsActivity.class);
      ActivityCompat.startActivity(this, intent, null);
      this.finish();
//    } else {
//      XLog.d(this.me);
//      List<Robot> myRobots = this.me.getRobots();
//      Stream.of(myRobots).filter(robot -> robot.getUuid().equalsIgnoreCase(this.robot.getUuid())).findSingle().ifPresent(
//          robot -> {
//            this.robot.refreshWith(robot);
//            this.me.removeRobot(robot);
//            this.me.addRobot(this.robot);
//          });
//      Class next;
//      next = (this.robot.getState() == Robot.State.READY && this.robot.getLifeCycle() == Robot.LifeCycle.ADOPTED) ? HomeActivity.class : ScanRobotsActivity.class;
//      Intent intent = new Intent(this, next);
//      ActivityCompat.startActivity(this, intent, null);
//      this.finish();
//    }
  }
}
