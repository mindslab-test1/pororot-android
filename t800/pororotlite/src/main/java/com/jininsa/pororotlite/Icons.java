package com.jininsa.pororotlite;

import com.pisome.iconify.Icon;
import com.pisome.iconify.IconFontDescriptor;

public class Icons implements IconFontDescriptor
{
  @Override
  public String ttfFileName()
  {
    return "fonts/t800.ttf";
  }

  @Override
  public Icon[] characters()
  {
    return Type.values();
  }

  @Override
  public String prefix()
  {
    return "t800";
  }

  public enum Type implements Icon
  {
    t800_reload('\ue900'),
    t800_logout('\ue901'),
    t800_network('\ue902'),
    t800_network_solid('\ue903'),
    t800_off('\ue904'),
    t800_kakaotalk('\ue905'),
    t800_phone('\ue906'),
    t800_facebook('\ue907'),
    t800_email('\ue908'),
    t800_key('\ue909'),
    t800_prize('\ue910'),
    t800_next('\ue911'),
    t800_warning('\ue912'),
    t800_unlink('\ue913'),
    t800_lock('\ue90a'),
    t800_settings('\ue90b'),
    t800_coin_dollar('\ue90c'),
    t800_check('\ue90d'),
    t800_chat('\ue90e'),
    t800_home('\ue90f'),

    t800_naver('\ue917'),
    t800_cancel('\ue919'),
    t800_send('\ue91a'),
    t800_link('\ue91b'),
    t800_profile('\ue923'),

    t800_user('\ue971'),
    t800_users('\ue972'),
    t800_user_plus('\ue973'),
    t800_user_minus('\ue974'),
    t800_user_check('\ue975'),
    t800_verification('\ue8e8'),
    t800_pause('\uea16'),
    t800_stop('\uea17');

    private char character;

    Type(char c)
    {
      this.character = c;
    }

    @Override
    public String key()
    {
      return name().replace('_', '-');
    }

    @Override
    public char character()
    {
      return this.character;
    }
  }
}