package com.jininsa.pororotlite.controller;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jininsa.pororotlite.R;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.model.contents.Book;
import com.jininsa.robot.model.robot.Robot;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

public class BookPlayerActivity extends RobotActivity {
  @BindView(R.id.contentsTextView)
  TextView contentsTextView;

  @BindView(R.id.titleTextView)
  TextView titleTextView;

  @BindView(R.id.authorTextView)
  TextView authorTextView;

  @BindView(R.id.stopButton)
  ImageView stopButton;

  Timer timer = null;

  @Override
  protected @LayoutRes int getLayoutResId() {
    return R.layout.book_player_activity;
  }

  @Override
  public void doAfterChecking() {
    Intent intent = getIntent();

    String title = intent.getExtras().getString("title");
    String author = intent.getExtras().getString("author");
    String contents = intent.getExtras().getString("contents");
    String code = intent.getExtras().getString("code");

    titleTextView.setText(title);
    authorTextView.setText(author);
    contentsTextView.setText(contents);

    if (Robot.get().isSinging()) {
      Robot.get().pauseSinging();
      Pauser.pauseAndRun(() -> {
        Robot.get().startReading(code, title);
      }, 1000);
    } else {
      Robot.get().startReading(code, title);
    }

    timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        BookPlayerActivity.this.runOnUiThread(() -> {
          if (ContentManager.get().getNowReadBook() == null) {
            return;
          }
          String contents = ContentManager.get().getNowReadBook().getContents();
          if (ContentManager.get().getNowReadingLine() == null) {
            contentsTextView.setText(contents);
            return;
          }

          contentsTextView.setText("");
          String line = ContentManager.get().getNowReadingLine();
          int start = contents.indexOf(line);
          int end = start + ContentManager.get().getNowReadingLine().length();

          if (start == -1) {
            contentsTextView.setText(contents);
            return;
          }

          SpannableStringBuilder builder = new SpannableStringBuilder(contents);
          builder.setSpan(new ForegroundColorSpan(Color.parseColor("#90c6ff")), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
          contentsTextView.append(builder);
        });
      }
    }, 0, 100);
  }

  @Override
  protected void onDestroy() {
    if (Robot.get().isReading()) {
      Robot.get().stopReading();
    }
    if (timer != null) {
      timer.cancel();
    }
    super.onDestroy();
  }

  @OnClick(R.id.stopButton)
  protected void stopButtonClick() {
    if (Robot.get().isReading()) {
      Robot.get().stopReading();
      stopButton.setImageResource(R.drawable.ic_player_play);
      Robot.get().say("그만 읽을게");
    } else {
      Intent intent = getIntent();
      String title = intent.getExtras().getString("title");
      String code = intent.getExtras().getString("code");
      Robot.get().startReading(code, title);
      stopButton.setImageResource(R.drawable.ic_player_stop);
      ContentManager.get().setNowReadingLine("");
      Robot.get().say("처음부터 다시 읽을게");
    }
  }

  @OnClick(R.id.nextButton)
  protected void nextButtonClick() {
    Book nextBook = ContentManager.get().getNextBookFromNow();
    if (nextBook == null) {
      Toast.makeText(this, "다음 책이 없습니다.", Toast.LENGTH_SHORT).show();
      return;
    }

    Robot.get().stopReading();
    Robot.get().say("다음 책 읽을게");
    Robot.get().startReading(nextBook.getCode(), nextBook.getTitle());

    titleTextView.setText(nextBook.getTitle());
    authorTextView.setText(nextBook.getAuthor());
    contentsTextView.setText(nextBook.getContents());
  }

  @OnClick(R.id.prevButton)
  protected void prevButtonClick() {
    Book prevBook = ContentManager.get().getPrevBookFromNow();
    if (prevBook == null) {
      Toast.makeText(this, "이전 책이 없습니다.", Toast.LENGTH_SHORT).show();
      return;
    }

    Robot.get().stopReading();
    Robot.get().say("이전 책 읽을게");
    Robot.get().startReading(prevBook.getCode(), prevBook.getTitle());

    titleTextView.setText(prevBook.getTitle());
    authorTextView.setText(prevBook.getAuthor());
    contentsTextView.setText(prevBook.getContents());
  }
}
