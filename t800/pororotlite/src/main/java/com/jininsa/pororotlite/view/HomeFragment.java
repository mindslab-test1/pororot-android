package com.jininsa.pororotlite.view;

import com.jininsa.robot.app.BaseFragment;

abstract public class HomeFragment extends BaseFragment {
  public static HomeFragment newInstance(int index) {
    HomeFragment fragment = null;

    switch (index) {
      case 0:
        fragment = new MyHomeFragment();
        break;
      case 1:
        fragment = new ContentsFragment();
        break;
      case 2:
        fragment = new AlarmFragment();
        break;
      case 3:
        fragment = new SettingFragment();
        break;
      case 4:
        fragment = new TalkFragment();
        break;
    }
    return fragment;
  }

  public HomeFragment() {}
}
