package com.jininsa.pororotlite.view;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.annimon.stream.Stream;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jininsa.pororotlite.R;
import com.jininsa.pororotlite.T800L;
import com.jininsa.robot.app.BaseFragment;
import com.jininsa.robot.model.contents.Video;
import com.jininsa.robot.model.contents.YouTube;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import io.reactivex.functions.Consumer;

public class VideoContentFragment extends BaseFragment {
  static class VideoSearchListAdapter extends RecyclerView.Adapter<VideoSearchListAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
      final View containerView;
      @BindView(R.id.titleTextView)
      TextView titleTextView;

      @BindView(R.id.authorTextView)
      TextView authorTextView;

      @BindView(R.id.thumbnailImageView)
      ImageView thumbnailImageView;

      ViewHolder(View view) {
        super(view);
        this.containerView = view;
        ButterKnife.bind(this, view);
      }
    }

    private final List<YouTube> items;
    private final Consumer<YouTube> action;
    private final Context context;

    VideoSearchListAdapter(Consumer<YouTube> action, Context context) {
      this.items = Lists.newArrayList();
      this.action = action;
      this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_search_list_item, parent, false);
      return new ViewHolder(view);
    }

    void reset() {
      int size = this.getItemCount();
      this.items.clear();
      this.notifyItemRangeRemoved(0, size);
    }

    void addItem(final YouTube item) {
      this.items.add(item);
      this.notifyItemInserted(this.getItemCount());
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      holder.titleTextView.setText(items.get(position).getSnippet().getTitle());
      holder.authorTextView.setText(items.get(position).getSnippet().getChannelTitle());
      T800L.imageLoader.loadImage(holder.thumbnailImageView, "https://img.youtube.com/vi/" + items.get(position).getId().getVideoId() + "/sddefault.jpg");
      holder.containerView.setOnClickListener(__ -> {
        try {
          this.action.accept(this.items.get(position));
          Bundle bundle = new Bundle();
          bundle.putString("title", this.items.get(position).getSnippet().getTitle());
          FirebaseAnalytics.getInstance(context).logEvent("Video", bundle);
        }
        catch (Exception e) {
          XLog.e(e.getMessage(), e);
        }
      });
    }

    @Override
    public int getItemCount() {
      return items.size();
    }
  }

  static class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder> {
    static class ViewHolder extends RecyclerView.ViewHolder {
      final View containerView;
      @BindView(R.id.videoTitleTextView)
      TextView videoTitleTextView;

      @BindView(R.id.videoAuthorTextView)
      TextView videoAuthorTextView;

      @BindView(R.id.videoPlayTimeTextView)
      TextView videoPlayTimeTextView;

      @BindView(R.id.videoThumbnailImageView)
      ImageView videoThumbnailImageView;

      ViewHolder(View view) {
        super(view);
        this.containerView = view;
        ButterKnife.bind(this, view);
      }
    }

    private final List<Video> items;
    private final Consumer<Video> action;
    private final Context context;

    VideoListAdapter(Consumer<Video> action, Context context) {
      this.items = Lists.newArrayList();
      this.action = action;
      this.context = context;
    }

    void reset() {
      int size = this.getItemCount();
      this.items.clear();
      this.notifyItemRangeRemoved(0, size);
    }

    void addItem(final Video item) {
      this.items.add(item);
      this.notifyItemInserted(this.getItemCount());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_content_list_item, parent, false);
      return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      holder.videoTitleTextView.setText(items.get(position).getTitle());
      holder.videoPlayTimeTextView.setText(items.get(position).getPlayTime());
      holder.videoAuthorTextView.setText(items.get(position).getAuthor());
      if (!items.get(position).getImageUrl().isEmpty()) {
        T800L.imageLoader.loadImage(holder.videoThumbnailImageView, items.get(position).getImageUrl());
      }
      holder.containerView.setOnClickListener(__ -> {
        try {
          this.action.accept(this.items.get(position));
          Bundle bundle = new Bundle();
          bundle.putString("title", this.items.get(position).getTitle());
          FirebaseAnalytics.getInstance(context).logEvent("Song", bundle);
        }
        catch (Exception e) {
          XLog.e(e.getMessage(), e);
        }
      });
    }

    @Override
    public int getItemCount() {
      return items.size();
    }
  }

  static final private String SUBSCRIPTION_YOUTUBE_SEARCH = "hGw6YdgxCdYSoAEQ";
  static final private String SUBSCRIPTION_RETRIEVE_VIDEOS = "EccD6lCK2SYyPEWi";

  @BindView(R.id.scrollView)
  NestedScrollView scrollView;

  @BindView(R.id.searchBar)
  EditText searchBar;

  @BindView(R.id.videoSearchListRecyclerView)
  RecyclerView videoSearchListRecyclerView;

  @BindView(R.id.videoListRecyclerView)
  RecyclerView videoListRecyclerView;

  private VideoListAdapter videoListAdapter;
  private VideoSearchListAdapter videoSearchListAdapter;

  @Override
  protected int getLayoutResId() {
    return R.layout.video_fragment;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    this.videoSearchListAdapter = new VideoSearchListAdapter(youTube -> {
//      Intent intent = new Intent(this.getActivity(), VideoPlayerActivity.class);
//      intent.putExtra("title", youTube.getSnippet().getTitle());
//      intent.putExtra("url", youTube.getId().getVideoId());
//      this.startActivity(intent);
      this.robot.videoPlayById(youTube.getId().getVideoId());
    }, getContext());
    this.videoSearchListRecyclerView.setAdapter(videoSearchListAdapter);

    this.videoListAdapter = new VideoListAdapter(video -> {
//      Intent intent = new Intent(this.getActivity(), VideoPlayerActivity.class);
//      intent.putExtra("title", video.getTitle());
//      intent.putExtra("url", video.getUrl());
//      this.startActivity(intent);
      this.robot.videoPlayById(video.getUrl());
    }, getContext());

    this.videoListRecyclerView.setAdapter(videoListAdapter);
    this.addSubscription(SUBSCRIPTION_RETRIEVE_VIDEOS, Video.getVideoList(0, 100).subscribe(
      videos -> {
        XLog.d(videos);
        Stream.of(videos).forEach(video -> videoListAdapter.addItem(video));
        this.removeSubscription(SUBSCRIPTION_RETRIEVE_VIDEOS);
      }));
  }

  @OnTextChanged(R.id.searchBar)
  protected void onTextChangedSearchBar() {
    String query = this.searchBar.getText().toString();

    if (query.length() < 2) {
      this.videoSearchListRecyclerView.setVisibility(View.GONE);
      videoSearchListAdapter.reset();
      return;
    }

    this.videoSearchListRecyclerView.setVisibility(View.VISIBLE);

    this.videoSearchListAdapter.reset();

    this.addSubscription(SUBSCRIPTION_YOUTUBE_SEARCH, YouTube.search(query).distinct().throttleFirst(500, TimeUnit.MILLISECONDS).subscribe(
      videos -> {
        Stream.of(videos).forEach(video -> videoSearchListAdapter.addItem(video));
        this.removeSubscription(SUBSCRIPTION_YOUTUBE_SEARCH);
      }));
  }
}
