package com.jininsa.pororotlite.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.jininsa.pororotlite.R;

/**
 * Created by architect on 2017. 5. 7..
 */

public class AlarmFragment extends HomeFragment {
//  @BindView(R.id.titleTextView)
//  TextView titleTextView;

  @Override
  protected int getLayoutResId() {
    return R.layout.alarm_fragment;
  }

  @Override
  public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
  }
}
