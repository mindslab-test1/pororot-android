package com.jininsa.pororotlite.controller;

import android.content.Intent;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.jininsa.pororotlite.R;
import com.jininsa.pororotlite.T800L;
import com.jininsa.robot.app.Key;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.helper.Env;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.model.contents.Song;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

public class SongPlayerActivity extends RobotActivity {
  @BindView(R.id.albumImage)
  ImageView albumImage;

  @BindView(R.id.titleTextView)
  TextView titleTextView;
  @BindView(R.id.artistTextView)
  TextView artistTextView;
  @BindView(R.id.playSeekBar)
  SeekBar playSeekBar;
  @BindView(R.id.volumeSeekBar)
  SeekBar volumeSeekBar;

  @BindView(R.id.pauseButton)
  ImageView pauseButton;

  private Player.EventListener eventListener;
  private Timer timer = null;
  SimpleExoPlayer player;

  @Override
  protected int getLayoutResId() {
    return R.layout.song_player_activity;
  }

  @Override
  public void doAfterChecking() {
    Intent intent = getIntent();
    String imageUrl = intent.getExtras().getString("imageUrl");
    String title = intent.getExtras().getString("title");
    String artist = intent.getExtras().getString("artist");

    if (!imageUrl.isEmpty()) {
      T800L.imageLoader.loadImage(albumImage, imageUrl);
    }

    this.player = this.robot == null ? null : this.robot.getMusicPlayer();

    titleTextView.setText(title);
    artistTextView.setText(artist);

    playSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
        SongPlayerActivity.this.player.seekTo(seekBar.getProgress());

      }
    });

    volumeSeekBar.setMax(100); // 저장 필요
    volumeSeekBar.setProgress(Env.getInt(Key.LAST_MUSIC_VOLUME, 50));
    volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        Env.setInt(Key.LAST_MUSIC_VOLUME, i);
        SongPlayerActivity.this.player.setVolume(i / 100.f);
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {

      }
    });

    boolean isList = intent.getExtras().getBoolean("isList");

    if (!isList) {
      String code = intent.getExtras().getString("code");
//      String title = intent.getExtras().getString("title");

      if (this.robot.isSinging()) {
        this.robot.pauseSinging();
      }
      Pauser.pauseAndRun(() -> this.robot.startSinging(code, title), 1000);
    }
    else {
      playSeekBar.setMax((int)this.robot.getMusicPlayer().getDuration());
    }

    timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        playSeekBar.setMax((int)SongPlayerActivity.this.player.getDuration());
        playSeekBar.setProgress((int)SongPlayerActivity.this.player.getCurrentPosition());
      }
    }, 0, 100);

    this.eventListener = new Player.EventListener() {
//      @Override
//      public void onTimelineChanged(Timeline timeline, Object manifest) {}

      @Override
      public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

      }

      @Override
      public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {}

      @Override
      public void onLoadingChanged(boolean isLoading) {}

      @Override
      public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == Player.STATE_READY) {
          playSeekBar.setMax((int)SongPlayerActivity.this.player.getDuration());
          int volume = Env.getInt(Key.LAST_MUSIC_VOLUME, 50);
          SongPlayerActivity.this.player.setVolume(volume / 100.f);
        }

        if (playWhenReady && playbackState == Player.STATE_ENDED) {
          if (ContentManager.get().getNextSongFromNow() == null) {
            SongPlayerActivity.this.robot.stopSinging();
          } else {
            nextButtonClick();
          }
        }
      }

      @Override
      public void onRepeatModeChanged(int repeatMode) {}

      @Override
      public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {}

      @Override
      public void onPlayerError(ExoPlaybackException error) {}

      @Override
      public void onPositionDiscontinuity(int reason) {}

      @Override
      public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {}

      @Override
      public void onSeekProcessed() {}
    };

    this.player.addListener(this.eventListener);
  }

  @Override
  protected void onDestroy() {
    timer.cancel();
    if (this.player != null) {
      this.player.removeListener(this.eventListener);
    }
    super.onDestroy();
  }

  @OnClick(R.id.pauseButton)
  protected void pauseButtonClick() {
    if (this.player.getPlayWhenReady()) {
      this.robot.pauseSinging();
      pauseButton.setImageResource(R.drawable.ic_player_play);
    } else {
      this.robot.resumeSinging();
      pauseButton.setImageResource(R.drawable.ic_player_pause);
    }
  }

  @OnClick(R.id.nextButton)
  protected void nextButtonClick() {
    Song nextSong = ContentManager.get().getNextSongFromNow();

    if (nextSong == null) {
      Toast.makeText(this, "다음 곡이 없습니다.", Toast.LENGTH_SHORT).show();
      return;
    }

    if (this.robot.isSinging()) {
      this.robot.pauseSinging();
    }

    Pauser.pauseAndRun(() -> this.robot.startSinging(nextSong.getCode(), nextSong.getTitle()), 800);

    if (!nextSong.getImageUrl().isEmpty()) {
      T800L.imageLoader.loadImage(albumImage, nextSong.getImageUrl());
    }
    titleTextView.setText(nextSong.getTitle());
    artistTextView.setText(nextSong.getArtist());
  }

  @OnClick(R.id.prevButton)
  protected void prevButtonClick() {
    Song prevSong = ContentManager.get().getPrevSongFromNow();
    if (prevSong == null) {
      Toast.makeText(this, "이전 곡이 없습니다.", Toast.LENGTH_SHORT).show();
      return;
    }

    if (this.robot.isSinging()) {
      this.robot.pauseSinging();
    }

    Pauser.pauseAndRun(() -> this.robot.startSinging(prevSong.getCode(), prevSong.getTitle()), 800);

    if (!prevSong.getImageUrl().isEmpty()) {
      T800L.imageLoader.loadImage(albumImage, prevSong.getImageUrl());
    }
    titleTextView.setText(prevSong.getTitle());
    artistTextView.setText(prevSong.getArtist());
  }
}
