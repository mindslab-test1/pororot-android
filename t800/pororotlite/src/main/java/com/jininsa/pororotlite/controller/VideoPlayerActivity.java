package com.jininsa.pororotlite.controller;

import android.os.Bundle;
import android.widget.TextView;

import com.jininsa.pororotlite.R;
import com.jininsa.robot.app.RobotActivity;
import com.jininsa.robot.model.robot.Robot;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;

import java.util.Objects;

import butterknife.BindView;

public class VideoPlayerActivity extends RobotActivity {
  @BindView(R.id.videoThumbnailImageView)
  YouTubePlayerView youtubeView;

  @BindView(R.id.videoTitleTextView)
  TextView videoTitleTextView;

  @Override
  protected int getLayoutResId() {
    return R.layout.video_player_activity;
  }

  @Override
  public void doAfterChecking() {
    Bundle bundle = Objects.requireNonNull(this.getIntent().getExtras());
    String title = bundle.getString("title");
    String url = bundle.getString("url");

    if (this.robot.isSinging()) {
      this.robot.pauseSinging();
    }

    this.videoTitleTextView.setText(title);

    this.youtubeView.initialize(initializedYouTubePlayer -> initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
      @Override
      public void onReady() {
        initializedYouTubePlayer.loadVideo(url, 0);
      }
//
//      @Override
//      public void onStateChange(int state) {
//        if (state == 0) { // ENDED
//          VideoPlayerActivity.this.onBackPressed();
//        }
//      }
    }), true);
    this.youtubeView.enterFullScreen();
  }

  @Override
  protected void onDestroy() {
    this.youtubeView.release();
    Robot.get().videoStop();
    super.onDestroy();
  }
}
