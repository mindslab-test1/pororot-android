package com.jininsa.pororotlite.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.jininsa.pororotlite.R;
import com.jininsa.robot.app.Key;
import com.jininsa.robot.helper.Env;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingFragment extends HomeFragment {
  static final private String SUBSCRIPTION_ROBOT_TERMINATED = "6nqHxsuitffkDFOe";
  static final private String SUBSCRIPTION_CHANGE_NAMES = "u5CYQCPwCXajNAR9";

  @BindView(R.id.uuidTextView)
  TextView uuidTextView;
  @BindView(R.id.pitchTextView)
  TextView pitchTextView;
  @BindView(R.id.pitchSeekBar)
  SeekBar pitchSeekBar;
  @BindView(R.id.showTalksSwitch)
  Switch showTalksSwitch;
  @BindView(R.id.beQuietSwitch)
  Switch beQuietSwitch;
  @BindView(R.id.beQuietInChatSwitch)
  Switch beQuietInChatSwitch;

  private KProgressHUD loadingProgress;

  @Override
  public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
    View v = super.onCreateView(inflater, container, savedInstanceState);
    this.loadingProgress = KProgressHUD.create(Objects.requireNonNull(this.getActivity()))
      .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
      .setCancellable(true)
      .setAnimationSpeed(2)
      .setDimAmount(0.5f);

    this.uuidTextView.setText(this.robot.getUuid());

    boolean beQuiet = Env.getBoolean(Key.SETTINGS_BE_QUIET, false);
    boolean beQuietInChat = Env.getBoolean(Key.SETTINGS_BE_QUIET_IN_CHAT, false);
    boolean showTalks = Env.getBoolean(Key.SETTINGS_SHOW_TALKS, false);

    this.beQuietInChatSwitch.setChecked(beQuietInChat);
    this.beQuietSwitch.setChecked(beQuiet);
    this.showTalksSwitch.setChecked(showTalks);

    int pitch = Env.getInt(Key.SETTINGS_PITCH, 0);
    this.pitchSeekBar.setMax(100);
    this.pitchSeekBar.setProgress(pitch);
    this.pitchTextView.setText(String.valueOf(pitch));
    this.pitchSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        Env.setInt(Key.SETTINGS_PITCH, progress);
        SettingFragment.this.pitchTextView.setText(String.valueOf(progress));
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {}

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {}
    });
    return v;
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.setting_fragment;
  }

//  @OnClick(R.id.disconnectButton)
//  void onClickDisconnect() {
//    final Activity activity = Objects.requireNonNull(this.getActivity());
//    new MaterialDialog.Builder(activity)
//      .content("현재 뽀로롯과 연결을 끊습니다.")
//      .positiveText("네")
//      .onPositive((dialog, which) -> {
//        Env.setString(Key.LAST_ROBOT_UUID, Env.EMPTY_STRING);
//        this.robot.terminate();
//        this.addSubscription(SUBSCRIPTION_ROBOT_TERMINATED, Adapter.get().event.observeOn(Schedulers.newThread()).subscribe(event -> {
//          String action = event.action;
//          if (BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
//            int p = event.intent.getIntExtra(BluetoothHeadset.EXTRA_PREVIOUS_STATE, Env.DEFAULT_INT);
//            int n = event.intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, Env.DEFAULT_INT);
//            XLog.d("[%s] %d-->%d", action, p, n);
//            if (n == BluetoothHeadset.STATE_DISCONNECTED) {
//              if (event.device != null && this.robot.getMac().equals(event.device.getAddress())) {
//                final Intent intent = new Intent(activity, ScanRobotsActivity.class);
//                ActivityCompat.startActivity(activity, intent, null);
//                this.getActivity().finish();
//              }
//              this.removeSubscription(SUBSCRIPTION_ROBOT_TERMINATED);
//            }
//          }
//        }));
//      })
//      .negativeText("아니오")
//      .show();
//  }

  @OnClick(R.id.aboutButton)
  void onClickAbout() {
    new LibsBuilder()
      //provide a style (optional) (LIGHT, DARK, LIGHT_DARK_TOOLBAR)
      .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
      //start the activity
      .start(Objects.requireNonNull(this.getActivity()));
  }

  @OnClick(R.id.showTalksSwitch)
  void onClickShowDialogCheckBox() {
    Env.setBoolean(Key.SETTINGS_SHOW_TALKS, this.showTalksSwitch.isChecked());
  }

  @OnClick(R.id.beQuietInChatSwitch)
  void onClickBeQuietDuringChatCheckBox() {
    Env.setBoolean(Key.SETTINGS_BE_QUIET_IN_CHAT, this.beQuietInChatSwitch.isChecked());
  }

  @OnClick(R.id.beQuietSwitch)
  void onClickBeQuietCheckBox() {
    Env.setBoolean(Key.SETTINGS_BE_QUIET, this.beQuietSwitch.isChecked());
  }

}
