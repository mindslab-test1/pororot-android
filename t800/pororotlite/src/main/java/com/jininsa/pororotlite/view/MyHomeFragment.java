package com.jininsa.pororotlite.view;

import com.jininsa.pororotlite.R;

/**
 * Created by architect on 2017. 5. 7..
 */

public class MyHomeFragment extends HomeFragment
{
  @Override
  protected int getLayoutResId()
  {
    return R.layout.home_fragment;
  }

//  // @DebugLog
//  @OnClick(R.id.startButton)
//  void onClickStart(View v) {
//    XLog.d("!!!!!!");
//    this.robot.startAcc();
//  }
//
//  // @DebugLog
//  @OnClick(R.id.stopButton)
//  void onClickStop(View v) {
//    this.robot.stopAcc();
//  }
}
