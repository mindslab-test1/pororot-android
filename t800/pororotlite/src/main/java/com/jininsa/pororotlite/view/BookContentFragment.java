package com.jininsa.pororotlite.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elvishew.xlog.XLog;
import com.google.common.collect.Lists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jininsa.pororotlite.R;
import com.jininsa.pororotlite.controller.BookPlayerActivity;
import com.jininsa.robot.app.BaseFragment;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.model.contents.Book;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

public class BookContentFragment extends BaseFragment {
  static class BookListAdapter extends RecyclerView.Adapter<BookListAdapter.ViewHolder> {
    static final int[] thumbnails = { R.drawable.ic_book_01, R.drawable.ic_book_02, R.drawable.ic_book_03, R.drawable.ic_book_04 };

    static class ViewHolder extends RecyclerView.ViewHolder {
      final View containerView;
      @BindView(R.id.titleTextView)
      TextView titleTextView;
      @BindView(R.id.descTextView)
      TextView descTextView;
      @BindView(R.id.thumbnailImageView)
      ImageView thumbnailImageView;

      ViewHolder(View view) {
        super(view);
        this.containerView = view;
        ButterKnife.bind(this, view);
      }
    }

    private final List<Book> items;
    private final Consumer<Book> action;
    private final Context context;

    BookListAdapter(Consumer<Book> action, Context context) {
      this.items = Lists.newArrayList();
      this.action = action;
      this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contents_item, parent, false);
      return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookListAdapter.ViewHolder holder, int position) {
      holder.thumbnailImageView.setImageResource(thumbnails[position % 4]);
      holder.titleTextView.setText(items.get(position).getTitle());
      holder.descTextView.setText(items.get(position).getAuthor());
      holder.containerView.setOnClickListener(__ -> {
        try {
          this.action.accept(this.items.get(position));
          Bundle bundle = new Bundle();
          bundle.putString("title", this.items.get(position).getTitle());
          FirebaseAnalytics.getInstance(context).logEvent("Book", bundle);
        }
        catch (Exception e) {
          XLog.e(e.getMessage(), e);
        }
      });
    }

    @Override
    public int getItemCount() {
      return items.size();
    }

    void reset() {
      int size = this.getItemCount();
      this.items.clear();
      this.notifyItemRangeRemoved(0, size);
    }

    void addItem(final Book item) {
      this.items.add(item);
      this.notifyItemInserted(this.getItemCount());
    }

    void setItems(final List<Book> items) {
      this.items.clear();
      this.items.addAll(items);
      this.notifyItemInserted(this.getItemCount());
    }
  }

  @BindView(R.id.audiobookListRecyclerView)
  RecyclerView audioBookListRecyclerView;
  private BookListAdapter bookListAdapter;

  @Override
  protected int getLayoutResId() {
    return R.layout.book_fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = super.onCreateView(inflater, container, savedInstanceState);
    this.bookListAdapter = new BookListAdapter(book -> {
      Intent intent = new Intent(this.getActivity(), BookPlayerActivity.class);
      intent.putExtra("title", book.getTitle());
      intent.putExtra("contents", book.getContents());
      intent.putExtra("code", book.getCode());
      intent.putExtra("author", book.getAuthor());
      this.startActivity(intent);
    }, getContext());

    this.audioBookListRecyclerView.setAdapter(this.bookListAdapter);
    this.bookListAdapter.setItems(ContentManager.get().getBooks());
    return view;
  }

  @Override
  public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

  }
}
