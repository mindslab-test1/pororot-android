package com.jininsa.pororotlite.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.jininsa.pororotlite.R;
import com.jininsa.pororotlite.view.HomeFragment;
import com.jininsa.robot.connector.ContentManager;
import com.jininsa.robot.helper.Pauser;
import com.jininsa.robot.model.contents.Song;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;

// ToDo: ViewPager 도입 필요 (Fragment 내용 보존)
public class HomeActivity extends PororotToolbarActivity {
  private final List<HomeFragment> fragments = new ArrayList<>();

  @BindView(R.id.bottom_navigation_bar)
  SpaceNavigationView navigationBar;

  @BindView(R.id.miniPlayer)
  ConstraintLayout miniPlayerContainer;

  @BindView(R.id.miniPlyaerTitle)
  TextView miniPlayerTitle;

  @BindView(R.id.miniPlayerArtist)
  TextView miniPlayerArtist;

  @BindView(R.id.closeButton)
  ImageButton miniPlayerCloseButton;

  @BindView(R.id.miniPlayerNext)
  ImageButton miniPlayerNext;

  @BindView(R.id.miniPlayerList)
  ImageButton miniPlayerList;

  @BindView(R.id.miniPlayerControl)
  ImageButton miniPlayerControl;

  private Player.EventListener eventListener;

  private SimpleExoPlayer player;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.robot.startHearing();
    this.navigationBar.initWithSaveInstanceState(savedInstanceState);
  }

  @Override
  public void doAfterChecking() {
    this.navigationBar.addSpaceItem(new SpaceItem("콘텐츠", R.drawable.button_contents));
    this.navigationBar.addSpaceItem(new SpaceItem("설정", R.drawable.button_setting));
    this.navigationBar.setCentreButtonSelectable(true);
    this.navigationBar.setCentreButtonSelected();

    for (int i = 0; i < 5; i++) {
      this.fragments.add(HomeFragment.newInstance(i));
    }

    this.navigationBar.setSpaceOnClickListener(new SpaceOnClickListener() {
      @Override
      public void onCentreButtonClick() {
        HomeActivity.this.navigationBar.setCentreButtonColor(getResources().getColor(R.color.primary));
        HomeActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.container0, HomeActivity.this.fragments.get(4)).commit();
        HomeActivity.this.setTitle("아바타톡");
      }

      @Override
      public void onItemClick(int itemIndex, String itemName) {
        HomeActivity.this.navigationBar.setCentreButtonColor(getResources().getColor(R.color.gray));
        hideKeyboard();
        if (itemIndex == 0) {
          HomeActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.container0, HomeActivity.this.fragments.get(1)).commit();
        }else {
          HomeActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.container0, HomeActivity.this.fragments.get(3)).commit();
        }
        HomeActivity.this.setTitle(itemName);
      }

      @Override
      public void onItemReselected(int itemIndex, String itemName) {
      }
    });

    this.getSupportFragmentManager().beginTransaction().add(R.id.container0, this.fragments.get(4)).commit();

    this.player = this.robot.getMusicPlayer();

    this.miniPlayerContainer.setOnClickListener((__) -> {});

    new Timer().schedule(new TimerTask() {
      @Override
      public void run() {
        HomeActivity.this.runOnUiThread(() -> {
          if (ContentManager.get().getNowPlaySong() != null) {
            Song song = ContentManager.get().getNowPlaySong();
            miniPlayerTitle.setText(song.getTitle());
            miniPlayerArtist.setText(song.getArtist());
            miniPlayerContainer.setVisibility(View.VISIBLE);
          } else {
            miniPlayerContainer.setVisibility(View.GONE);
          }

          if (HomeActivity.this.robot != null) {
            SimpleExoPlayer player = HomeActivity.this.robot.getMusicPlayer();
            if (player != null && player.getPlayWhenReady()) {
              miniPlayerControl.setImageResource(R.drawable.ic_miniplayer_pause);
              miniPlayerCloseButton.setVisibility(View.GONE);
            }
            else {
              miniPlayerControl.setImageResource(R.drawable.ic_miniplayer_play);
              miniPlayerCloseButton.setVisibility(View.VISIBLE);
            }
          }
        });
      }
    }, 0, 1000);

    this.eventListener = new Player.EventListener() {
      //@Override
      //public void onTimelineChanged(Timeline timeline, Object manifest) {}

      @Override
      public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

      }

      @Override
      public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {}

      @Override
      public void onLoadingChanged(boolean isLoading) {}

      @Override
      public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
//        if (this.robot.isSingingBySpeak() == false) {
//          if (playWhenReady == true && playbackState == ExoPlayer.STATE_ENDED) {
//            if (ContentManager.get().getNextSongFromNow() == null) {
//              this.robot.stopSinging();
//            } else {
//              miniPlayerNextClick();
//            }
//          }
//        }

        if (playWhenReady && playbackState == Player.STATE_ENDED) {
          if (HomeActivity.this.robot.isSingingBySpeak()) {
            HomeActivity.this.robot.stopSinging();
          }
          else {
            HomeActivity.this.miniPlayerNextClick();
          }
        }
      }

      @Override
      public void onRepeatModeChanged(int repeatMode) {}

      @Override
      public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {}

      @Override
      public void onPlayerError(ExoPlaybackException error) {}

      @Override
      public void onPositionDiscontinuity(int reason) {}

      @Override
      public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {}

      @Override
      public void onSeekProcessed() {}
    };

    this.player.addListener(this.eventListener);
  }

  @Override
  protected void onDestroy() {
    if (this.player != null) {
      this.player.removeListener(this.eventListener);
    }
    super.onDestroy();
  }

  @Override
  protected String title() {
    return "아바타톡";
  }

  @OnClick(R.id.miniPlayerControl)
  protected void miniPlayerControlClick() {
    if (this.player.getPlayWhenReady()) {
      this.robot.pauseSinging();
      miniPlayerControl.setImageResource(R.drawable.ic_miniplayer_play);
      miniPlayerCloseButton.setVisibility(View.VISIBLE);
    } else {
      this.robot.resumeSinging();
      miniPlayerControl.setImageResource(R.drawable.ic_miniplayer_pause);
      miniPlayerCloseButton.setVisibility(View.GONE);
    }
  }

  @OnClick(R.id.closeButton)
  protected void miniPlayerCloseButtonClick() {
    ContentManager.get().setNowPlaySong(null);
    miniPlayerContainer.setVisibility(View.GONE);
  }

  @OnClick(R.id.miniPlayerNext)
  protected void miniPlayerNextClick() {
    Song nextSong = ContentManager.get().getNextSongFromNow();
    if (nextSong == null) {
      Toast.makeText(this, "다음 곡이 없습니다.", Toast.LENGTH_SHORT).show();
      return;
    }

    if (this.robot.isSinging()) {
      this.robot.pauseSinging();
    }

    Pauser.pauseAndRun(() -> this.robot.startSinging(nextSong.getCode(), nextSong.getTitle()), 1000);

    HomeActivity.this.runOnUiThread(() -> {
      miniPlayerTitle.setText(nextSong.getTitle());
      miniPlayerArtist.setText(nextSong.getArtist());
    });
  }

  @OnClick(R.id.miniPlayerList)
  protected void miniPlayerListClick() {
    if (ContentManager.get().getNowPlaySong() != null) {
      Song song = ContentManager.get().getNowPlaySong();
      Intent intent = new Intent(this, SongPlayerActivity.class);

      intent.putExtra("title", song.getTitle());
      intent.putExtra("artist", song.getArtist());
      intent.putExtra("code", song.getCode());
      intent.putExtra("imageUrl", song.getImageUrl());
      intent.putExtra("isList", true);

      this.startActivity(intent);
    }
  }

  private void hideKeyboard() {
    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    if (imm != null) {
      if (imm.isAcceptingText() && getCurrentFocus() != null) {
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
      }
    }
  }

  @Override
  protected int getLayoutResId() {
    return R.layout.home_activity;
  }
}
