package com.jininsa.pororotlite;

import android.os.Build;

import com.bumptech.glide.Glide;
import com.elvishew.xlog.LogConfiguration;
import com.elvishew.xlog.LogLevel;
import com.elvishew.xlog.XLog;
import com.google.common.collect.Maps;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jininsa.pororotlite.controller.VideoPlayerActivity;
import com.jininsa.robot.app.App;
import com.jininsa.robot.app.Const;
import com.jininsa.robot.helper.Permission;
import com.jininsa.robot.helper.XLogBorderFormatter;
import com.pisome.iconify.Iconify;
import com.stfalcon.chatkit.commons.ImageLoader;

import java.util.Collections;
import java.util.Map;

public class T800L extends App {
  public static final ImageLoader imageLoader = (imageView, url) -> Glide.with(imageView).load(url).into(imageView);

//  private static GoogleAnalytics sAnalytics;
//  private static Tracker sTracker;

  static FirebaseAnalytics analytics;

  @Override
  public Const.Settings getSettings() {
    return new Const.Settings() {
      @Override
      public String robotName() {
        return "pororot";
      }

      @Override
      public String chatBotId() {
        return BuildConfig.CHATBOT_ID;
      }

      @Override
      public String ttsDatabaseName() {
        return "tts_single_db_kor_custom13.vtdb";
      }

      @Override
      public String ttsKey() {
        return BuildConfig.TTS_KEY;
      }

      @Override
      public Class videoPlayerContext() { return VideoPlayerActivity.class; }

      @Override
      public String earType() { return "ASR"; }

      @Override
      public String bizServerUrl() { return BuildConfig.BIZ_SERVER_URL; }

      @Override
      public String chatBotServerUrl() { return BuildConfig.CHATBOT_URL; }

      @Override
      public Map<Permission, String> permissionRationales() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
          Map<Permission, String> permissionRationales = Maps.newHashMap();
//          permissionRationales.put(Permission.ACCESS_COARSE_LOCATION, String.format("주변 로봇을 찾기 위해 '%s' 권한을 요청합니다.", Permission.ACCESS_COARSE_LOCATION.desc));
//          permissionRationales.put(Permission.CAMERA, String.format("로봇 식별 코드를 인식하기 위해 '%s' 권한을 요청합니다.", Permission.CAMERA.desc));
          permissionRationales.put(Permission.RECORD_AUDIO, String.format("음성을 인식하기 위해 '%s' 권한을 요청합니다.", Permission.RECORD_AUDIO.desc));
          permissionRationales.put(Permission.WRITE_EXTERNAL_STORAGE, String.format("노래와 책을 채생하기 위해 '%s' 권한을 요청합니다.", Permission.WRITE_EXTERNAL_STORAGE.desc));
          permissionRationales.put(Permission.READ_PHONE_STATE, String.format("전화번호로 가입하기 위해 '%s' 권한을 요청합니다.", Permission.READ_PHONE_STATE.desc));
//          permissionRationales.put(Permission.READ_SMS, String.format("전화번호 인증을 위해 '%s' 권한을 요청합니다.", Permission.READ_SMS.desc));
//          permissionRationales.put(Permission.RECEIVE_SMS, String.format("전화번호 인증을 위해 '%s' 권한을 요청합니다.", Permission.RECEIVE_SMS.desc));
//          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            permissionRationales.put(Permission.READ_PHONE_NUMBERS, String.format("전화번호로 가입하기 위해 '%s' 권한을 요청합니다.", Permission.READ_PHONE_NUMBERS.desc));
//          }
          return permissionRationales;
        }
        else {
          return Collections.emptyMap();
        }
      }

      @Override
      public Boolean hasSpp() { return false; }

      @Override
      public Boolean isLite() { return true; }

      @Override
      public Boolean shouldPhoneControl() { return true; }
    };
  }

  @Override
  public void onCreate() {
    XLog.init(new LogConfiguration.Builder()
      .logLevel(BuildConfig.DEBUG ? LogLevel.ALL : LogLevel.NONE)
      .tag("T800P").t().b().st(1)
      .borderFormatter(XLogBorderFormatter.INSTANCE).build());

    super.onCreate();

    Iconify.init(this.getApplicationContext(), new Icons());

    T800L.analytics = FirebaseAnalytics.getInstance(this);

  }
}
